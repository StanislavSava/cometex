﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using cometex.Clase;
using cometex.Utils;

namespace cometex.User
{
    public partial class ListaCereri : System.Web.UI.Page
    {
        static Societati societatea = new Societati();
        static List<Cheltuieli> plafoane = new List<Cheltuieli>();
        static List<Cheltuieli> cheltuieliLunaInCurs = new List<Cheltuieli>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["idUtilizator"] == null) Response.Redirect("~/login.aspx");
            if (!IsPostBack)
            {
                Master.AtribuieButoaneMeniu(MeniuUtils.AranjeazaElementeMeniu(4, new List<int> { 1 }));
                TextBoxDataScadenta.Attributes.Add("readonly", "true");
                TextBoxScadentaIncepandCu.Text = "01." + DateTime.Now.AddMonths(-1).Month.ToString() + "." + DateTime.Now.Year.ToString();
                ddlFiltrareStatus.DataBind();
                ddlFiltrareStatus.SelectedIndex = 0;
                GridListaCereri.SelectedIndex = -1;
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            btVerifica.Visible = StergeCerere.Visible = (GridListaCereri.SelectedIndex != -1 && GridListaCereri.Rows.Count > 0);
            // status 10 - cerut explicatii pt cheltuieli grila
            btModifica.Visible = GridListaCereri.SelectedIndex != -1 && ((Label)GridListaCereri.SelectedRow.FindControl("lblIdStatusSimplificat")).Text != "3" && ((Label)GridListaCereri.SelectedRow.FindControl("lblIdStatusSimplificat")).Text != "1";

        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            PanelListaCereri.Visible = false;
            PanelAdauga.Visible = true;
        }
        protected void ButtonRenunta_Click(object sender, EventArgs e)
        {
            PanelAdauga.Visible = false;
            PanelListaCereri.Visible = true;
        }
        protected void ButtonSalveaza_Click(object sender, EventArgs e)
        {
            DateTime x = Convert.ToDateTime(TextBoxDataScadenta.Text);
            string xs = x.ToString("s");
            cheltuieliLunaInCurs = DatabaseUtils.ListaCheltuieliPeLunaInCurs(societatea, Convert.ToDateTime(xs));

            Cheltuieli cheltuiala = new Cheltuieli();
            if (TextBoxSumaBruta.Text.Trim() == String.Empty) TextBoxSumaBruta.Text = "0";
            //if (TextBoxSumaNeta.Text.Trim() == String.Empty) TextBoxSumaNeta.Text = "0";
            cheltuiala.CheltuialaBrut = Convert.ToDecimal(TextBoxSumaBruta.Text.Replace(".", ",").Trim());
            //cheltuiala.CheltuialaNet = Convert.ToDecimal(TextBoxSumaNeta.Text.Replace(".", ",").Trim());
            cheltuiala.CategorieCheltuiala.Id = Convert.ToInt32(DropDownListCategoria.SelectedValue);
            cheltuiala.DelegatAIntrodusCheltuiala.Id = Convert.ToInt32(Session["idUtilizator"]);
            cheltuiala.Destinatia = TextBoxDestinatia.Text.Trim();
            cheltuiala.Explicatii = TextBoxExplicatii.Text.Trim();
            cheltuiala.Societatea.Id = Convert.ToInt32(Session["idSocietate"]);
            cheltuiala.IntrodusLaData = DateTime.Now;
            cheltuiala.Scadenta = Convert.ToDateTime(xs);

            //String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
            //SqlConnection con = new SqlConnection(connectionString);
            //con.Open();
            // alegem daca e adauga sau modifica
            //string cmdStr = "";

            //SqlCommand cmd = new SqlCommand(cmdStr, con);
            //cmd.Parameters.Clear();
            //cmd.Parameters.AddWithValue("idSocietate", cheltuiala.Societatea.Id);
            //cmd.Parameters.AddWithValue("Denumire", cheltuiala.Destinatia);
            //cmd.Parameters.AddWithValue("Obs", cheltuiala.Explicatii);
            //cmd.Parameters.AddWithValue("DataIntroducere", cheltuiala.IntrodusLaData.ToString("s"));
            //cmd.Parameters.AddWithValue("IntrodusDe", cheltuiala.DelegatAIntrodusCheltuiala.Id);
            //cmd.Parameters.AddWithValue("Scadenta", cheltuiala.Scadenta);
            //cmd.Parameters.AddWithValue("Suma", cheltuiala.CheltuialaBrut);
            //cmd.Parameters.AddWithValue("SumaNeta", cheltuiala.CheltuialaNet);

            //if (!ckbAutomatSpreAprobare.Checked)
            //{
            //cmd.Parameters.AddWithValue("idCategorie", cheltuiala.CategorieCheltuiala.Id);
            // verificam daca este in grila sau depaseste
            //if (GrilaUtils.VerificaDacaSeIncadreazaInGrilaPeLunaScadentei(cheltuiala))
            //{
            //cmd.Parameters.AddWithValue("Status", 1);
            //cmd.Parameters.AddWithValue("RezolutionatDe", 1006); // utilizator ce rezolutioneaza implicit pentru cheltuieli aflate in grila
            //cmd.Parameters.AddWithValue("DataRezolutie", DateTime.Now.ToString("s"));

            //}
            //else
            //{
            //    //cmd.Parameters.AddWithValue("Status", 0);
            //    //cmd.Parameters.AddWithValue("RezolutionatDe", 1002); // depasit grila -  spre aprobare
            //    //cmd.Parameters.AddWithValue("DataRezolutie", "01.01.1900");
            //}
            //}
            //else
            //{
            //cmd.Parameters.AddWithValue("idCategorie", 53); // 53 - categoria EXCEPTIONALA
            //cmd.Parameters.AddWithValue("Status", 4); // exceptionala - spre aprobare
            //cmd.Parameters.AddWithValue("RezolutionatDe", 1002);
            //cmd.Parameters.AddWithValue("DataRezolutie", "01.01.1900");
            //}
            // Int32 id = Convert.ToInt32(cmd.ExecuteScalar());
            //Int32 id = DatabaseUtils.InsertCheltuiala(cheltuiala);
            //con.Close();
            if (lblTipOperatie.Text == "Adauga")
            {
                if (GrilaUtils.VerificaDacaSeIncadreazaInGrilaPeLunaScadentei(cheltuiala))
                {
                    cheltuiala.Status.Id = 1; // incadrata in grila
                    cheltuiala.RezolutionatDe.Id = 1006; // utilizator generic ce rezolutioneaza implicit pentru cheltuieli aflate in grila
                    cheltuiala.RezolutionatLaData = DateTime.Now;
                }
                else
                {
                    cheltuiala.Status.Id = 0; // depasit grila -  spre aprobare
                    cheltuiala.RezolutionatDe.Id = 1002; // utilizator generic pt depasit grila -  spre aprobare
                    cheltuiala.RezolutionatLaData = Convert.ToDateTime("01.01.1900");
                }
            }
            else
            {
                if (GrilaUtils.VerificaDacaSeIncadreazaInGrilaPeLunaScadentei(cheltuiala, Convert.ToDecimal(((Label)GridListaCereri.SelectedRow.FindControl("lblSuma")).Text)))
                {
                    cheltuiala.Status.Id = 1; // incadrata in grila
                    cheltuiala.RezolutionatDe.Id = 1006; // utilizator generic ce rezolutioneaza implicit pentru cheltuieli aflate in grila
                    cheltuiala.RezolutionatLaData = DateTime.Now;
                }
                else
                {
                    if (((Label)GridListaCereri.SelectedRow.FindControl("lblIdStatus")).Text == "10") // cerut explicatii
                    {
                        cheltuiala.Status.Id = 11; //explicatii acordate
                    }
                    else
                    {
                        cheltuiala.Status.Id = 0;//depasit grila -  spre aprobare
                    }
                    cheltuiala.RezolutionatDe.Id = 1002; // utilizator generic pt depasit grila -  spre aprobare si explicatii acordate
                    cheltuiala.RezolutionatLaData = Convert.ToDateTime("01.01.1900");
                }
            }
            if (lblTipOperatie.Text == "Adauga")
            {
                cheltuiala.IdCheltuieli = DatabaseUtils.InsertCheltuiala(cheltuiala);
                //cmdStr = "insert into investitii (idSocietate, Denumire, Obs, DataIntroducere, IntrodusDe, Scadenta, Suma, SumaNeta, status, idCategorie, RezolutionatDe, DataRezolutie) values (@idSocietate, @Denumire, @Obs, @DataIntroducere, @IntrodusDe, @Scadenta, @Suma, @SumaNeta, @status, @idCategorie, @RezolutionatDe, @DataRezolutie); SELECT SCOPE_IDENTITY()";
            }
            else
            {
                cheltuiala.IdCheltuieli = Convert.ToInt32(((Label)GridListaCereri.SelectedRow.FindControl("lblIdCheltuiala")).Text);
                DatabaseUtils.UpdateCheltuiala(cheltuiala);
                //cmdStr = "UPDATE ";
            }


            GridListaCereri.SelectedIndex = -1;
            GridListaCereri.DataBind();

            PanelAdauga.Visible = false;
            PanelListaCereri.Visible = true;

            cheltuiala.Societatea = DatabaseUtils.DetaliiSocietate(cheltuiala.Societatea.Id);
            // cheltuiala.IdCheltuieli = id;
            SAGA.UpdateCheltuialaCuStatusSAGA(cheltuiala);


        }

        protected void Adauga_Click(object sender, EventArgs e)
        {
            PanelListaCereri.Visible = false;
            PanelAdauga.Visible = true;
            lblTipOperatie.Text = "Adauga";
            TextBoxDataScadenta.Text = DateTime.Now.ToString("dd.MM.yyyy");
        }
        protected void Modifica_Click(object sender, EventArgs e)
        {
            PanelListaCereri.Visible = false;
            PanelAdauga.Visible = true;
            TextBoxDestinatia.Text = ((Label)GridListaCereri.SelectedRow.FindControl("lblDestinatia")).Text;
            TextBoxExplicatii.Text = ((Label)GridListaCereri.SelectedRow.FindControl("lblExplicatii")).Text;
            TextBoxDataScadenta.Text = ((Label)GridListaCereri.SelectedRow.FindControl("lblScadenta")).Text;
            TextBoxSumaBruta.Text = ((Label)GridListaCereri.SelectedRow.FindControl("lblSuma")).Text;
            DropDownListCategoria.SelectedValue = ((Label)GridListaCereri.SelectedRow.FindControl("lblIdCategorie")).Text;
            lblTipOperatie.Text = "Modifica";
        }
        protected void Sterge_Click(object sender, EventArgs e)
        {
            if (GridListaCereri.SelectedIndex >= 0)
            {
                String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();
                String cmdStr = "delete from investitii where idInvestitie = @idInvestitie";
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("idInvestitie", GridListaCereri.SelectedDataKey[0]);
                cmd.ExecuteNonQuery();
                GridListaCereri.SelectedIndex = -1;
                GridListaCereri.DataBind();

                con.Close();
            }
        }

        protected void GridListaCereri_SelectedIndexChanged(object sender, EventArgs e)
        {
            Cheltuieli cheltuiala = new Cheltuieli();
            cheltuiala.IdCheltuieli = Convert.ToInt32(GridListaCereri.SelectedDataKey[0]);
            SAGA.UpdateCheltuialaCuStatusSAGA(cheltuiala);
            Grid.CurataClaseGridSelectedRow(GridListaCereri);
        }

        protected void GridListaCereri_Sorting(object sender, GridViewSortEventArgs e)
        {
            GridListaCereri.SelectedIndex = -1;
        }

        protected void GridListaCereri_PreRender(object sender, EventArgs e)
        {
            LabelSumaTotala.Text =
                "Totaluri cheltuieli: <span class='danger'>introduse: " +
                 DatabaseUtils.GridFooterCheltuieliSelectateInFunctieDeStatus(Session["idSocietate"].ToString(), ddlFiltrareCategorie.SelectedValue.ToString(), Session["idUtilizator"].ToString(), "%", Convert.ToDateTime(TextBoxScadentaIncepandCu.Text)) +
                 "lei</span> | <span class='danger'>depasit grila - spre aprobare: " +
                 DatabaseUtils.GridFooterCheltuieliSelectateInFunctieDeStatus(Session["idSocietate"].ToString(), ddlFiltrareCategorie.SelectedValue.ToString(), Session["idUtilizator"].ToString(), "0", Convert.ToDateTime(TextBoxScadentaIncepandCu.Text)) +
                 "lei</span> | <span class='danger'>incadrate in grila: " +
                 DatabaseUtils.GridFooterCheltuieliSelectateInFunctieDeStatus(Session["idSocietate"].ToString(), ddlFiltrareCategorie.SelectedValue.ToString(), Session["idUtilizator"].ToString(), "1", Convert.ToDateTime(TextBoxScadentaIncepandCu.Text)) +
                 "lei</span> | <span class='danger'>depasit grila si respinse:  " +
                 DatabaseUtils.GridFooterCheltuieliSelectateInFunctieDeStatus(Session["idSocietate"].ToString(), ddlFiltrareCategorie.SelectedValue.ToString(), Session["idUtilizator"].ToString(), "2", Convert.ToDateTime(TextBoxScadentaIncepandCu.Text)) +
                 "lei</span> | <span class='danger'>depasit grila si aprobate: " +
                 DatabaseUtils.GridFooterCheltuieliSelectateInFunctieDeStatus(Session["idSocietate"].ToString(), ddlFiltrareCategorie.SelectedValue.ToString(), Session["idUtilizator"].ToString(), "3", Convert.ToDateTime(TextBoxScadentaIncepandCu.Text)) +
                 "lei</span> | <span class='danger'>exceptionale spre aprobare: " +
                 DatabaseUtils.GridFooterCheltuieliSelectateInFunctieDeStatus(Session["idSocietate"].ToString(), ddlFiltrareCategorie.SelectedValue.ToString(), Session["idUtilizator"].ToString(), "4", Convert.ToDateTime(TextBoxScadentaIncepandCu.Text)) +
                 "lei</span> | <span class='danger'>exceptionale aprobate: " +
                 DatabaseUtils.GridFooterCheltuieliSelectateInFunctieDeStatus(Session["idSocietate"].ToString(), ddlFiltrareCategorie.SelectedValue.ToString(), Session["idUtilizator"].ToString(), "5", Convert.ToDateTime(TextBoxScadentaIncepandCu.Text)) +
                 "lei</span> | <span class='danger'>exceptionale respinse: " +
                 DatabaseUtils.GridFooterCheltuieliSelectateInFunctieDeStatus(Session["idSocietate"].ToString(), ddlFiltrareCategorie.SelectedValue.ToString(), Session["idUtilizator"].ToString(), "6", Convert.ToDateTime(TextBoxScadentaIncepandCu.Text)) +
                 "lei</span>"
                ;
        }
        protected void GridListaCereri_Load(object sender, EventArgs e) { }

        protected void GridListaCereri_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Clase.Grid.SelectGrid(GridListaCereri, e, this);
        }

        protected void btVerifica_Click(object sender, EventArgs e)
        {
            Cheltuieli cheltuiala = new Cheltuieli();
            cheltuiala.Societatea = DatabaseUtils.DetaliiSocietate(Convert.ToInt32(((Label)GridListaCereri.SelectedRow.FindControl("lblIdSocietate")).Text));
            cheltuiala.IdCheltuieli = Convert.ToInt32(GridListaCereri.SelectedDataKey[0]);
            cheltuiala.CheltuialaBrut = Convert.ToDecimal(((Label)GridListaCereri.SelectedRow.FindControl("lblSuma")).Text);
            SAGA.UpdateCheltuialaCuStatusSAGA(cheltuiala);
            GridListaCereri.DataBind();
        }

        protected void btVerificaToate_Click(object sender, EventArgs e)
        {
            societatea.Id = Convert.ToInt32(Session["idSocietate"]);
            SAGA.UpdateCheltuialaCuStatusSAGA(DatabaseUtils.ListaCheltuieliNeVerificateInSAGA(societatea));
            GridListaCereri.DataBind();
        }
    }
}