﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/Us.master" AutoEventWireup="true" CodeBehind="ContulMeu.aspx.cs" Inherits="cometex.User.ContulMeu" %>

<%@ MasterType VirtualPath="~/User/Us.master" %>
<%@ Reference VirtualPath="~/Cometex.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:Panel ID="PanelAdauga" runat="server" Visible="true" CssClass="adauga panel">
        <asp:Label runat="server" Font-Bold="true" Font-Size="large" Text="Contul meu" />
        <asp:Panel ID="Panel1" runat="server">
            <asp:Label runat="server" Text="Nume si prenume:" />
            <asp:TextBox ID="tbNume" runat="server" Width="20em"></asp:TextBox>
        </asp:Panel>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanelParole" runat="server">
            <Triggers>
                <asp:PostBackTrigger ControlID="tbParola" />
                <asp:PostBackTrigger ControlID="tbConfirmaParola" />
            </Triggers>
            <ContentTemplate>
                <asp:Label ID="lblSchimbaParola" runat="server" Text="Daca nu doriti schimbarea parolei, lasati campurile goale." Font-Italic="true" Font-Size="XX-Small" Visible="true" />
                <asp:CompareValidator ID="validatorParole" runat="server" ErrorMessage="Verifica parola!" ControlToCompare="tbParola" ControlToValidate="tbConfirmaParola" Font-Bold="true" ForeColor="Red" />
                <asp:Panel ID="Panel3" runat="server">
                    <asp:Label runat="server" Text="Parola:" />
                    <asp:TextBox ID="tbParola" runat="server" ReadOnly="false" Width="20em" TextMode="Password" AutoPostBack="false" />
                </asp:Panel>
                <asp:Panel ID="Panel4" runat="server">
                    <asp:Label runat="server" Text="Confirma Parola:" />
                    <asp:TextBox ID="tbConfirmaParola" ReadOnly="false" runat="server" Width="20em" TextMode="Password" AutoPostBack="false" />
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>


    <div class="clearfix" id="butoane" runat="server">
        <asp:Panel ID="PanelSucces" runat="server" CssClass="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <asp:Label ID="LabelSucces" runat="server" />
        </asp:Panel>
        <asp:Button ID="ButonSalveaza" CssClass="btn btn-success" runat="server" OnClick="ButonSalveaza_Click" Text="Salveaza" />
    </div>


</asp:Content>
