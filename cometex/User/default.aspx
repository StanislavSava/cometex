﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="cometex.User._default" Culture="ro-RO" uiCulture="ro" %>

    <form id="form1" runat="server">
    <asp:panel runat="server" ID="PanelGrid">
    <table style="width:100%;">
        <tr>
            <td>&nbsp;</td>
            <td>
                &nbsp;</td>
            <td>Categorie</td>
            <td>
                <asp:DropDownList ID="DropDownList2" runat="server" AppendDataBoundItems="True" AutoPostBack="True" DataSourceID="SqlDataSource3" DataTextField="Categorie" DataValueField="IdCategorie">
                    <asp:ListItem Value="%">Toate</asp:ListItem>
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT [IdCategorie], [Categorie] FROM [Categorii] ORDER BY [Categorie]"></asp:SqlDataSource>
            </td>
            <td>Status</td>
            <td>
                <asp:DropDownList ID="DropDownList3" runat="server" AppendDataBoundItems="True" AutoPostBack="True" DataSourceID="SqlDataSource4" DataTextField="Status" DataValueField="IdStatus">
                    <asp:ListItem Value="%">Toate</asp:ListItem>
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT * FROM [Statusuri] ORDER BY [Status]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataSourceID="SqlDataSource1" GridLines="Horizontal" Width="100%" DataKeyNames="idInvestitie">
                    <AlternatingRowStyle BackColor="#F7F7F7" />
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                    </Columns>
                    <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                    <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                    <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                    <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                    <SortedAscendingCellStyle BackColor="#F4F4FD" />
                    <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                    <SortedDescendingCellStyle BackColor="#D8D8F0" />
                    <SortedDescendingHeaderStyle BackColor="#3E3277" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT Investitii.Denumire, Investitii.Obs, Investitii.IdInvestitie, Investitii.DataIntroducere, Investitii.Scadenta, Investitii.Suma, Investitii.DataRezolutie, Categorii.Categorie, Utilizatori.Nume as AdaugatDe, Statusuri.Status, Utilizatori_1.Nume AS RezolutionatDe FROM Investitii LEFT OUTER JOIN Categorii ON Investitii.idCategorie = Categorii.IdCategorie LEFT OUTER JOIN Societati ON Investitii.idSocietate = Societati.IdSocietate LEFT OUTER JOIN Utilizatori ON Investitii.IntrodusDe = Utilizatori.IdUtilizator LEFT OUTER JOIN Utilizatori AS Utilizatori_1 ON Investitii.RezolutionatDe = Utilizatori_1.IdUtilizator LEFT OUTER JOIN Statusuri ON Investitii.Status = Statusuri.IdStatus AND Investitii.IntrodusDe = @IntrodusDe WHERE (Categorii.Categorie LIKE @categorie) AND (Statusuri.Status LIKE @status) ORDER BY Investitii.Scadenta">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DropDownList2" Name="categorie" PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DropDownList3" Name="status" PropertyName="SelectedValue" />
                        <asp:SessionParameter Name="IntrodusDe" SessionField="idUtilizator" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="Button1" runat="server" Text="Adauga cerere" OnClick="Button1_Click" />
            </td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="Button2" runat="server" OnClientClick="return confirm('Sigur stergeti cererea?');" Text="Stergere cerere" OnClick="Button2_Click" />
            </td>
            <td>
                &nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:panel>
        <asp:Panel ID="PanelAdauga" runat="server" Visible="False">
            <table style="width:100%;">
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Adauga cerere</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>Denumire cheltuiala</td>
                    <td>
                        <asp:TextBox ID="TextBox1" runat="server" Width="525px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>Observatii</td>
                    <td>
                        <asp:TextBox ID="TextBox2" runat="server" Height="118px" TextMode="MultiLine" Width="524px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>Suma</td>
                    <td>
                        <asp:TextBox ID="TextBox3" runat="server" Width="225px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>Data Scadenta</td>
                    <td>
                        <asp:TextBox ID="TextBox4" runat="server" Width="224px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>Categorie</td>
                    <td>
                        <asp:DropDownList ID="DropDownList4" runat="server" DataSourceID="SqlDataSource5" DataTextField="Categorie" DataValueField="IdCategorie">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT * FROM [Categorii] ORDER BY [Categorie]"></asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <asp:Button ID="ButtonSalveaza" runat="server" OnClick="ButtonSalveaza_Click" Text="Salveaza" />
                        &nbsp;<asp:Button ID="ButtonRenunta" runat="server" OnClick="ButtonRenunta_Click" Text="Renunta" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </asp:Panel>

</form>


