﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using cometex.Clase;
using cometex.Utils;

namespace cometex.User
{
    public partial class ContulMeu : System.Web.UI.Page
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
            ((Label)Master.Master.FindControl("LabelUser")).Text = Session["numeUtilizator"].ToString();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Master.AtribuieButoaneMeniu(MeniuUtils.AranjeazaElementeMeniu(4, new List<int> { 4 }));
                UtilizatorInit();
            }
        }
        public void UtilizatorInit()
        {
            tbNume.Text = Session["numeUtilizator"].ToString().Trim();
            tbParola.Text = "";
            tbConfirmaParola.Text = "";
            PanelSucces.Visible = false;
            LabelSucces.Visible = false;
            LabelSucces.Text = "";

        }
        protected void ButonSalveaza_Click(object sender, EventArgs e)
        {
            String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            String cmdStr = "";
            //  criptez parola
            string Parola = Criptare.Encrypt(tbParola.Text);
            // string Parola = tbParola.Text;
            // verific admin
            //modificare
            // verificam daca se schimba parola
            if (tbParola.Text.Length > 0 && tbParola.Text == tbConfirmaParola.Text)
            {
                if (tbNume.Text.Length == 0)
                    tbNume.Text = Session["numeUtilizator"].ToString();
                Parola = ", Parola ='" + Criptare.Encrypt(tbParola.Text) + "' ";
                cmdStr = "update Utilizatori set Nume= '" + tbNume.Text + "'" + Parola + " WHERE idUtilizator='" + Convert.ToInt32(Session["idUtilizator"]) + "'";
            }
            else if (tbNume.Text != Session["idUtilizator"].ToString() && tbNume.Text.Length > 0)
            {
                cmdStr = "update Utilizatori set Nume= '" + tbNume.Text + "' WHERE idUtilizator='" + Convert.ToInt32(Session["idUtilizator"]) + "'";
            }
            else
                cmdStr = "";
            if (cmdStr != "")
            {
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.ExecuteNonQuery();
                PanelSucces.Visible = true;
                LabelSucces.Visible = true;
                LabelSucces.Text = "Actualizarea a fost efectuata!";
                if (tbNume.Text.Length > 0)
                {
                    Session["numeUtilizator"] = tbNume.Text;

                }
            }
            con.Close();
        }
    }
}