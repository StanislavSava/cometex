﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using cometex.Clase;
using cometex.Utils;

namespace cometex.User
{
    public partial class Exceptionale : System.Web.UI.Page
    {
        static Societati societatea = new Societati();
        static List<Cheltuieli> plafoane = new List<Cheltuieli>();
        static List<Cheltuieli> cheltuieliLunaInCurs = new List<Cheltuieli>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["idUtilizator"] == null) Response.Redirect("~/login.aspx");
            if (!IsPostBack)
            {
                Master.AtribuieButoaneMeniu(MeniuUtils.AranjeazaElementeMeniu(4, new List<int> { 2 }));
                TextBoxDataScadenta.Attributes.Add("readonly", "true");
                TextBoxScadentaIncepandCu.Text = "01." + DateTime.Now.AddMonths(-1).Month.ToString() + "." + DateTime.Now.Year.ToString();
                ddlFiltrareStatus.DataBind();
                ddlFiltrareStatus.SelectedIndex = 0;
                GridListaCereri.SelectedIndex = -1;
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (GridListaCereri.Rows.Count > 0)
            {
                btVerifica.Visible = StergeCerere.Visible = (GridListaCereri.SelectedIndex != -1 && GridListaCereri.Rows.Count > 0);
                btModifica.Visible = GridListaCereri.SelectedIndex != -1 && ((Label)GridListaCereri.SelectedRow.FindControl("lblIdStatusSimplificat")).Text != "1";
            }
            else
            {
                btModifica.Visible = btVerifica.Visible = StergeCerere.Visible = false;
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            PanelListaCereri.Visible = false;
            PanelAdauga.Visible = true;
        }
        protected void ButtonRenunta_Click(object sender, EventArgs e)
        {
            PanelAdauga.Visible = false;
            PanelListaCereri.Visible = true;
        }
        protected void ButtonSalveaza_Click(object sender, EventArgs e)
        {
            DateTime x = Convert.ToDateTime(TextBoxDataScadenta.Text);
            string xs = x.ToString("s");
            cheltuieliLunaInCurs = DatabaseUtils.ListaCheltuieliPeLunaInCurs(societatea, Convert.ToDateTime(xs));

            Cheltuieli cheltuiala = new Cheltuieli();
            if (TextBoxSumaBruta.Text.Trim() == String.Empty) TextBoxSumaBruta.Text = "0";
            cheltuiala.CheltuialaBrut = Convert.ToDecimal(TextBoxSumaBruta.Text.Replace(".", ",").Trim());
            cheltuiala.CategorieCheltuiala.Id = Convert.ToInt32(DropDownListCategoria.SelectedValue);
            cheltuiala.DelegatAIntrodusCheltuiala.Id = Convert.ToInt32(Session["idUtilizator"]);
            cheltuiala.Destinatia = TextBoxDestinatia.Text.Trim();
            cheltuiala.Explicatii = TextBoxExplicatii.Text.Trim();
            cheltuiala.Societatea.Id = Convert.ToInt32(Session["idSocietate"]);
            cheltuiala.IntrodusLaData = DateTime.Now;
            cheltuiala.Scadenta = Convert.ToDateTime(xs);

            if (lblTipOperatie.Text == "Adauga")
            {
                cheltuiala.Status.Id = 4; // exceptionala -  spre aprobare
                cheltuiala.RezolutionatDe.Id = 1002; // utilizator generic pt -  spre aprobare
                cheltuiala.RezolutionatLaData = Convert.ToDateTime("01.01.1900");
            }
            else
            {
                if (((Label)GridListaCereri.SelectedRow.FindControl("lblIdStatus")).Text == "15") // cerut explicatii
                {
                    cheltuiala.Status.Id = 16; //exceptionala - explicatii acordate
                }
                else
                {
                    cheltuiala.Status.Id = 4;//exceptionala -  spre aprobare
                }
                cheltuiala.RezolutionatDe.Id = 1002; // utilizator generic pt - spre aprobare si explicatii acordate
                cheltuiala.RezolutionatLaData = Convert.ToDateTime("01.01.1900");
            }
            if (lblTipOperatie.Text == "Adauga")
            {
                cheltuiala.IdCheltuieli = DatabaseUtils.InsertCheltuiala(cheltuiala);
            }
            else
            {
                cheltuiala.IdCheltuieli = Convert.ToInt32(((Label)GridListaCereri.SelectedRow.FindControl("lblIdCheltuiala")).Text);
                DatabaseUtils.UpdateCheltuiala(cheltuiala);
            }

            GridListaCereri.SelectedIndex = -1;
            GridListaCereri.DataBind();

            PanelAdauga.Visible = false;
            PanelListaCereri.Visible = true;

            cheltuiala.Societatea = DatabaseUtils.DetaliiSocietate(cheltuiala.Societatea.Id);
            SAGA.UpdateCheltuialaCuStatusSAGA(cheltuiala);
        }

        protected void Adauga_Click(object sender, EventArgs e)
        {
            PanelListaCereri.Visible = false;
            PanelAdauga.Visible = true;
            lblTipOperatie.Text = "Adauga";
            TextBoxDataScadenta.Text = DateTime.Now.ToString("dd.MM.yyyy");
        }
        protected void Modifica_Click(object sender, EventArgs e)
        {
            PanelListaCereri.Visible = false;
            PanelAdauga.Visible = true;
            TextBoxDestinatia.Text = ((Label)GridListaCereri.SelectedRow.FindControl("lblDestinatia")).Text;
            TextBoxExplicatii.Text = ((Label)GridListaCereri.SelectedRow.FindControl("lblExplicatii")).Text;
            TextBoxDataScadenta.Text = ((Label)GridListaCereri.SelectedRow.FindControl("lblScadenta")).Text;
            TextBoxSumaBruta.Text = ((Label)GridListaCereri.SelectedRow.FindControl("lblSuma")).Text;
            DropDownListCategoria.SelectedValue = ((Label)GridListaCereri.SelectedRow.FindControl("lblIdCategorie")).Text;
            lblTipOperatie.Text = "Modifica";
        }
        protected void Sterge_Click(object sender, EventArgs e)
        {
            if (GridListaCereri.SelectedIndex >= 0)
            {
                String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();
                using (con)
                {
                    String cmdStr = "delete from investitii where idInvestitie = @idInvestitie";
                    SqlCommand cmd = new SqlCommand(cmdStr, con);
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("idInvestitie", GridListaCereri.SelectedDataKey[0]);
                    cmd.ExecuteNonQuery();
                    GridListaCereri.SelectedIndex = -1;
                    GridListaCereri.DataBind();
                    con.Close();
                }
            }
        }

        protected void GridListaCereri_SelectedIndexChanged(object sender, EventArgs e)
        {
            Cheltuieli cheltuiala = new Cheltuieli();
            cheltuiala.IdCheltuieli = Convert.ToInt32(GridListaCereri.SelectedDataKey[0]);
            SAGA.UpdateCheltuialaCuStatusSAGA(cheltuiala);
            Grid.CurataClaseGridSelectedRow(GridListaCereri);
        }

        protected void GridListaCereri_Sorting(object sender, GridViewSortEventArgs e)
        {
            GridListaCereri.SelectedIndex = -1;
        }

        protected void GridListaCereri_PreRender(object sender, EventArgs e)
        {
            LabelSumaTotala.Text =
                "Totaluri cheltuieli: introduse: " +
                 DatabaseUtils.GridFooterCheltuieliSelectateInFunctieDeStatus(Session["idSocietate"].ToString(), ddlFiltrareCategorie.SelectedValue.ToString(), Session["idUtilizator"].ToString(), "%", "exceptionala", Convert.ToDateTime(TextBoxScadentaIncepandCu.Text)) +
                "lei</span> | <span class='danger'>spre aprobare: " +
                 DatabaseUtils.GridFooterCheltuieliSelectateInFunctieDeStatus(Session["idSocietate"].ToString(), ddlFiltrareCategorie.SelectedValue.ToString(), Session["idUtilizator"].ToString(), "0", "exceptionala", Convert.ToDateTime(TextBoxScadentaIncepandCu.Text)) +
                 "lei</span> | <span class='danger'>respinse: " +
                 DatabaseUtils.GridFooterCheltuieliSelectateInFunctieDeStatus(Session["idSocietate"].ToString(), ddlFiltrareCategorie.SelectedValue.ToString(), Session["idUtilizator"].ToString(), "2", "exceptionala", Convert.ToDateTime(TextBoxScadentaIncepandCu.Text)) +
                 "lei</span> | <span class='danger'>aprobate: " +
                 DatabaseUtils.GridFooterCheltuieliSelectateInFunctieDeStatus(Session["idSocietate"].ToString(), ddlFiltrareCategorie.SelectedValue.ToString(), Session["idUtilizator"].ToString(), "1", "exceptionala", Convert.ToDateTime(TextBoxScadentaIncepandCu.Text)) +
                 "lei</span>"
                ;
        }
        protected void GridListaCereri_Load(object sender, EventArgs e) { }

        protected void GridListaCereri_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Clase.Grid.SelectGrid(GridListaCereri, e, this);
        }

        protected void btVerifica_Click(object sender, EventArgs e)
        {
            Cheltuieli cheltuiala = new Cheltuieli();
            cheltuiala.Societatea = DatabaseUtils.DetaliiSocietate(Convert.ToInt32(((Label)GridListaCereri.SelectedRow.FindControl("lblIdSocietate")).Text));
            cheltuiala.IdCheltuieli = Convert.ToInt32(GridListaCereri.SelectedDataKey[0]);
            cheltuiala.CheltuialaBrut = Convert.ToDecimal(((Label)GridListaCereri.SelectedRow.FindControl("lblSuma")).Text);
            SAGA.UpdateCheltuialaCuStatusSAGA(cheltuiala);
            GridListaCereri.DataBind();
        }

        protected void btVerificaToate_Click(object sender, EventArgs e)
        {
            societatea.Id = Convert.ToInt32(Session["idSocietate"]);
            SAGA.UpdateCheltuialaCuStatusSAGA(DatabaseUtils.ListaCheltuieliNeVerificateInSAGA(societatea));
            GridListaCereri.DataBind();
        }

        protected void ddlFiltrareCategorie_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridListaCereri.SelectedIndex = -1;
        }

        protected void ddlFiltrareStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridListaCereri.SelectedIndex = -1;
        }
    }
}