﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cometex.User
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["idUtilizator"] == null) Response.Redirect("~/login.aspx");
            if (!IsPostBack)
            {
                DropDownList3.SelectedIndex = 0;
            }
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            if (GridView1.SelectedIndex >= 0)
            {
                String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();
                String cmdStr = "update investitii set status = @status, RezolutionatDe = @RezolutionatDe, DataRezolutie = @dataRezolutie where idInvestitie = @idInvestitie";
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("idInvestitie", GridView1.SelectedDataKey[0]);
                cmd.Parameters.AddWithValue("status", 1);
                cmd.Parameters.AddWithValue("RezolutionatDe", Session["idUtilizator"]);
                cmd.Parameters.AddWithValue("DataRezolutie", DateTime.Now);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            if (GridView1.SelectedIndex >= 0)
            {
                String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();
                String cmdStr = "update investitii set status = @status, RezolutionatDe = @RezolutionatDe, DataRezolutie = @dataRezolutie where idInvestitie = @idInvestitie";
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("idInvestitie", GridView1.SelectedDataKey[0]);
                cmd.Parameters.AddWithValue("status", 2);
                cmd.Parameters.AddWithValue("RezolutionatDe", Session["idUtilizator"]);
                cmd.Parameters.AddWithValue("DataRezolutie", DateTime.Now);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        protected void Button7_Click(object sender, EventArgs e)
        {
            if (GridView1.SelectedIndex >= 0)
            {
                String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();
                String cmdStr = "update investitii set status = @status, RezolutionatDe = @RezolutionatDe, DataRezolutie = @dataRezolutie where idInvestitie = @idInvestitie";
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("idInvestitie", GridView1.SelectedDataKey[0]);
                cmd.Parameters.AddWithValue("status", 0);
                cmd.Parameters.AddWithValue("RezolutionatDe", Session["idUtilizator"]);
                cmd.Parameters.AddWithValue("DataRezolutie", DateTime.Now);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (GridView1.SelectedIndex >= 0)
            {
                String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();
                String cmdStr = "delete from investitii where idInvestitie = @idInvestitie";
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("idInvestitie", GridView1.SelectedDataKey[0]);
                cmd.ExecuteNonQuery();
                GridView1.DataBind();
                con.Close();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            PanelGrid.Visible = false;
            PanelAdauga.Visible = true;

        }

        protected void ButtonRenunta_Click(object sender, EventArgs e)
        {
            PanelAdauga.Visible = false;
            PanelGrid.Visible = true;
        }

        protected void ButtonSalveaza_Click(object sender, EventArgs e)
        {
            String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            String cmdStr = "insert into investitii (idSocietate, Denumire, Obs, DataIntroducere, IntrodusDe, Scadenta, Suma, status, idCategorie) values (@idSocietate, @Denumire, @Obs, @DataIntroducere, @IntrodusDe, @Scadenta, @Suma, @status, @idCategorie)";
            SqlCommand cmd = new SqlCommand(cmdStr, con);
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("idSocietate", Session["idSocietate"]);
            cmd.Parameters.AddWithValue("Denumire", TextBox1.Text.Trim());
            cmd.Parameters.AddWithValue("Obs", TextBox2.Text.Trim());
            cmd.Parameters.AddWithValue("DataIntroducere", DateTime.Now);
            cmd.Parameters.AddWithValue("IntrodusDe", Session["idUtilizator"]);
            cmd.Parameters.AddWithValue("Scadenta", TextBox4.Text.Trim());
            cmd.Parameters.AddWithValue("Suma", TextBox3.Text.Trim());
            cmd.Parameters.AddWithValue("Status", 0);
            cmd.Parameters.AddWithValue("idCategorie", DropDownList4.SelectedValue);
            cmd.ExecuteNonQuery();
            con.Close();
            GridView1.DataBind();
            PanelAdauga.Visible = false;
            PanelGrid.Visible = true;
        }
    }
}