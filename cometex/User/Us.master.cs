﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cometex.Users
{
    public partial class Us : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["idUtilizator"] == null) Response.Redirect("~/login.aspx");
        }
        public void AtribuieButoaneMeniu(List<Clase.Meniu> elementeMeniu)
        {
            liGrila.Attributes.Clear();
            liGrila.Attributes.Add(elementeMeniu[0].Atribute[0], elementeMeniu[0].Atribute[1]);
            liGrila.Attributes.Add(elementeMeniu[0].Atribute[2], elementeMeniu[0].Atribute[3]);
            liExceptionale.Attributes.Clear();
            liExceptionale.Attributes.Add(elementeMeniu[1].Atribute[0], elementeMeniu[1].Atribute[1]);
            liExceptionale.Attributes.Add(elementeMeniu[1].Atribute[2], elementeMeniu[1].Atribute[3]);
            liPlati.Attributes.Clear();
            liPlati.Attributes.Add(elementeMeniu[2].Atribute[0], elementeMeniu[2].Atribute[1]);
            liPlati.Attributes.Add(elementeMeniu[2].Atribute[2], elementeMeniu[2].Atribute[3]);

            liContulMeu.Attributes.Clear();
            liContulMeu.Attributes.Add(elementeMeniu[3].Atribute[0], elementeMeniu[3].Atribute[1]);
            liContulMeu.Attributes.Add(elementeMeniu[3].Atribute[2], elementeMeniu[3].Atribute[3]);

        }
    }
}