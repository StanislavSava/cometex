﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using cometex.Admin.Raport;
using System.Web;
using cometex.Utils;
using System.Globalization;
using System.Data.SqlClient;
using System.Configuration;
using FirebirdSql.Data.FirebirdClient;

namespace cometex.Clase
{
    public class CheltuieliGenerale
    {
        #region Adu date
        public  CheltuialaDate AdaugaRand(Societati societate, DateTime dateStart, DateTime dateEnd) {
            CheltuialaDate objCheltuieli = new CheltuialaDate();
            objCheltuieli = DateSocietate(societate,dateStart,dateEnd);
                return objCheltuieli; 
                }
        #endregion

        public decimal AdaugaIncasari(Societati societate, DateTime dateStart, DateTime dateEnd)
        {
            decimal incasari = 0;
            DataSourceCashFlow.CashflowConturi conturi = DataSourceCashFlow.CashFlowConturi(societate.Id.ToString());
            SAGA.SAGACreeazaStringWhereSQL(conturi);
            string select = @"SELECT SUM(suma) AS SumaTotala FROM REGISTRU a
                            WHERE 1=1 and (
                                ("  + conturi.ContCurent.Replace("DEINLOCUIT", "cont_d").Replace("AND", "") + @")
                             OR (" + conturi.Depozit.Replace("DEINLOCUIT", "cont_d").Replace("AND", "") + @") 
                             OR (" + conturi.DepozitTaxe.Replace("DEINLOCUIT", "cont_d").Replace("AND", "") + @") 
                             OR (" + conturi.Casierie.Replace("DEINLOCUIT", "cont_d").Replace("AND", "") + @") 
                            )
                            AND data >= '" + dateStart.ToString("dd.MM.yyyy") + "' AND data <= '" + dateEnd.ToString("dd.MM.yyyy") + @"'
                            AND (explicatie SIMILAR TO 'Incas.%K[[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]]%'
                            or explicatie SIMILAR TO 'Incas.%U[[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]]%')";
            FbConnectionStringBuilder ret = new FbConnectionStringBuilder();
            ret.Database = ConfigurationManager.AppSettings["caleSAGA"].ToString() + "\\" + societate.SAGAfolder + "\\cont_baza.fdb";
            ret.DataSource = "86.120.111.69";
            ret.Port = 3050;
            ret.UserID = "SYSDBA";
            ret.Password = "masterkey";
            ret.Charset = "NONE";
            ret.ConnectionTimeout = 60000;
            ret.Pooling = false;
            string Log = "";
            FbConnection sagaConnection = new FbConnection(ret.ToString());
            using (sagaConnection)
            {
                try
                {

                    sagaConnection.Open();
                    FbCommand fbCommand = new FbCommand(select, sagaConnection);
                    FbDataReader reader = fbCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        incasari = Convert.ToDecimal(reader["SumaTotala"]);
                    }
                }
                catch (Exception ex_AdaugaIncasariFB)
                {
                    Log = "Nu s-a putut conecta: " + ex_AdaugaIncasariFB.ToString();
                    //afiseaza mesaj exceptie
                }
                return incasari;
            }
        }
        public CheltuialaDate DateSocietate(Societati societate, DateTime dataStart, DateTime dataEnd)
        {
            #region Date&Culture
            CultureInfo cultura = new CultureInfo("ro-RO");
            cultura.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
            cultura.DateTimeFormat.LongDatePattern = "dd.MM.yyyy";
            #endregion

            CheltuialaDate date = new CheltuialaDate();
            String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);
            using (con)
            {
                con.Open();
                string cmdStr =
                         @"if(SELECT COUNT(*) FROM [dbo].[Investitii] INNER  JOIN 
                          societati on investitii.idsocietate = societati.idsocietate
                          where scadenta >= convert(datetime, '" + dataStart.ToString("dd.MM.yyyy") + "', 104) and scadenta <= convert(datetime, '" + dataEnd.ToString("dd.MM.yyyy") + @"', 104)

                               and investitii.idsocietate = '"+societate.Id+ @"')>0
                                begin
                                SELECT 
                                COALESCE(sum([Suma]),0) as sumatotala, 
                                societati.idsocietate, societati.idfacturare
                                , COALESCE(status,0) as status
                                , societate, oras
                          FROM [dbo].[Investitii]
                          RIGHT  JOIN 
                          societati on investitii.idsocietate = societati.idsocietate
                          where scadenta >= convert(datetime, '" + dataStart.ToString("dd.MM.yyyy") +"', 104) and scadenta <= convert(datetime, '"+dataEnd.ToString("dd.MM.yyyy")+ @"', 104)
       
	  
	                           and investitii.idsocietate = '"+societate.Id+ @"'
                        group by societati.idsocietate, societati.idfacturare, status, societate, oras
						 order by societati.idsocietate, status
						 end
						 else
						 SELECT societate,oras,societati.idsocietate, societati.idfacturare , 0 as status, 0 as sumatotala FROM [Societati] where [IdSocietate]='" + societate.Id+"'";
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("societate", societate.Id);
                SqlDataReader dateGasite = cmd.ExecuteReader();
                int contor = 0;
                while (dateGasite.Read())
                {
                    if (contor == 0)
                    {
                        date.SocietateDenumire = dateGasite["societate"].ToString();
                        date.Localitatea = dateGasite["oras"].ToString();
                    }
                    contor++;
                    switch (dateGasite["status"].ToString())
                    {
                        case "0":
                            date.DepasitGrilaSpreAprobare = Convert.ToDecimal(dateGasite["sumaTotala"]);
                            break;
                        case "1":
                            date.CheltuieliIncadrate = Convert.ToDecimal(dateGasite["sumaTotala"]);
                            break;
                        case "2":
                            date.DepasitGrilaSiRespinsa = Convert.ToDecimal(dateGasite["sumaTotala"]);
                            break;
                        case "3":
                            date.DepasitGrilaSiAprobata= Convert.ToDecimal(dateGasite["sumaTotala"]);
                            break;
                        case "4":
                            date.ExceptionalaSpreAprobare = Convert.ToDecimal(dateGasite["sumaTotala"]);
                            break;
                        case "5":
                            date.ExceptionalaAprobata = Convert.ToDecimal(dateGasite["sumaTotala"]);
                            break;
                        case "6":
                            date.ExceptionalaRespinsa = Convert.ToDecimal(dateGasite["sumaTotala"]);
                            break;
                        case "7":
                            date.PlataSpreAprobare = Convert.ToDecimal(dateGasite["sumaTotala"]);
                            break;
                        case "8":
                            date.PlataAprobata = Convert.ToDecimal(dateGasite["sumaTotala"]);
                            break;
                        case "9":
                            date.PlataRespinsa = Convert.ToDecimal(dateGasite["sumaTotala"]);
                            break;
                        case "10":
                            date.CerutExplicatiiGrila = Convert.ToDecimal(dateGasite["sumaTotala"]);
                            break;
                        case "11":
                            date.ExplicatiiAcordate = Convert.ToDecimal(dateGasite["sumaTotala"]);
                            break;
                        case "15":
                            date.CerutExplicatiiExceptionala = Convert.ToDecimal(dateGasite["sumaTotala"]);
                            break;
                        case "16":
                            date.ExplicatiiAcordateExceptionala = Convert.ToDecimal(dateGasite["sumaTotala"]);
                            break;
                        case "20":
                            date.CerutExplicatiiPlata = Convert.ToDecimal(dateGasite["sumaTotala"]);
                            break;
                        case "21":
                            date.ExplicatiiAcordatePlata = Convert.ToDecimal(dateGasite["sumaTotala"]);
                            break;

                    }
                }
                con.Close();
            }
            return date;
        }
        public List<CheltuialaDate>CampuriSocietati()
        {
            return CampuriSocietatiList(HttpContext.Current.Session["dateStart"].ToString(), HttpContext.Current.Session["dateEnd"].ToString());
        }
        #region Liste societati/localitati/incasari/cheltuieli incadrate/cheltuieli depasite/plati/cash generat 
        public List<CheltuialaDate> CampuriSocietatiList(string dataPrecedenta, string dataFinala)
        {
            #region Date&Culture
            CultureInfo cultura = new CultureInfo("ro-RO");
            cultura.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
            cultura.DateTimeFormat.LongDatePattern = "dd.MM.yyyy";
            #endregion

            List<CheltuialaDate> camp = new List<CheltuialaDate>();
            List<Societati> societati = DatabaseUtils.ListaSocietati();
            foreach (Societati societate in societati)
            {
                CheltuialaDate rand = AdaugaRand(societate, Convert.ToDateTime(dataPrecedenta, cultura), Convert.ToDateTime(dataFinala, cultura));
                rand.Incasari = AdaugaIncasari(societate, Convert.ToDateTime(dataPrecedenta,cultura), Convert.ToDateTime(dataFinala, cultura));
                rand.SocietateDenumire = societate.Denumire;
                camp.Add(rand);
                rand = new CheltuialaDate();
            }
            return camp;
        }
        #endregion

        #region Constructor
        public CheltuieliGenerale() { }
        #endregion
    }
        #region Clasa Cheltuiala Date
    public class CheltuialaDate
    {
        /* DateTime */
        private DateTime _data = DateTime.Now;
        public DateTime Data { get { return _data; } set { _data = value; } }

        /* Societate */
        private string _societateDenumire;
        public string SocietateDenumire {
            get
            {
                return _societateDenumire;
            }
            set
            {
                _societateDenumire = value;
            }
        }

        /* Localitatea */
        private string _localitatea = string.Empty;
        public string Localitatea
        {
            get
            {
                return _localitatea;
            }
            set
            {
                _localitatea = value;
            }
        }

        /* Incasari */
        private decimal _incasari = 0;
        public decimal Incasari
        {
            get
            {
                return _incasari;
            }
            set
            {
                _incasari = value;
            }
        }

        /* Depasire Grila - Spre aprobare */
        private decimal _depasitGrilaSpreAprobare = 0;
        public decimal DepasitGrilaSpreAprobare
        {
            get
            {
                return _depasitGrilaSpreAprobare;
            }
            set
            {
                _depasitGrilaSpreAprobare = value;
            }
        }

        /* Incadrata in grila */
        private decimal _incadrataInGrila = 0;
        public decimal IncadrataInGrila
        {
            get
            {
                return _incadrataInGrila;
            }
            set
            {
                _incadrataInGrila = value;
            }
        }

        /* Depasit grila si respinsa */
        private decimal _depasitGrilaSiRespinsa = 0;
        public decimal DepasitGrilaSiRespinsa
        {
            get
            {
                return _depasitGrilaSiRespinsa;
            }
            set
            {
                _depasitGrilaSiRespinsa = value;
            }
        }

        /* Depasit grila si aprobata */
        private decimal _depasitGrilaSiAprobata = 0;
        public decimal DepasitGrilaSiAprobata
        {
            get
            {
                return _depasitGrilaSiAprobata;
            }
            set
            {
                _depasitGrilaSiAprobata = value;
            }
        }

        /* Exceptionala spre aprobare */
        private decimal _exceptionalaSpreAprobare = 0;
        public decimal ExceptionalaSpreAprobare
        {
            get
            {
                return _exceptionalaSpreAprobare;
            }
            set
            {
                _exceptionalaSpreAprobare = value;
            }
        }

        /* Exceptionala aprobata */
        private decimal _exceptionalaAprobata = 0;
        public decimal ExceptionalaAprobata
        {
            get
            {
                return _exceptionalaAprobata;
            }
            set
            {
                _exceptionalaAprobata = value;
            }
        }

        /* Exceptionala respinsa */
        private decimal _exceptionalaRespinsa = 0;
        public decimal ExceptionalaRespinsa
        {
            get
            {
                return _exceptionalaRespinsa;
            }
            set
            {
                _exceptionalaRespinsa = value;
            }
        }

        /* Plata spre aprobare */
        private decimal _plataSpreAprobare = 0;
        public decimal PlataSpreAprobare
        {
            get
            {
                return _plataSpreAprobare;
            }
            set
            {
                _plataSpreAprobare = value;
            }
        }

        /* Plata aprobata */
        private decimal _plataAprobata = 0;
        public decimal PlataAprobata
        {
            get
            {
                return _plataAprobata;
            }
            set
            {
                _plataAprobata = value;
            }
        }

        /* Plata respinsa */
        private decimal _plataRespinsa = 0;
        public decimal PlataRespinsa
        {
            get
            {
                return _plataRespinsa;
            }
            set
            {
                _plataRespinsa = value;
            }
        }

        /* Cerut explicatii */
        private decimal _cerutExplicatiiGrila = 0;
        public decimal CerutExplicatiiGrila
        {
            get
            {
                return _cerutExplicatiiGrila;
            }
            set
            {
                _cerutExplicatiiGrila = value;
            }
        }

        /* Explicatii acordate */
        private decimal _explicatiiAcordate = 0;
        public decimal ExplicatiiAcordate
        {
            get
            {
                return _explicatiiAcordate;
            }
            set
            {
                _explicatiiAcordate = value;
            }
        }

        /* Cerut explicatii exceptionala*/
        private decimal _cerutExplicatiiExceptionala = 0;
        public decimal CerutExplicatiiExceptionala
        {
            get
            {
                return _cerutExplicatiiExceptionala;
            }
            set
            {
                _cerutExplicatiiExceptionala = value;
            }
        }

        /* Explicatii acordate exceptionala */
        private decimal _explicatiiAcordateExceptionala = 0;
        public decimal ExplicatiiAcordateExceptionala
        {
            get
            {
                return _explicatiiAcordateExceptionala;
            }
            set
            {
                _explicatiiAcordateExceptionala = value;
            }
        }

        /* Cerut explicatii plata */
        private decimal _cerutExplicatiiPlata = 0;
        public decimal CerutExplicatiiPlata
        {
            get
            {
                return _cerutExplicatiiPlata;
            }
            set
            {
                _cerutExplicatiiPlata = value;
            }
        }

        /* Explicatii acordate plata */
        private decimal _explicatiiAcordatePlata = 0;
        public decimal ExplicatiiAcordatePlata
        {
            get
            {
                return _explicatiiAcordatePlata;
            }
            set
            {
                _explicatiiAcordatePlata = value;
            }
        }

        /* Cheltuieli incadrate in grila */
        private decimal _cheltuieliIncadrate = 0;
        public decimal CheltuieliIncadrate
        {
            get
            {
                return _cheltuieliIncadrate;
            }
            set
            {
                _cheltuieliIncadrate = value;
            }
        }

        /* Depasire cheltuieli grila */
        private decimal _cheltuieliDepasite = 0;
        public decimal CheltuieliDepasite
        {
            get
            {
                return _cheltuieliDepasite;
            }
            set
            {
                _cheltuieliDepasite = value;
            }
        }


        /* Plati */
        private decimal _plati = 0;
        public decimal Plati
        {
            get
            {
                return _plati = 0;
            }
            set
            {
                _plati = value;
            }
        }

        /* Cash Generat */
        private decimal _cashGenerat = 0;
        public decimal CashGenerat
        {
            get
            {
                return _cashGenerat;
            }
            set
            {
                _cashGenerat = value;
            }
        }

    }
    #endregion
}