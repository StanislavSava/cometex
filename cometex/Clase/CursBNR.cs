﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cometex.Clase
{
    public class Curs
    {
        public int Id = 0;
        private decimal valoare = 0;
        public decimal Valoare
        {
            get { return valoare; }
            set { valoare = value; }
        }

        private DateTime data = DateTime.Now;
        public DateTime Data
        {
            get { return data; }
            set { data = value; }
        }
        public Curs()
        { }

        public static Curs Cauta(DateTime data)
        {
            Curs curs = new Curs();

            string select = @"SELECT TOP (1) [Id]
                                  ,[Curs]
                                  ,[Data]
                              FROM [dbo].[Cursuri]
                              WHERE Data BETWEEN CONVERT(DATETIME,'" + data.AddDays(-data.Day + 1).AddHours(-data.Hour).AddMinutes(-data.Minute).AddSeconds(-data.Second) + "', 104) AND CONVERT(DATETIME,'" + data.AddMonths(1).AddDays(-data.AddMonths(1).Day+1).AddHours(-data.Hour).AddMinutes(-data.Minute).AddSeconds(-data.Second - 1) + "',104)";
            SqlConnection conexiune = Utils.DatabaseUtils.ConnectToDatabase();
            using (conexiune)
            {
                SqlCommand cmd = new SqlCommand(select, conexiune);
                SqlDataReader dateGasite = cmd.ExecuteReader();
                while (dateGasite.Read())
                {
                    curs.Id = Convert.ToInt32(dateGasite["Id"]);
                    curs.Valoare = Convert.ToDecimal(dateGasite["Curs"]);
                    curs.Data = Convert.ToDateTime(dateGasite["Data"], new CultureInfo("ro-RO"));
                }
                return curs;
            }
        }
    }
}