﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cometex.Clase
{
    class Grid
    {
        public static void SelectGrid(GridView gv, GridViewRowEventArgs e, Page page)
        {
            if (e.Row.DataItemIndex == -1)
                return;
            e.Row.Attributes.Add("onMouseOver", @" if(this.className != 'danger') {this.className ='success';}");
            e.Row.Attributes.Add("onMouseOut", " if(this.className != 'danger') {this.className ='';}");
          //  e.Row.Attributes.Add("onclick", page.ClientScript.GetPostBackEventReference(gv, "Select$" + e.Row.RowIndex.ToString()));
          //  e.Row.Attributes.Add("onclick", String.Format("javascript:myModal.show();"));

//            e.Row.Attributes.Add("onclick", String.Format("javascript:$find('{0}').show();", "myModal"));

        }
        public static void CurataClaseGridSelectedRow(GridView gv)
        {
            foreach (GridViewRow row in gv.Rows)
            {
                if (row.RowIndex == gv.SelectedIndex) row.CssClass = "danger"; else row.CssClass = "curata";
            }
            
        }
    }
}
