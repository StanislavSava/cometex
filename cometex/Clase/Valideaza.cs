﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace cometex.Clase
{
    public class Valideaza
    {

        public static void DimensiuneString(string pCamp, string pValoare, int pDimensiuneaMinima, int pDimensiuneaMaxima, out string pExpresie, out int pTrece)
        {
            // daca dimensiune = 0, atunci nu conteaza

            pExpresie = "";
            pTrece = 1;
            if (pDimensiuneaMaxima >= pDimensiuneaMinima || pDimensiuneaMaxima == 0)
            {
                if (pDimensiuneaMaxima != 0)
                {
                    if (pDimensiuneaMaxima < pValoare.Length)
                    {
                        pTrece = 0; // nu e valid
                        pExpresie = pCamp + " trebuie sa contina maxim " + pDimensiuneaMaxima + " caractere!";
                    }
                    else
                        if (pDimensiuneaMinima != 0)
                    {
                        if (pDimensiuneaMinima > pValoare.Length)
                        {
                            pTrece = 0;
                            pExpresie = pCamp + " trebuie sa contina minim " + pDimensiuneaMinima + " caractere!";
                        }
                        else { pTrece = 1; }
                    }
                    else { pTrece = 1; }
                }
                else
                    if (pDimensiuneaMinima != 0)
                {
                    if (pDimensiuneaMinima > pValoare.Length)
                    {
                        pTrece = 0;
                        pExpresie = pCamp + " trebuie sa contina minim " + pDimensiuneaMinima + " caractere!";
                    }
                    else { pTrece = 1; }
                }
                else { pTrece = 1; }
            }
        }

        public static void Egale(string[] pCamp, string[] pValoare, out string pExpresie, out int pTrece)
        {
            pExpresie = "";
            pTrece = 1;
            string vValoareIntermediara = "";

            if (pValoare.Length > 0) { vValoareIntermediara = pValoare[1]; }
            foreach (string vValoare in pValoare)
            {
                if (vValoareIntermediara != vValoare)
                {
                    pTrece = 0;
                    CreeazaString(pCamp, out pExpresie);
                    pExpresie = "Campurile " + pExpresie + " nu sunt egale!";
                    break;
                }
            }
        }
        public static void CreeazaString(string[] pTexte, out string pExpresie)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("");
            int vContor = 1;
            if (pTexte.Length > 0)
            {
                foreach (string vText in pTexte)
                {
                    if (vContor == 1)
                    {
                        builder.Append(vText);
                        vContor++;
                    }
                    else
                    {
                        builder.Append(", ");
                        builder.Append(vText);
                    }
                }
            }
            pExpresie = builder.ToString();
        }
        public static bool isEmail(string inputEmail)
        {
            //inputEmail = NulltoString(inputEmail);
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                  @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                  @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(inputEmail))
                return (true);
            else
                return (false);
        }

        private static string NulltoString(string inputEmail)
        {
            throw new NotImplementedException();
        }



        public static void Email(string pCamp, string pValoare, out string pExpresie, out int pTrece)
        {
            pTrece = 1;
            StringBuilder builder = new StringBuilder();
            builder.Append("");
            if (!isEmail(pValoare))
            {
                builder.Append("Adresa ");
                builder.Append(pCamp);
                builder.Append(": ");
                builder.Append(pValoare);
                builder.Append(" nu e validă!");
                pTrece = 0;
            }
            pExpresie = builder.ToString();
        }


        public static bool ENumeric(string pCamp)
        {
            return Regex.IsMatch(pCamp, "^\\d+(\\.\\d+)?$");
        }
        public static void Coordonate(string[] pValori, out string pExpresie, out int pTrece)
        {
            pExpresie = "";
            pTrece = 1;
            foreach (string vValoare in pValori)
            {
                try
                {
                    //Convert.ToInt32(Convert.ToDecimal("1222,236")).ToString();
                    if (Convert.ToInt32(Convert.ToDecimal(vValoare.Replace('.', ','))).ToString().Length == 6)
                    {
                        pTrece = 1;
                    }
                    else
                    {
                        pTrece = 0;
                        pExpresie = "Cel puţin o coordonată nu este validă!";
                        break;
                    }
                }
                catch
                {
                    pTrece = 0;
                    pExpresie = "Cel puţin o coordonată nu este validă!";
                    break;
                }
            }
        }
        public static bool Parola(string pCamp)
        {
            return Regex.IsMatch(pCamp, "^.*(?=.{8,})(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=?!><.*~|]).*$");
        }
    }

}