﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cometex.Clase
{
    public class GraficCont
    {
        public List<string> conturi = new List<string>();
        public Int32 idGrafic = 0;
        public Societati societate = new Societati();
        public string modCalcul = "Sold";
        public GraficCont()
        {

        }
    }
    public class Grafice
    {
        public string x = DateTime.Now.ToString();
        public decimal y = 0;
        public Grafice() { }
        //        public Culori CuloareRGB
    }
    public class AreaChart
    {
        public AreaChartData areaChartData = new AreaChartData();
    }
    public class AreaChartData
    {
        public Labels labels = new Labels();
        public Datasets datasets = new Datasets();
        public AreaChartOptions areaChartOptions = new AreaChartOptions();
        public AreaChartData() { }
    }

    public class Labels
    {
        public List<string> labels = new List<string>();
    }
    public class Datasets
    {
        public List<DatasetsItems> DataSet = new List<DatasetsItems>();
    }
    public class DatasetsItems
    {
        public string label = "";
        public CuloriRGB fillColor = new CuloriRGB();
        public CuloriRGB strokeColor = new CuloriRGB();
        public CuloriRGB pointColor = new CuloriRGB();
        public CuloriRGB pointStrokeColor = new CuloriRGB();
        public CuloriRGB pointHighlightFill = new CuloriRGB();
        public CuloriRGB pointHighlightStroke = new CuloriRGB();
        public List<ItemsData> data = new List<ItemsData>();
        //      data: [28, 48, 40, 19, 86, 27, 90, 1, 1, 1, 1, 0]
    }
    public class ItemsData
    {
        public double itemData = 0;
    }
    public class AreaChartOptions
    {
        //Boolean - Whether grid lines are shown across the chart
        public bool scaleShowGridLines = true;
        //String - Colour of the grid lines
        public string scaleGridLineColor = "rgba(0,0,0,.05)";
        //Number - Width of the grid lines
        public Int32 scaleGridLineWidth = 1;
        //Boolean - Whether to show horizontal lines (except X axis)
        public bool scaleShowHorizontalLines = true;
        //Boolean - Whether to show vertical lines (except Y axis)
        public bool scaleShowVerticalLines = true;
        //Boolean - Whether the line is curved between points
        public bool bezierCurve = true;
        //Number - Tension of the bezier curve between points
        public double bezierCurveTension = 0.4;
        //Boolean - Whether to show a dot for each point
        public bool pointDot = true;
        //Number - Radius of each point dot in pixels
        public Int32 pointDotRadius = 4;
        //Number - Pixel width of point dot stroke
        public Int32 pointDotStrokeWidth = 1;
        //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
        public Int32 pointHitDetectionRadius = 20;
        //Boolean - Whether to show a stroke for datasets
        public bool datasetStroke = true;
        //Number - Pixel width of dataset stroke
        public Int32 datasetStrokeWidth = 2;
        //Boolean - Whether to fill the dataset with a colour
        public bool datasetFill = true;
        //String - A legend template
        public string legendTemplate = "";
        //"<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i = 0; i < datasets.length; i++)
        //{%><li><span style =\"background-color:<%=datasets[i].strokeColor%>\"></span><%if (datasets[i].label)
        //{%><%=datasets[i].label%><%}%></li><%}%></ul>"
    }
    public class CuloriRGB
    {
        public string R = "255";
        public string G = "255";
        public string B = "255";
        public string transparenta = "0";
        public CuloriRGB()
        {
        }
    }

    // PENTRU AREA CHART //
    // fundal - rgba cu transparenta "rgba(60,141,188,1)"
    // margine - rgba fara transparenta "rgba(60,141,188)"
    // punct fundal - rgba fara transparenta "rgba(60,141,188)"
    // punct margine - rgba fara transparenta "rgba(60,141,188)"
    // punct highlight fundal - rgba fara transparenta "rgba(60,141,188)"
    // punct highlight margine - rgba fara transparenta "rgba(60,141,188)"







}
