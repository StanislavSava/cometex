﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace cometex.Clase
{
    public class Butoane
    {
        static public void AscundeArata(List<Button> Butoane, bool Stare)
        {
            foreach (Button Buton in Butoane)
                Buton.Visible = Stare;
        }
    }
}