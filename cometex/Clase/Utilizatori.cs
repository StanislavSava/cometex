﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cometex.Clase
{
    public class Utilizatori
    {
        public static void ParolaVerifica(string pParolaNoua, string pParolaConfirm, string pUtilizator, out string pExpresie)
        {
            pExpresie = "";
            // verifica dimensiunea si alte caractere
            // daca parola moua nu corespunde criteriilor
            if (!Valideaza.Parola(pParolaNoua))
            {
                pExpresie = "Parola nouă trebuie să conţină minim 8 caractere, cel puţin o literă mare şi una mică, o cifră şi un caracter special ( @ # $ % ^ & + = ? ! > < . * ~ | ). Mai incercaţi!";
                return;
            }
            // daca nu sunt egale noua = confirm
            if (pParolaNoua != pParolaConfirm)
            {
                pExpresie = "Parola nouă şi confirmarea acesteia nu sunt identice. Mai incercaţi!";
                return;
            }
            string vParolaNoua = Criptare.Encrypt(pParolaNoua);
            string vParolaConfirm = Criptare.Encrypt(pParolaConfirm);
            //if (pUtilizator != "")
            //{
            //    // verificam si sa nu mai fie introdusa in ultimele 10 parole
            //    // luam ultimele 10 parole din utilizatoriParole
            //    List<string> vListaCampuri = new List<string> { "utilizatorParola", "utilizatorData" };
            //    List<List<string>> vListaParole = ManipuleazaBD.fRezultaListaStringuri("SELECT TOP(10) * FROM utilizatoriParole WHERE utilizatorId = " + pUtilizator + " ORDER BY utilizatorData DESC", vListaCampuri);
            //    // daca nu are nicio parola salvata verificam cu cea din utilizatori
            //    if (vListaParole.Count == 0)
            //    {
            //        string vParolaUtilizator = ManipuleazaBD.fRezultaUnString("SELECT * FROM utilizatori WHERE utilizatorId = " + pUtilizator, "utilizatorParola");
            //        if (vParolaUtilizator == Criptare.Encrypt(pParolaNoua))
            //        {
            //            pExpresie = Resources.Resursa.raParolaFolositaRecent;
            //            return;
            //        }
            //        // introducem si parola care nu a existat
            //        ManipuleazaBD.fManipuleazaBD("INSERT INTO utilizatoriParole (utilizatorParola,utilizatorId,utilizatorData) VALUES (N'" + vParolaUtilizator + "', " + pUtilizator + ", CONVERT(datetime, '" + DateTime.Now.ToString() + "', 104))");
            //    }
            //    else
            //    {
            //        // daca are parole salvate
            //        foreach (List<string> vParola1 in vListaParole)
            //        {
            //            if (vParola1[0] == Criptare.Encrypt(pParolaNoua))
            //            {
            //                pExpresie = Resources.Resursa.raParolaFolositaRecent;
            //                return;
            //            }
            //        }
            //    }
            //}
        }
    }
}
