﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cometex.Clase
{
    public class MesajeCls
    {
        public Int32 IdMesaj { get; set; }
        public string Subiect { get; set; }
        public string Continut { get; set; }
        public DateTime DataExpedierii { get; set; }
        public DateTime DataCitirii { get; set; }
        // pentru SMS
        public string TelefonDestinatar { get; set; }
        // pentru email
        public List<string> AdresaDestinatar { get; set; }
        public List<string> AdresaExpeditor { get; set; }
        public MesajeCls()
        {
            Subiect = "";
            Continut = "";
            DataExpedierii = new DateTime();
            DataCitirii = new DateTime();
            TelefonDestinatar = "";
            AdresaDestinatar = new List<string>();
            AdresaDestinatar.Add("");
            AdresaDestinatar.Add("");
            AdresaExpeditor = new List<string>();
            AdresaExpeditor.Add("");
            AdresaExpeditor.Add("");
        }
    }
}