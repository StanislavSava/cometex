﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cometex.Clase
{
    public class Societati
    {
        public int Id = 0;
        public int IdFacturare = 0;
        public string Denumire = "";
        public string NrRegistrulComertului = "";
        public int CapitalSocial = 0;
        public string CodIdentificareFiscala = "";
        public string Adresa = "";
        public string Contul = "";
        public string Banca = "";
        public List<string> Telefoane = new List<string>();
        public List<Delegati> Delegati = new List<Delegati>();
        public Delegati PersoanaDeContact = new Delegati();
        public string SAGAfolder = "0000";
        public decimal SuprafataTotala = 0;
        public string Oras = "";
        public Societati()
        { }
    }
}
