﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cometex.Clase
{
    public class Cheltuieli
    {
        public int IdCheltuieli = 0;
        public CategoriiCheltuieli CategorieCheltuiala = new CategoriiCheltuieli();
        public string Explicatii = string.Empty;
        public string Destinatia = string.Empty;
        public decimal Plafon = 0;
        public decimal CheltuialaBrut = 0;
        public decimal CheltuialaNet = 0;
        public Societati Societatea = new Societati();
        public Delegati DelegatAIntrodusCheltuiala = new Delegati();
        public DateTime Scadenta = DateTime.Now;
        public DateTime IntrodusLaData = DateTime.Now;
        public Statusuri Status = new Statusuri();
        public Delegati RezolutionatDe = new Delegati();
        public DateTime RezolutionatLaData = DateTime.Now;
        public bool SeIncadreazaInGrila = true;
        public Int32 SAGAStatus = 0;
        public string SAGAExplicatii = string.Empty;
        public decimal SAGASuma = 0;
        public Cheltuieli()
        {

        }

    }
}
