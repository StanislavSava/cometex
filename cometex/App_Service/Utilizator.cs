﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cometex.App_Service
{
    public class Utilizator
    {
        string id = string.Empty;
        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        string nume = string.Empty;
        public string Nume
        {
            get
            {
                return nume;
            }

            set
            {
                nume = value;
            }
        }

        string username = string.Empty;
        public string Username
        {
            get
            {
                return username;
            }

            set
            {
                username = value;
            }
        }

        string parola = string.Empty;
        public string Parola
        {
            get
            {
                return parola;
            }

            set
            {
                parola = value;
            }
        }

        Societate societate;
        public Societate Societate
        {
            get
            {
                return societate;
            }

            set
            {
                societate = value;
            }
        }

        bool admin = false;
        public bool Admin
        {
            get
            {
                return admin;
            }

            set
            {
                admin = value;
            }
        }

    }
}