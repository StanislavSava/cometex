﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cometex.App_Service
{
    public class Investitie
    {
        string id = string.Empty;
        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        Societate societate = new Societate();
        public Societate Societate
        {
            get
            {
                return societate;
            }

            set
            {
                societate = value;
            }
        }

        string denumire = string.Empty;
        public string Denumire
        {
            get
            {
                return denumire;
            }

            set
            {
                denumire = value;
            }
        }

        string observatie = string.Empty;
        public string Observatie
        {
            get
            {
                return observatie;
            }

            set
            {
                observatie = value;
            }
        }

        Utilizator introdusDe = new Utilizator();
        internal Utilizator IntrodusDe
        {
            get
            {
                return introdusDe;
            }

            set
            {
                introdusDe = value;
            }
        }

        DateTime scadenta = new DateTime();
        public DateTime Scadenta
        {
            get
            {
                return scadenta;
            }

            set
            {
                scadenta = value;
            }
        }

        Decimal suma = 0;
        public decimal Suma
        {
            get
            {
                return suma;
            }

            set
            {
                suma = value;
            }
        }

        Status status = new Status();
        public Status Status
        {
            get
            {
                return status;
            }

            set
            {
                status = value;
            }
        }

        Utilizator rezolutionatDe = new Utilizator();
        public Utilizator RezolutionatDe
        {
            get
            {
                return rezolutionatDe;
            }

            set
            {
                rezolutionatDe = value;
            }
        }

        DateTime dataRezolutie = new DateTime();
        public DateTime DataRezolutie
        {
            get
            {
                return dataRezolutie;
            }

            set
            {
                dataRezolutie = value;
            }
        }

        Categorie categorie = new Categorie();
        public Categorie Categorie
        {
            get
            {
                return categorie;
            }

            set
            {
                categorie = value;
            }
        }
    }
}