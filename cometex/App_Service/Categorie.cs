﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cometex.App_Service
{
    public class Categorie
    {
        string id = string.Empty;
        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        string nume = string.Empty;
        public string Nume
        {
            get
            {
                return nume;
            }

            set
            {
                nume = value;
            }
        }
    }
}