﻿<%@ Page Title="Configurari grafice si rapoarte" Language="C#" MasterPageFile="~/Admin/Ad.master" AutoEventWireup="true" CodeBehind="GraficeConfigurari.aspx.cs" Inherits="cometex.Admin.GraficeConfigurari" %>

<%@ MasterType VirtualPath="~/Admin/Ad.master" %>
<%@ Reference VirtualPath="~/Cometex.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="PanelListaGrafice" runat="server" CssClass="form-horizontal">
        <asp:Panel runat="server" CssClass="block table-responsive text-center">
            <asp:GridView ID="GridListaGrafice" runat="server" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" DataSourceID="SqlDataSourceGrafice" CssClass="table table-hover table-condensed " ShowHeaderWhenEmpty="True" GridLines="None" OnSelectedIndexChanged="GridListaGrafice_SelectedIndexChanged" DataKeyNames="id">
                <AlternatingRowStyle BackColor="White" />
                <SelectedRowStyle BackColor="#738A9C" ForeColor="White" />
                <Columns>
                    <asp:CommandField ShowSelectButton="True" />
                    <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" Visible="false" />
                    <asp:BoundField DataField="denumire" HeaderText="Denumire" SortExpression="denumire" />
                    <asp:BoundField DataField="descriere" HeaderText="Descriere" SortExpression="descriere" />
                </Columns>
            </asp:GridView>
        </asp:Panel>
        <asp:SqlDataSource ID="SqlDataSourceGrafice" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT id, denumire, descriere FROM Grafice"></asp:SqlDataSource>
        <div class="clearfix" id="Butoane" runat="server">
            <asp:Button ID="ButonAdauga" CssClass="btn btn-primary" runat="server" OnClick="Adauga_Click" Text="Adauga" />
            <asp:Button ID="ButonModifica" CssClass="btn btn-danger" runat="server" OnClick="Modifica_Click" Text="Modifica" />
            <asp:Button ID="ButonSterge" CssClass="btn btn-danger" runat="server" OnClick="Sterge_Click" OnClientClick="return confirm('Sigur stergeti?');" Text="Sterge" />
        </div>
    </asp:Panel>
    <asp:Panel ID="PanelAdauga" runat="server" Visible="False" CssClass="form-horizontal">
        <asp:Label runat="server" Text="Adauga / Modifica" />
        <div class="form-group">
            <asp:Label runat="server" Text="Denumire grafic:" AssociatedControlID="tbGrafic" CssClass="col-md-2 control-label" />
            <div class="col-md-10">
                <asp:TextBox ID="tbGrafic" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="Descriere:" AssociatedControlID="tbDescriere" CssClass="col-md-2 control-label" />
            <div class="col-md-10">
                <asp:TextBox ID="tbDescriere" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="clearfix" id="idvButoaneAdauga" runat="server">
            <asp:Button ID="ButonSalveaza" CssClass="btn btn-success" runat="server" OnClick="ButonSalveaza_Click" Text="Salveaza" />
            <asp:Button ID="ButonRenunta" CssClass="btn btn-danger" runat="server" OnClick="ButonRenunta_Click" Text="Renunta" />
        </div>
    </asp:Panel>
    <script type="text/javascript">
        function pageLoad() {
            // for cities dropdowns
            $(".selectpicker").selectpicker();
        }
    </script>
</asp:Content>
