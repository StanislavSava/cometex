﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Ad.master" AutoEventWireup="true" CodeBehind="SeteazaParametriRaportSintetice.aspx.cs" Inherits="cometex.Admin.Raport.SeteazaParametriRaportSintetice" Culture="ro-RO" UICulture="ro-RO" %>

<%@ MasterType VirtualPath="~/Admin/Ad.master" %>
<%@ Reference VirtualPath="~/Cometex.Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel CssClass="container" runat="server">
        <asp:Panel ID="PanelGeneral" runat="server" CssClass="container-fluid ">
            <asp:Panel ID="PanelTitlu" runat="server" CssClass="block">
                <span style="font-weight: 500 !important; font-size: 30px">Situatie Facturi Chirii si Utilitati</span><br />
                <br />
            </asp:Panel>
            <asp:Panel ID="Panel3" runat="server" CssClass="block">
                <asp:Panel ID="Panel5" runat="server" CssClass="form-group">
                    <asp:Label ID="LabelSocietate" runat="server" Text="Raportare pentru societatea: "></asp:Label>
                    <asp:DropDownList ID="DropDownListSocietate" runat="server" DataSourceID="SocietatiDS" DataTextField="Societate" DataValueField="idFacturare" >
                    </asp:DropDownList>
                </asp:Panel>
                <asp:Panel ID="Panel8" runat="server" CssClass="form-group">
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <asp:Label ID="MessageLabel" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                    <asp:SqlDataSource ID="SocietatiDS" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT * FROM [Societati]"></asp:SqlDataSource>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="Panel4" runat="server" CssClass="container-fluid ">
                <asp:Panel ID="Panel6" runat="server" CssClass="row">
                    <asp:Button ID="Button1" CssClass="btn btn-primary" runat="server" Text="Afiseaza Raport" OnClick="Button1_Click" />
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
