﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Ad.master" AutoEventWireup="true" CodeBehind="Utilizatori.aspx.cs" Inherits="cometex.Admin.Utilizatori" %>

<%@ MasterType VirtualPath="~/Admin/Ad.master" %>
<%@ Reference VirtualPath="~/Cometex.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:Panel ID="PanelListaCereri" runat="server" CssClass="panel">
        <asp:Panel ID="Panel8" runat="server" CssClass="block">
            <asp:Panel ID="Panel5" runat="server" CssClass="inline">
                <asp:Label runat="server" Text="Societate" />
                <asp:DropDownList ID="ddlSocietati" runat="server" AppendDataBoundItems="True" DataSourceID="SqlSocietati" DataTextField="Societate" DataValueField="IdSocietate" AutoPostBack="true" OnSelectedIndexChanged="ddlSocietati_Toate">
                    <asp:ListItem Value="0" Selected="True">Toate</asp:ListItem>
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlSocietati" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT [IdSocietate], [Societate] FROM [Societati]"></asp:SqlDataSource>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel runat="server" CssClass="block table-responsive text-center">
            <asp:GridView ID="GridListaUtilizatori" runat="server" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="True" DataSourceID="SqlUtilizatori" CssClass="table table-hover table-condensed " ShowHeaderWhenEmpty="True" GridLines="None" OnSelectedIndexChanged="GridUtilizatori_SelectedIndexChanged" DataKeyNames="IdUtilizator, idSocietate" OnDataBound="GridListaUtilizatori_DataBound" PagerSettings-PageButtonCount="30">
                <AlternatingRowStyle BackColor="White" />
                <SelectedRowStyle BackColor="#738A9C" />
                <Columns>

                    <asp:CommandField ShowSelectButton="True" />
                    <asp:BoundField DataField="Utilizator" HeaderText="Utilizator" SortExpression="Utilizator" />
                    <asp:BoundField DataField="Nume" HeaderText="Nume" SortExpression="Nume" />
                    <asp:BoundField DataField="Parola" HeaderText="Parola" SortExpression="Parola" Visible="false" />
                    <asp:BoundField DataField="Societate" HeaderText="Societate" SortExpression="Societate" Visible="true" />
                    <asp:CheckBoxField DataField="Admin" HeaderText="Admin" SortExpression="Admin" />
                    <asp:BoundField DataField="IdUtilizator" HeaderText="IdUtilizator" InsertVisible="False" ReadOnly="True" SortExpression="IdUtilizator" Visible="False" />
                    <asp:BoundField DataField="idSocietate" HeaderText="idSocietate" SortExpression="idSocietate" Visible="False" />
                </Columns>
            </asp:GridView>
        </asp:Panel>
        <asp:SqlDataSource ID="SqlUtilizatori" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT Utilizatori.IdUtilizator, Utilizatori.Utilizator, Utilizatori.Nume, Utilizatori.Parola, Utilizatori.idSocietate, Utilizatori.Admin, Societati.Societate FROM Utilizatori INNER JOIN Societati ON Utilizatori.idSocietate = Societati.IdSocietate WHERE (Utilizatori.idSocietate = @societate) order by Utilizatori.Utilizator">
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlSocietati" Name="societate" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <div class="clearfix" id="Butoane" runat="server">
            <asp:Button ID="ButonAdauga" runat="server" CssClass="btn btn-primary" OnClick="Adauga_Click" Text="Adauga utilizator" />

            <asp:Button ID="ButonModifica" runat="server" CssClass="btn btn-danger" OnClick="Modifica_Click" Text="Modifica utilizator" />

            <asp:Button ID="ButonSterge" runat="server" CssClass="btn btn-danger" OnClick="Sterge_Click" OnClientClick="return confirm('Sigur stergeti utilizatorul?');" Text="Sterge utilizator" />
        </div>
    </asp:Panel>
    <asp:Panel ID="PanelAdauga" runat="server" Visible="False" CssClass="adauga panel">
        <asp:Label runat="server" Font-Bold="true" Font-Size="large" Text="Adauga un utilizator" />
        <asp:Panel ID="Panel1" runat="server">
            <asp:Label runat="server" Text="Nume si prenume:" />
            <asp:TextBox ID="tbNume" runat="server" Width="20em"></asp:TextBox>
        </asp:Panel>
        <asp:Panel ID="Panel2" runat="server">
            <asp:Label ID="lblUtilizatorExista" runat="server" Text="Numele de utilizator deja exista!" Font-Italic="true" Font-Size="Large" Visible="false" ForeColor="Red" />
            <br />
            <asp:Label runat="server" Text="Nume de utilizator:" />
            <asp:TextBox ID="tbUtilizator" runat="server" Width="20em"></asp:TextBox>
        </asp:Panel>
        <br />
        <asp:Label ID="lblSchimbaParola" runat="server" Text="Daca nu doriti schimbarea parolei, lasati campurile goale." Font-Italic="true" Font-Size="XX-Small" Visible="false" />
        <asp:CompareValidator ID="validatorParole" runat="server" ErrorMessage="Verifica parola!" ControlToCompare="tbParola" ControlToValidate="tbConfirmaParola" Font-Bold="true" ForeColor="Red" />
        <asp:Panel ID="Panel3" runat="server">
            <asp:Label runat="server" Text="Parola:" />
            <asp:TextBox ID="tbParola" runat="server" ReadOnly="false" Width="20em" TextMode="Password" OnTextChanged="tbParola_TextChanged" AutoPostBack="false"></asp:TextBox>

        </asp:Panel>
        <asp:Panel ID="Panel4" runat="server">
            <asp:Label runat="server" Text="Confirma Parola:" />
            <asp:TextBox ID="tbConfirmaParola" ReadOnly="false" runat="server" Width="20em" TextMode="Password" OnTextChanged="tbConfirmaParola_TextChanged" AutoPostBack="false"></asp:TextBox>
        </asp:Panel>
        <asp:Panel ID="Panel6" runat="server">
            <asp:Label runat="server" Text="Este administrator" />
            <asp:CheckBox ID="ckbAdministrator" runat="server" TextAlign="Left" CssClass="" />
        </asp:Panel>
        <asp:Panel ID="Panel7" runat="server">
            <asp:Label runat="server" Text="Societatea" />
            <asp:DropDownList ID="ddlSocietateAdauga" runat="server" AppendDataBoundItems="True" DataSourceID="SqlSocietati" DataTextField="Societate" DataValueField="IdSocietate" OnDataBound="ddlSocietateAdauga_DataBound" OnPreRender="ddlSocietateAdauga_DataBound">
            </asp:DropDownList>
        </asp:Panel>

    </asp:Panel>
    <div class="clearfix" id="Div1" runat="server">
        <asp:Button ID="ButonSalveaza" CssClass="btn btn-success" runat="server" OnClick="ButonSalveaza_Click" Text="Salveaza" />
        <asp:Button ID="ButonRenunta" CssClass="btn btn-danger" runat="server" OnClick="ButonRenunta_Click" Text="Renunta" />
    </div>


</asp:Content>
