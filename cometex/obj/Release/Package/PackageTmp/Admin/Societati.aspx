﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Ad.master" AutoEventWireup="true" CodeBehind="Societati.aspx.cs" Inherits="cometex.Admin.Societati" %>
<%@ MasterType VirtualPath="~/Admin/Ad.master" %>
<%@ Reference VirtualPath="~/Cometex.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="PanelListaCereri" runat="server">
        <asp:Panel runat="server" CssClass="block table-responsive text-center">
            <asp:GridView ID="GridListaSocietati" runat="server" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" DataSourceID="SqlDataSource1" CssClass="table table-hover table-condensed " ShowHeaderWhenEmpty="True" GridLines="None" OnSelectedIndexChanged="GridListaSocietati_SelectedIndexChanged" DataKeyNames="IdSocietate">
                <AlternatingRowStyle BackColor="White" />
                <SelectedRowStyle BackColor="#738A9C" />
                <Columns>
                    <asp:CommandField ShowSelectButton="True" />
                    <asp:BoundField DataField="IdSocietate" HeaderText="IdSocietate" InsertVisible="False" ReadOnly="True" SortExpression="IdSocietate" Visible="False" />
                    <asp:BoundField DataField="Societate" HeaderText="Societate" SortExpression="Societate" />
                </Columns>
            </asp:GridView>
        </asp:Panel>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT [IdSocietate], [Societate] FROM [Societati]"></asp:SqlDataSource>
        <div class="clearfix" id="Butoane" runat="server">
            <asp:Button ID="ButonAdauga"  CssClass="btn btn-primary" runat="server" OnClick="Adauga_Click" Text="Adauga societate" />

            <asp:Button ID="ButonModifica"  CssClass="btn btn-danger" runat="server" OnClick="Modifica_Click" Text="Modifica societate" />

            <asp:Button ID="ButonSterge"  CssClass="btn btn-danger" runat="server" OnClick="Sterge_Click" OnClientClick="return confirm('Sigur stergeti societatea?');" Text="Sterge societate" />
        </div>
    </asp:Panel>
    <asp:Panel ID="PanelAdauga" runat="server" Visible="False">
        <asp:Label runat="server" Text="Adauga o societate" />
        <div>
            <asp:Label runat="server" Text="Denumire:" />
            <asp:TextBox ID="Societate" runat="server" Width="20em"></asp:TextBox>

        </div>
        <div class="clearfix" id="Div1" runat="server">
            <asp:Button ID="ButonSalveaza" CssClass="btn btn-success" runat="server" OnClick="ButonSalveaza_Click" Text="Salveaza" />
            <asp:Button ID="ButonRenunta" CssClass="btn btn-danger" runat="server" OnClick="ButonRenunta_Click" Text="Renunta" />
        </div>

    </asp:Panel>
</asp:Content>
