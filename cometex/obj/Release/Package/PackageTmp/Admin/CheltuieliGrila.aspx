﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Ad.master" AutoEventWireup="true" CodeBehind="CheltuieliGrila.aspx.cs" Inherits="cometex.Admin.ListaCereriGrila" %>
<%@ MasterType VirtualPath="~/Admin/Ad.master" %>
<%@ Reference VirtualPath="~/Cometex.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="PanelListaCereri" runat="server" CssClass="panel">
        <asp:Panel ID="Panel3" runat="server" CssClass="block">
            <asp:Panel ID="Panel5" runat="server" CssClass="inline">
                <asp:Label runat="server" Text="Societate" />
                <asp:DropDownList ID="DropDownList1" runat="server" AppendDataBoundItems="True" AutoPostBack="True" DataSourceID="SqlDataSource2" DataTextField="Societate" DataValueField="IdSocietate">
                    <asp:ListItem Value="%">Toate</asp:ListItem>
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT [IdSocietate], [Societate] FROM [Societati] ORDER BY [Societate]"></asp:SqlDataSource>
            </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" CssClass="inline">
                <asp:Label runat="server" Text="Categorie" />
                <asp:DropDownList ID="DropDownList2" runat="server" AppendDataBoundItems="True" AutoPostBack="True" DataSourceID="SqlDataSource3" DataTextField="Categorie" DataValueField="IdCategorie">
                    <asp:ListItem Value="%">Toate</asp:ListItem>
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT [IdCategorie], [Categorie] FROM [Categorii] ORDER BY [Categorie]"></asp:SqlDataSource>
            </asp:Panel>
            <asp:Panel ID="Panel2" runat="server" CssClass="inline">
                <asp:Label runat="server" Text="Status" />

                <asp:DropDownList ID="DropDownList3" runat="server" AppendDataBoundItems="True" AutoPostBack="True" DataSourceID="SqlDataSource4" DataTextField="Status" DataValueField="IdStatus">
                    <asp:ListItem Value="%">Toate</asp:ListItem>
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT * FROM [Statusuri] ORDER BY [Status]"></asp:SqlDataSource>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel runat="server" CssClass="block table-responsive text-center">
            <asp:GridView ID="GridListaCereri" runat="server" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" DataSourceID="SqlDataSource1" DataKeyNames="idInvestitie" CssClass="table table-hover table-condensed" ShowHeaderWhenEmpty="True" GridLines="None" OnSelectedIndexChanged="GridListaCereri_SelectedIndexChanged">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:CommandField ShowSelectButton="True" />
                    <asp:BoundField DataField="Denumire" HeaderText="Denumire" ReadOnly="True" SortExpression="Denumire" Visible="True" />
                    <asp:BoundField DataField="Obs" HeaderText="Observatii" ReadOnly="True" SortExpression="Obs" Visible="True" />
                    <asp:BoundField DataField="DataIntroducere" HeaderText="Data introducerii" ReadOnly="True" SortExpression="DataIntroducere" Visible="True" />
                    <asp:BoundField DataField="Scadenta" HeaderText="Scadenta" ReadOnly="True" SortExpression="Scadenta" Visible="True" />
                    <asp:BoundField DataField="Suma" HeaderText="Suma" ReadOnly="True" SortExpression="Suma" Visible="True" />
                    <asp:BoundField DataField="DataRezolutie" HeaderText="Data rezolutiei" ReadOnly="True" SortExpression="DataRezolutie" Visible="True" />
                    <asp:BoundField DataField="Categorie" HeaderText="Categorie" ReadOnly="True" SortExpression="Categorie" Visible="True" />
                    <asp:BoundField DataField="AdaugatDe" HeaderText="Adaugata de" ReadOnly="True" SortExpression="AdaugatDe" Visible="True" />
                    <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="True" SortExpression="Status" Visible="True" />
                    <asp:BoundField DataField="RezolutionatDe" HeaderText="Rezolutionat de" ReadOnly="True" SortExpression="RezolutionatDe" Visible="True" />
                    <asp:BoundField DataField="IdInvestitie" Visible="False" />
                </Columns>
                <SelectedRowStyle BackColor="#738A9C" />
            </asp:GridView>
        </asp:Panel>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT Investitii.Denumire, Investitii.Obs, Investitii.IdInvestitie, Investitii.DataIntroducere, Investitii.Scadenta, Investitii.Suma, Investitii.DataRezolutie, Categorii.Categorie, Utilizatori.Nume as AdaugatDe, Statusuri.Status, Utilizatori_1.Nume AS RezolutionatDe FROM Investitii LEFT OUTER JOIN Categorii ON Investitii.idCategorie = Categorii.IdCategorie LEFT OUTER JOIN Societati ON Investitii.idSocietate = Societati.IdSocietate LEFT OUTER JOIN Utilizatori ON Investitii.IntrodusDe = Utilizatori.IdUtilizator LEFT OUTER JOIN Utilizatori AS Utilizatori_1 ON Investitii.RezolutionatDe = Utilizatori_1.IdUtilizator LEFT OUTER JOIN Statusuri ON Investitii.Status = Statusuri.IdStatus WHERE (Societati.IdSocietate LIKE @societate) AND (Categorii.IdCategorie LIKE @categorie) AND (Investitii.Status LIKE @status) ORDER BY Investitii.Scadenta">
            <SelectParameters>
                <asp:ControlParameter ControlID="DropDownList1" Name="societate" PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="DropDownList2" Name="categorie" PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="DropDownList3" Name="status" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <div class="clearfix" id="Butoane" runat="server" visible="false">
            <asp:Button ID="ButonAproba" CssClass="btn btn-success" runat="server" OnClick="ButonAproba_Click" Text="Aproba" Visible="false" />

            <asp:Button ID="ButonRespinge" CssClass="btn btn-danger" runat="server" OnClick="ButonRespinge_Click" Text="Respinge" Visible="false" />

            <asp:Button ID="ButonNerezolvat" CssClass="btn btn-default" runat="server" OnClick="ButonNerezolvat_Click" Text="Nerezolvat" Visible="false" />
        </div>

    </asp:Panel>

</asp:Content>
