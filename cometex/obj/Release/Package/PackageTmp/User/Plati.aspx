﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/Us.master" AutoEventWireup="true" CodeBehind="Plati.aspx.cs" Inherits="cometex.User.Plati" Culture="ro-RO" UICulture="ro-RO" %>

<%@ MasterType VirtualPath="~/User/Us.master" %>
<%@ Reference VirtualPath="~/Cometex.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="PanelListaCereri" runat="server" CssClass="panel">
        <asp:Panel ID="Panel7" runat="server" CssClass="block">
            <asp:Panel ID="Panel8" runat="server" CssClass="inline">
                <asp:Label runat="server" Text="Categorie" />
                <asp:DropDownList ID="ddlFiltrareCategorie" runat="server" AppendDataBoundItems="True" AutoPostBack="True" DataSourceID="SqlDataSource3" DataTextField="Categorie" DataValueField="IdCategorie" OnSelectedIndexChanged="ddlFiltrareCategorie_SelectedIndexChanged">
                    <asp:ListItem Value="%">Toate</asp:ListItem>
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT Categorii.IdCategorie, CONVERT (nvarchar, Categorii.Categorie) AS Categorie FROM Categorii where Categorii.activa = 1 and Categorii.tip like 'plata%' ORDER BY Categorie">
                    <SelectParameters>
                        <asp:SessionParameter Name="societate" SessionField="idSocietate" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </asp:Panel>
            <asp:Panel ID="Panel9" runat="server" CssClass="inline">
                <asp:Label runat="server" Text="Status" />
                <asp:DropDownList ID="ddlFiltrareStatus" runat="server" AppendDataBoundItems="True" AutoPostBack="True" DataSourceID="SqlDataSource4" DataTextField="statusSimplificat" DataValueField="idStatusSimplificat" OnSelectedIndexChanged="ddlFiltrareStatus_SelectedIndexChanged">
                    <asp:ListItem Value="%">Toate</asp:ListItem>
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT DISTINCT idStatusSimplificat, statusSimplificat FROM Statusuri WHERE (tip LIKE 'plata%') ORDER BY idStatusSimplificat"></asp:SqlDataSource>
            </asp:Panel>
            <asp:Panel ID="PanelLuna" runat="server" CssClass="inline">
                <asp:Label ID="lblScadentaIncepandCu" runat="server" Text="Scadenta incepand cu"></asp:Label>
                <asp:TextBox ID="TextBoxScadentaIncepandCu" runat="server" CssClass="form-control" AutoPostBack="true"></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="CalendarExtenderScadentaIncepandCu" runat="server" DefaultView="Days" TargetControlID="TextBoxScadentaIncepandCu" ValidateRequestMode="Enabled" ClearTime="True" FirstDayOfWeek="Monday" Format="dd.MM.yyyy" TodaysDateFormat="dd.MM.yyyy" />
            </asp:Panel>
        </asp:Panel>
        <asp:Panel runat="server" CssClass="block table-responsive text-center">
            <asp:Panel runat="server" CssClass="block table-responsive text-center">
                <asp:GridView ID="GridListaCereri" runat="server" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" DataSourceID="SqlDataSource1" CssClass="table table-hover table-condensed " ShowHeaderWhenEmpty="True" GridLines="None" OnSelectedIndexChanged="GridListaCereri_SelectedIndexChanged" OnSorting="GridListaCereri_Sorting" OnLoad="GridListaCereri_Load" OnPreRender="GridListaCereri_PreRender" OnRowDataBound="GridListaCereri_RowDataBound" DataKeyNames="IdInvestitie" ShowFooter="false">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:TemplateField HeaderText="Cod pt. SAGA" SortExpression="IdInvestitie"  ItemStyle-CssClass="alert-info">
                            <ItemTemplate>
                                <asp:Label ID="lblIdCheltuiala" runat="server" Text='<%# Bind("IdInvestitie") %>' Visible="true"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Suma<br />(lei)" SortExpression="Suma" ItemStyle-CssClass="alert-warning">
                            <ItemTemplate>
                                <asp:Label ID="LabelSuma" runat="server" Text='<%# Eval("Suma") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Destinatia" SortExpression="Denumire">
                            <ItemTemplate>
                                <asp:Label ID="lblDestinatia" runat="server" Text='<%# Bind("Denumire") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Explicatii" SortExpression="Obs">
                            <ItemTemplate>
                                <asp:Label ID="lblExplicatii" runat="server" Text='<%# Bind("Obs") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Data Introducerii" SortExpression="DataIntroducere">
                            <ItemTemplate>
                                <asp:Label ID="lblDataIntroducere" runat="server" Text='<%# Bind("DataIntroducere", "{0:dd.MM.yyyy, HH:MM}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Scadenta" SortExpression="Scadenta">
                            <ItemTemplate>
                                <asp:Label ID="lblScadenta" runat="server" Text='<%# Bind("Scadenta","{0:dd.MM.yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Rezolutionat De" SortExpression="RezolutionatDe">
                            <ItemTemplate>
                                <%# "<div class=\"center\">" + 
                                        ((Eval("NumeRezolutionatDe").ToString().Contains("ne-procesat")) 
                                          ? 
                                            "<div class=\"glyphicon glyphicon-warning-sign block\" aria-hidden=\"true\"></div><div class=\"label label-warning\">-</div>" 
                                          : 
                                            Eval("NumeRezolutionatDe")  )
                                     
                                         + "</div>" %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Data Rezolutie" SortExpression="DataRezolutie">
                            <ItemTemplate>
                                <%# "<div class=\"center\">" + 
                                        ((Eval("DataRezolutie").ToString().Contains("1900")) 
                                          ? 
                                            "<div class=\"glyphicon glyphicon-warning-sign block\" aria-hidden=\"true\"></div><div class=\"label label-warning\">-</div>" 
                                          : 
                                            Eval("DataRezolutie", "{0:dd.MM.yyyy, HH:MM}")  )
                                     
                                         + "</div>" %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Nume" SortExpression="Nume">
                            <ItemTemplate>
                                <asp:Label ID="Label15" runat="server" Text='<%# Bind("Nume") %>'></asp:Label>
                                <asp:Label ID="Label14" runat="server" Text='<%# Bind("IdUtilizator") %>' Visible="false"></asp:Label>
                                <asp:Label ID="Label16" runat="server" Text='<%# Bind("Utilizator") %>' Visible="false"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Categorie" SortExpression="Categorie">
                            <ItemTemplate>
                                <asp:Label ID="Label18" runat="server" Text='<%# Bind("Categorie") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" SortExpression="statusDenumire">
                            <ItemTemplate>
                                <%# "<div class=\"center\">" +
              // 0 depasit grila - spre aprobare
              (Eval("statusDenumire").ToString().Contains("plata spre aprobare")
                   ?
                     "<div class=\"glyphicon glyphicon-warning-sign block\" aria-hidden=\"true\"></div><div class=\"label label-warning\">plata spre aprobare</div>"
                  :
                        Eval("statusDenumire").ToString().Contains("plata respinsa")
                            ? "<div class=\"glyphicon glyphicon-warning-sign block\" aria-hidden=\"true\"></div><div class=\"label label-danger\">exceptionala respinsa</div>"
                            :
                                    Eval("statusDenumire").ToString().Contains("cerut explicatii")
                                    ? "<div class=\"glyphicon glyphicon-warning-sign block\" aria-hidden=\"true\"></div><div class=\"label label-danger\">cerut explicatii</div>"
                                    :
                                           Eval("statusDenumire").ToString().Contains("explicatii acordate")
                                            ? "<div class=\"glyphicon glyphicon-warning-sign block\" aria-hidden=\"true\"></div><div class=\"label label-warning\">explicatii acordate</div>"
                                            : Eval("statusDenumire")
                                            
                                            )

                                         + "</div>" %>
                                <asp:Label ID="lblIdStatus" runat="server" Text='<%# Bind("Status") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblIdStatusSimplificat" runat="server" Text='<%# Bind("idStatusSimplificat") %>' Visible="false"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Stadiu verificare SAGA" SortExpression="SAGAStatus" ItemStyle-CssClass="alert-info">
                            <ItemTemplate>
                                <asp:Label ID="gvLblSAGAExplicatii" runat="server" Text='<%# Bind("SAGAExplicatii") %>' Visible="true" CssClass=""></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="alert-info small" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Societate" SortExpression="Societate">
                            <ItemTemplate>
                                <asp:Label ID="Label20" runat="server" Text='<%# Bind("Societate") %>'></asp:Label>
                                <asp:Label ID="lblIdSocietate" runat="server" Text='<%# Bind("idSocietate") %>' Visible="false"></asp:Label>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("IdInvestitie") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblIdCategorie" runat="server" Text='<%# Bind("idCategorie") %>' Visible="false"></asp:Label>
                                <asp:Label ID="Label7" runat="server" Text='<%# Bind("IntrodusDe") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblSuma" runat="server" Text='<%# Bind("Suma") %>' Visible="false"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:Panel>
        </asp:Panel>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>"
            SelectCommand="SELECT i.IdInvestitie, i.idSocietate, i.idCategorie, i.Denumire, i.Obs, i.DataIntroducere, i.IntrodusDe, i.Scadenta, i.Suma, i.Status, i.RezolutionatDe, i.DataRezolutie, i.SumaNeta, u.IdUtilizator, u.Nume, u.Utilizator, u.Parola, u.Admin, c.Categorie, s.Status AS statusDenumire, s.idStatusSimplificat, s.statusSimplificat, so.Societate,
            
            (SELECT SUM(Suma) AS sumaPanaAcum000 FROM Investitii AS i WHERE 
                    (CONVERT (nvarchar, Status) LIKE '1') AND (CONVERT (nvarchar, IntrodusDe) LIKE CONVERT (nvarchar, @utilizator)) AND (CONVERT (nvarchar, idSocietate) LIKE CONVERT (nvarchar, @societate)) AND (CONVERT (nvarchar, idCategorie) LIKE CONVERT (nvarchar, @categorie))) AS TotalAprobateIntroduse, 
            
            (SELECT SUM(Suma) AS sumaPanaAcum00 FROM Investitii AS ii WHERE 
                    (CONVERT (nvarchar, i.IntrodusDe) LIKE CONVERT (nvarchar, @utilizator)) AND (CONVERT (nvarchar, i.idSocietate) LIKE CONVERT (nvarchar, @societate)) AND (CONVERT (nvarchar, i.idCategorie) LIKE CONVERT (nvarchar, @categorie))) AS TotalGeneralIntroduse, 
            
            (SELECT SUM(Suma) AS sumaPanaAcum0 FROM Investitii AS ii WHERE 
                    (idSocietate = so.IdSocietate) AND (idCategorie = c.IdCategorie) AND (MONTH(Scadenta) = MONTH(i.Scadenta))) AS sumaPanaAcum, 
                    COALESCE (ur.Nume, 'ne-procesat') AS NumeRezolutionatDe, CASE WHEN CONVERT (nvarchar , i.DataRezolutie) LIKE '%1900%' THEN 'ne-procesat' ELSE CONVERT (nvarchar , i.DataRezolutie) END AS DataRezolutieX, i.DataRezolutie AS Expr1,
            
                i.SAGAExplicatii,  
                i.SAGAStatus,
                i.SAGASuma
            
            
            FROM Investitii AS i 
            INNER JOIN Utilizatori AS u ON i.IntrodusDe = u.IdUtilizator 
            INNER JOIN Categorii AS c ON i.idCategorie = c.IdCategorie 
            INNER JOIN Statusuri AS s ON i.Status = s.IdStatus 
            INNER JOIN Societati AS so ON i.idSocietate = so.IdSocietate 
            LEFT OUTER JOIN Utilizatori AS ur ON i.RezolutionatDe = ur.IdUtilizator 
            WHERE 
                (CONVERT (nvarchar, i.idCategorie) LIKE @categorie) 
                AND (CONVERT (nvarchar, i.idSocietate) LIKE @societate) 
                AND (i.Scadenta &gt;= CONVERT (datetime, SUBSTRING(@scadenta, 0, 11), 103)) 
                AND (CONVERT (nvarchar, i.IntrodusDe) LIKE CONVERT (nvarchar, @utilizator)) 
                AND (CONVERT (nvarchar, s.idStatusSimplificat) LIKE @status) 
                AND s.tip like 'plata%'
            ORDER BY i.Scadenta">
            <SelectParameters>
                <asp:SessionParameter Name="utilizator" SessionField="idUtilizator" />
                <asp:SessionParameter Name="societate" SessionField="idSocietate" />
                <asp:ControlParameter ControlID="ddlFiltrareCategorie" Name="categorie" PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="ddlFiltrareStatus" Name="status" PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="TextBoxScadentaIncepandCu" Name="scadenta" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:Panel runat="server" CssClass="alert alert-success farapadding">
            <asp:Label ID="LabelSumaTotala" runat="server" Text='' />
        </asp:Panel>
        <div id="ButoaneGrid" runat="server" class="clearfix">
            <asp:Button CssClass="btn btn-primary" ID="AdaugaCerere" runat="server" Text="Adauga" OnClick="Adauga_Click" />
            <asp:Button CssClass="btn btn-warning" ID="btModifica" runat="server" Text="Modifica" OnClick="Modifica_Click" />
            <asp:Button CssClass="btn btn-danger" ID="StergeCerere" runat="server" OnClientClick="return confirm('Sigur stergeti?');" Text="Stergere" OnClick="Sterge_Click" />
            <asp:Button ID="btVerifica" CssClass="btn btn-default" runat="server" OnClick="btVerifica_Click" Text="Verifica in SAGA" />
            <asp:Button ID="btVerificaToate" CssClass="btn btn-default" runat="server" OnClick="btVerificaToate_Click" Text="Verifica TOATE in SAGA" />
        </div>

    </asp:Panel>
    <asp:Panel ID="PanelAdauga" runat="server" Visible="False" CssClass="col-lg-4">
        <asp:Panel ID="Panel6" runat="server" CssClass="form-group">
            <asp:Label ID="Label1" runat="server" Text="Adauga plata" Font-Size="Large"></asp:Label>
            <asp:Panel ID="PanelDestinatia" runat="server" CssClass="form-group">
                Destinatia<asp:TextBox ID="TextBoxDestinatia" runat="server" CssClass="form-control"></asp:TextBox>
            </asp:Panel>
            <asp:Panel ID="PanelExplicatii" runat="server" CssClass="form-group">
                <asp:Label ID="lblExplicatii" runat="server" Text="Explicatii"></asp:Label>
                <asp:TextBox ID="TextBoxExplicatii" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
            </asp:Panel>
            <asp:Panel ID="PanelSumaBruta" runat="server" CssClass="form-group">
                <asp:Label ID="LabelSumaBruta" runat="server" Text="Suma Bruta"></asp:Label>
                <asp:TextBox ID="TextBoxSumaBruta" runat="server" CssClass="form-control"></asp:TextBox>
            </asp:Panel>
            <asp:Panel ID="PanelSumaNeta" runat="server" CssClass="form-group" Visible="false">
                <asp:Label ID="LabelSumaNeta" runat="server" Text="Suma Neta"></asp:Label>
                <asp:TextBox ID="TextBoxSumaNeta" runat="server" CssClass="form-control"></asp:TextBox>
            </asp:Panel>
            <asp:Panel ID="PanelDataScadenta" runat="server" CssClass="form-group">
                <asp:Label ID="LabelDataScadenta" runat="server" Text="Data Scadenta"></asp:Label>
                <asp:TextBox ID="TextBoxDataScadenta" runat="server" CssClass="form-control"></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" DefaultView="Days" TargetControlID="TextBoxDataScadenta" ValidateRequestMode="Enabled" ClearTime="True" FirstDayOfWeek="Monday" Format="dd.MM.yyyy" TodaysDateFormat="dd.MM.yyyy" PopupPosition="TopLeft" />
            </asp:Panel>
            <asp:ScriptManager ID="ScriptManager2" runat="server"></asp:ScriptManager>
            <asp:Panel ID="PanelCategoriaCheltuiala" runat="server" CssClass="form-group">
                <asp:Label ID="LabelCategoria" runat="server" Text="Categorie de cheltuiala"></asp:Label>
                <asp:DropDownList ID="DropDownListCategoria" runat="server" DataSourceID="SqlDataSource5" DataTextField="Categorie" DataValueField="IdCategorie" CssClass="form-control">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT Categorii.IdCategorie, CONVERT (nvarchar, Categorii.Categorie) AS Categorie FROM Categorii where Categorii.activa = 1 and Categorii.tip like 'plata%' ORDER BY Categorie">
                    <SelectParameters>
                        <asp:SessionParameter Name="societate" SessionField="idSocietate" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </asp:Panel>
        </asp:Panel>
        <div class="clearfix" id="Div1" runat="server">
            <asp:Button ID="ButtonSalveaza" runat="server" OnClick="ButtonSalveaza_Click" Text="Salveaza" CssClass="btn btn-success" />
            <asp:Button ID="ButtonRenunta" runat="server" OnClick="ButtonRenunta_Click" Text="Renunta" CssClass="btn btn-danger" />
            <asp:Label ID="lblTipOperatie" runat="server" Text="Adauga" Visible="false"></asp:Label>
        </div>


    </asp:Panel>

</asp:Content>
