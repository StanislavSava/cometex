﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="cometex.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/style.css" rel="stylesheet" />
    <meta name="viewport" content="width = device-width, initial-scale = 1.0, minimum-scale = 1.0, maximum-scale = 1.0, user-scalable = no" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous" />

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css" integrity="sha384-aUGj/X2zp5rLCbBxumKTCw2Z50WgIr1vs/PFN4praOTvYXWlVyh2UtNUU0KAUhAX" crossorigin="anonymous" />

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
    <link href="css/default.css" rel="stylesheet" />
</head>
<body style="background: url('images/background.png') fixed bottom center no-repeat;">
    <form id="form1" runat="server">
        <div class="login">
            <h1>COMETEX</h1>
            <h2>Modul cheltuieli grila</h2>
            <asp:Label ID="LabelEroare" runat="server" ForeColor="Red" Text="fff" Visible="False"></asp:Label>
            <asp:Panel runat="server" CssClass="form-group">
                <asp:Label ID="Label2" runat="server" Text="Utilizator"></asp:Label>
                <asp:TextBox ID="TextBoxUtilizator" CssClass="form-control" runat="server" />
            </asp:Panel>
            <asp:Panel runat="server">
                <asp:Label ID="Label3" runat="server" Text="Parola"></asp:Label>
                <asp:TextBox ID="TextBoxParola" runat="server" CssClass="form-control" TextMode="Password" />
            </asp:Panel>
            <asp:Panel ID="PanelMeniu" runat="server" CssClass="clearfix">
                <asp:Button ID="ButtonConectare" runat="server" OnClick="ButtonConectare_Click" Text="Conectare" CssClass="btn btn-primary" />
                <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
            </asp:Panel>
        </div>
    </form>
</body>
</html>

