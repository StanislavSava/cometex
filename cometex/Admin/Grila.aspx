﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Ad.master" AutoEventWireup="true" CodeBehind="Grila.aspx.cs" Inherits="cometex.Admin.Grila" Culture="ro-RO" UICulture="ro-RO" %>

<%@ MasterType VirtualPath="~/Admin/Ad.master" %>
<%@ Reference VirtualPath="~/Cometex.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager2" runat="server"></asp:ScriptManager>
    <asp:Panel ID="PanelListaCereri" runat="server" CssClass="panel">
        <asp:Panel ID="Panel3" runat="server" CssClass="block">
            <asp:Panel ID="Panel5" runat="server" CssClass="inline">
                <asp:Label runat="server" Text="Societate" />
                <asp:DropDownList ID="ddlFiltrareSocietati" runat="server" AppendDataBoundItems="True" AutoPostBack="True" DataSourceID="SqlDataSource2" DataTextField="Societate" DataValueField="IdSocietate" OnSelectedIndexChanged="ddlFiltrareSocietati_SelectedIndexChanged" CssClass="form-control">
                    <asp:ListItem Value="%">Toate</asp:ListItem>
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT [IdSocietate], [Societate] FROM [Societati] ORDER BY [Societate]"></asp:SqlDataSource>
            </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" CssClass="inline">
                <asp:Label runat="server" Text="Categorie" />
                <asp:DropDownList ID="ddlFiltrareCategorii" runat="server" AppendDataBoundItems="True" AutoPostBack="True" DataSourceID="SqlDataSource3" DataTextField="Categorie" DataValueField="IdCategorie" OnPreRender="ddlFiltrareCategorii_PreRender" CssClass="form-control" OnSelectedIndexChanged="ddlFiltrareCategorii_SelectedIndexChanged">
                    <asp:ListItem Value="%">Toate</asp:ListItem>
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT DISTINCT Categorii.IdCategorie, CONVERT (nvarchar, Categorii.Categorie) + (CASE WHEN CONVERT (nvarchar , @societate) = '%' THEN ' ' ELSE ' (' + CONVERT (nvarchar , PlafoaneGrila.Suma) + ')' END) AS Categorie FROM Societati INNER JOIN PlafoaneGrila ON Societati.IdSocietate = PlafoaneGrila.idSocietate INNER JOIN Categorii ON PlafoaneGrila.idCategorie = Categorii.IdCategorie 
                    AND PlafoaneGrila.idCategorie = Categorii.IdCategorie AND (Categorii.activa = 1 and Categorii.tip like 'grila%')
                    WHERE (CONVERT (nvarchar, Societati.IdSocietate) LIKE CONVERT (nvarchar, CASE WHEN CONVERT (nvarchar , @societate) = '%' THEN '%' ELSE @societate END)) AND (Categorii.activa = 1) ORDER BY Categorie">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddlFiltrareSocietati" Name="societate" PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </asp:Panel>
            <asp:Panel ID="Panel2" runat="server" CssClass="inline">
                <asp:Label runat="server" Text="Status" />

                <asp:DropDownList ID="ddlFiltrareStatus" runat="server" CssClass="form-control" AppendDataBoundItems="True" AutoPostBack="True" DataSourceID="SqlDataSource4" DataTextField="statusSimplificat" DataValueField="idStatusSimplificat" OnSelectedIndexChanged="ddlFiltrareStatus_SelectedIndexChanged">
                    <asp:ListItem Value="%">Toate</asp:ListItem>
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT DISTINCT idStatusSimplificat, statusSimplificat FROM Statusuri WHERE (tip like 'grila%') ORDER BY idStatusSimplificat"></asp:SqlDataSource>
            </asp:Panel>
            <asp:Panel ID="PanelLuna" runat="server" CssClass="inline">
                <asp:Label ID="lblScadentaIncepandCu" runat="server" Text="Scadenta incepand cu"></asp:Label>
                <asp:TextBox ID="TextBoxScadentaIncepandCu" runat="server" CssClass="form-control" AutoPostBack="true"></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="CalendarExtenderScadentaIncepandCu" runat="server" DefaultView="Days" TargetControlID="TextBoxScadentaIncepandCu" ValidateRequestMode="Enabled" ClearTime="True" FirstDayOfWeek="Monday" Format="dd.MM.yyyy" TodaysDateFormat="dd.MM.yyyy" />
            </asp:Panel>
            <asp:Panel ID="PanelPanaLa" runat="server" CssClass="inline">
                <asp:Label ID="lblPanaLa" runat="server" Text="pana la"></asp:Label>
                <asp:TextBox ID="tbPanaLa" runat="server" CssClass="form-control" AutoPostBack="true"></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="CalendarExtenderScadentaPanaLa" runat="server" DefaultView="Days" TargetControlID="tbPanaLa" ValidateRequestMode="Enabled" ClearTime="True" FirstDayOfWeek="Monday" Format="dd.MM.yyyy" TodaysDateFormat="dd.MM.yyyy" />
            </asp:Panel>
            <asp:Panel ID="PanelDestinatie" runat="server" CssClass="inline">
                <asp:Label ID="LabelDestinatie" runat="server" Text="Destinatia"></asp:Label>
                <asp:TextBox ID="TextBoxDestinatie" runat="server" CssClass="form-control" AutoPostBack="true"></asp:TextBox>
            </asp:Panel>
        </asp:Panel>
        <div class="clearfix block" id="ButoaneSus" runat="server">
            <asp:Button ID="ButonAprobaSus" CssClass="btn btn-success" runat="server" OnClick="ButonAproba_Click" Text="Aproba" />
            <asp:Button ID="ButonRespingeSus" CssClass="btn btn-danger" runat="server" OnClick="ButonRespinge_Click" Text="Respinge" />
            <asp:Button ID="ButonNerezolvatSus" CssClass="btn btn-default" runat="server" OnClick="ButonNerezolvat_Click" Text="Nerezolvat" Visible="false" />
        </div>
        <asp:Panel runat="server" CssClass="block table-responsive text-center">
            <asp:Panel runat="server" CssClass="block table-responsive text-center">
                <asp:GridView ID="GridListaCereri" runat="server" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" DataSourceID="SqlDataSource1" DataKeyNames="idInvestitie" CssClass="table table-hover table-condensed " ShowHeaderWhenEmpty="True" GridLines="None" OnSelectedIndexChanged="GridListaCereri_SelectedIndexChanged"
                    OnSorted="GridListaCereri_Sorted" OnSorting="GridListaCereri_Sorting" OnLoad="GridListaCereri_Load" OnPreRender="GridListaCereri_PreRender" OnRowDataBound="GridListaCereri_RowDataBound">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />

                        <asp:TemplateField HeaderText="Societate" SortExpression="Societate">
                            <ItemTemplate>
                                <asp:Label ID="lblDenumireSocietate" runat="server" Text='<%# Bind("Societate") %>'></asp:Label>
                                <asp:Label ID="lblIdPlafon" runat="server" Text='<%# Bind("idPlafon") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblSumaPlafon" runat="server" Text='<%# Bind("plafonSuma") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblIdSocietate" runat="server" Text='<%# Bind("idSocietate") %>' Visible="false"></asp:Label>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("IdInvestitie") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblIdCategorie" runat="server" Text='<%# Bind("idCategorie") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblIntrodusDe" runat="server" Text='<%# Bind("IntrodusDe") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblNumeRezolutionatDe" runat="server" Text='<%# Bind("NumeRezolutionatDe") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblIdRezolutionatDe" runat="server" Text='<%# Bind("RezolutionatDe") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblDataRezolutie" runat="server" Text='<%# Bind("DataRezolutie") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblSuma" runat="server" Text='<%# Bind("Suma") %>' Visible="false"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cod pt. SAGA" SortExpression="IdInvestitie" ItemStyle-CssClass="alert-info">
                            <ItemTemplate>
                                <asp:Label ID="lblIdCheltuiala" runat="server" Text='<%# Bind("IdInvestitie") %>' Visible="true"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Suma<br />(lei)" SortExpression="Suma" ItemStyle-CssClass="alert-warning">
                            <ItemTemplate>
                                <asp:Label ID="LabelSuma" runat="server" Text='<%# Eval("Suma") + "<br /><small>(tot.luna " + Eval("sumaPanaAcum") + " | plafon: " + Eval("plafonSuma") + ")</small></small>" %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Destinatia" SortExpression="Denumire">
                            <ItemTemplate>
                                <asp:Label ID="lblDestinatia" runat="server" Text='<%# Bind("Denumire") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Explicatii" SortExpression="Obs">
                            <ItemTemplate>
                                <asp:Label ID="lblExplicatii" runat="server" Text='<%# Bind("Obs") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Data Introducerii" SortExpression="DataIntroducere">
                            <ItemTemplate>
                                <asp:Label ID="lblDataIntroducere" runat="server" Text='<%# Bind("DataIntroducere", "{0:dd.MM.yyyy, HH:MM}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Scadenta" SortExpression="Scadenta">
                            <ItemTemplate>
                                <asp:Label ID="lblScadenta" runat="server" Text='<%# Bind("Scadenta","{0:dd.MM.yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Rezolutionat De" SortExpression="RezolutionatDe">
                            <ItemTemplate>
                                <%# "<div class=\"center\">" + 
                                        ((Eval("NumeRezolutionatDe").ToString().Contains("ne-procesat")) 
                                          ? 
                                            "<div class=\"glyphicon glyphicon-warning-sign block\" aria-hidden=\"true\"></div><div class=\"label label-warning\">-</div>" 
                                          : 
                                            Eval("NumeRezolutionatDe")  )
                                     
                                         + "</div>" %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Data Rezolutie" SortExpression="DataRezolutie">
                            <ItemTemplate>
                                <%# "<div class=\"center\">" + 
                                        ((Eval("DataRezolutie").ToString().Contains("1900")) 
                                          ? 
                                            "<div class=\"glyphicon glyphicon-warning-sign block\" aria-hidden=\"true\"></div><div class=\"label label-warning\">-</div>" 
                                          : 
                                            Eval("DataRezolutie", "{0:dd.MM.yyyy, HH:MM}")  )
                                     
                                         + "</div>" %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Nume" SortExpression="Nume">
                            <ItemTemplate>
                                <asp:Label ID="lblNumeUtilizator" runat="server" Text='<%# Bind("Nume") %>'></asp:Label>
                                <asp:Label ID="lblIdUtilizator" runat="server" Text='<%# Bind("IdUtilizator") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblEmailUtilizator" runat="server" Text='<%# Bind("email") %>' Visible="false"></asp:Label>
                                <asp:Label ID="Label16" runat="server" Text='<%# Bind("Utilizator") %>' Visible="false"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Categorie" SortExpression="Categorie">
                            <ItemTemplate>
                                <asp:Label ID="Label18" runat="server" Text='<%# Bind("Categorie") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" SortExpression="statusDenumire">
                            <ItemTemplate>
                                <%# "<div class=\"center\">" +
              // 0 depasit grila - spre aprobare
              (Eval("statusDenumire").ToString().Contains("depasit grila - spre aprobare")
                   ?
                     "<div class=\"glyphicon glyphicon-warning-sign block\" aria-hidden=\"true\"></div><div class=\"label label-warning\">depasit grila - spre aprobare</div>"
                  :
                        Eval("statusDenumire").ToString().Contains("depasit grila si respinsa")
                            ? "<div class=\"glyphicon glyphicon-warning-sign block\" aria-hidden=\"true\"></div><div class=\"label label-danger\">depasit grila si respinsa</div>"
                            :
                            Eval("statusDenumire").ToString().Contains("explicatii acordate")
                                ? "<div class=\"glyphicon glyphicon-warning-sign block\" aria-hidden=\"true\"></div><div class=\"label label-warning\">explicatii acordate</div>"
                                :
                                Eval("statusDenumire").ToString().Contains("cerut explicatii")
                                    ? "<div class=\"glyphicon glyphicon-warning-sign block\" aria-hidden=\"true\"></div><div class=\"label label-danger\">cerut explicatii !!!</div>"
                                    :  Eval("statusDenumire")

                                            )

                                         + "</div>" %>
                                <asp:Label ID="lblIdStatus" runat="server" Text='<%# Bind("Status") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblDenumireStatus" runat="server" Text='<%# Bind("statusDenumire") %>' Visible="false"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Stadiu verificare SAGA" SortExpression="SAGAStatus" ItemStyle-CssClass="alert-info small">
                            <ItemTemplate>
                                <asp:Label ID="lblSAGAStatus" runat="server" Text='<%# Bind("SAGAStatus") %>' Visible="false"></asp:Label>
                                <asp:Label ID="gvLblSAGAExplicatii" runat="server" Text='<%# Bind("SAGAExplicatii") %>' Visible="true" CssClass="small"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <FooterStyle CssClass="footerGrid" />
                    <PagerStyle CssClass="footerGrid" />
                </asp:GridView>
            </asp:Panel>
        </asp:Panel>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="
            SELECT i.IdInvestitie, i.idSocietate, i.idCategorie, i.Denumire, i.Obs, i.DataIntroducere, i.IntrodusDe, i.Scadenta, i.Suma, i.Status, i.RezolutionatDe, i.DataRezolutie, i.SumaNeta, i.SAGAStatus, u.IdUtilizator, u.Nume, u.Utilizator, u.Parola, u.Admin, u.email, 
            c.Categorie, 
            s.Status AS statusDenumire,  s.idStatusSimplificat, s.statusSimplificat,
            so.Societate, 
            p.idPlafon, p.Suma AS plafonSuma, p.Observatii, (SELECT SUM(Suma) AS sumaPanaAcum0 FROM Investitii AS ii WHERE (idSocietate = so.IdSocietate) AND (idCategorie = c.IdCategorie) AND (MONTH(Scadenta) = MONTH(i.Scadenta))) AS sumaPanaAcum, 
            COALESCE (ur.Nume, 'ne-procesat') AS NumeRezolutionatDe, 
            CASE WHEN CONVERT (nvarchar , i.DataRezolutie) LIKE '%1900%' THEN 'ne-procesat' ELSE CONVERT (nvarchar , i.DataRezolutie) END AS DataRezolutieX, 
            i.DataRezolutie AS Expr1, i.SAGAExplicatii, i.SAGAStatus AS Expr2, i.SAGASuma 
            
            FROM Investitii AS i 
            INNER JOIN Utilizatori AS u ON i.IntrodusDe = u.IdUtilizator 
            INNER JOIN Categorii AS c ON i.idCategorie = c.IdCategorie 
            INNER JOIN Statusuri AS s ON i.Status = s.IdStatus 
            INNER JOIN Societati AS so ON i.idSocietate = so.IdSocietate 
            INNER JOIN PlafoaneGrila AS p ON p.idSocietate = so.IdSocietate AND p.idCategorie = c.IdCategorie 
            LEFT OUTER JOIN Utilizatori AS ur ON i.RezolutionatDe = ur.IdUtilizator 
            
            WHERE (CONVERT (nvarchar, i.idCategorie) LIKE @categorie) 
            AND (CONVERT (nvarchar, i.idSocietate) LIKE @societate) 
            AND (i.Scadenta &gt;= CONVERT (datetime, SUBSTRING(@scadenta, 0, 11), 103)) 
            AND (i.Scadenta &lt;= CONVERT (datetime, SUBSTRING(@panaLa, 0, 11), 103)) 
            AND (CONVERT (nvarchar, i.Denumire) LIKE '%' + @destinatie + '%') 
            AND (CONVERT (nvarchar, s.idStatusSimplificat) LIKE @status) 
            AND (s.tip LIKE 'grila%') 
            
            ORDER BY i.IdInvestitie DESC">
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlFiltrareCategorii" Name="categorie" PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="ddlFiltrareStatus" Name="status" PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="ddlFiltrareSocietati" Name="societate" PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="TextBoxScadentaIncepandCu" Name="scadenta" />
                <asp:ControlParameter ControlID="tbPanaLa" Name="panaLa" />
                <asp:ControlParameter ControlID="TextBoxDestinatie" Name="destinatie" DefaultValue="%" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:Panel runat="server" CssClass="alert alert-success farapadding">
            <asp:Label ID="LabelSumaTotala" runat="server" Text='' />
        </asp:Panel>
        <div class="clearfix" id="Butoane" runat="server">
            <asp:Button ID="ButonAproba" CssClass="btn btn-success" runat="server" OnClick="ButonAproba_Click" Text="Aproba" />
            <asp:Button ID="ButonRespinge" CssClass="btn btn-danger" runat="server" OnClick="ButonRespinge_Click" Text="Respinge" />
            <asp:Button ID="ButonNerezolvat" CssClass="btn btn-default" runat="server" OnClick="ButonNerezolvat_Click" Text="Nerezolvat" Visible="false" />
            <asp:Button ID="btVerifica" CssClass="btn btn-default" runat="server" OnClick="btVerifica_Click" Text="Verifica in SAGA" />
            <asp:Button ID="btVerificaToate" CssClass="btn btn-default" runat="server" OnClick="btVerificaToate_Click" Text="Verifica TOATE in SAGA" />
        </div>
    </asp:Panel>
    <script type="text/javascript">
        function openModal() {
            $('#ContentPlaceHolder2_ContentPlaceHolder1_Opereaza').modal('show');
        }
    </script>
    <!-- Modal -->
    <div class="modal fade in" runat="server" id="Opereaza" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Rezolutie pentru cheltuiala:</h4>
                </div>
                <div class="modal-body">
                    <asp:Label runat="server" ID="lblDetaliiRandInModal"></asp:Label>
                    <asp:Panel ID="pnMesaj" runat="server" CssClass="block" Visible="false">
                        <hr />
                        <asp:Panel ID="pnMesajSubiect" runat="server" CssClass="block">
                            <asp:Label ID="lblSubiect" runat="server" Text="Subiect" CssClass=""></asp:Label>
                            <asp:TextBox ID="tbSubiect" runat="server" CssClass="form-control full100"></asp:TextBox>
                        </asp:Panel>
                        <asp:Panel ID="pnMesajMesaj" runat="server" CssClass="block">
                            <asp:Label ID="Label2" runat="server" Text="Mesaj"></asp:Label>
                            <asp:TextBox ID="tbMesaj" runat="server" CssClass="form-control" Rows="3" TextMode="MultiLine"></asp:TextBox>
                        </asp:Panel>
                    </asp:Panel>
                </div>
                <div class="modal-footer">
                    <asp:Panel ID="pnModalButoanePrincipale" runat="server" CssClass="block">
                        <asp:Button ID="Button1" CssClass="btn btn-success" runat="server" OnClick="ButonAproba_Click" Text="Aproba" />
                        <asp:Button ID="Button2" CssClass="btn btn-danger" runat="server" OnClick="ButonRespinge_Click" Text="Respinge" />
                        <asp:Button ID="Button3" CssClass="btn btn-default" runat="server" OnClick="ButonNerezolvat_Click" Enabled="true" Text="Cere Detalii" />
                    </asp:Panel>
                    <asp:Panel ID="pnModalButoaneMesaj" runat="server" CssClass="block">
                        <asp:Button ID="btTrimiteMesaj" CssClass="btn btn-success" runat="server" OnClick="btTrimiteMesaj_Click" Text="Trimite mesaj" />
                        <asp:Button ID="btRenunta" CssClass="btn btn-info" runat="server" OnClick="btRenunta_Click" Text="Renunta" />
                    </asp:Panel>
                    <button type="button" class="btn btn-default block" data-dismiss="modal">Inchide</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
