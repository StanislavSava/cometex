﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Ad.master" AutoEventWireup="true" CodeBehind="CursuriBNR.aspx.cs" Inherits="cometex.Admin.CursBNR" Culture="ro-RO" UICulture="ro-RO" %>

<%@ MasterType VirtualPath="~/Admin/Ad.master" %>
<%@ Reference VirtualPath="~/Cometex.Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
        <h2>Cursuri BNR</h2>

    <asp:Panel CssClass="form-horizontal col-md-12" ID="PanelListaCereri" runat="server">
        <asp:Panel runat="server" CssClass="">
            <asp:Button ID="btAdaugaSus" CssClass="btn btn-primary" runat="server" OnClick="Adauga_Click" Text="Adauga" />

            <asp:Button ID="btModificaSus" CssClass="btn btn-danger" runat="server" OnClick="Modifica_Click" Text="Modifica" />

            <asp:Button ID="btStergeSus" CssClass="btn btn-danger" runat="server" OnClick="Sterge_Click" OnClientClick="return confirm('Sigur stergeti cursul?');" Text="Sterge" />
        </asp:Panel>
        <asp:Panel runat="server" CssClass="row table-responsive">
            <asp:Panel runat="server" CssClass="block text-center">
                <asp:GridView ID="GridListaCursuri" runat="server" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" DataSourceID="SqlDataSource1" CssClass="table table-hover table-condensed " ShowHeaderWhenEmpty="True" GridLines="None" OnSelectedIndexChanged="GridListaCursuri_SelectedIndexChanged" DataKeyNames="Id">
                    <AlternatingRowStyle BackColor="White" />
                    <SelectedRowStyle BackColor="#738A9C" />
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:BoundField DataField="IdSocietate" HeaderText="Id" InsertVisible="False" ReadOnly="True" Visible="False" />
                        <asp:BoundField DataField="Curs" HeaderText="Curs" SortExpression="Curs" />
                        <asp:BoundField DataField="Data" HeaderText="Data" SortExpression="Data" DataFormatString="{0:dd.MM.yyyy}" />
                    </Columns>
                </asp:GridView>
            </asp:Panel>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="
                      SELECT [Id]
                          ,[Curs]
                          ,[Data]
                      FROM [dbo].[Cursuri]
                      ORDER BY Data DESC"></asp:SqlDataSource>
        </asp:Panel>
        <asp:Panel runat="server" CssClass="">
            <asp:Button ID="ButonAdauga" CssClass="btn btn-primary" runat="server" OnClick="Adauga_Click" Text="Adauga" />

            <asp:Button ID="ButonModifica" CssClass="btn btn-danger" runat="server" OnClick="Modifica_Click" Text="Modifica" />

            <asp:Button ID="ButonSterge" CssClass="btn btn-danger" runat="server" OnClick="Sterge_Click" OnClientClick="return confirm('Sigur stergeti cursul?');" Text="Sterge" />
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="PanelAdauga" runat="server" CssClass="col-md-12" Visible="False">
        <asp:Panel ID="Panel1" runat="server" CssClass="col-md-6">
            <asp:Label runat="server" Text="Valoare curs" />
            <asp:TextBox ID="tbValoareCurs" runat="server"></asp:TextBox>
        </asp:Panel>
        <asp:Panel ID="Panel2" runat="server" CssClass="col-md-6">
            <asp:Label runat="server" Text="Data cursului" />
            <asp:TextBox ID="tbDataCurs" runat="server"></asp:TextBox>
            <ajaxtoolkit:CalendarExtender ID="tbDataCurs_CalendarExtender" runat="server" Format="dd/MM/yyyy" PopupButtonID="CalendarButtonDataCurs" PopupPosition="TopRight" TargetControlID="tbDataCurs" />
        </asp:Panel>
        <asp:Panel ID="Panel3" runat="server" CssClass="col-md-12">
            <asp:Button ID="ButonSalveaza" CssClass="btn btn-success" runat="server" OnClick="ButonSalveaza_Click" Text="Salveaza" />
            <asp:Button ID="ButonRenunta" CssClass="btn btn-danger" runat="server" OnClick="ButonRenunta_Click" Text="Renunta" />
        </asp:Panel>
    </asp:Panel>
</asp:Content>
