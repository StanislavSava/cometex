﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using cometex.Clase;
using cometex.Utils;
using System.Data;

namespace cometex.Admin
{
    public partial class CashFlowConfigurari : System.Web.UI.Page
    {
        private DataSourceCashFlow.CashflowConturi cashFlowConturi = new DataSourceCashFlow.CashflowConturi();// DataSourceCashFlow.CashFlowConturi();
       

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlSocietati.DataBind();

                cashFlowConturi = new DataSourceCashFlow.CashflowConturi();
                cashFlowConturi = DataSourceCashFlow.CashFlowConturi(ddlSocietati.SelectedValue);
                if (ddlCont1.Items.Count == 0)
                {
                    SAGA.SAGAPopuleazaDropDownListConturi(ddlCont1, ddlSocietati.SelectedItem.Text.Substring(0, ddlSocietati.SelectedItem.Text.IndexOf(" - ")));
                    SAGA.SAGAPopuleazaDropDownListConturi(ddlCont2, ddlSocietati.SelectedItem.Text.Substring(0, ddlSocietati.SelectedItem.Text.IndexOf(" - ")));
                    SAGA.SAGAPopuleazaDropDownListConturi(ddlCont3, ddlSocietati.SelectedItem.Text.Substring(0, ddlSocietati.SelectedItem.Text.IndexOf(" - ")));
                    SAGA.SAGAPopuleazaDropDownListConturi(ddlCont4, ddlSocietati.SelectedItem.Text.Substring(0, ddlSocietati.SelectedItem.Text.IndexOf(" - ")));
                    
                    //ddlCont2.Items.AddRange(ddlCont1.Items.OfType<ListItem>().ToArray());
                    //ddlCont3.Items.AddRange(ddlCont1.Items.OfType<ListItem>().ToArray());
                    //ddlCont4.Items.AddRange(ddlCont1.Items.OfType<ListItem>().ToArray());
                }
                SelecteazaInDropdownuri();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Master.AtribuieButoaneMeniu(MeniuUtils.AranjeazaElementeMeniu(22, new List<int> { 4, 15 }));

            }
        }
        protected void ButonSalveaza_Click(object sender, EventArgs e)
        {
            String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);
            using (con)
            {
                con.Open();
                String cmdStr = "";
                cmdStr = @"UPDATE [dbo].[ConturiAsociateRapoarte]  SET [conturiAsociate] = @conturiAsociateContCurent WHERE id =@conturiAsociateContCurentId ;" +
                         @"UPDATE [dbo].[ConturiAsociateRapoarte]  SET [conturiAsociate] = @conturiAsociateDepozit WHERE id =@conturiAsociateDepozitId;" + @"UPDATE [dbo].[ConturiAsociateRapoarte]  SET [conturiAsociate] = @conturiAsociateDepozitTaxe WHERE id =@conturiAsociateDepozitTaxeId;" +
                         @"UPDATE [dbo].[ConturiAsociateRapoarte]  SET [conturiAsociate] = @conturiAsociateCasierie WHERE id =@conturiAsociateCasierieId;";
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("conturiAsociateContCurent", ConturiUtils.ListaConturiStringAdauga(lblConturiAsociate1.Text, "<br />", " -"));
                cmd.Parameters.AddWithValue("conturiAsociateDepozit", ConturiUtils.ListaConturiStringAdauga(lblConturiAsociate2.Text, "<br />", " -"));
                cmd.Parameters.AddWithValue("conturiAsociateDepozitTaxe", ConturiUtils.ListaConturiStringAdauga(lblConturiAsociate3.Text, "<br />", " -"));
                cmd.Parameters.AddWithValue("conturiAsociateCasierie", ConturiUtils.ListaConturiStringAdauga(lblConturiAsociate4.Text, "<br />", " -"));

                cmd.Parameters.AddWithValue("conturiAsociateContCurentId", lblCont1.ToolTip);
                cmd.Parameters.AddWithValue("conturiAsociateDepozitId", lblCont2.ToolTip);
                cmd.Parameters.AddWithValue("conturiAsociateDepozitTaxeId", lblCont3.ToolTip);
                cmd.Parameters.AddWithValue("conturiAsociateCasierieId", lblCont4.ToolTip);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        protected void ddlSocietati_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCont1.Items.Clear();
            ddlCont2.Items.Clear();
            ddlCont3.Items.Clear();
            ddlCont4.Items.Clear();
            SAGA.SAGAPopuleazaDropDownListConturi(ddlCont1, ddlSocietati.SelectedItem.Text.Substring(0, ddlSocietati.SelectedItem.Text.IndexOf(" - ")));
            SAGA.SAGAPopuleazaDropDownListConturi(ddlCont2, ddlSocietati.SelectedItem.Text.Substring(0, ddlSocietati.SelectedItem.Text.IndexOf(" - ")));
            SAGA.SAGAPopuleazaDropDownListConturi(ddlCont3, ddlSocietati.SelectedItem.Text.Substring(0, ddlSocietati.SelectedItem.Text.IndexOf(" - ")));
            SAGA.SAGAPopuleazaDropDownListConturi(ddlCont4, ddlSocietati.SelectedItem.Text.Substring(0, ddlSocietati.SelectedItem.Text.IndexOf(" - ")));
            //ddlCont2.Items.AddRange(ddlCont1.Items.OfType<ListItem>().ToArray());
            //ddlCont3.Items.AddRange(ddlCont1.Items.OfType<ListItem>().ToArray());
            //ddlCont4.Items.AddRange(ddlCont1.Items.OfType<ListItem>().ToArray());
            SelecteazaInDropdownuri();

        }
        protected void SelecteazaInDropdownuri()
        {
            cashFlowConturi = new DataSourceCashFlow.CashflowConturi();
            cashFlowConturi = DataSourceCashFlow.CashFlowConturi(ddlSocietati.SelectedValue);
            lblCont1.Text = cashFlowConturi.ContCurentText;
            lblCont2.Text = cashFlowConturi.DepozitText;
            lblCont3.Text = cashFlowConturi.DepozitTaxeText;
            lblCont4.Text = cashFlowConturi.CasierieText;
            lblCont1.ToolTip = cashFlowConturi.ContCurentId;
            lblCont2.ToolTip = cashFlowConturi.DepozitId;
            lblCont3.ToolTip = cashFlowConturi.DepozitTaxeId;
            lblCont4.ToolTip = cashFlowConturi.CasierieId;
            lblConturiAsociate1.Text = "Conturi asociate:<br />";
            lblConturiAsociate1.Text += ConturiUtils.ListaConturiStringModificaInit(cashFlowConturi.ContCurent, "#", ddlCont1);
            lblConturiAsociate2.Text = "Conturi asociate:<br />";
            lblConturiAsociate2.Text += ConturiUtils.ListaConturiStringModificaInit(cashFlowConturi.Depozit, "#", ddlCont2);
            lblConturiAsociate3.Text = "Conturi asociate:<br />";
            lblConturiAsociate3.Text += ConturiUtils.ListaConturiStringModificaInit(cashFlowConturi.DepozitTaxe, "#", ddlCont3);
            lblConturiAsociate4.Text = "Conturi asociate:<br />";
            lblConturiAsociate4.Text += ConturiUtils.ListaConturiStringModificaInit(cashFlowConturi.Casierie, "#", ddlCont4);
            try
            {
                //ddlCont1.SelectedValue = cashFlowConturi.ContCurent;
                //ddlCont2.SelectedValue = cashFlowConturi.Depozit;
                //ddlCont3.SelectedValue = cashFlowConturi.DepozitTaxe;
                //ddlCont4.SelectedValue = cashFlowConturi.Casierie;
            }
            catch { }
        }

        protected void btnReseteazaConturi1_Click(object sender, EventArgs e)
        {
            ReseteazaConturi(lblConturiAsociate1, ddlCont1);
        }

        protected void btnReseteazaConturi2_Click(object sender, EventArgs e)
        {
            ReseteazaConturi(lblConturiAsociate2, ddlCont2);
        }

        protected void btnReseteazaConturi3_Click(object sender, EventArgs e)
        {
            ReseteazaConturi(lblConturiAsociate3, ddlCont3);
        }
        protected void btnReseteazaConturi4_Click(object sender, EventArgs e)
        {
            ReseteazaConturi(lblConturiAsociate4, ddlCont4);
        }

        protected void btnAdaugaContDinDdl1_Click(object sender, EventArgs e)
        {
            AdaugaContDinDdl(lblConturiAsociate1, ddlCont1);
        }

        protected void btnAdaugaContDinDdl2_Click(object sender, EventArgs e)
        {
            AdaugaContDinDdl(lblConturiAsociate2, ddlCont2);
        }

        protected void btnAdaugaContDinDdl3_Click(object sender, EventArgs e)
        {
            AdaugaContDinDdl(lblConturiAsociate3, ddlCont3);
        }

        protected void btnAdaugaContDinDdl4_Click(object sender, EventArgs e)
        {
            AdaugaContDinDdl(lblConturiAsociate4, ddlCont4);
        }
        public void AdaugaContDinDdl(Label label, DropDownList ddl)
        {
            label.Text += ddl.SelectedItem.Text + "<br />";
            int index = ddl.SelectedIndex;
            ddl.Items.Remove(ddl.SelectedItem);
            ddl.SelectedIndex = ddl.Items.Count <= index ? 0 : index;
        }
        public void ReseteazaConturi(Label label, DropDownList ddl)
        {
            label.Text = "Conturi asociate:<br />";
            ddl.Items.Clear();
            SAGA.SAGAPopuleazaDropDownListConturi(ddl, ddlSocietati.SelectedItem.Text.Substring(0, ddlSocietati.SelectedItem.Text.IndexOf(" - ")));
        }
    }
}