﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using cometex.Clase;
using cometex.Utils;
namespace cometex.Admin
{
    public partial class Categorii : System.Web.UI.Page
    {
        List<Button> ButoaneVizibile = new List<Button> { };
        List<Button> ButoaneAscunse = new List<Button> { };
        protected void Page_PreRender(object sender, EventArgs e)
        {
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Master.AtribuieButoaneMeniu(MeniuUtils.AranjeazaElementeMeniu(22, new List<int> { 4, 5 }));
                GridListaCategorii.DataBind();
                if (GridListaCategorii.SelectedIndex == -1)
                {
                    ButonAdauga.Visible = true;
                    ButonModifica.Visible = false;
                    ButonSterge.Visible = false;
                }
                else
                {
                    ButonAdauga.Visible = true;
                    ButonModifica.Visible = true;
                    ButonSterge.Visible = true;
                }
            }
        }
        protected void GridListaCategorii_SelectedIndexChanged(object sender, EventArgs e)
        {
            ButonAdauga.Visible = true;
            ButonModifica.Visible = true;
            ButonSterge.Visible = true;
        }

        protected void Adauga_Click(object sender, EventArgs e)
        {
            PanelAdauga.Visible = true;
            PanelListaCategorii.Visible = false;
            Categorie.Text = "";
            Session["TipActiune"] = "0";
        }

        protected void Modifica_Click(object sender, EventArgs e)
        {
            PanelAdauga.Visible = true;
            PanelListaCategorii.Visible = false;
            Categorie.Text = GridListaCategorii.SelectedRow.Cells[2].Text.ToString();
            Session["TipActiune"] = "1";
        }

        protected void Sterge_Click(object sender, EventArgs e)
        {
            CategoriiCheltuieli categorie = new CategoriiCheltuieli();
            categorie.Id = Convert.ToInt32(GridListaCategorii.SelectedValue);
            if (DatabaseUtils.VerificaExistentaCheltuieliPeCategorie(categorie))
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Nu puteti sterge", "alert('Categoria nu se poate sterge pentru ca exista cheltuieli deja aferente acesteia. Mai intai trebuie sa se schimbe categoria sau sa se stearga cheltuielile respective.');", true);
                return;
            }
           
        
            if (GridListaCategorii.SelectedIndex >= 0)
            {
                String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
        SqlConnection con = new SqlConnection(connectionString);
        con.Open();
                String cmdStr = "delete from Categorii where idCategorie = @idCategorie";
        SqlCommand cmd = new SqlCommand(cmdStr, con);
        cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("idCategorie", GridListaCategorii.SelectedDataKey[0]);
                cmd.ExecuteNonQuery();
                GridListaCategorii.DataBind();
                GridListaCategorii.SelectedIndex = -1;
                con.Close();
                ButonAdauga.Visible = true;
                ButonModifica.Visible = false;
                ButonSterge.Visible = false;
                GridListaCategorii.SelectedIndex = -1;
                GridListaCategorii.DataBind();
            }
}

protected void ButonSalveaza_Click(object sender, EventArgs e)
{
    String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
    SqlConnection con = new SqlConnection(connectionString);
    con.Open();
    String cmdStr = "";
    if (Session["TipActiune"].ToString() == "1")
    {
        cmdStr = "update Categorii set Categorie = @Categorie, tip = @tip where idCategorie = @idCategorie";
        SqlCommand cmd = new SqlCommand(cmdStr, con);
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("idCategorie", GridListaCategorii.SelectedDataKey[0]);
        cmd.Parameters.AddWithValue("Categorie", Categorie.Text);
        cmd.Parameters.AddWithValue("tip", ddlCategorieTip.SelectedValue);
        cmd.ExecuteNonQuery();
    }
    else if (Session["TipActiune"].ToString() == "0" && Categorie.Text.Length > 0)
    {
        cmdStr = "insert into Categorii (Categorie, tip, activa) values ( '" + Categorie.Text + "', '" + ddlCategorieTip.SelectedValue + "', 1)";
        SqlCommand cmd = new SqlCommand(cmdStr, con);
        cmd.Parameters.Clear();
        cmd.ExecuteNonQuery();
    }
    con.Close();
    GridListaCategorii.DataBind();
    PanelAdauga.Visible = false;
    PanelListaCategorii.Visible = true;
    ButonAdauga.Visible = true;
    ButonModifica.Visible = false;
    ButonSterge.Visible = false;
    GridListaCategorii.SelectedIndex = -1;
    GridListaCategorii.DataBind();
}

protected void ButonRenunta_Click(object sender, EventArgs e)
{
    PanelAdauga.Visible = false;
    PanelListaCategorii.Visible = true;
}


    }
}