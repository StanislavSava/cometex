﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Ad.master" AutoEventWireup="true" CodeBehind="Categorii.aspx.cs" Inherits="cometex.Admin.Categorii" %>

<%@ MasterType VirtualPath="~/Admin/Ad.master" %>
<%@ Reference VirtualPath="~/Cometex.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="PanelListaCategorii" runat="server">
        <asp:Panel runat="server" CssClass="block table-responsive text-center">
            <asp:GridView ID="GridListaCategorii" runat="server" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" DataSourceID="SqlDataSource1" CssClass="table table-hover table-condensed " ShowHeaderWhenEmpty="True" GridLines="None" OnSelectedIndexChanged="GridListaCategorii_SelectedIndexChanged" DataKeyNames="IdCategorie">
                <AlternatingRowStyle BackColor="White" />
                <SelectedRowStyle BackColor="#738A9C" />
                <Columns>
                    <asp:CommandField ShowSelectButton="True" />
                    <asp:BoundField DataField="IdCategorie" HeaderText="IdCategorie" InsertVisible="False" ReadOnly="True" SortExpression="IdCategorie" Visible="False" />
                    <asp:BoundField DataField="Categorie" HeaderText="Categorie" SortExpression="Categorie" />
                    <asp:BoundField DataField="tip" HeaderText="Tip categorie" SortExpression="tip" />
                </Columns>
            </asp:GridView>
        </asp:Panel>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT [IdCategorie], [Categorie], [tip] FROM [Categorii] ORDER BY tip, categorie"></asp:SqlDataSource>

        <div class="clearfix" id="Butoane" runat="server">
            <asp:Button ID="ButonAdauga" CssClass="btn btn-primary" runat="server" OnClick="Adauga_Click" Text="Adauga Categorie" />

            <asp:Button ID="ButonModifica" CssClass="btn btn-danger" runat="server" OnClick="Modifica_Click" Text="Modifica" />

            <asp:Button ID="ButonSterge" CssClass="btn btn-danger" runat="server" OnClick="Sterge_Click" OnClientClick="return confirm('Sigur stergeti categoria?');" Text="Sterge Categorie" />
        </div>
    </asp:Panel>
    <asp:Panel ID="PanelAdauga" runat="server" Visible="False">
        <asp:Label runat="server" Text="Adauga o categorie" />
        <div>
            <asp:Label runat="server" Text="Denumire:" />
            <asp:TextBox ID="Categorie" runat="server" Width="20em"></asp:TextBox>
        </div>
        <div>
            <asp:Label runat="server" Text="Tip de categorie:" />
            <asp:DropDownList ID="ddlCategorieTip" runat="server">
                <asp:ListItem>grila</asp:ListItem>
                <asp:ListItem>exceptionala</asp:ListItem>
                <asp:ListItem>plata</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="clearfix" id="Div1" runat="server">
            <asp:Button ID="ButonSalveaza" runat="server" CssClass="btn btn-success" OnClick="ButonSalveaza_Click" Text="Salveaza" />
            <asp:Button ID="ButonRenunta" runat="server" CssClass="btn btn-danger" OnClick="ButonRenunta_Click" Text="Renunta" />
        </div>

    </asp:Panel>
</asp:Content>
