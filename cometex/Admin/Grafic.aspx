﻿<%@ Page Title="Grafice" Language="C#" MasterPageFile="~/Admin/Ad.master" AutoEventWireup="true" CodeBehind="Grafic.aspx.cs" Inherits="cometex.Admin.Grafic" %>

<%@ MasterType VirtualPath="~/Admin/Ad.master" %>
<%@ Reference VirtualPath="~/Cometex.Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager2" runat="server"></asp:ScriptManager>
    <%--    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" />
    <!-- Theme style -->--%>
    <link rel="stylesheet" href="../grafice/dist/css/AdminLTE.min.css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <div class="wrapper">

        <!-- Left side column. contains the logo and sidebar -->


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h2><%=Title %></h2>
                <asp:Label runat="server" ID="conectare" Text="---" Visible="false"></asp:Label>
                <div class="col-md-12 zero jos">
                    <div class="col-md-3">
                        <asp:Label ID="Label1" runat="server" AssociatedControlID="ddlSocietati" Text="Societatea:"></asp:Label>
                        <asp:DropDownList ID="ddlSocietati" AutoPostBack="true" runat="server" DataSourceID="SqlDSSocietati" DataTextField="Societate" DataValueField="IdSocietate" CssClass="selectpicker" data-selected-text-format="count" OnSelectedIndexChanged="ddlSocietati_SelectedIndexChanged"></asp:DropDownList>
                        <asp:SqlDataSource ID="SqlDSSocietati" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT IdSocietate, (SAGACod + ' - ' + Societate) as Societate  FROM Societati"></asp:SqlDataSource>
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="Label2" runat="server" AssociatedControlID="ddlGrafice" Text="Grafic:   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"></asp:Label>
                        <asp:DropDownList ID="ddlGrafice" AutoPostBack="true" runat="server" DataSourceID="SqlDGrafice" DataTextField="denumire" DataValueField="grafic" CssClass="selectpicker" data-selected-text-format="count"></asp:DropDownList>
                        <asp:SqlDataSource ID="SqlDGrafice" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT Grafice.denumire, Conturi.cont, Conturi.grafic, Conturi.idSocietate, Conturi.modCalcul, Grafice.descriere, Conturi.id FROM Grafice INNER JOIN Conturi ON Grafice.id = Conturi.grafic WHERE (Conturi.idSocietate = @idsocietate)">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="ddlSocietati" Name="idsocietate" PropertyName="SelectedValue" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </div>
                    <asp:Panel ID="pnlDeLa" runat="server" CssClass="col-md-3">
                        <asp:Label ID="lblDeLa" runat="server" AssociatedControlID="tbDeLa" Text="De la data:"></asp:Label>
                        <asp:TextBox ID="tbDeLa" runat="server" CssClass="form-control" AutoPostBack="true"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="CalendarExtenderScadentaDeLa" runat="server" DefaultView="Days" TargetControlID="tbDeLa" ValidateRequestMode="Enabled" ClearTime="True" FirstDayOfWeek="Monday" Format="dd.MM.yyyy" TodaysDateFormat="dd.MM.yyyy" />
                    </asp:Panel>
                    <asp:Panel ID="Panel1" runat="server" CssClass="col-md-3">
                        <asp:Label ID="lblPanaLa" runat="server" Text="Pana la data:" AssociatedControlID="tbPanaLa"></asp:Label>
                        <asp:TextBox ID="tbPanaLa" runat="server" CssClass="form-control" AutoPostBack="true"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="CalendarExtenderScadentaPanaLa" runat="server" DefaultView="Days" TargetControlID="tbPanaLa" ValidateRequestMode="Enabled" ClearTime="True" FirstDayOfWeek="Monday" Format="dd.MM.yyyy" TodaysDateFormat="dd.MM.yyyy" />
                    </asp:Panel>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i></a></li>
                    <li><a href="#"></a></li>
                    <li class="active"></li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <!-- AREA CHART -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">
                                    <asp:Literal Text="" runat="server" ID="titlulGraficului"></asp:Literal></h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="chart">
                                    <canvas id="areaChart" style="height: 250px"></canvas>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->


                    </div>
                    <!-- /.col (LEFT) -->

                </div>
                <!-- /.row -->

            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->


        <!-- Control Sidebar -->
        <!-- /.control-sidebar -->
        <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->


    <!-- ChartJS 1.0.1 -->
    <script src="../grafice/plugins/chartjs/Chart.js"></script>
    <%--    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>--%>
    <!-- page script -->
    <script>
        $(function () {

            function creareChart(chartData, chartType, chartOptions, canvas){

                function generareData(chartData){
                    var labels = chartData.labels;
                    var datasets = function(){
                        var elemente = [];  
                        for (i in chartData.datasets){
                            try {
                                chartData.datasets[i].data = JSON.parse(chartData.datasets[i].data);
                            } catch (e){
                                "";
                            }
                            chartData.datasets[i].pointBackgroundColor = "#fff";
                            chartData.datasets[i].pointBorderColor = chartData.datasets[i].borderColor;
                            chartData.datasets[i].backgroundColor = chartData.datasets[i].borderColor;
                            chartData.datasets[i].pointHoverBackgroundColor = chartData.datasets[i].pointBackgroundColor;
                            chartData.datasets[i].pointHoverBorderColor = chartData.datasets[i].pointBorderColor;
                            chartData.datasets[i].pointRadius = 3;
                            chartData.datasets[i].pointBorderWidth = 2;
                            chartData.datasets[i].pointHoverRadius = Number(chartData.datasets[i].pointRadius) + 1;
                            chartData.datasets[i].pointHoverBorderWidth = Number(chartData.datasets[i].pointBorderWidth) * 2;
                            elemente[i] = chartData.datasets[i];
                        };
                        return elemente;
                    };
                    var data = {};
                    try {
                        data.labels = JSON.parse(labels);
                    } catch (e){
                        data.labels = labels;
                    }

                    data.datasets = datasets();
                    return data;
                };

                var data = generareData(chartData);

                function generareOptiuni(chartOptions){
                    try {
                        return chartOptions = JSON.parse(chartOptions);
                    } catch (e){
                        return chartOptions;
                    };
                };
                var options = generareOptiuni(chartOptions) || defaultOptions;

                var chart = new Chart(canvas || areaChartCanvas, {
                    data: generareData(chartData),
                    type: chartType || "line",
                    options: options
                });
                return chart;
            };
            
            
            var areaChartCanvas = $("#areaChart").get(0).getContext("2d");     
         //   var areaChaData = creareChart(areaCharData);
           <%-- var areaChartData = {
                //labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
                labels: <%=TexteJavascript()[0] %>,
               
                datasets:
            [{
                label: "Cometex",
                fillColor: "<%=culoriFundal[0]%>",
                strokeColor: "<%=culoriMargine[0]%>",
                pointColor: "<%=culoriMargine[0]%>",
                pointStrokeColor: "<%=culoriMargine[5]%>",
                pointHighlightFill: "#000",
                pointHighlightStroke: "rgba(220,220,220,1)",
                //  data: [65, 59, 80, 81, 56, 55, 40]
                data: [<%=texte%>]
            },--%>
               <%--  {
                     label: "Astral",
                     fillColor: "<%=culoriFundal[1]%>",
                     strokeColor: "<%=culoriMargine[1]%>",
                     pointColor: "<%=culoriMargine[1]%>",
                     pointStrokeColor: "<%=culoriMargine[5]%>",
                     pointHighlightFill: "#fff",
                     pointHighlightStroke: "rgba(60,141,188,1)",
                     data: [28, 48, 40, 19, 86, 27, 90, 1, 1, 1, 1, 0]
                 },--%>
                <%-- {
                      label: "Astral",
                      fillColor: "<%=culoriFundal[2]%>",
                strokeColor: "<%=culoriMargine[2]%>",
                pointColor: "<%=culoriMargine[2]%>",
                pointStrokeColor: "<%=culoriMargine[5]%>",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(60,141,188,1)",
                data: <%=TexteJavascript()[1]%>,
            }
        ]
            };--%>

                  var defaultOptions =  {

                      ///boolean - whether grid lines are shown across the chart
                      scaleshowgridlines: true,

                      //string - colour of the grid lines
                      scalegridlinecolor: "rgba(0,0,0,.05)",

                      //number - width of the grid lines
                      scalegridlinewidth: 1,

                      //boolean - whether to show horizontal lines (except x axis)
                      scaleshowhorizontallines: true,

                      //boolean - whether to show vertical lines (except y axis)
                      scaleshowverticallines: true,

                      //boolean - whether the line is curved between points
                      beziercurve: true,

                      //number - tension of the bezier curve between points
                      beziercurvetension:0.5,

                      //boolean - whether to show a dot for each point
                      pointdot: true,

                      //number - radius of each point dot in pixels
                      pointdotradius: 2,

                      //number - pixel width of point dot stroke
                      pointdotstrokewidth: 1,

                      //number - amount extra to add to the radius to cater for hit detection outside the drawn point
                      pointhitdetectionradius: 0,

                      //boolean - whether to show a stroke for datasets
                      datasetstroke: true,

                      //number - pixel width of dataset stroke
                      datasetstrokewidth: 1,

                      //boolean - whether to fill the dataset with a colour
                      datasetfill: true,

                      //string - a legend template
                      legendtemplate: "<ul><li><span>grafice</span></li></ul>"

                  };
                  
            var chart = creareChart(<%=areaChartDataTextJS%>);


                  });
    </script>

</asp:Content>
