﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using cometex.Clase;
using cometex.Utils;
namespace cometex.Admin
{
    public partial class ListaCereriGrila : System.Web.UI.Page
    {
        protected void Page_PreRenderComplete(object sender, EventArgs e)
        {
            Footable.AranjeazaGrid(GridListaCereri);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Master.AtribuieButoaneMeniu(MeniuUtils.AranjeazaElementeMeniu(11, new List<int> { 1 }));
                DropDownList3.DataBind();
                DropDownList3.SelectedIndex = 2;
                GridListaCereri.DataBind();
            }
            AranjeazaGrid(GridListaCereri);
            if (GridListaCereri.SelectedIndex == -1)
            { Butoane.Visible = false; }
            else Butoane.Visible = true;

        }
        public static void AranjeazaGrid(GridView gv)
        {
        }
        public bool VerificaSelect(GridView Gridview)
        {
            if (Gridview.Rows.Count < 1)
                return false;
            else return true;

        }
        protected void ButonAproba_Click(object sender, EventArgs e)
        {
            if (GridListaCereri.SelectedIndex >= 0)
            {
                String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();
                String cmdStr = "update investitii set status = @status, RezolutionatDe = @RezolutionatDe, DataRezolutie = @dataRezolutie where idInvestitie = @idInvestitie";
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("idInvestitie", GridListaCereri.SelectedDataKey[0]);
                cmd.Parameters.AddWithValue("status", 1);
                cmd.Parameters.AddWithValue("RezolutionatDe", Session["idUtilizator"]);
                cmd.Parameters.AddWithValue("DataRezolutie", DateTime.Now);
                cmd.ExecuteNonQuery();
                con.Close();
                GridListaCereri.DataBind();
                if (!VerificaSelect(GridListaCereri)) { Butoane.Visible = false; GridListaCereri.SelectedIndex = -1; }
            }
            //Footable.AranjeazaGrid(GridListaCereri);
        }

        protected void ButonRespinge_Click(object sender, EventArgs e)
        {
            if (GridListaCereri.SelectedIndex >= 0)
            {
                String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();
                String cmdStr = "update investitii set status = @status, RezolutionatDe = @RezolutionatDe, DataRezolutie = @dataRezolutie where idInvestitie = @idInvestitie";
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("idInvestitie", GridListaCereri.SelectedDataKey[0]);
                cmd.Parameters.AddWithValue("status", 2);
                cmd.Parameters.AddWithValue("RezolutionatDe", Session["idUtilizator"]);
                cmd.Parameters.AddWithValue("DataRezolutie", DateTime.Now);
                cmd.ExecuteNonQuery();
                con.Close();
                GridListaCereri.DataBind();
                if (!VerificaSelect(GridListaCereri)) { Butoane.Visible = false; GridListaCereri.SelectedIndex = -1; }
            }
          //  Footable.AranjeazaGrid(GridListaCereri);
        }

        protected void ButonNerezolvat_Click(object sender, EventArgs e)
        {
            if (GridListaCereri.SelectedIndex >= 0)
            {
                String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();
                String cmdStr = "update investitii set status = @status, RezolutionatDe = @RezolutionatDe, DataRezolutie = @dataRezolutie where idInvestitie = @idInvestitie";
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("idInvestitie", GridListaCereri.SelectedDataKey[0]);
                cmd.Parameters.AddWithValue("status", 0);
                cmd.Parameters.AddWithValue("RezolutionatDe", Session["idUtilizator"]);
                cmd.Parameters.AddWithValue("DataRezolutie", DateTime.Now);
                cmd.ExecuteNonQuery();
                con.Close();
                GridListaCereri.DataBind();
                if (!VerificaSelect(GridListaCereri)) { Butoane.Visible = false; GridListaCereri.SelectedIndex = -1; }
            }
        }

        protected void GridListaCereri_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (GridListaCereri.SelectedIndex == -1)
            { Butoane.Visible = false; }
            else Butoane.Visible = true;
        }
    }
}