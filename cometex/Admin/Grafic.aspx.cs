﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using cometex.Utils;
using cometex.Clase;
using System.Data;
using System.Configuration;
using Newtonsoft.Json;

namespace cometex.Admin
{
    public partial class Grafic : System.Web.UI.Page
    {
        protected List<int> variabile = new List<int>();
        protected string texte = "1,2,3,4";
        protected List<string> culoriFundal = Utils.JavascriptUtils.CuloriRGBACuRGBATextSiTransparenta(Utils.JavascriptUtils.CuloriRGBADoarValori(), "0.1");
        protected List<string> culoriMargine = Utils.JavascriptUtils.CuloriRGBACuRGBATextSiTransparenta(Utils.JavascriptUtils.CuloriRGBADoarValori(), "1");
        protected List<string> texteDeAfisat = new List<string>();
        public areaChartData areaChartDataText = new areaChartData();
        public string areaChartDataTextJS
        {
            get
            {
                return JsonConvert.SerializeObject(TrimiteChartData());
            }
        }

        

        public areaChartData TrimiteChartData()
        {
            if (texteDeAfisat.Count > 0) return areaChartDataText;
            Clase.Societati societate = new Clase.Societati();
            societate.Id = Convert.ToInt32(ddlSocietati.SelectedValue);
            societate.Denumire = ddlSocietati.SelectedItem.Text.Substring(ddlSocietati.SelectedItem.Text.IndexOf(" - ") + 3).Trim();
            societate.SAGAfolder = ddlSocietati.SelectedItem.Text.Substring(0, ddlSocietati.SelectedItem.Text.IndexOf(" - ")).Trim();
            string log = "";
            List<List<string>> tabele = SAGA.SagaValoriCont(tbDeLa.Text, tbPanaLa.Text, ddlGrafice.SelectedValue, ConfigurationManager.AppSettings["caleSAGA"], societate, out log);


            DatabaseUtils.ScrieLog(log, "grafic - " + Request.Url.GetLeftPart(UriPartial.Authority) + "/" + Request.Url.LocalPath, DateTime.Now.ToString());
            conectare.Text = log;
            texteDeAfisat = ListaDate(tabele);
            areaChartDataText.labels = texteDeAfisat[0];

            datasets datasets = new datasets();

            datasets.label = "Cometex";
            datasets.data = "[65, 59, 80, 81, 56, 55, 40, 2, 3, 4, 1,5, 6,7,23,46,5,7,4]";
            datasets.borderColor = culoriFundal[0];
            datasets.fill = false;

            areaChartDataText.datasets.Add(datasets);

            datasets = new datasets();

            datasets.label = "Astral";
            datasets.data = "[28, 48, 40, 19, 86, 27, 90, 1, 1, 1, 1, 0,32,5,23,4,6,7,54,7,5,4,3]";
            datasets.borderColor = culoriFundal[1];
            datasets.fill = false;

            areaChartDataText.datasets.Add(datasets);

            datasets = new datasets();
            datasets.label = "Astral";
            datasets.data = "[12,3,45,6,5,6,78,34,65,65,7,23,34,65,31,6,2,1,6,6,4,5,67,58,6,8,5,6,7]";
            datasets.borderColor = "#0066CC";
            datasets.fill = false;

            areaChartDataText.datasets.Add(datasets);

            return areaChartDataText;

        }
        protected List<string> TexteJavascript()
        {
            if (texteDeAfisat.Count > 0) return texteDeAfisat;
            Clase.Societati societate = new Clase.Societati();
            societate.Id = Convert.ToInt32(ddlSocietati.SelectedValue);
            societate.Denumire = ddlSocietati.SelectedItem.Text.Substring(ddlSocietati.SelectedItem.Text.IndexOf(" - ") + 3).Trim();
            societate.SAGAfolder = ddlSocietati.SelectedItem.Text.Substring(0, ddlSocietati.SelectedItem.Text.IndexOf(" - ")).Trim();
            string log = "";
            List<List<string>> tabele = SAGA.SagaValoriCont(tbDeLa.Text, tbPanaLa.Text, ddlGrafice.SelectedValue, ConfigurationManager.AppSettings["caleSAGA"], societate, out log);
            DatabaseUtils.ScrieLog(log, "grafic - " + Request.Url.GetLeftPart(UriPartial.Authority) + "/" + Request.Url.LocalPath, DateTime.Now.ToString());
            conectare.Text = log;
            return texteDeAfisat = ListaDate(tabele);
        }


        protected static List<string> ListaDate(List<List<string>> tabele)
        {
            // cifrele coordonata X
            List<string> listaString = new List<string>();
            List<string> listaValori = new List<string>();
            foreach (var tabel in tabele)
            {
                listaString.Add(tabel[1]);
                listaValori.Add(tabel[0].Replace(",", "."));
            }
            List<string> listaDate = new List<string>();
            listaDate.Add(JavascriptUtils.GraficValoriString(listaString, true));
            listaDate.Add(JavascriptUtils.GraficValoriString(listaValori, false));
            return listaDate;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Master.AtribuieButoaneMeniu(MeniuUtils.AranjeazaElementeMeniu(22, new List<int> { 3, 13 }));
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Data.PrimaZiAAnului(tbDeLa);
                Data.DataCurenta(tbPanaLa);
                ddlSocietati.DataBind();
                ddlGrafice.DataBind();
            }
            titlulGraficului.Text = string.Empty;
            try { titlulGraficului.Text = ddlSocietati.SelectedItem.Text; } catch { }
            try { titlulGraficului.Text += ": " + ddlGrafice.SelectedItem.Text; } catch { }
        }

        protected void ddlSocietati_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlGrafice.DataBind();
            titlulGraficului.Text = string.Empty;
            try { titlulGraficului.Text = ddlSocietati.SelectedItem.Text; } catch { }
            try { titlulGraficului.Text += ": " + ddlGrafice.SelectedItem.Text; } catch { }
        }
    }
}