﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using cometex.Clase;
using cometex.Utils;

namespace cometex.Admin
{
    public partial class Exceptionale : System.Web.UI.Page
    {
        static Clase.Societati societatea = new Clase.Societati();
        static List<Cheltuieli> plafoane = new List<Cheltuieli>();
        static List<Cheltuieli> cheltuieliLunaInCurs = new List<Cheltuieli>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["idUtilizator"] == null) Response.Redirect("~/login.aspx");
            if (!IsPostBack)
            {
                Master.AtribuieButoaneMeniu(MeniuUtils.AranjeazaElementeMeniu(22, new List<int> { 1 }));
                TextBoxScadentaIncepandCu.Text = DateTime.Now.AddDays(-DateTime.Now.Day+1).AddMonths(-1).ToString("dd.MM.yyyy");
                tbPanaLa.Text = DateTime.Now.AddMonths(1).ToString("dd.MM.yyyy");
                ddlFiltrareStatus.DataBind();
                ddlFiltrareStatus.SelectedIndex = 1;
                GridListaCereri.SelectedIndex = -1;
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (GridListaCereri.Rows.Count > 0)
            {
                ButonRespinge.Visible = ButonAproba.Visible = btVerifica.Visible = ButonRespingeSus.Visible = ButonAprobaSus.Visible = (GridListaCereri.SelectedIndex != -1 && GridListaCereri.Rows.Count > 0);
            }
            else
            {
                ButonRespinge.Visible = ButonAproba.Visible = btVerifica.Visible = ButonRespingeSus.Visible = ButonAprobaSus.Visible = false;
            }
            pnModalButoaneMesaj.Visible = pnMesaj.Visible = pnModalButoanePrincipale.Visible ? false : true;
        }
        public bool VerificaSelect(GridView Gridview)
        {
            if (Gridview.Rows.Count < 1)
                return false;
            else return true;
        }
        protected void ButonAproba_Click(object sender, EventArgs e)
        {
            if (GridListaCereri.SelectedIndex >= 0)
            {
                String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();
                String cmdStr = "update investitii set status = @status, RezolutionatDe = @RezolutionatDe, DataRezolutie = @dataRezolutie where idInvestitie = @idInvestitie";
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("idInvestitie", GridListaCereri.SelectedDataKey[0]);
                cmd.Parameters.AddWithValue("status", 5);
                cmd.Parameters.AddWithValue("RezolutionatDe", Session["idUtilizator"]);
                cmd.Parameters.AddWithValue("DataRezolutie", DateTime.Now);
                cmd.ExecuteNonQuery();
                con.Close();
                GridListaCereri.DataBind();
                if (VerificaSelect(GridListaCereri))
                {
                    GridListaCereri.SelectedIndex = -1;
                }
            }
        }

        protected void ButonRespinge_Click(object sender, EventArgs e)
        {
            if (GridListaCereri.SelectedIndex >= 0)
            {
                String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();
                String cmdStr = "update investitii set status = @status, RezolutionatDe = @RezolutionatDe, DataRezolutie = @dataRezolutie where idInvestitie = @idInvestitie";
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("idInvestitie", GridListaCereri.SelectedDataKey[0]);
                cmd.Parameters.AddWithValue("status", 6);
                cmd.Parameters.AddWithValue("RezolutionatDe", Session["idUtilizator"]);
                cmd.Parameters.AddWithValue("DataRezolutie", DateTime.Now);
                cmd.ExecuteNonQuery();
                con.Close();
                GridListaCereri.DataBind();
                if (VerificaSelect(GridListaCereri))
                {
                    GridListaCereri.SelectedIndex = -1;
                }
            }
        }

        protected void ButonNerezolvat_Click(object sender, EventArgs e)
        {
            pnModalButoanePrincipale.Visible = false;
            tbSubiect.Text = "Completare necesara pt. cheltuiala cod: " + ((Label)GridListaCereri.SelectedRow.FindControl("lblIdCheltuiala")).Text;
            tbMesaj.Text = "";
            tbMesaj.Focus();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
        }
        protected void btTrimiteMesaj_Click(object sender, EventArgs e)
        {
            {
                MesajeCls mesaj = new MesajeCls();
                mesaj.AdresaExpeditor = new List<string>() { Session["email"].ToString(), Session["numeUtilizator"].ToString() };
                mesaj.AdresaDestinatar = new List<string>() { ((Label)GridListaCereri.SelectedRow.FindControl("lblEmailUtilizator")).Text, ((Label)GridListaCereri.SelectedRow.FindControl("lblNumeUtilizator")).Text };
                mesaj.Subiect = tbSubiect.Text;
                mesaj.Continut = tbMesaj.Text +
                    @"
                <hr />                
                Detalii cheltuiala exceptionala: 
                <br />
                Cod SAGA: " + ((Label)GridListaCereri.SelectedRow.FindControl("lblIdCheltuiala")).Text + @"<br />
                Suma: " + ((Label)GridListaCereri.SelectedRow.FindControl("LabelSuma")).Text + @"<br />
                Destinatia: " + ((Label)GridListaCereri.SelectedRow.FindControl("lblDestinatia")).Text + @"<br />   
                Explicatii: " + ((Label)GridListaCereri.SelectedRow.FindControl("lblExplicatii")).Text + @"<br />
                Scadenta: " + ((Label)GridListaCereri.SelectedRow.FindControl("lblScadenta")).Text + @"<br />
                <hr />
                Acest email a fost trimis din cadrul programului cheltuieli.programdecontabilitate.com. Va rugam nu raspundeti / reply pe aceasta adresa de email, ci urmati doar instructiunile de mai sus. 
                ";
                EmailOperare.TrimiteEmail(mesaj);
                Cheltuieli cheltuiala = GridViewUtils.DateChetuialaDinRandGrid(GridListaCereri.SelectedRow);
                cheltuiala.Status.Id = 15;
                DatabaseUtils.UpdateCheltuiala(cheltuiala);
                GridListaCereri.DataBind();
                GridListaCereri.SelectedIndex = -1;
            }
            pnModalButoanePrincipale.Visible = true;
        }
        protected void btRenunta_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
            pnModalButoanePrincipale.Visible = true;
        }
        protected void GridListaCereri_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblDetaliiRandInModal.Text = ((Label)GridListaCereri.SelectedRow.FindControl("lblDenumireSocietate")).Text
    + ": " + ((Label)GridListaCereri.SelectedRow.FindControl("LabelSuma")).Text.Replace("<br />", string.Empty) + " | "
    + ((Label)GridListaCereri.SelectedRow.FindControl("lblDestinatia")).Text + " - "
    + ((Label)GridListaCereri.SelectedRow.FindControl("lblExplicatii")).Text;
            pnModalButoanePrincipale.Visible = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
            Grid.CurataClaseGridSelectedRow(GridListaCereri);
        }

        protected void GridListaCereri_Sorting(object sender, GridViewSortEventArgs e)
        {
            GridListaCereri.SelectedIndex = -1;
        }

        protected void GridListaCereri_PreRender(object sender, EventArgs e)
        {
            LabelSumaTotala.Text =
                "Totaluri cheltuieli: introduse: " +
                 DatabaseUtils.GridFooterCheltuieliSelectateInFunctieDeStatus(ddlFiltrareSocietati.SelectedValue.ToString(), ddlFiltrareCategorie.SelectedValue.ToString(), "%", "%", "exceptionala", Convert.ToDateTime(TextBoxScadentaIncepandCu.Text)) +
                "lei</span> | <span class='danger'>spre aprobare: " +
                 DatabaseUtils.GridFooterCheltuieliSelectateInFunctieDeStatus(ddlFiltrareSocietati.SelectedValue.ToString(), ddlFiltrareCategorie.SelectedValue.ToString(), "%", "0", "exceptionala", Convert.ToDateTime(TextBoxScadentaIncepandCu.Text)) +
                 "lei</span> | <span class='danger'>respinse: " +
                 DatabaseUtils.GridFooterCheltuieliSelectateInFunctieDeStatus(ddlFiltrareSocietati.SelectedValue.ToString(), ddlFiltrareCategorie.SelectedValue.ToString(), "%", "2", "exceptionala", Convert.ToDateTime(TextBoxScadentaIncepandCu.Text)) +
                 "lei</span> | <span class='danger'>aprobate: " +
                 DatabaseUtils.GridFooterCheltuieliSelectateInFunctieDeStatus(ddlFiltrareSocietati.SelectedValue.ToString(), ddlFiltrareCategorie.SelectedValue.ToString(), "%", "1", "exceptionala", Convert.ToDateTime(TextBoxScadentaIncepandCu.Text)) +
                 "lei</span>"
                ;
        }
        protected void GridListaCereri_Load(object sender, EventArgs e) { }

        protected void GridListaCereri_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Clase.Grid.SelectGrid(GridListaCereri, e, this);
        }

        protected void btVerifica_Click(object sender, EventArgs e)
        {
            Cheltuieli cheltuiala = new Cheltuieli();
            cheltuiala.Societatea = DatabaseUtils.DetaliiSocietate(Convert.ToInt32(((Label)GridListaCereri.SelectedRow.FindControl("lblIdSocietate")).Text));
            cheltuiala.IdCheltuieli = Convert.ToInt32(GridListaCereri.SelectedDataKey[0]);
            cheltuiala.CheltuialaBrut = Convert.ToDecimal(((Label)GridListaCereri.SelectedRow.FindControl("lblSuma")).Text);
            SAGA.UpdateCheltuialaCuStatusSAGA(cheltuiala);
            GridListaCereri.DataBind();
        }

        protected void btVerificaToate_Click(object sender, EventArgs e)
        {
            societatea.Id = Convert.ToInt32(Session["idSocietate"]);
            SAGA.UpdateCheltuialaCuStatusSAGA(DatabaseUtils.ListaCheltuieliNeVerificateInSAGA(societatea));
            GridListaCereri.DataBind();
        }

        protected void ddlFiltrareCategorie_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridListaCereri.SelectedIndex = -1;
        }

        protected void ddlFiltrareStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridListaCereri.SelectedIndex = -1;
        }

        protected void ddlFiltrareSocietati_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFiltrareCategorie.Items.Count > 1)
            {
                ddlFiltrareCategorie.Items.Clear();
                ddlFiltrareCategorie.Items.Add(new ListItem("Toate", "%"));
                ddlFiltrareCategorie.DataBind();
            }
            GridListaCereri.SelectedIndex = -1;
        }
    }
}