﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cometex.Admin
{
    public partial class Ad : System.Web.UI.MasterPage
    {
        public void FindGridView()
        {
            foreach (Control ctrl in Page.Controls)
            {

                GridView gv = ctrl as GridView;

                if (gv == null)
                {
                    //ParseCollection(ctrl);
                }
                else
                {
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                    gv.ShowFooter = true;
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["idUtilizator"] == null) Response.Redirect("~/login.aspx");
            FindGridView();
        }

        protected void ListaCereri_Click(object sender, EventArgs e)
        {
            Response.Redirect("ListaCereri.aspx");
        }

        protected void Societati_Click(object sender, EventArgs e)
        {
            Response.Redirect("Societati.aspx");
        }

        protected void Utilizatori_Click(object sender, EventArgs e)
        {

            Response.Redirect("Utilizatori.aspx");
        }

        protected void Categorii_Click(object sender, EventArgs e)
        {
            // Response.Redirect("Categorii.aspx");
        }
        public void AtribuieButoaneMeniu(List<Clase.Meniu> elementeMeniu)
        {
            liBtnExceptionale.Attributes.Clear();
            liBtnExceptionale.Attributes.Add(elementeMeniu[0].Atribute[0], elementeMeniu[0].Atribute[1]);
            liBtnExceptionale.Attributes.Add(elementeMeniu[0].Atribute[2], elementeMeniu[0].Atribute[3]);

            liBtnGrila.Attributes.Clear();
            liBtnGrila.Attributes.Add(elementeMeniu[1].Atribute[0], elementeMeniu[1].Atribute[1]);
            liBtnGrila.Attributes.Add(elementeMeniu[1].Atribute[2], elementeMeniu[1].Atribute[3]);

            liBtnRapoarte.Attributes.Clear();
            liBtnRapoarte.Attributes.Add(elementeMeniu[2].Atribute[0], elementeMeniu[2].Atribute[1]);
            liBtnRapoarte.Attributes.Add(elementeMeniu[2].Atribute[2], elementeMeniu[2].Atribute[3]);

            liBtnSetari.Attributes.Clear();
            liBtnSetari.Attributes.Add(elementeMeniu[3].Atribute[0], elementeMeniu[3].Atribute[1]);
            liBtnSetari.Attributes.Add(elementeMeniu[3].Atribute[2], elementeMeniu[3].Atribute[3]);

            liBtnCategorii.Attributes.Clear();
            liBtnCategorii.Attributes.Add(elementeMeniu[4].Atribute[0], elementeMeniu[4].Atribute[1]);
            liBtnCategorii.Attributes.Add(elementeMeniu[4].Atribute[2], elementeMeniu[4].Atribute[3]);

            liBtnUtilizatori.Attributes.Clear();
            liBtnUtilizatori.Attributes.Add(elementeMeniu[5].Atribute[0], elementeMeniu[5].Atribute[1]);
            liBtnUtilizatori.Attributes.Add(elementeMeniu[5].Atribute[2], elementeMeniu[5].Atribute[3]);

            liBtnSocietati.Attributes.Clear();
            liBtnSocietati.Attributes.Add(elementeMeniu[6].Atribute[0], elementeMeniu[6].Atribute[1]);
            liBtnSocietati.Attributes.Add(elementeMeniu[6].Atribute[2], elementeMeniu[6].Atribute[3]);

            liBtnPlafoaneGrila.Attributes.Clear();
            liBtnPlafoaneGrila.Attributes.Add(elementeMeniu[7].Atribute[0], elementeMeniu[7].Atribute[1]);
            liBtnPlafoaneGrila.Attributes.Add(elementeMeniu[7].Atribute[2], elementeMeniu[7].Atribute[3]);

            //liRaportGeneral.Attributes.Clear();
            //liRaportGeneral.Attributes.Add(elementeMeniu[8].Atribute[0], elementeMeniu[8].Atribute[1]);
            //liRaportGeneral.Attributes.Add(elementeMeniu[8].Atribute[2], elementeMeniu[8].Atribute[3]);

            //liRaportNominal.Attributes.Clear();
            //liRaportNominal.Attributes.Add(elementeMeniu[9].Atribute[0], elementeMeniu[9].Atribute[1]);
            //liRaportNominal.Attributes.Add(elementeMeniu[9].Atribute[2], elementeMeniu[9].Atribute[3]);

            liBtnAsociereConturi.Attributes.Clear();
            liBtnAsociereConturi.Attributes.Add(elementeMeniu[10].Atribute[0], elementeMeniu[10].Atribute[1]);
            liBtnAsociereConturi.Attributes.Add(elementeMeniu[10].Atribute[2], elementeMeniu[10].Atribute[3]);

            liBtnGraficeConfigurari.Attributes.Clear();
            liBtnGraficeConfigurari.Attributes.Add(elementeMeniu[11].Atribute[0], elementeMeniu[11].Atribute[1]);
            liBtnGraficeConfigurari.Attributes.Add(elementeMeniu[11].Atribute[2], elementeMeniu[11].Atribute[3]);

            liGrafic.Attributes.Clear();
            liGrafic.Attributes.Add(elementeMeniu[12].Atribute[0], elementeMeniu[12].Atribute[1]);
            liGrafic.Attributes.Add(elementeMeniu[12].Atribute[2], elementeMeniu[12].Atribute[3]);

            liRaportCashFlow.Attributes.Clear();
            liRaportCashFlow.Attributes.Add(elementeMeniu[13].Atribute[0], elementeMeniu[13].Atribute[1]);
            liRaportCashFlow.Attributes.Add(elementeMeniu[13].Atribute[2], elementeMeniu[13].Atribute[3]);

            liBtnCashFlowConfigurari.Attributes.Clear();
            liBtnCashFlowConfigurari.Attributes.Add(elementeMeniu[14].Atribute[0], elementeMeniu[14].Atribute[1]);
            liBtnCashFlowConfigurari.Attributes.Add(elementeMeniu[14].Atribute[2], elementeMeniu[14].Atribute[3]);

            LinkButtonRaportNominalGrila.Attributes.Clear();
            LinkButtonRaportNominalGrila.Attributes.Add(elementeMeniu[15].Atribute[0], elementeMeniu[15].Atribute[1]);
            LinkButtonRaportNominalGrila.Attributes.Add(elementeMeniu[15].Atribute[2], elementeMeniu[15].Atribute[3]);

            LinkButtonRaportNominalExceptionala.Attributes.Clear();
            LinkButtonRaportNominalExceptionala.Attributes.Add(elementeMeniu[16].Atribute[0], elementeMeniu[16].Atribute[1]);
            LinkButtonRaportNominalExceptionala.Attributes.Add(elementeMeniu[16].Atribute[2], elementeMeniu[16].Atribute[3]);

            LinkButtonRapGenGrila.Attributes.Clear();
            LinkButtonRapGenGrila.Attributes.Add(elementeMeniu[17].Atribute[0], elementeMeniu[17].Atribute[1]);
            LinkButtonRapGenGrila.Attributes.Add(elementeMeniu[17].Atribute[2], elementeMeniu[17].Atribute[3]);

            LinkButtonRapGenExceptionala.Attributes.Clear();
            LinkButtonRapGenExceptionala.Attributes.Add(elementeMeniu[18].Atribute[0], elementeMeniu[18].Atribute[1]);
            LinkButtonRapGenExceptionala.Attributes.Add(elementeMeniu[18].Atribute[2], elementeMeniu[18].Atribute[3]);

            liBtnPlati.Attributes.Clear();
            liBtnPlati.Attributes.Add(elementeMeniu[19].Atribute[0], elementeMeniu[19].Atribute[1]);
            liBtnPlati.Attributes.Add(elementeMeniu[19].Atribute[2], elementeMeniu[19].Atribute[3]);

            liRaportChirii.Attributes.Clear();
            liRaportChirii.Attributes.Add(elementeMeniu[20].Atribute[0], elementeMeniu[20].Atribute[1]);
            liRaportChirii.Attributes.Add(elementeMeniu[20].Atribute[2], elementeMeniu[20].Atribute[3]);

            liBtnCursuri.Attributes.Clear();
            liBtnCursuri.Attributes.Add(elementeMeniu[21].Atribute[0], elementeMeniu[21].Atribute[1]);
            liBtnCursuri.Attributes.Add(elementeMeniu[21].Atribute[2], elementeMeniu[21].Atribute[3]);
        }
    }
}