﻿<%@ Page Title="Asocieri conturi pentru rapoarte" Language="C#" MasterPageFile="~/Admin/Ad.master" AutoEventWireup="true" CodeBehind="Conturi.aspx.cs" Inherits="cometex.Admin.Conturi" %>

<%@ MasterType VirtualPath="~/Admin/Ad.master" %>
<%@ Reference VirtualPath="~/Cometex.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="PanelListaCereri" runat="server" CssClass="form-horizontal">
        <asp:Panel runat="server" CssClass="block table-responsive text-center">
            <asp:GridView ID="GridListaConturi" runat="server" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" DataSourceID="SqlDataSource1" CssClass="table table-hover table-condensed " ShowHeaderWhenEmpty="True" GridLines="None" OnSelectedIndexChanged="GridListaSocietati_SelectedIndexChanged" DataKeyNames="Id">
                <AlternatingRowStyle BackColor="White" />
                <SelectedRowStyle BackColor="#738A9C" ForeColor="White" />
                <Columns>
                    <asp:CommandField ShowSelectButton="True" />
                    <asp:TemplateField HeaderText="Cont" SortExpression="Cont">
                        <ItemTemplate>
                            <asp:Label ID="lblCont" runat="server" Text='<%# Bind("cont") %>'></asp:Label><br />
                            <asp:Label ID="lblIdSocietate" runat="server" Text='<%# Bind("idSocietate") %>' Visible="false" />
                            <asp:Label ID="lblIdGrafic" runat="server" Text='<%# Bind("idGrafic") %>' Visible="false" />
                            <asp:Label ID="lblContId" runat="server" Text='<%# Bind("id") %>' Visible="false" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Grafic" SortExpression="denumire">
                        <ItemTemplate>
                            <asp:Label ID="lblRaportContDenumire" runat="server" Text='<%# Bind("denumire") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Societate" SortExpression="societateDenumire">
                        <ItemTemplate>
                            <asp:Label ID="lblSocietateDenumire" runat="server" Text='<%# Bind("societateDenumire") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mod de calcul" SortExpression="modCalcul">
                        <ItemTemplate>
                            <asp:Label ID="lblModCalcul" runat="server" Text='<%# Bind("modCalcul") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:Panel>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT Conturi.id, Conturi.cont, Conturi.grafic, Conturi.idSocietate, Conturi.modCalcul, Grafice.id AS idGrafic, Grafice.denumire, Grafice.descriere, Societati.IdSocietate AS idSocietate, Societati.Societate as societateDenumire, Societati.CuloareGrafice, Societati.SAGACod FROM Societati INNER JOIN Conturi ON Societati.IdSocietate = Conturi.idSocietate INNER JOIN Grafice ON Conturi.grafic = Grafice.id"></asp:SqlDataSource>
        <div class="clearfix" id="Butoane" runat="server">
            <asp:Button ID="ButonAdauga" CssClass="btn btn-primary" runat="server" OnClick="Adauga_Click" Text="Adauga" />
            <asp:Button ID="ButonModifica" CssClass="btn btn-danger" runat="server" OnClick="Modifica_Click" Text="Modifica" />
            <asp:Button ID="ButonSterge" CssClass="btn btn-danger" runat="server" OnClick="Sterge_Click" OnClientClick="return confirm('Sigur stergeti?');" Text="Sterge" />
        </div>
    </asp:Panel>
    <asp:Panel ID="PanelAdauga" runat="server" Visible="False" CssClass="form-horizontal">
        <asp:Label runat="server" Text="Adauga / Modifica un cont" />
        <div class="form-group">
            <asp:Label runat="server" Text="Societate:" AssociatedControlID="ddlSocietati" CssClass="col-md-2 control-label" />
            <div class="col-md-10">
                <asp:DropDownList ID="ddlSocietati" AutoPostBack="true" runat="server" DataSourceID="SqlDSSocietati" DataTextField="Societate" DataValueField="IdSocietate" CssClass="selectpicker" data-selected-text-format="count" OnSelectedIndexChanged="ddlSocietati_SelectedIndexChanged"></asp:DropDownList>
                <asp:SqlDataSource ID="SqlDSSocietati" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT IdSocietate, (SAGACod + ' - ' + Societate) as Societate  FROM Societati"></asp:SqlDataSource>
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="Grafic/Linie raport:" AssociatedControlID="ddlGrafic" CssClass="col-md-2 control-label" />
            <div class="col-md-10">
                <asp:DropDownList ID="ddlGrafic" runat="server" DataSourceID="SqlDSGrafice" DataTextField="denumire" DataValueField="id" CssClass="selectpicker" data-selected-text-format="count"></asp:DropDownList>
                <asp:SqlDataSource ID="SqlDSGrafice" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT denumire, id FROM Grafice"></asp:SqlDataSource>
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="Asociaza contul:" AssociatedControlID="ddlCont" CssClass="col-md-2 control-label" />
            <div class="col-md-10">
                <asp:DropDownList ID="ddlCont" runat="server" CssClass="selectpicker"></asp:DropDownList>
                <asp:Button ID="btnAdaugaContDinDdl" CssClass="btn btn-success" runat="server" OnClick="btnAdaugaContDinDdl_Click" Text="+" />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="" AssociatedControlID="lblConturiAsociate" CssClass="col-md-2 control-label" />
            <div class="col-md-10">
                <asp:Label ID="lblConturiAsociate" runat="server" Text="Conturi asociate:<br />" CssClass=" control-label" />
                <asp:Button ID="btnReseteazaConturi" CssClass="btn btn-danger" runat="server" OnClick="btnReseteazaConturi_Click" Text="reseteaza conturi" />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="Mod de calcul:" AssociatedControlID="rblModCalcul" CssClass="col-md-2 control-label" />
            <div class="col-md-10">
                <asp:RadioButtonList ID="rblModCalcul" runat="server">
                    <asp:ListItem Text="Sold" />
                    <asp:ListItem Text="Debitor" />
                    <asp:ListItem Text="Creditor" />
                    <asp:ListItem Text="Rulaj" />
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="clearfix" id="idvButoaneAdauga" runat="server">
            <asp:Button ID="ButonSalveaza" CssClass="btn btn-success" runat="server" OnClick="ButonSalveaza_Click" Text="Salveaza" />
            <asp:Button ID="ButonRenunta" CssClass="btn btn-danger" runat="server" OnClick="ButonRenunta_Click" Text="Renunta" />
        </div>

    </asp:Panel>
    <script type="text/javascript">
        function pageLoad() {
            // for cities dropdowns
            $(".selectpicker").selectpicker();
        }
    </script>
</asp:Content>
