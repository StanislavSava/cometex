﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Ad.master" AutoEventWireup="true" CodeBehind="PlafoaneGrila.aspx.cs" Inherits="cometex.Admin.PlafoaneGrila" UICulture="ro-RO" Culture="ro-RO" %>
<%@ MasterType VirtualPath="~/Admin/Ad.master" %>
<%@ Reference VirtualPath="~/Cometex.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="PanelListaCereri" runat="server" CssClass="panel">
        <asp:Panel ID="Panel3" runat="server" CssClass="block">
            <asp:Panel ID="Panel5" runat="server" CssClass="inline">
                <asp:Label ID="LabelSocietatea" runat="server" Text="Societatea "></asp:Label>
                <asp:DropDownList ID="DropDownListSocietati" runat="server" AppendDataBoundItems="True" AutoPostBack="True" DataSourceID="SqlDataSource2" DataTextField="Societate" DataValueField="IdSocietate" OnSelectedIndexChanged="DropDownListSocietati_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT [IdSocietate], [Societate] FROM [Societati] ORDER BY [Societate]"></asp:SqlDataSource>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="Panel2" runat="server" CssClass="inline">
            <asp:Label runat="server" Text="Categorie" />
            <asp:DropDownList ID="DropDownListCategoriiCheltuieli" runat="server" AppendDataBoundItems="True" AutoPostBack="True" DataSourceID="SqlDataSource3" DataTextField="Categorie" DataValueField="IdCategorie" OnSelectedIndexChanged="DropDownListCategoriiCheltuieli_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT [IdCategorie], [Categorie] FROM [Categorii] WHERE [activa] = 1 AND [tip] = 'grila' ORDER BY [Categorie]"></asp:SqlDataSource>
        </asp:Panel>
        <asp:Panel ID="PanelSuma" runat="server" CssClass="inline">
            <asp:Label ID="LabelSuma" runat="server" Text="Plafonul maxim lunar"></asp:Label><asp:TextBox ID="TextBoxSuma" runat="server"></asp:TextBox>
        </asp:Panel>

        <asp:Panel ID="PanelButoane" runat="server" CssClass="clearfix">
            <asp:Button ID="ButonAdauga" CssClass="btn btn-primary" runat="server" OnClick="ButonAdauga_Click" Text="Adauga" />
            <asp:Button ID="ButonModifica" CssClass="btn btn-danger" runat="server" OnClick="ButonModifica_Click" Text="Modifica" />
            <asp:Button ID="btSterge" CssClass="btn btn-danger" runat="server" OnClick="btSterge_Click" Text="Sterge" />
            <asp:Button ID="ButonRenunta" CssClass="btn btn-danger" runat="server" OnClick="ButonRenunta_Click" Text="Renunta" />
        </asp:Panel>

        <asp:Panel runat="server" CssClass="block table-responsive text-center">
            <asp:GridView ID="GridListaPlafoane" runat="server" AutoGenerateColumns="False" AllowSorting="True" DataSourceID="SqlDataSource1" CssClass="table table-hover table-condensed" DataKeyNames="idPlafon" ShowHeaderWhenEmpty="True" GridLines="None" OnSelectedIndexChanged="GridListaPlafoane_SelectedIndexChanged">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:CommandField ShowSelectButton="True" />
                    <asp:BoundField DataField="Suma" HeaderText="Suma" SortExpression="Suma" Visible="True" />
                    <asp:TemplateField HeaderText="Societate" SortExpression="Societate">
                        <ItemTemplate>
                            <asp:Label ID="DenumireSocietate" runat="server" Text='<%# Bind("Societate") %>'></asp:Label>
                            <asp:Label ID="idSocietate" runat="server" Text='<%# Bind("idSocietate") %>' Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Categorie" SortExpression="Categorie">
                        <ItemTemplate>
                            <asp:Label ID="DenumireCategorie" runat="server" Text='<%# Bind("Categorie") %>' Visible="true"></asp:Label>
                            <asp:Label ID="idCategorie" runat="server" Text='<%# Bind("idCategorie") %>' Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Observatii" HeaderText="Observatii" SortExpression="Observatii" Visible="False" />
                    <asp:BoundField DataField="idPlafon" HeaderText="idPlafon" ReadOnly="True" SortExpression="idPlafon" Visible="False" />
                </Columns>
                <SelectedRowStyle BackColor="#738A9C" />
            </asp:GridView>
        </asp:Panel>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT PlafoaneGrila.idPlafon, PlafoaneGrila.idSocietate, PlafoaneGrila.idCategorie, PlafoaneGrila.Suma, PlafoaneGrila.Observatii, Societati.Societate, Categorii.Categorie FROM PlafoaneGrila INNER JOIN Categorii ON PlafoaneGrila.idCategorie = Categorii.IdCategorie INNER JOIN Societati ON PlafoaneGrila.idSocietate = Societati.IdSocietate WHERE (PlafoaneGrila.idSocietate = @idSocietate)">
            <SelectParameters>
                <asp:ControlParameter ControlID="DropDownListSocietati" Name="idSocietate" PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </asp:Panel>


</asp:Content>
