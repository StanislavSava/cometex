﻿<%@ Page Title="Asocieri conturi pentru rapoarte" Language="C#" MasterPageFile="~/Admin/Ad.master" AutoEventWireup="true" CodeBehind="CashFlowConfigurari.aspx.cs" Inherits="cometex.Admin.CashFlowConfigurari" %>

<%@ MasterType VirtualPath="~/Admin/Ad.master" %>
<%@ Reference VirtualPath="~/Cometex.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="PanelAdauga" runat="server" Visible="True" CssClass="form-horizontal">
        <asp:Label runat="server" Text="Conturi asociate pentru Raport CashFlow" />
        <div class="form-group">
            <asp:Label runat="server" Text="Societate:" AssociatedControlID="ddlSocietati" CssClass="col-md-2 control-label" />
            <div class="col-md-10">
                <asp:DropDownList ID="ddlSocietati" AutoPostBack="true" runat="server" DataSourceID="SqlDSSocietati" DataTextField="Societate" DataValueField="IdSocietate" CssClass="selectpicker" data-selected-text-format="count" OnSelectedIndexChanged="ddlSocietati_SelectedIndexChanged"></asp:DropDownList>
                <asp:SqlDataSource ID="SqlDSSocietati" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT IdSocietate, (SAGACod + ' - ' + Societate) as Societate  FROM Societati"></asp:SqlDataSource>
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" ID="lblCont1" Text="" AssociatedControlID="ddlCont1" CssClass="col-md-2 control-label" />
            <div class="col-md-10">
                <asp:DropDownList ID="ddlCont1" runat="server" CssClass="selectpicker"></asp:DropDownList>
                <asp:Button ID="btnAdaugaContDinDdl1" CssClass="btn btn-success" runat="server" OnClick="btnAdaugaContDinDdl1_Click" Text="+" />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="" AssociatedControlID="lblConturiAsociate1" CssClass="col-md-2 control-label" />
            <div class="col-md-10">
                <asp:Label ID="lblConturiAsociate1" runat="server" Text="Conturi asociate:<br />" CssClass=" control-label" />
                <asp:Button ID="btnReseteazaConturi1" CssClass="btn btn-danger" runat="server" OnClick="btnReseteazaConturi1_Click" Text="reseteaza conturi" />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" ID="lblCont2" Text="" AssociatedControlID="ddlCont2" CssClass="col-md-2 control-label" />
            <div class="col-md-10">
                <asp:DropDownList ID="ddlCont2" runat="server" CssClass="selectpicker"></asp:DropDownList>
                <asp:Button ID="btnAdaugaContDinDdl2" CssClass="btn btn-success" runat="server" OnClick="btnAdaugaContDinDdl2_Click" Text="+" />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="" AssociatedControlID="lblConturiAsociate2" CssClass="col-md-2 control-label" />
            <div class="col-md-10">
                <asp:Label ID="lblConturiAsociate2" runat="server" Text="Conturi asociate:<br />" CssClass=" control-label" />
                <asp:Button ID="btnReseteazaConturi2" CssClass="btn btn-danger" runat="server" OnClick="btnReseteazaConturi2_Click" Text="reseteaza conturi" />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" Text="" ID="lblCont3" AssociatedControlID="ddlCont3" CssClass="col-md-2 control-label" />
            <div class="col-md-10">
                <asp:DropDownList ID="ddlCont3" sdg="" runat="server" CssClass="selectpicker"></asp:DropDownList>
                <asp:Button ID="btnAdaugaContDinDdl3" CssClass="btn btn-success" runat="server" OnClick="btnAdaugaContDinDdl3_Click" Text="+" />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="" AssociatedControlID="lblConturiAsociate3" CssClass="col-md-2 control-label" />
            <div class="col-md-10">
                <asp:Label ID="lblConturiAsociate3" runat="server" Text="Conturi asociate:<br />" CssClass=" control-label" />
                <asp:Button ID="btnReseteazaConturi3" CssClass="btn btn-danger" runat="server" OnClick="btnReseteazaConturi3_Click" Text="reseteaza conturi" />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" Text="" ID="lblCont4" AssociatedControlID="ddlCont4" CssClass="col-md-2 control-label" />
            <div class="col-md-10">
                <asp:DropDownList ID="ddlCont4" sdg="" runat="server" CssClass="selectpicker"></asp:DropDownList>
                <asp:Button ID="btnAdaugaContDinDdl4" CssClass="btn btn-success" runat="server" OnClick="btnAdaugaContDinDdl4_Click" Text="+" />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="" AssociatedControlID="lblConturiAsociate4" CssClass="col-md-2 control-label" />
            <div class="col-md-10">
                <asp:Label ID="lblConturiAsociate4" runat="server" Text="Conturi asociate:<br />" CssClass=" control-label" />
                <asp:Button ID="btnReseteazaConturi4" CssClass="btn btn-danger" runat="server" OnClick="btnReseteazaConturi4_Click" Text="reseteaza conturi" />
            </div>
        </div>

        <div class="clearfix" id="idvButoaneAdauga" runat="server">
            <asp:Button ID="ButonSalveaza" CssClass="btn btn-success" runat="server" OnClick="ButonSalveaza_Click" Text="Salveaza" />
        </div>

    </asp:Panel>
    <script type="text/javascript">
        function pageLoad() {
            // for cities dropdowns
            $(".selectpicker").selectpicker();
        }
    </script>
</asp:Content>
