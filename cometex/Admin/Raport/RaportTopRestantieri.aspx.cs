﻿using cometex.App_Service;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cometex.Admin.Raport
{
    public partial class RaportTopRestantieri : System.Web.UI.Page
    {
        DateTime dateStart;
        DateTime dateEnd;
        Investitie investitie;

        string societate;
        string idSocietate =string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IncarcaRaport();
              //  AfiseazaInFormatPDF();
            }
        }

        private void AfiseazaInFormatPDF()
        {
            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string fileNameExtension;
            byte[] bytes = ReportViewer1.LocalReport.Render("PDF", null, out mimeType, out encoding, out fileNameExtension, out streamids, out warnings);
            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "inline; filename=Nume" + fileNameExtension);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
            Response.Close();
        }

        private void IncarcaRaport()
        {
            ReportViewer1.LocalReport.DataSources.Clear();

            DSReportTableAdapters.RaportGeneralTableAdapter adapter = new DSReportTableAdapters.RaportGeneralTableAdapter();
            DSReport.RaportGeneralDataTable table = new DSReport.RaportGeneralDataTable();

            List<Utils.DataSourceChiriasi.Chirii> date = Utils.DataSourceChiriasi.RaportTopRestantieriChiriiAlimentara();
            ReportDataSource reportDataSource = new ReportDataSource("RaportTopRestantieriChiriiAlimentara", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportTopRestantieriChiriiAstral();
            reportDataSource = new ReportDataSource("RaportTopRestantieriChiriiAstral", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportTopRestantieriChiriiCometex();
            reportDataSource = new ReportDataSource("RaportTopRestantieriChiriiCometex", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportTopRestantieriChiriiComtex();
            reportDataSource = new ReportDataSource("RaportTopRestantieriChiriiComtex", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportTopRestantieriChiriiGaleriileVictoria();
            reportDataSource = new ReportDataSource("RaportTopRestantieriChiriiGaleriileVictoria", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportTopRestantieriChiriiMinerva();
            reportDataSource = new ReportDataSource("RaportTopRestantieriChiriiMinerva", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportTopRestantieriChiriiMinervaGaleriileComerciale();
            reportDataSource = new ReportDataSource("RaportTopRestantieriChiriiMinervaGaleriileComerciale", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportTopRestantieriChiriiRomana();
            reportDataSource = new ReportDataSource("RaportTopRestantieriChiriiRomana", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportTopRestantieriChiriiUnicom();
            reportDataSource = new ReportDataSource("RaportTopRestantieriChiriiUnicom", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportTopRestantieriUtilitatiAlimentara();
            reportDataSource = new ReportDataSource("RaportTopRestantieriUtilitatiAlimentara", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportTopRestantieriUtilitatiAstral();
            reportDataSource = new ReportDataSource("RaportTopRestantieriUtilitatiAstral", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportTopRestantieriUtilitatiCometex();
            reportDataSource = new ReportDataSource("RaportTopRestantieriUtilitatiCometex", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportTopRestantieriUtilitatiComtex();
            reportDataSource = new ReportDataSource("RaportTopRestantieriUtilitatiComtex", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportTopRestantieriUtilitatiGaleriileVictoria();
            reportDataSource = new ReportDataSource("RaportTopRestantieriUtilitatiGaleriileVictoria", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportTopRestantieriUtilitatiMinerva();
            reportDataSource = new ReportDataSource("RaportTopRestantieriUtilitatiMinerva", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportTopRestantieriUtilitatiMinervaGaleriileComerciale();
            reportDataSource = new ReportDataSource("RaportTopRestantieriUtilitatiMinervaGaleriileComerciale", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportTopRestantieriUtilitatiRomana();
            reportDataSource = new ReportDataSource("RaportTopRestantieriUtilitatiRomana", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportTopRestantieriUtilitatiUnicom();
            reportDataSource = new ReportDataSource("RaportTopRestantieriUtilitatiUnicom", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

        }
    }
}