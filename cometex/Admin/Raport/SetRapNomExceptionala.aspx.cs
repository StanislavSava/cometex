﻿using cometex.App_Service;
using cometex.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cometex.Test
{
    public partial class SetRapNomExceptionala : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                Master.AtribuieButoaneMeniu(MeniuUtils.AranjeazaElementeMeniu(22, new List<int> { 3, 17 }));
                Session["investitie"] = null;
                Session["dateStart"] = null;
                Session["dateEnd"] = null;
                TextDateStart.Text = DateTime.Now.AddDays(-30).ToString("dd.MM.yyyy");
                TextDateEnd.Text = DateTime.Now.ToString("dd.MM.yyyy");
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            Clear();
            TrimiteInformatiile();

        }

        private void TrimiteInformatiile()
        {
            try
            {
                Investitie investitie = new Investitie();


                Session["dateStart"] = Convert.ToDateTime(TextDateStart.Text).ToString("s");

                Session["dateEnd"] = Convert.ToDateTime(TextDateEnd.Text).ToString("s");
                investitie.Societate.Id = DropDownListSocietate.SelectedValue;
                investitie.Societate.Nume = DropDownListSocietate.SelectedItem.Text;
                investitie.Status.Nume = DropDownListStatus.SelectedItem.Text;
                investitie.Status.Id = DropDownListStatus.SelectedValue.ToString();
                //investitie.Categorie.Nume = DropDownListCategorii.SelectedItem.Text;
                //investitie.Categorie.Id = DropDownListCategorii.SelectedValue.ToString();
                investitie.IntrodusDe.Nume = DropDownListAdaugatDe.SelectedItem.Text;
                investitie.IntrodusDe.Id = DropDownListAdaugatDe.SelectedValue.ToString();
                Session["investitie"] = investitie;
                if (Convert.ToDateTime(Session["dateStart"]) < Convert.ToDateTime(Session["dateEnd"]))
                {
                    //string url = "RapNomExceptionala.aspx";
                    //string popupScript = "<script language=\"javascript\">" + "window.open('" + url + "', 'newWindow', 'left=2, top=2,location=no, width=1010, height=660, menubar=no, resizable=yes,statusbar=yes,scrollbars=yes');" + "</script>";
                    //Page.RegisterStartupScript("Redirect", popupScript);
                    Response.Redirect("RapNomExceptionala.aspx");
                }
                else
                {
                    MessageLabel.Text = "Va rugam sa alegeti o perioada corecta !";
                }
            }
            catch
            {
                MessageLabel.Text = "Va rugam completati toate casutele !";
            }
        }

        void Clear()
        {
            MessageLabel.Text = "";
        }

        protected void DropDownListStatus_DataBound(object sender, EventArgs e)
        {
            ListItem listItem = new ListItem();
            listItem.Text = "Toate";
            listItem.Value = "%";
            DropDownListStatus.Items.Insert(0, listItem);
        }

        protected void DropDownListCategorii_DataBound(object sender, EventArgs e)
        {
            //ListItem listItem = new ListItem();
            //listItem.Text = "Toate Categoriile";
            //listItem.Value = "%";
            //DropDownListCategorii.Items.Insert(0, listItem);
        }

        protected void DropDownListAdaugatDe_DataBound(object sender, EventArgs e)
        {
            ListItem listItem = new ListItem();
            listItem.Text = "Toti";
            listItem.Value = "%";
            DropDownListAdaugatDe.Items.Insert(0, listItem);
        }

        protected void DropDownListSocietate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListSocietate.SelectedValue != "%")
            {
                DropDownListAdaugatDe.DataSource = AdaugatDe;
                DropDownListAdaugatDe.DataValueField = "idUtilizator";
                DropDownListAdaugatDe.DataTextField = "Nume";
                string idSocietate = DropDownListSocietate.SelectedValue.ToString() ;
                Session["SocietateAdaugat"] = idSocietate;
                DropDownListAdaugatDe.DataBind();
            }
            else
            {

                DropDownListAdaugatDe.DataSource = AdaugatDeInToateSocietatile;
                DropDownListAdaugatDe.DataValueField = "idUtilizator";
                DropDownListAdaugatDe.DataTextField = "Nume";
                
                
                DropDownListAdaugatDe.DataBind();
            }
        }

        protected void DropDownListSocietate_DataBound(object sender, EventArgs e)
        {
            ListItem listItem = new ListItem();
            listItem.Text = "Toate Societatile";
            listItem.Value = "%";
            DropDownListSocietate.Items.Insert(0, listItem);
            DropDownListSocietate_SelectedIndexChanged(sender, e);

        }
    }
}