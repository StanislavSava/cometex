﻿<%@ Page Title="Raport Debitori" Language="C#" MasterPageFile="~/Admin/Ad.master" AutoEventWireup="true" CodeBehind="RaportDebitori.aspx.cs" Inherits="cometex.Admin.Raport.RaportDebitori" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Raport Debitori - toate societatile</h2>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="100%"  Height="700px">
                <LocalReport ReportPath="RaportDebitori.rdlc">
                    <DataSources>
                        <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DSChirii" />
                    </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="RaportChirii" TypeName="cometex.Utils.DataSourceChiriasi"></asp:ObjectDataSource>
</asp:Content>
