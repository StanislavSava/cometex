﻿using cometex.App_Service;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cometex.Admin.Raport
{
    public partial class RaportSintetice : System.Web.UI.Page
    {
        DateTime dateStart;
        DateTime dateEnd;
        Investitie investitie;

        string societate;
        string idSocietate =string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IncarcaRaport();
              //  AfiseazaInFormatPDF();
            }
        }

        private void AfiseazaInFormatPDF()
        {
            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string fileNameExtension;
            byte[] bytes = ReportViewer1.LocalReport.Render("PDF", null, out mimeType, out encoding, out fileNameExtension, out streamids, out warnings);
            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "inline; filename=Nume" + fileNameExtension);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
            Response.Close();
        }

        private void IncarcaRaport()
        {
            ReportViewer1.LocalReport.DataSources.Clear();

            DSReportTableAdapters.RaportGeneralTableAdapter adapter = new DSReportTableAdapters.RaportGeneralTableAdapter();
            DSReport.RaportGeneralDataTable table = new DSReport.RaportGeneralDataTable();
            List<Utils.DataSourceChiriasi.Chirii>  date = Utils.DataSourceChiriasi.RaportSintetice();
            ReportDataSource reportDataSource = new ReportDataSource("DSChirii", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

        }
    }
}