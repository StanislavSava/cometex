﻿using cometex.Admin.Raport;
using cometex.App_Service;
using cometex.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

namespace cometex.Admin.Raport
{
    public partial class SeteazaParametriRaportChirii : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try { ddlSinteticeAnul.SelectedValue = DateTime.Now.Year.ToString(); } catch { }
                try { ddlSinteticeLuna.SelectedValue = DateTime.Now.Month.ToString(); } catch { }
                try { ddlNominaleAnul.SelectedValue = DateTime.Now.Year.ToString(); } catch { }
                try { ddlNominaleLuna.SelectedValue = DateTime.Now.Month.ToString(); } catch { }
                Master.AtribuieButoaneMeniu(MeniuUtils.AranjeazaElementeMeniu(22, new List<int> { 3, 21 }));
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Master.AtribuieButoaneMeniu(MeniuUtils.AranjeazaElementeMeniu(22, new List<int> { 3, 21 }));
                Session["dataSoldFinal"] = TextDateEnd.Text = DateTime.Now.AddDays(-1).ToString("dd.MM.yyyy");
                Session["dataSoldPrecedent"] = DateTime.Now.AddDays(-2).ToString("dd.MM.yyyy");
                TextDateStartRapCheltuileiGrila.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).AddMonths(-1).ToString("dd.MM.yyyy");
                TextDateEndRapCheltuieliGrila.Text = DateTime.Now.AddDays(-DateTime.Now.Day).ToString("dd.MM.yyyy");
            }
        }
        #region cashflow

        protected void btCashFlow_Click(object sender, EventArgs e)
        {
            TrimiteInformatiile();
        }
        private void TrimiteInformatiile()
        {
            try
            {
                Investitie investitie = new Investitie();
                Session["dataSoldPrecedent"] = Convert.ToDateTime(TextDateEnd.Text).AddDays(-1).ToString("dd.MM.yyyy");
                Session["dataSoldFinal"] = Convert.ToDateTime(TextDateEnd.Text).ToString("dd.MM.yyyy");
                Response.Redirect("RaportCash.aspx");
            }
            catch (Exception x) { }
        }
        #endregion
        #region sinteza cheltuieli pe societati
        protected void ButtonAfiseazaRaport_Click(object sender, EventArgs e)
        {
            TrimiteInformatiileRaportGrila();
        }
        private void TrimiteInformatiileRaportGrila()
        {
            try
            {
                Investitie investitie = new Investitie();
                Session["dateStart"] = Convert.ToDateTime(TextDateStartRapCheltuileiGrila.Text, CultureInfo.CurrentCulture).ToString("dd.MM.yyyy");
                Session["dateEnd"] = Convert.ToDateTime(TextDateEndRapCheltuieliGrila.Text, CultureInfo.CurrentCulture).ToString("dd.MM.yyyy");
                if (Convert.ToDateTime(Session["dateStart"]) < Convert.ToDateTime(Session["dateEnd"]))
                {
                    Response.Redirect("RaportSintezaCheltuieli.aspx");

                }
                else
                {
                    MessageLabel.Text = "Va rugam sa alegeti o perioada corecta !";
                }
            }
            catch
            {
                MessageLabel.Text = "Formatul datei este unul incorect!";
            }
        }

        #endregion

        #region sintetice
        protected void btRapoarteSinteticeLunaAleasa_Click(object sender, EventArgs e)
        {
            Session["esteListaContracteActive"] = "1";
            TrimiteInformatiileSintetice();
        }

        protected void btRapoarteSinteticeLunaAleasaInactivi_Click(object sender, EventArgs e)
        {
            Session["esteListaContracteActive"] = "0";
            TrimiteInformatiileSintetice();
        }
        private void TrimiteInformatiileSintetice()
        {
            try
            {
                Session["lunaRapoarteSintetice"] = "01." + ddlSinteticeLuna.SelectedValue + "." + ddlSinteticeAnul.SelectedValue;
                Response.Redirect("RaportSinteticeLunaAleasa.aspx");
            }
            catch
            {
            }
        }
        protected void btRapoarteSinteticeLunaAnterioara_Click(object sender, EventArgs e)
        {
            DateTime data = DateTime.Now;
            try
            {
                Session["esteListaContracteActive"] = "1";
                Session["lunaRapoarteSintetice"] = data.AddMonths(-1).AddDays(-data.Day + 1).ToString("dd.MM.yyyy");
                Response.Redirect("RaportSinteticeLunaAleasa.aspx");
            }
            catch
            {
            }
        }
        protected void btRapoarteSinteticeLunaCurenta_Click(object sender, EventArgs e)
        {
            DateTime data = DateTime.Now;
            try
            {
                Session["esteListaContracteActive"] = "1";
                Session["lunaRapoarteSintetice"] = data.AddDays(-data.Day + 1).ToString("dd.MM.yyyy");
                Response.Redirect("RaportSinteticeLunaAleasa.aspx");
            }
            catch
            {
            }
        }
        #endregion

        #region top restantieri
        protected void btTopRestantieri_Click(object sender, EventArgs e)
        {
            try
            {
                Session["coeficientTopRestantieri"] = Convert.ToDecimal(tbCoeficientTopRestantieri.Text.Replace(".", ","));
            }
            catch
            {
                Session["coeficientTopRestantieri"] = 0;
            }
            Response.Redirect("RaportTopRestantieri.aspx");
        }
        #endregion
        #region chirii si utilitati
        protected void DropDownListSocietateRapoarte_DataBound(object sender, EventArgs e)
        {
            ListItem listItem = new ListItem();
            listItem.Text = "Toate";
            listItem.Value = "%";
        }
        protected void btChiriiUtilitati_Click(object sender, EventArgs e)
        {
            Session["esteListaContracteActive"] = "1";
            Session["esteListaContracteActiveDupaPopulareClienti"] = "0";
            TrimiteInformatiileChiriiUtilitati();
        }
        protected void btChiriiUtilitatiInactivi_Click(object sender, EventArgs e)
        {
            Session["esteListaContracteActive"] = "0";
            Session["esteListaContracteActiveDupaPopulareClienti"] = "0";
            TrimiteInformatiileChiriiUtilitati();
        }
        private void TrimiteInformatiileChiriiUtilitati()
        {
            try
            {
                Session["societateId"] = DropDownListSocietate.SelectedValue.ToString();
                Session["lunaRapoarteNominale"] = "01." + ddlNominaleLuna.SelectedValue + "." + ddlNominaleAnul.SelectedValue;
                Session["ordonareRapoarteNominale"] = ddlOrdonareRapoarteNominale.SelectedValue;

                Response.Redirect("RaportChiriiSocietateLunaCurenta.aspx");
            }
            catch (Exception x) { }
        }


        #endregion

        #region Debitori
        protected void btDebitori_Click(object sender, EventArgs e)
        {
            Session["ordonareRaportDebitori"] = ddlDebitoriOrdonare.SelectedValue;
            Session["esteListaContracteActive"] = "0";
            if (ddlSocietatiDebitori.SelectedValue == "%")
                Response.Redirect("RaportDebitori.aspx");
            else
            {
                Session["societateRaportDebitori"] = ddlSocietatiDebitori.SelectedValue;
                Response.Redirect("RaportDebitoriPeOSocietate.aspx");
            }
        }
        #endregion

    }
}