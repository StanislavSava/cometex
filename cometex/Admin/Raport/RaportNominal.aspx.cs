﻿using cometex.App_Service;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cometex.Admin.Raport
{
    public partial class RaportNominal : System.Web.UI.Page
    {
        DateTime dateStart = new DateTime();
        DateTime dateEnd = new DateTime();
        Investitie investitie = new Investitie();
        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ExtrageParametriiDinURL();
                IncarcaRaport();
                AdaugaParametriiPeRaport();

                AfiseazaInFormatPDF();
            }
        }

        private void AfiseazaInFormatPDF()
        {
            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string fileNameExtension;
            byte[] bytes = ReportViewer1.LocalReport.Render("PDF", null, out mimeType, out encoding, out fileNameExtension, out streamids, out warnings);
            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "inline; filename=Nume" + fileNameExtension);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
            Response.Close();
        }

        private void IncarcaRaport()
        {

            DSReportTableAdapters.RaportNominalTableAdapter adapter = new DSReportTableAdapters.RaportNominalTableAdapter();
            DSReport.RaportNominalDataTable table = new DSReport.RaportNominalDataTable();

            adapter.Fill(table, investitie.Societate.Id, dateStart.ToString(), dateEnd.ToString(), investitie.Categorie.Id, investitie.IntrodusDe.Id, investitie.Status.Id);

            ReportDataSource reportDataSource = new ReportDataSource("DataSet", (DataTable)table);

            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);
        }

        private void ExtrageParametriiDinURL()
        {
            investitie = (Investitie)Session["investitie"];
            dateEnd = Convert.ToDateTime(Session["dateEnd"]);
            dateStart = Convert.ToDateTime(Session["dateStart"]);
            if (investitie.Status.Nume == "Toate")
            {
                investitie.Status.Nume = "";
            }
        }

        private void AdaugaParametriiPeRaport()
        {
            ReportParameter[] param = new ReportParameter[6];
            param[0] = new ReportParameter("status", investitie.Status.Nume);
            param[1] = new ReportParameter("dateTimeStart", dateStart.ToString("dd.MM.yyyy"));
            param[2] = new ReportParameter("dateTimeEnd", dateEnd.ToString("dd.MM.yyyy"));
            param[3] = new ReportParameter("categorie", investitie.Categorie.Nume);
            param[4] = new ReportParameter("adaugatDe", investitie.IntrodusDe.Nume);
            param[5] = new ReportParameter("societate", investitie.Societate.Nume);



            ReportViewer1.LocalReport.SetParameters(param);
            ReportViewer1.LocalReport.Refresh();
        }

    }
}