﻿<%@ Page Title="Raport Sinteza Cheltuieli" Language="C#" MasterPageFile="~/Admin/Ad.master" AutoEventWireup="true" CodeBehind="RaportSintezaCheltuieli.aspx.cs" Inherits="cometex.Admin.Raport.RaportSintezaCheltuieli" UICulture="ro-RO" Culture="ro-RO" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Raport Sinteza Cheltuieli</h2>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <rsweb:ReportViewer ID="ReportViewer_Test" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="100%"  Height="700px">
        <LocalReport ReportPath="Admin\Raport\RaportSintezaCheltuieli.rdlc">
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:Label ID="Label_Temp_Errors" runat="server" Text=""></asp:Label>
</asp:Content>
