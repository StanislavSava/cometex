﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Ad.master" AutoEventWireup="true" CodeBehind="SeteazaParametriRaportNominal.aspx.cs" Inherits="cometex.Test.SeteazaParametriiRaportNormal" UICulture="ro-RO" Culture="ro-RO" %>

<%@ MasterType VirtualPath="~/Admin/Ad.master" %>
<%@ Reference VirtualPath="~/Cometex.Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel CssClass="container" runat="server">
        <asp:Panel ID="PanelGeneral" runat="server" CssClass="container-fluid ">
            <asp:Panel ID="PanelTitlu" runat="server" CssClass="block">
                <asp:Label ID="LabelTitlu" runat="server" Text="Raport Nominal" CssClass="h2" /><br /><br />
            </asp:Panel>
            <asp:Panel ID="Panel3" runat="server" CssClass="block">
                <asp:Panel ID="Panel5" runat="server" CssClass="form-group">
                    <asp:Label ID="LabelSocietate" runat="server" Text="Societate"></asp:Label>
                    <asp:DropDownList ID="DropDownListSocietate" runat="server" DataSourceID="SocietatiDS" DataTextField="Societate" DataValueField="IdSocietate" AutoPostBack="True" OnSelectedIndexChanged="DropDownListSocietate_SelectedIndexChanged" OnDataBound="DropDownListSocietate_DataBound">
                    </asp:DropDownList>
                </asp:Panel>
                <asp:Panel ID="Panel2" runat="server" CssClass="form-group">
                    <asp:Label ID="LabelDataStart" runat="server" Text="Scadenta de la"></asp:Label>
                    <asp:TextBox ID="TextDateStart" runat="server" OnClick="TextDateStartClick"></asp:TextBox>
                    <ajaxtoolkit:CalendarExtender ID="calExtender4" runat="server"
                        PopupButtonID="TextDateStart" PopupPosition="TopRight"
                        TargetControlID="TextDateStart" Format="dd/MM/yyyy"></ajaxtoolkit:CalendarExtender>
                    <asp:Label ID="LabelDataEnd" runat="server" Text="pana la"></asp:Label>
                    <asp:TextBox ID="TextDateEnd" runat="server"></asp:TextBox>
                    <ajaxtoolkit:CalendarExtender ID="Calendarextender1" runat="server"
                        PopupButtonID="CalendarButton2" PopupPosition="TopRight"
                        TargetControlID="TextDateEnd" Format="dd/MM/yyyy"></ajaxtoolkit:CalendarExtender>
                </asp:Panel>
                <asp:Panel ID="Panel4" runat="server" CssClass="form-group">
                    <asp:Label ID="LabelStatus" runat="server" Text="Status"></asp:Label>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="StatusInvestitii" DataTextField="Status" DataValueField="IdStatus" OnDataBound="DropDownListStatus_DataBound">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="StatusInvestitii" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT [Status], [IdStatus] FROM [Statusuri]"></asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="Panel6" runat="server" CssClass="form-group">
                    <asp:Label ID="LabelCategorii" runat="server" Text="Categorie"></asp:Label>
                    <asp:DropDownList ID="DropDownListCategorii" runat="server" DataSourceID="CategorieInvestitii" DataTextField="Categorie" DataValueField="IdCategorie" OnDataBound="DropDownListCategorii_DataBound"></asp:DropDownList>
                    <asp:SqlDataSource ID="CategorieInvestitii" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT [IdCategorie], [Categorie] FROM [Categorii]"></asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="Panel7" runat="server" CssClass="form-group">
                    <asp:Label ID="LabelAdaugatDe" runat="server" Text="Adaugat de"></asp:Label>
                    <asp:DropDownList ID="DropDownListAdaugatDe" runat="server" OnDataBound="DropDownListAdaugatDe_DataBound" AutoPostBack="True"></asp:DropDownList>
                    <asp:SqlDataSource ID="AdaugatDeInToateSocietatile" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT [Nume], [IdUtilizator] FROM [Utilizatori] WHERE (idSocietate <> '0') AND (Admin = 'false')"></asp:SqlDataSource>
                    <asp:SqlDataSource ID="AdaugatDe" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT [Nume], [IdUtilizator] FROM [Utilizatori] WHERE ([idSocietate] = @idSocietate)">
                        <SelectParameters>
                            <asp:SessionParameter Name="idSocietate" SessionField="SocietateAdaugat" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:Label ID="MessageLabel" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                    <asp:SqlDataSource ID="SocietatiDS" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT [IdSocietate], [Societate] FROM [Societati]"></asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="Panel9" runat="server" CssClass="form-group">
                    <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary" Text="Afiseaza Raport" OnClick="Button1_Click" />
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
