﻿using cometex.App_Service;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cometex.Admin.Raport
{
    public partial class RaportChirii : System.Web.UI.Page
    {
        DateTime dateStart;
        DateTime dateEnd;
        Investitie investitie;

        string societate;
        string idSocietate =string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //ExtrageParametri();
                IncarcaRaport();
                //AdaugaParametriiPeRaport();

             //   AfiseazaInFormatPDF();
            }
        }

        private void AfiseazaInFormatPDF()
        {
            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string fileNameExtension;
            byte[] bytes = ReportViewer1.LocalReport.Render("PDF", null, out mimeType, out encoding, out fileNameExtension, out streamids, out warnings);
            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "inline; filename=Nume" + fileNameExtension);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
            Response.Close();
        }

        private void AdaugaParametriiPeRaport()
        {
            ReportParameter[] param = new ReportParameter[3];
            param[0] = new ReportParameter("societate", investitie.Societate.Nume);
            param[1] = new ReportParameter("dateTimeStart", dateStart.ToString("dd.MM.yyyy"));
            param[2] = new ReportParameter("dateTimeEnd", dateEnd.ToString("dd.MM.yyyy"));
            ReportViewer1.LocalReport.SetParameters(param);
            ReportViewer1.LocalReport.Refresh();
        }

        private void IncarcaRaport()
        {
            ReportViewer1.LocalReport.DataSources.Clear();

            DSReportTableAdapters.RaportGeneralTableAdapter adapter = new DSReportTableAdapters.RaportGeneralTableAdapter();
            DSReport.RaportGeneralDataTable table = new DSReport.RaportGeneralDataTable();
            //List<Utils.DataSourceCashFlow.CampRaport> date = Utils.DataSourceCashFlow.CampuriRaport();
            List<Utils.DataSourceChiriasi.Chirii> date = Utils.DataSourceChiriasi.RaportChiriiAstral();
            ReportDataSource reportDataSource = new ReportDataSource("Astral", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportChiriiCometex();
            reportDataSource = new ReportDataSource("Cometex", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportChiriiMinerva();
            reportDataSource = new ReportDataSource("Minerva", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportChiriiMinervaGaleriileComerciale();
            reportDataSource = new ReportDataSource("MinervaGaleriileComerciale", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportChiriiComtex();
            reportDataSource = new ReportDataSource("Comtex", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportChiriiAlimentara();
            reportDataSource = new ReportDataSource("Alimentara", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportChiriiRomana();
            reportDataSource = new ReportDataSource("Romana", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportChiriiGaleriileVictoria();
            reportDataSource = new ReportDataSource("GaleriileVictoria", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportChiriiUnicom();
            reportDataSource = new ReportDataSource("Unicom", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportChirii();
            reportDataSource = new ReportDataSource("DSChirii", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

        }

        private void ExtrageParametri()
        {
            investitie =(Investitie)Session["investitie"];
            dateEnd = Convert.ToDateTime(Session["dateEnd"]);
            dateStart = Convert.ToDateTime(Session["dateStart"]); 
        }
    }
}