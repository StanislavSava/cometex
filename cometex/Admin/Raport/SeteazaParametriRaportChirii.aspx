﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Ad.master" AutoEventWireup="true" CodeBehind="SeteazaParametriRaportChirii.aspx.cs" Inherits="cometex.Admin.Raport.SeteazaParametriRaportChirii" Culture="ro-RO" UICulture="ro-RO" %>

<%@ MasterType VirtualPath="~/Admin/Ad.master" %>
<%@ Reference VirtualPath="~/Cometex.Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel CssClass="form-horizontal" runat="server" ID="pnGeneral">

        <asp:Panel ID="pnCashFlow" runat="server" CssClass="col-md-12">
            <asp:Panel ID="pnMargineCashFlow" runat="server" CssClass="col-md-2"></asp:Panel>
            <asp:Panel ID="pnTitluCashFlow" runat="server" CssClass="row col-md-10">
                <h2>Raport Cash</h2>
            </asp:Panel>
            <asp:Panel ID="pnFormCashFlow" runat="server" CssClass="row form-group">
                <asp:Label ID="LabelDataEnd" CssClass="col-md-2 control-label" runat="server" Text="Data sold final: " AssociatedControlID="TextDateEnd"></asp:Label>
                <asp:Panel runat="server" CssClass="col-md-10">
                    <asp:TextBox ID="TextDateEnd" runat="server" CssClass="form-control"></asp:TextBox>
                    <ajaxtoolkit:CalendarExtender ID="Calendarextender1" runat="server"
                        PopupButtonID="CalendarButton2" PopupPosition="TopRight"
                        TargetControlID="TextDateEnd" Format="dd/MM/yyyy"></ajaxtoolkit:CalendarExtender>
                    <asp:Button ID="btCashFlow" CssClass="btn btn-primary" runat="server" Text="Genereaza Raport Cash" OnClick="btCashFlow_Click" />
                </asp:Panel>
                <asp:SqlDataSource ID="SocietatiDS0" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT [IdSocietate], [Societate] FROM [Societati]"></asp:SqlDataSource>
                <asp:ScriptManager ID="ScriptManager2" runat="server">
                </asp:ScriptManager>
            </asp:Panel>
        </asp:Panel>

        <asp:Panel ID="pnSintezaChetuieli" runat="server" CssClass="col-md-12">
            <asp:Panel ID="Panel1" runat="server" CssClass="col-md-2"></asp:Panel>
            <asp:Panel ID="Panel2" runat="server" CssClass="row col-md-10">
                <h2>Sinteza cheltuieli - toate societatile</h2>
            </asp:Panel>
            <asp:Panel ID="pnSintezaCheltuieli" runat="server" CssClass="row form-group">
                <asp:Label ID="LabelDataStart0" CssClass="col-md-2 control-label" runat="server" Text="De la: " AssociatedControlID="TextDateStartRapCheltuileiGrila"></asp:Label>
                <asp:Panel runat="server" CssClass="col-md-10">
                    <asp:TextBox ID="TextDateStartRapCheltuileiGrila" CssClass="form-control" runat="server" OnClick="TextDateStartClick"></asp:TextBox>
                    <ajaxtoolkit:CalendarExtender ID="TextDateStartRapCheltuileiGrila_CalendarExtender" runat="server" Format="dd/MM/yyyy" PopupButtonID="TextDateStartRapCheltuileiGrila" PopupPosition="TopRight" TargetControlID="TextDateStartRapCheltuileiGrila" />
                    <asp:Label ID="MessageLabel" runat="server" Font-Bold="true" ForeColor="Red" Text=""></asp:Label>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="Panel4" runat="server" CssClass="row form-group">
                <asp:Label ID="LabelDataEnd0" runat="server" Text="Pana la: " CssClass="col-md-2 control-label" AssociatedControlID="TextDateEndRapCheltuieliGrila"></asp:Label>
                <asp:Panel runat="server" CssClass="col-md-10">
                    <asp:TextBox ID="TextDateEndRapCheltuieliGrila" CssClass="form-control" runat="server"></asp:TextBox>
                    <ajaxtoolkit:CalendarExtender ID="TextDateEndRapCheltuieliGrila_CalendarExtender" runat="server" Format="dd/MM/yyyy" PopupButtonID="CalendarButton2" PopupPosition="TopRight" TargetControlID="TextDateEndRapCheltuieliGrila" />
                    <asp:Label ID="MessageLabelRT" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                    <asp:Button ID="ButtonAfiseazaRaport" runat="server" CssClass="btn btn-primary" OnClick="ButtonAfiseazaRaport_Click" Text="Genereaza Sinteza Cheltuieli" />
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="pnTopRestantieri" runat="server" CssClass="col-md-12">
            <asp:Panel ID="Panel7" runat="server" CssClass="col-md-2"></asp:Panel>
            <asp:Panel ID="Panel8" runat="server" CssClass="row col-md-10">
                <h2>Restantieri</h2>
                <h4>(toti restantierii pana la finalul lunii precedente)</h4>
            </asp:Panel>
            <asp:Panel ID="pnBtTopRestantieri" runat="server" CssClass="row form-group">
                <asp:Label ID="Label1" CssClass="col-md-2 control-label" runat="server" Text="" AssociatedControlID="btTopRestantieri">Coeficient:</asp:Label>
                <asp:Panel runat="server" CssClass="col-md-10">
                    <asp:TextBox ID="tbCoeficientTopRestantieri" CssClass="form-control col-md-1" Width="60" runat="server" Text="1,5"></asp:TextBox>
                    <asp:Button ID="btTopRestantieri" CssClass="btn btn-primary" runat="server" Text="Restantieri" OnClick="btTopRestantieri_Click" />

                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="pnDebitori" runat="server" CssClass="col-md-12">
            <asp:Panel ID="Panel20" runat="server" CssClass="col-md-2"></asp:Panel>
            <asp:Panel ID="Panel21" runat="server" CssClass="row col-md-10">
                <h2>Debitori</h2>
                <h4>(chiriasi plecati, cu debite)</h4>
            </asp:Panel>
            <asp:Panel ID="Panel22" runat="server" CssClass="row form-group">
                <asp:Label ID="Label8" CssClass="col-md-2 control-label" runat="server" Text="Societatea: " AssociatedControlID="ddlSocietatiDebitori"></asp:Label>
                <asp:Panel runat="server" CssClass="col-md-10">
                    <asp:Panel runat="server" CssClass="col-md-10" Style="left: 0px; top: -1px">
                        <asp:Panel runat="server" CssClass="row">
                            <asp:DropDownList ID="ddlSocietatiDebitori" runat="server" DataSourceID="SocietatiDS" DataTextField="Societate" DataValueField="idFacturare" CssClass="form-control col-md-6" Width="180" AppendDataBoundItems="true">
                                <asp:ListItem Value="%">- toate -</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlDebitoriOrdonare" runat="server" CssClass="form-control col-md-6" Width="273">
                                <asp:ListItem Value="1">ordonare după sumă</asp:ListItem>
                                <asp:ListItem Value="2">ordonare după denumire chiriași</asp:ListItem>
                                <asp:ListItem Value="3">ordonare după vechime debit</asp:ListItem>
                            </asp:DropDownList>
                        </asp:Panel>
                        <asp:Panel runat="server" CssClass="row">
                            <asp:Button ID="btDebitori" CssClass="btn btn-primary" runat="server" Text="Genereaza Raport Debitori" OnClick="btDebitori_Click" />
                        </asp:Panel>
                    </asp:Panel>

                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="Panel19" runat="server" CssClass="row form-group">
            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="pnChiriiUtilitati" runat="server" CssClass="col-md-12">
            <asp:Panel ID="Panel6" runat="server" CssClass="col-md-2"></asp:Panel>
            <asp:Panel ID="Panel9" runat="server" CssClass="row col-md-10">
                <h2>Raport Nominal Chirii si Utilitati</h2>
                <h4>situatia solduri/restante la zi</h4>
            </asp:Panel>
            <asp:Panel ID="Panel16" runat="server" CssClass="row form-group">
                <asp:Label ID="Label6" CssClass="col-md-2 control-label small" runat="server" AssociatedControlID="ddlNominaleLuna"></asp:Label>
                <asp:Panel runat="server" CssClass="col-md-10" Style="left: 0px; top: -1px">
                    <asp:DropDownList ID="ddlNominaleLuna" runat="server" Width="110" CssClass="form-control col-lg-6">
                        <asp:ListItem Value="1">dec - ian</asp:ListItem>
                        <asp:ListItem Value="2">ian - feb</asp:ListItem>
                        <asp:ListItem Value="3">feb - mar</asp:ListItem>
                        <asp:ListItem Value="4">mar - apr</asp:ListItem>
                        <asp:ListItem Value="5">apr - mai</asp:ListItem>
                        <asp:ListItem Value="6">mai - iun</asp:ListItem>
                        <asp:ListItem Value="7">iun - iul</asp:ListItem>
                        <asp:ListItem Value="8">iul - aug</asp:ListItem>
                        <asp:ListItem Value="9">aug - sep</asp:ListItem>
                        <asp:ListItem Value="10">sep - oct</asp:ListItem>
                        <asp:ListItem Value="11">oct - nov</asp:ListItem>
                        <asp:ListItem Value="12">nov - dec</asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddlNominaleAnul" runat="server" Width="80" CssClass="form-control col-lg-6">
                        <asp:ListItem Value="2014">2014</asp:ListItem>
                        <asp:ListItem Value="2015">2015</asp:ListItem>
                        <asp:ListItem Value="2016">2016</asp:ListItem>
                        <asp:ListItem Value="2017">2017</asp:ListItem>
                        <asp:ListItem Value="2018">2018</asp:ListItem>
                        <asp:ListItem Value="2019">2019</asp:ListItem>
                        <asp:ListItem Value="2020">2020</asp:ListItem>
                        <asp:ListItem Value="2020">2021</asp:ListItem>
                        <asp:ListItem Value="2020">2022</asp:ListItem>
                        <asp:ListItem Value="2020">2023</asp:ListItem>
                        <asp:ListItem Value="2020">2024</asp:ListItem>
                        <asp:ListItem Value="2020">2025</asp:ListItem>
                        <asp:ListItem Value="2020">2026</asp:ListItem>
                        <asp:ListItem Value="2020">2027</asp:ListItem>
                        <asp:ListItem Value="2020">2028</asp:ListItem>
                        <asp:ListItem Value="2020">2029</asp:ListItem>
                        <asp:ListItem Value="2020">2030</asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddlOrdonareRapoarteNominale" runat="server" CssClass="form-control col-md-6" Width="273">
                        <asp:ListItem Value="1">ordonare după coeficient restante</asp:ListItem>
                        <asp:ListItem Value="2">ordonare după denumire chiriași</asp:ListItem>
                        <asp:ListItem Value="3">ordonare după denumire locație</asp:ListItem>
                    </asp:DropDownList>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="Panel10" runat="server" CssClass="row form-group">
                <asp:Label ID="LabelSocietate" CssClass="col-md-2 control-label" runat="server" Text="Societatea: " AssociatedControlID="DropDownListSocietate"></asp:Label>
                <asp:Panel runat="server" CssClass="col-md-10" Style="left: 0px; top: -1px">
                    <asp:Panel runat="server" CssClass="row">
                        <asp:DropDownList ID="DropDownListSocietate" runat="server" DataSourceID="SocietatiDS" DataTextField="Societate" DataValueField="idFacturare" CssClass="form-control col-md-6" Width="180">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SocietatiDS" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT * FROM [Societati] ORDER BY Societate"></asp:SqlDataSource>
                    </asp:Panel>
                    <asp:Panel runat="server" CssClass="row">
                        <asp:Button ID="Button1" CssClass="btn btn-danger" runat="server" Text="Situatia Incasarilor Chirii si Utilitati" OnClick="btChiriiUtilitati_Click" />
                        <asp:Button ID="Button3" Visible="true" CssClass="btn btn-primary" runat="server" Text="Chiriasi Plecati" OnClick="btChiriiUtilitatiInactivi_Click" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>

    <asp:Panel ID="pnSinteticeLunaAleasa" runat="server" CssClass="form-horizontal">
        <asp:Panel ID="pn" runat="server" CssClass="col-md-2"></asp:Panel>
        <asp:Panel ID="Panel17" runat="server" CssClass="row col-md-10">
            <h2>Rapoarte SINTETICE Chirii si Utilitati</h2>
        </asp:Panel>
        <asp:Panel ID="Panel18" runat="server" CssClass="row form-group">
            <asp:Label ID="Label2" CssClass="col-md-2 control-label small" runat="server" AssociatedControlID="ddlSinteticeLuna">Contracte active la 01.</asp:Label>
            <asp:Panel runat="server" CssClass="col-md-10" Style="left: 0px; top: -1px">
                <asp:DropDownList ID="ddlSinteticeLuna" runat="server" Width="110" CssClass="form-control col-lg-6">
                    <asp:ListItem Value="1">ianuarie</asp:ListItem>
                    <asp:ListItem Value="2">februarie</asp:ListItem>
                    <asp:ListItem Value="3">martie</asp:ListItem>
                    <asp:ListItem Value="4">aprilie</asp:ListItem>
                    <asp:ListItem Value="5">mai</asp:ListItem>
                    <asp:ListItem Value="6">iunie</asp:ListItem>
                    <asp:ListItem Value="7">iulie</asp:ListItem>
                    <asp:ListItem Value="8">august</asp:ListItem>
                    <asp:ListItem Value="9">septembrie</asp:ListItem>
                    <asp:ListItem Value="10">octombrie</asp:ListItem>
                    <asp:ListItem Value="11">noiembrie</asp:ListItem>
                    <asp:ListItem Value="12">decembrie</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="ddlSinteticeAnul" runat="server" Width="80" CssClass="form-control col-lg-6">
                    <asp:ListItem Value="2014">2014</asp:ListItem>
                    <asp:ListItem Value="2015">2015</asp:ListItem>
                    <asp:ListItem Value="2016">2016</asp:ListItem>
                    <asp:ListItem Value="2017">2017</asp:ListItem>
                    <asp:ListItem Value="2018">2018</asp:ListItem>
                    <asp:ListItem Value="2019">2019</asp:ListItem>
                    <asp:ListItem Value="2020">2020</asp:ListItem>
                    <asp:ListItem Value="2020">2021</asp:ListItem>
                    <asp:ListItem Value="2020">2022</asp:ListItem>
                    <asp:ListItem Value="2020">2023</asp:ListItem>
                    <asp:ListItem Value="2020">2024</asp:ListItem>
                    <asp:ListItem Value="2020">2025</asp:ListItem>
                    <asp:ListItem Value="2020">2026</asp:ListItem>
                    <asp:ListItem Value="2020">2027</asp:ListItem>
                    <asp:ListItem Value="2020">2028</asp:ListItem>
                    <asp:ListItem Value="2020">2029</asp:ListItem>
                    <asp:ListItem Value="2020">2030</asp:ListItem>
                </asp:DropDownList>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="Panel15" runat="server" CssClass="row form-group">
            <asp:Label ID="Label5" CssClass="col-md-2 control-label" runat="server" Text="" AssociatedControlID="btRapoarteSinteticeLunaAleasa"></asp:Label>
            <asp:Panel runat="server" CssClass="col-md-10">
                <asp:Button ID="btRapoarteSinteticeLunaAleasa" CssClass="btn btn-danger" runat="server" Text="Raport Sintetic pe Luna Aleasa" OnClick="btRapoarteSinteticeLunaAleasa_Click" />
                <asp:Button ID="Button2" Visible="true" CssClass="btn btn-primary" runat="server" Text="Chiriasi Plecati" OnClick="btRapoarteSinteticeLunaAleasaInactivi_Click" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnSintetice" runat="server" CssClass="form-horizontal">
        <asp:Panel ID="Panel12" runat="server" CssClass="col-md-2"></asp:Panel>
        <asp:Panel ID="Panel13" runat="server" CssClass="row col-md-10">
        </asp:Panel>
        <asp:Panel ID="pnBtSintetice" runat="server" CssClass="row form-group">
            <asp:Label ID="Label4" CssClass="col-md-2 control-label" runat="server" AssociatedControlID="btRapoarteSintetice">(<%=DateTime.Now.AddDays(-DateTime.Now.Day).ToString("MMMM") %>)</asp:Label>
            <asp:Panel runat="server" CssClass="col-md-10">
                <asp:Button ID="btRapoarteSintetice" CssClass="btn btn-primary" runat="server" Text="Raport Sintetic Luna Anterioara" OnClick="btRapoarteSinteticeLunaAnterioara_Click" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="Panel3" runat="server" CssClass="form-horizontal">
        <asp:Panel ID="Panel5" runat="server" CssClass="col-md-2"></asp:Panel>
        <asp:Panel ID="Panel11" runat="server" CssClass="row col-md-10">
        </asp:Panel>
        <asp:Panel ID="Panel14" runat="server" CssClass="row form-group">
            <asp:Label ID="Label3" CssClass="col-md-2 control-label" runat="server" AssociatedControlID="DropDownListSocietate">(<%=DateTime.Now.ToString("MMMM") %>)</asp:Label>
            <asp:Panel runat="server" CssClass="col-md-10">
                <asp:Button ID="btRapoarteSinteticeLunaCurenta" CssClass="btn btn-primary" runat="server" Text="Raport Sintetic Luna Curenta" OnClick="btRapoarteSinteticeLunaCurenta_Click" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
