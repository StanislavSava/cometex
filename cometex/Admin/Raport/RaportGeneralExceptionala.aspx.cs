﻿using cometex.App_Service;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cometex.Admin.Raport
{
    public partial class RaportGeneralExceptionala : System.Web.UI.Page
    {
        DateTime dateStart;
        DateTime dateEnd;
        Investitie investitie;

        string societate;
        string idSocietate =string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ExtrageParametri();
                IncarcaRaport();
                AdaugaParametriiPeRaport();

                AfiseazaInFormatPDF();
            }
        }

        private void AfiseazaInFormatPDF()
        {
            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string fileNameExtension;
            byte[] bytes = ReportViewer1.LocalReport.Render("PDF", null, out mimeType, out encoding, out fileNameExtension, out streamids, out warnings);
            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "inline; filename=Nume" + fileNameExtension);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
            Response.Close();
        }

        private void AdaugaParametriiPeRaport()
        {
            ReportParameter[] param = new ReportParameter[3];


            param[0] = new ReportParameter("societate", investitie.Societate.Nume);
            param[1] = new ReportParameter("dateTimeStart", dateStart.ToString("dd.MM.yyyy"));
            param[2] = new ReportParameter("dateTimeEnd", dateEnd.ToString("dd.MM.yyyy"));

            ReportViewer1.LocalReport.SetParameters(param);
            ReportViewer1.LocalReport.Refresh();
        }

        private void IncarcaRaport()
        {
            DSReportTableAdapters.RaportGeneralTableAdapter adapter = new DSReportTableAdapters.RaportGeneralTableAdapter();
            DSReport.RaportGeneralDataTable table = new DSReport.RaportGeneralDataTable();

            adapter.FillByGeneralExceptionala(table, investitie.Societate.Id, dateStart, dateEnd);
            ReportDataSource reportDataSource = new ReportDataSource("DataSet", (DataTable)table);

            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);
        }

        private void ExtrageParametri()
        {
            investitie =(Investitie)Session["investitie"];
            dateEnd = Convert.ToDateTime(Session["dateEnd"]);
            dateStart = Convert.ToDateTime(Session["dateStart"]); 
        }
    }
}