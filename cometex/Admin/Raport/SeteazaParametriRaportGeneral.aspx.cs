﻿using cometex.Admin.Raport;
using cometex.App_Service;
using cometex.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cometex.Test
{
    public partial class BetaAlegeRaport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                Master.AtribuieButoaneMeniu(MeniuUtils.AranjeazaElementeMeniu(22, new List<int> { 3,9 }));
                Session["investitie"] = null;
                Session["dateStart"] = null;
                Session["dateEnd"] = null;
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Clear();
            TrimiteInformatiile();

        }

        private void TrimiteInformatiile()
        {
            try
            {
                Investitie investitie = new Investitie();
                investitie.Societate.Id = DropDownListSocietate.SelectedValue.ToString();
                investitie.Societate.Nume = DropDownListSocietate.SelectedItem.Text;
                Session["investitie"] = investitie;

                Session["dateStart"] = Convert.ToDateTime(TextDateStart.Text).ToString("s");

                Session["dateEnd"] = Convert.ToDateTime(TextDateEnd.Text).ToString("s");

                if (Convert.ToDateTime(Session["dateStart"]) < Convert.ToDateTime(Session["dateEnd"]))
                {
                    //string url = "RaportGeneral.aspx";
                    //string popupScript = "<script language=\"javascript\">" + "window.open('" + url + "', 'newWindow', 'left=2, top=2,location=no, width=1010, height=660, menubar=no, resizable=yes,statusbar=yes,scrollbars=yes');" + "</script>";
                    //Page.RegisterStartupScript("Redirect", popupScript);
                    Response.Redirect("RaportGeneral.aspx");
                }
                else
                {
                    MessageLabel.Text = "Va rugam sa alegeti o perioada corecta !";
                }
            }
            catch
            {
                MessageLabel.Text = "Va rugam completati toate casutele !";
            }
        }

        void Clear()
        {
            MessageLabel.Text = "";
        }

        protected void DropDownListSocietate_DataBound(object sender, EventArgs e)
        {
            ListItem listItem = new ListItem();
            listItem.Text = "Toate";
            listItem.Value = "%";
            DropDownListSocietate.Items.Insert(0, listItem);
        }
    }


}