﻿using cometex.Admin.Raport;
using cometex.App_Service;
using cometex.Clase;
using cometex.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cometex.Admin.Raport
{
    public partial class SeteazaParametriRaportCashFlow : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Master.AtribuieButoaneMeniu(MeniuUtils.AranjeazaElementeMeniu(22, new List<int> { 3, 14 }));
              //  Session["dataSoldPrecedent"] = TextDateStart.Text = DateTime.Now.AddDays(-1).ToString("dd.MM.yyyy");
             //   Session["dataSoldFinal"] = TextDateEnd.Text = DateTime.Now.ToString("dd.MM.yyyy");
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Clear();
            TrimiteInformatiile();

        }

        private void TrimiteInformatiile()
        {
            try
            {
                Investitie investitie = new Investitie();
                //Session["dataSoldPrecedent"] = Convert.ToDateTime(TextDateStart.Text).ToString("dd.MM.yyyy");
              //  Session["dataSoldPrecedent"] = Convert.ToDateTime(TextDateEnd.Text).AddDays(-1).ToString("dd.MM.yyyy");
              //  Session["dataSoldFinal"] = Convert.ToDateTime(TextDateEnd.Text).ToString("dd.MM.yyyy");

                //if (Convert.ToDateTime(Session["dateStart"]) < Convert.ToDateTime(Session["dateEnd"]))
                //{
                //    string url = "RaportCashFlow.aspx";

                //    string popupScript = "<script language=\"javascript\">" + "window.open('" + url + "', 'newWindow', 'left=2, top=2,location=no, width=1010, height=660, menubar=no, resizable=yes,statusbar=yes,scrollbars=yes');" + "</script>";
                //    Page.RegisterStartupScript("Redirect", popupScript);
                //}
                //else
                //{
                //    MessageLabel.Text = "Va rugam sa alegeti o perioada corecta !";
                //}
                Response.Redirect("RaportCashFlow.aspx");
            }
            catch
            {
              //  MessageLabel.Text = "Eroare !";
            }
        }
        private void TrimiteInformatiileRaportGrila()
        {

            try
            {
                //Investitie investitie = new Investitie();
               // investitie.Societate.Id = DropDownListSocietateRapoarte.SelectedValue.ToString();
               // investitie.Societate.Nume = DropDownListSocietateRapoarte.SelectedItem.Text;
              //  Session["investitie"] = investitie;

               // Session["dateStart"] = Convert.ToDateTime(TextDateStartRapCheltuileiGrila.Text).ToString("s");

               // Session["dateEnd"] = Convert.ToDateTime(TextDateEndRapCheltuieliGrila.Text).ToString("s");
                
                if (Convert.ToDateTime(Session["dateStart"]) < Convert.ToDateTime(Session["dateEnd"]))
                {
                    //string url = "RaportGeneralGrila_Test.aspx";
                    //string popupScript = "<script language=\"javascript\">" + "window.open('" + url + "', 'newWindow', 'left=2, top=2,location=no, width=1010, height=660, menubar=no, resizable=yes,statusbar=yes,scrollbars=yes');" + "</script>";
                    //Page.RegisterStartupScript("Redirect", popupScript);
                    Response.Redirect("RaportGeneralGrila_Test.aspx");
                }
                else
                {
                 //   MessageLabel.Text = "Va rugam sa alegeti o perioada corecta !";
                }
            }
            catch
            {
              ///  MessageLabel.Text = "Va rugam completati toate casutele !";
            }
        }
            void Clear()
            {
              //  MessageLabel.Text = "";
              //  MessageLabelRT.Text = "";
            }

        protected void ButtonAfiseazaRaport_Click(object sender, EventArgs e)
        {
            Clear();
            TrimiteInformatiileRaportGrila();
        }

        protected void DropDownListSocietateRapoarte_DataBound(object sender, EventArgs e)
        {
            ListItem listItem = new ListItem();
            listItem.Text = "Toate";
            listItem.Value = "%";
            //DropDownListSocietateRapoarte.Items.Insert(0, listItem);
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            CheltuieliGenerale objRaportCheltuieliGrila = new CheltuieliGenerale();
            objRaportCheltuieliGrila.CampuriSocietati();
        }
    }
}