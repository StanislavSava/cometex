﻿using cometex.App_Service;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cometex.Admin.Raport
{
    public partial class RaportDebitori : System.Web.UI.Page
    {
        DateTime dateStart;
        DateTime dateEnd;
        Investitie investitie;

        string societate;
        string idSocietate =string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IncarcaRaport();
              //  AfiseazaInFormatPDF();
            }
        }

        private void AfiseazaInFormatPDF()
        {
            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string fileNameExtension;
            byte[] bytes = ReportViewer1.LocalReport.Render("PDF", null, out mimeType, out encoding, out fileNameExtension, out streamids, out warnings);
            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "inline; filename=Nume" + fileNameExtension);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
            Response.Close();
        }

        private void IncarcaRaport()
        {
            ReportViewer1.LocalReport.DataSources.Clear();

            //DSReportTableAdapters.RaportGeneralTableAdapter adapter = new DSReportTableAdapters.RaportGeneralTableAdapter();
            //DSReport.RaportGeneralDataTable table = new DSReport.RaportGeneralDataTable();

            List<Utils.DataSourceChiriasi.Debitor> date = Utils.DataSourceChiriasi.RaportDebitoriAlimentara();
            ReportDataSource reportDataSource = new ReportDataSource("RaportDebitoriAlimentara", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportDebitoriAstral();
            reportDataSource = new ReportDataSource("RaportDebitoriAstral", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportDebitoriCometex();
            reportDataSource = new ReportDataSource("RaportDebitoriCometex", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportDebitoriComtex();
            reportDataSource = new ReportDataSource("RaportDebitoriComtex", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportDebitoriGaleriileVictoria();
            reportDataSource = new ReportDataSource("RaportDebitoriGaleriileVictoria", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportDebitoriMinerva();
            reportDataSource = new ReportDataSource("RaportDebitoriMinerva", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportDebitoriMinervaGaleriileComerciale();
            reportDataSource = new ReportDataSource("RaportDebitoriMinervaGaleriileComerciale", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportDebitoriRomana();
            reportDataSource = new ReportDataSource("RaportDebitoriRomana", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            date = Utils.DataSourceChiriasi.RaportDebitoriUnicom();
            reportDataSource = new ReportDataSource("RaportDebitoriUnicom", date);
            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);

            ReportParameter ordonare = new ReportParameter("ordonare", Session["ordonareRaportDebitori"].ToString());
            ReportViewer1.LocalReport.SetParameters(ordonare);

        }
    }
}