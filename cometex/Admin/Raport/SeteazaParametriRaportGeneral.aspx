﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Ad.master" AutoEventWireup="true" CodeBehind="SeteazaParametriRaportGeneral.aspx.cs" Inherits="cometex.Test.BetaAlegeRaport" Culture="ro-RO" UICulture="ro-RO" %>

<%@ MasterType VirtualPath="~/Admin/Ad.master" %>
<%@ Reference VirtualPath="~/Cometex.Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel CssClass="container" runat="server">
        <asp:Panel ID="PanelGeneral" runat="server" CssClass="container-fluid ">
            <asp:Panel ID="PanelTitlu" runat="server" CssClass="block">
                <asp:Label ID="LabelTitlu" runat="server" Text="Raport General" CssClass="h2" /><br /><br />
            </asp:Panel>
            <asp:Panel ID="Panel3" runat="server" CssClass="block">
                <asp:Panel ID="Panel5" runat="server" CssClass="form-group">
                    <asp:Label ID="LabelSocietate" runat="server" Text="Raportare pentru societatea: "></asp:Label>
                    <asp:DropDownList ID="DropDownListSocietate" runat="server" DataSourceID="SocietatiDS" DataTextField="Societate" DataValueField="IdSocietate" OnDataBound="DropDownListSocietate_DataBound">
                    </asp:DropDownList>
                </asp:Panel>
                <asp:Panel ID="Panel2" runat="server" CssClass="form-group">
                    <asp:Label ID="LabelDataStart" runat="server" Text="In perioada: "></asp:Label>
                    <asp:TextBox ID="TextDateStart" runat="server" OnClick="TextDateStartClick"></asp:TextBox>
                    <ajaxtoolkit:CalendarExtender ID="calExtender4" runat="server"
                        PopupButtonID="TextDateStart" PopupPosition="TopRight"
                        TargetControlID="TextDateStart" Format="dd/MM/yyyy"></ajaxtoolkit:CalendarExtender>
                </asp:Panel>
                <asp:Panel ID="Panel1" runat="server" CssClass="form-group">
                    <asp:Label ID="LabelDataEnd" runat="server" Text=" pana la: "></asp:Label>
                    <asp:TextBox ID="TextDateEnd" runat="server"></asp:TextBox>
                    <ajaxtoolkit:CalendarExtender ID="Calendarextender1" runat="server"
                        PopupButtonID="CalendarButton2" PopupPosition="TopRight"
                        TargetControlID="TextDateEnd" Format="dd/MM/yyyy"></ajaxtoolkit:CalendarExtender>
                </asp:Panel>
                <asp:Panel ID="Panel8" runat="server" CssClass="form-group">
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <asp:Label ID="MessageLabel" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                    <asp:SqlDataSource ID="SocietatiDS" runat="server" ConnectionString="<%$ ConnectionStrings:CometexInvestitii_dbConnectionString %>" SelectCommand="SELECT [IdSocietate], [Societate] FROM [Societati]"></asp:SqlDataSource>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="Panel4" runat="server" CssClass="container-fluid ">
                <asp:Panel ID="Panel6" runat="server" CssClass="row">
                    <asp:Button ID="Button1" CssClass="btn btn-primary" runat="server" Text="Afiseaza Raport" OnClick="Button1_Click" />
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
