﻿using cometex.App_Service;
using cometex.Clase;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cometex.Admin.Raport
{
    public partial class RaportSintezaCheltuieli : System.Web.UI.Page
    {
        DateTime dateStart;
        DateTime dateEnd;
        Investitie investitie;

       // string societate;
        string idSocietate = string.Empty;
        private void AfiseazaInFormatPDF()
        {
            try {
               
                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string fileNameExtension;
                byte[] bytes = ReportViewer_Test.LocalReport.Render("PDF", null, out mimeType, out encoding, out fileNameExtension, out streamids, out warnings);
                Response.Buffer = true;
                Response.Clear();
                Response.ContentType = mimeType;
                Response.AddHeader("content-disposition", "inline; filename=Nume" + fileNameExtension);
                Response.BinaryWrite(bytes);
                Response.Flush();
                Response.End();
                Response.Close();
            } catch (Exception ex_RenderPDF)
            {
               // Label_Temp_Errors.Text += "\n AfiseazaInFormatPDF :: " + ex_RenderPDF.Message;
            }

        }

        private void AdaugaParametriiPeRaport()
        {
            try {

                ReportParameter[] param = new ReportParameter[2];
                param[0] = new ReportParameter("dateStart", dateStart.ToString("dd.MM.yyyy"));
                param[1] = new ReportParameter("dateEnd", dateEnd.ToString("dd.MM.yyyy"));
                ReportViewer_Test.LocalReport.SetParameters(param);
                ReportViewer_Test.LocalReport.Refresh();
            }
            catch (Exception ex_AdaugaParametriiPeRaport)

            {
              //  Label_Temp_Errors.Text += "\n AdaugaParametriiPeRaport :: "+ex_AdaugaParametriiPeRaport.Message;
            }
        }

        private void IncarcaRaport()
        {
            try {

               CheltuieliGenerale objRaportCheltuieliGrila = new CheltuieliGenerale();
                DSReport.RaportGeneralDataTable table = new DSReport.RaportGeneralDataTable();
                List<CheltuialaDate> date = objRaportCheltuieliGrila.CampuriSocietati();
                ReportDataSource reportDataSource = new ReportDataSource("DataSetRaportGeneral", date);

                //  ReportDataSource reportDataSource = objRaportCheltuieliGrila.CampuriSocietati(dateStart.ToString(), dateEnd.ToString());

                ReportViewer_Test.LocalReport.DataSources.Clear();
                ReportViewer_Test.LocalReport.DataSources.Add(reportDataSource);
            }
            catch(Exception ex_IncarcaRaport)
            {
              //  Label_Temp_Errors.Text += "\n AduRaport :: " + ex_IncarcaRaport.Message;
            }
            
        }

        private void ExtrageParametri()
        {
            investitie = (Investitie)Session["investitie"];
            dateStart = Convert.ToDateTime(Session["dateStart"]);
            dateEnd = Convert.ToDateTime(Session["dateEnd"]);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               ExtrageParametri();
               IncarcaRaport();
               AdaugaParametriiPeRaport();
              // AfiseazaInFormatPDF();
            }

        }
    }
}