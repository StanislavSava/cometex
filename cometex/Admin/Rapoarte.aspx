﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Ad.master" AutoEventWireup="true" CodeBehind="Rapoarte.aspx.cs" Inherits="cometex.Admin.Rapoarte" %>

<%@ MasterType VirtualPath="~/Admin/Ad.master" %>
<%@ Reference VirtualPath="~/Cometex.Master" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="PanelGeneral" runat="server" CssClass="panel">
        <asp:Panel ID="Panel3" runat="server" CssClass="block">
            <asp:Panel ID="Panel5" runat="server" CssClass="inline">
                <asp:HyperLink ID="RaportGeneral" runat="server" NavigateUrl="~/Admin/Raport/SeteazaParametriRaportGeneral.aspx">Raport General</asp:HyperLink>
                <asp:HyperLink ID="RaportNominal" runat="server" NavigateUrl="~/Admin/Raport/SeteazaParametriRaportNominal.aspx">Raport Nominal</asp:HyperLink>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
