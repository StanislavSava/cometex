﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using cometex.Clase;
using cometex.Utils;

namespace cometex.Admin
{
    public partial class PlafoaneGrila : System.Web.UI.Page
    {
        protected void Page_PreRenderComplete(object sender, EventArgs e)
        {
            btSterge.Visible = ButonModifica.Visible = GridListaPlafoane.SelectedIndex == -1 ? false : true;
            AscundeToateButoanele();
            if (IsPostBack) PopuleazaTextBoxSuma();
            else { DupaAdaugaSauRenunta(); ButonModifica.Visible = false; ButonRenunta.Visible = false; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            AscundeToateButoanele();
            if (!IsPostBack)
            {
                Master.AtribuieButoaneMeniu(MeniuUtils.AranjeazaElementeMeniu(22, new List<int> { 4, 8 }));
                DropDownListSocietati.DataBind();
                DropDownListSocietati.SelectedIndex = 0;
                GridListaPlafoane.DataBind();
                DupaAdaugaSauRenunta();

            }

        }

        protected void ButonAdauga_Click(object sender, EventArgs e)
        {
            Cheltuieli plafon = new Cheltuieli();
            plafon.CategorieCheltuiala.Id = Convert.ToInt32(DropDownListCategoriiCheltuieli.SelectedValue);
            plafon.CheltuialaBrut = Convert.ToDecimal(TextBoxSuma.Text.Replace(".",","));
            plafon.Societatea.Id = Convert.ToInt32(DropDownListSocietati.SelectedValue);
            DatabaseUtils.InsereazaPlafon(plafon);
            GridListaPlafoane.DataBind();
            DupaAdaugaSauRenunta();
        }

        protected void ButonModifica_Click(object sender, EventArgs e)
        {
            Cheltuieli plafon = new Cheltuieli();
            plafon.CheltuialaBrut = Convert.ToDecimal(TextBoxSuma.Text.Replace(".", ","));
            plafon.IdCheltuieli = Convert.ToInt32(GridListaPlafoane.SelectedDataKey.Value.ToString());
            DatabaseUtils.ModificaPlafon(plafon);
            GridListaPlafoane.DataBind();
            DupaAdaugaSauRenunta();
        }
        protected void btSterge_Click(object sender, EventArgs e)
        {
            Cheltuieli plafon = new Cheltuieli();
            plafon.IdCheltuieli = Convert.ToInt32(GridListaPlafoane.SelectedDataKey.Value.ToString());
            DatabaseUtils.StergePlafon(plafon);
            GridListaPlafoane.DataBind();
            DupaAdaugaSauRenunta();
        }


        protected void GridListaPlafoane_SelectedIndexChanged(object sender, EventArgs e)
        {
            StareDropdownPentruModifica();
            try
            {
                DropDownListCategoriiCheltuieli.SelectedValue = ((Label)GridListaPlafoane.SelectedRow.Cells[3].FindControl("idCategorie")).Text;
            }
            catch
            {
                Cheltuieli plafon = new Cheltuieli();
                plafon.IdCheltuieli = Convert.ToInt32(GridListaPlafoane.SelectedDataKey.Value.ToString());
                DatabaseUtils.StergePlafon(plafon);
                GridListaPlafoane.DataBind();
                DupaAdaugaSauRenunta();
                return;
            }
            TextBoxSuma.Text = GridListaPlafoane.SelectedRow.Cells[1].Text;

        }
        protected void StareDropdownPentruModifica()
        {
        }
        protected void StareDropdownPentruAdauga()
        {
            DropDownListSocietati.Enabled = true;
            DropDownListCategoriiCheltuieli.Enabled = true;
            TextBoxSuma.Text = string.Empty;
        }
        protected void PopuleazaTextBoxSuma()
        {
            foreach (GridViewRow row in GridListaPlafoane.Rows)
            {
                if (((Label)row.Cells[3].FindControl("idCategorie")).Text == DropDownListCategoriiCheltuieli.SelectedValue)
                {
                    StareDropdownPentruModifica();
                    TextBoxSuma.Text = row.Cells[1].Text;
                    GridListaPlafoane.SelectedIndex = row.RowIndex;
                    return;
                }

            }
            //if (DropDownListCategoriiCheltuieli.SelectedValue != "0")
            //    ButonAdauga.Visible = true;
            //else ButonAdauga.Visible = false;

        }
        protected void ButonRenunta_Click(object sender, EventArgs e)
        {
            StareDropdownPentruAdauga();
            GridListaPlafoane.DataBind();
            GridListaPlafoane.SelectedIndex = -1;
            DupaAdaugaSauRenunta();
        }
        protected void DupaAdaugaSauRenunta()
        {
            GridListaPlafoane.SelectedIndex = -1;
            AscundeToateButoanele();
            ListItem goala = new ListItem();
            goala.Text = "selecteaza o categorie";
            goala.Value = "0";
            foreach (ListItem categorie in DropDownListCategoriiCheltuieli.Items)
            {
                if (categorie.Value == "0")
                {
                    DropDownListCategoriiCheltuieli.SelectedValue = "0";
                    return;
                }
            }
            DropDownListCategoriiCheltuieli.Items.Add(goala);
            DropDownListCategoriiCheltuieli.SelectedValue = "0";

        }
        protected void AscundeToateButoanele()
        {
            ButonAdauga.Visible = (GridListaPlafoane.SelectedIndex == -1) && (DropDownListCategoriiCheltuieli.SelectedValue != "0") && (DropDownListCategoriiCheltuieli.SelectedValue != "");
            ButonModifica.Visible = (GridListaPlafoane.SelectedIndex != -1);
            ButonRenunta.Visible = (GridListaPlafoane.SelectedIndex != -1);

            DropDownListCategoriiCheltuieli.Enabled = (GridListaPlafoane.SelectedIndex == -1);
            DropDownListSocietati.Enabled = (GridListaPlafoane.SelectedIndex == -1);

        }
        protected void DropDownListSocietati_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopuleazaTextBoxSuma();
        }

        protected void DropDownListCategoriiCheltuieli_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopuleazaTextBoxSuma();
        }

    }
}