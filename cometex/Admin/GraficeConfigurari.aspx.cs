﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using cometex.Clase;
using cometex.Utils;
using System.Data;

namespace cometex.Admin
{
    public partial class GraficeConfigurari : System.Web.UI.Page
    {
        List<Button> ButoaneVizibile = new List<Button> { };
        List<Button> ButoaneAscunse = new List<Button> { };
        protected void Page_PreRender(object sender, EventArgs e)
        {
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Master.AtribuieButoaneMeniu(MeniuUtils.AranjeazaElementeMeniu(22, new List<int> { 4, 12 }));
                GridListaGrafice.DataBind();
                if (GridListaGrafice.SelectedIndex == -1)
                {
                    ButonAdauga.Visible = true;
                    ButonModifica.Visible = false;
                    ButonSterge.Visible = false;
                    GridListaGrafice.SelectedIndex = -1;
                }
                else
                {
                    ButonAdauga.Visible = true;
                    ButonModifica.Visible = true;
                    ButonSterge.Visible = true;
                }
            }
        }
        protected void GridListaGrafice_SelectedIndexChanged(object sender, EventArgs e)
        {
            ButonAdauga.Visible = true;
            ButonModifica.Visible = true;
            ButonSterge.Visible = true;
        }
        protected void Adauga_Click(object sender, EventArgs e)
        {
            PanelAdauga.Visible = true;
            PanelListaGrafice.Visible = false;
            tbDescriere.Text = "";
            tbGrafic.Text = "";
            Session["TipActiune"] = "0";
        }
        protected void Modifica_Click(object sender, EventArgs e)
        {
            PanelAdauga.Visible = true;
            PanelListaGrafice.Visible = false;
            tbGrafic.Text = GridListaGrafice.SelectedRow.Cells[2].Text;
            tbDescriere.Text = GridListaGrafice.SelectedRow.Cells[3].Text;
            Session["TipActiune"] = "1";
        }
        protected void Sterge_Click(object sender, EventArgs e)
        {
            if (GridListaGrafice.SelectedIndex >= 0)
            {
                String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();
                String cmdStr = "delete from grafice where id = @idGrafic";
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("idGrafic", GridListaGrafice.SelectedDataKey[0]);
                cmd.ExecuteNonQuery();
                GridListaGrafice.DataBind();
                GridListaGrafice.SelectedIndex = -1;
                con.Close();
                ButonAdauga.Visible = true;
                ButonModifica.Visible = false;
                ButonSterge.Visible = false;
                GridListaGrafice.SelectedIndex = -1;
                GridListaGrafice.DataBind();
            }
        }
        protected void ButonSalveaza_Click(object sender, EventArgs e)
        {
            String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            String cmdStr = "";

            if (Session["TipActiune"].ToString() == "1")
            {
                cmdStr = @"UPDATE [dbo].[Grafice]
                           SET [denumire] = @grafic
                               ,[descriere] = @descriere
                         WHERE id = @id";
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("id", GridListaGrafice.SelectedDataKey[0]);
                cmd.Parameters.AddWithValue("grafic", tbGrafic.Text);
                cmd.Parameters.AddWithValue("descriere", tbDescriere.Text);

                cmd.ExecuteNonQuery();
            }
            else if (Session["TipActiune"].ToString() == "0")
            {
                cmdStr = @"INSERT INTO [dbo].[Grafice]   ([denumire],[descriere])  VALUES (@grafic, @descriere)";
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("grafic", tbGrafic.Text);
                cmd.Parameters.AddWithValue("descriere", tbDescriere.Text);
                cmd.ExecuteNonQuery();
            }
            con.Close();
            GridListaGrafice.DataBind();
            PanelAdauga.Visible = false;
            PanelListaGrafice.Visible = true;
            ButonAdauga.Visible = true;
            ButonModifica.Visible = false;
            ButonSterge.Visible = false;
            GridListaGrafice.SelectedIndex = -1;
            GridListaGrafice.DataBind();
        }
        protected void ButonRenunta_Click(object sender, EventArgs e)
        {
            PanelAdauga.Visible = false;
            PanelListaGrafice.Visible = true;
        }

        protected void btnFacturare_Click(object sender, EventArgs e)
        {
            //List<List<string>> caleProgramSagaFolderSocietati = SAGA.SAGAListaSocietati();
            //Perioade perioada = SAGA.PerioadaSold(DateTime.Now.ToString("dd.MM.yyyy"));
            //string cont = "5121";
            //string caleProgramSaga = "D:\\SAGA C.3.0";

            //List<DataTable> date = SAGA.SagaValoriCont(perioada.DataInceput, perioada.DataFinal, cont, caleProgramSaga, caleProgramSagaFolderSocietati);

        }

        protected void DropDownListCont_DataBound(object sender, EventArgs e)
        {

        }

        protected void btnAdaugaContDinDdl_Click(object sender, EventArgs e)
        {

        }

        protected void btnReseteazaConturi_Click(object sender, EventArgs e)
        {
        }

        protected void ddlSocietati_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
    }
}