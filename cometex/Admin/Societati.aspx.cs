﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using cometex.Clase;
using cometex.Utils;

namespace cometex.Admin
{
    public partial class Societati : System.Web.UI.Page
    {
        List<Button> ButoaneVizibile = new List<Button> { };
        List<Button> ButoaneAscunse = new List<Button> { };
        protected void Page_PreRender(object sender, EventArgs e)
        {
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Master.AtribuieButoaneMeniu(MeniuUtils.AranjeazaElementeMeniu(22, new List<int> { 4, 7 }));
                GridListaSocietati.DataBind();
                if (GridListaSocietati.SelectedIndex == -1)
                {
                    ButonAdauga.Visible = true;
                    ButonModifica.Visible = false;
                    ButonSterge.Visible = false;
                    GridListaSocietati.SelectedIndex = -1;
                }
                else
                {
                    ButonAdauga.Visible = true;
                    ButonModifica.Visible = true;
                    ButonSterge.Visible = true;
                }
            }
        }
        public static void AranjeazaGrid(GridView gv)
        {
            gv.DataBind();
            gv.UseAccessibleHeader = true;
            gv.HeaderRow.TableSection = TableRowSection.TableHeader;
            TableCellCollection cells = gv.HeaderRow.Cells;
            cells[0].Attributes.Add("data-hide", "phone,tablet");
            cells[1].Attributes.Add("data-class", "expand");
            cells[2].Attributes.Add("data-class", "expand");
        }
        protected void GridListaSocietati_SelectedIndexChanged(object sender, EventArgs e)
        {
            ButonAdauga.Visible = true;
            ButonModifica.Visible = true;
            ButonSterge.Visible = true;
        }
        protected void Adauga_Click(object sender, EventArgs e)
        {
            PanelAdauga.Visible = true;
            PanelListaCereri.Visible = false;
            Societate.Text = "";
            Session["TipActiune"] = "0";
        }
        protected void Modifica_Click(object sender, EventArgs e)
        {
            PanelAdauga.Visible = true;
            PanelListaCereri.Visible = false;
            Societate.Text = GridListaSocietati.SelectedRow.Cells[2].Text.ToString();
            Session["TipActiune"] = "1";
        }
        protected void Sterge_Click(object sender, EventArgs e)
        {
            if (GridListaSocietati.SelectedIndex >= 0)
            {
                String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();
                String cmdStr = "delete from societati where idSocietate = @idSocietate";
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("idSocietate", GridListaSocietati.SelectedDataKey[0]);
                cmd.ExecuteNonQuery();
                GridListaSocietati.DataBind();
                GridListaSocietati.SelectedIndex = -1;
                con.Close();
                ButonAdauga.Visible = true;
                ButonModifica.Visible = false;
                ButonSterge.Visible = false;
                GridListaSocietati.SelectedIndex = -1;
                GridListaSocietati.DataBind();
            }
        }
        protected void ButonSalveaza_Click(object sender, EventArgs e)
        {
            String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            String cmdStr = "";

            if (Session["TipActiune"].ToString() == "1")
            {
                cmdStr = "update societati set societate = @societate where idSocietate = @idSocietate";
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("idSocietate", GridListaSocietati.SelectedDataKey[0]);
                cmd.Parameters.AddWithValue("societate", Societate.Text);
                cmd.ExecuteNonQuery();
            }
            else if (Session["TipActiune"].ToString() == "0" && Societate.Text.Length > 0)
            {
                cmdStr = "insert into Societati (Societate) values ( '" + Societate.Text + "')";
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.Parameters.Clear();
                cmd.ExecuteNonQuery();
            }
            con.Close();
            GridListaSocietati.DataBind();
            PanelAdauga.Visible = false;
            PanelListaCereri.Visible = true;
            ButonAdauga.Visible = true;
            ButonModifica.Visible = false;
            ButonSterge.Visible = false;
            GridListaSocietati.SelectedIndex = -1;
            GridListaSocietati.DataBind();
        }
        protected void ButonRenunta_Click(object sender, EventArgs e)
        {
            PanelAdauga.Visible = false;
            PanelListaCereri.Visible = true;
        }
    }
}