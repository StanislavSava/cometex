﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using cometex.Clase;
using cometex.Utils;
using System.Globalization;

namespace cometex.Admin
{
    public partial class CursBNR : System.Web.UI.Page
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Master.AtribuieButoaneMeniu(MeniuUtils.AranjeazaElementeMeniu(22, new List<int> { 4, 22 }));
                GridListaCursuri.DataBind();
                if (GridListaCursuri.SelectedIndex == -1)
                {
                    ButonAdauga.Visible = true;
                    btAdaugaSus.Visible = true;
                    ButonModifica.Visible = false;
                    btModificaSus.Visible = false;
                    ButonSterge.Visible = false;
                    btStergeSus.Visible = false;
                    GridListaCursuri.SelectedIndex = -1;
                }
                else
                {
                    ButonAdauga.Visible = true;
                    btAdaugaSus.Visible = true;
                    ButonModifica.Visible = true;
                    btModificaSus.Visible = true;
                    ButonSterge.Visible = true;
                    btStergeSus.Visible = true;
                }
            }
        }
        protected void GridListaCursuri_SelectedIndexChanged(object sender, EventArgs e)
        {
            ButonAdauga.Visible = true;
            btAdaugaSus.Visible = true;
            ButonModifica.Visible = true;
            btModificaSus.Visible = true;
            ButonSterge.Visible = true;
            btStergeSus.Visible = true;
        }
        protected void Adauga_Click(object sender, EventArgs e)
        {
            PanelAdauga.Visible = true;
            PanelListaCereri.Visible = false;
            tbDataCurs.Text = DateTime.Now.ToString("dd.MM.yyyy");
            tbValoareCurs.Text = "0";
            Session["TipActiune"] = "0";
        }
        protected void Modifica_Click(object sender, EventArgs e)
        {
            PanelAdauga.Visible = true;
            PanelListaCereri.Visible = false;
            tbValoareCurs.Text = GridListaCursuri.SelectedRow.Cells[2].Text.ToString();
            tbDataCurs.Text = GridListaCursuri.SelectedRow.Cells[3].Text.ToString();
            Session["TipActiune"] = "1";
        }
        protected void Sterge_Click(object sender, EventArgs e)
        {
            if (GridListaCursuri.SelectedIndex >= 0)
            {
                String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();
                String cmdStr = "delete from Cursuri where id = @id";
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("id", GridListaCursuri.SelectedDataKey[0]);
                cmd.ExecuteNonQuery();
                GridListaCursuri.DataBind();
                GridListaCursuri.SelectedIndex = -1;
                con.Close();
                ButonAdauga.Visible = true;
                btAdaugaSus.Visible = true;
                ButonModifica.Visible = false;
                btModificaSus.Visible = false;
                ButonSterge.Visible = false;
                btStergeSus.Visible = false;
                GridListaCursuri.SelectedIndex = -1;
                GridListaCursuri.DataBind();
            }
        }
        protected void ButonSalveaza_Click(object sender, EventArgs e)
        {
            String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            String cmdStr = "";
            using (con)
            {
                try {
                    if (Session["TipActiune"].ToString() == "1")
                    {
                        cmdStr = @"     UPDATE [Cursuri]
                                SET     [Curs] = @Curs
                                        ,[Data] = @Data
                                WHERE Id = @Id";
                        SqlCommand cmd = new SqlCommand(cmdStr, con);
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("Id", GridListaCursuri.SelectedDataKey[0]);
                        cmd.Parameters.AddWithValue("Curs", tbValoareCurs.Text.Replace(",","."));
                        cmd.Parameters.AddWithValue("Data", Convert.ToDateTime(tbDataCurs.Text, new CultureInfo("ro-RO")));
                        cmd.ExecuteNonQuery();
                    }
                    else if (Session["TipActiune"].ToString() == "0" && Convert.ToDecimal(tbValoareCurs.Text.Replace(",", ".")) > 0 && Convert.ToDateTime(tbDataCurs.Text,new CultureInfo("ro-RO")) > DateTime.Now.AddYears(-100))
                    {
                        cmdStr = @"INSERT INTO [Cursuri]
                                           ([Curs]
                                           ,[Data])
                                     VALUES
                                           (@Curs
                                           ,@Data)";
                        SqlCommand cmd = new SqlCommand(cmdStr, con);
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("Curs", tbValoareCurs.Text.Replace(",", "."));
                        cmd.Parameters.AddWithValue("Data", Convert.ToDateTime(tbDataCurs.Text, new CultureInfo("ro-RO")));
                        cmd.ExecuteNonQuery();
                    }
                }
                catch { return; }
            }
            GridListaCursuri.DataBind();
            PanelAdauga.Visible = false;
            PanelListaCereri.Visible = true;
            ButonAdauga.Visible = true;
            btAdaugaSus.Visible = true;
            ButonModifica.Visible = false;
            btModificaSus.Visible = false;
            ButonSterge.Visible = false;
            btStergeSus.Visible = false;
            GridListaCursuri.SelectedIndex = -1;
            GridListaCursuri.DataBind();
        }
        protected void ButonRenunta_Click(object sender, EventArgs e)
        {
            PanelAdauga.Visible = false;
            PanelListaCereri.Visible = true;
        }
    }
}