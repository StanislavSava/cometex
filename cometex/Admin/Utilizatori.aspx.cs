﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using cometex.Clase;
using cometex.Utils;

namespace cometex.Admin
{
    public partial class Utilizatori : System.Web.UI.Page
    {
        protected void Page_Prerender(object sender, EventArgs e)
        {
          }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Master.AtribuieButoaneMeniu(MeniuUtils.AranjeazaElementeMeniu(22, new List<int> {4,6}));
                ButonModifica.Visible = false;
                ButonAdauga.Visible = true;
                ButonRenunta.Visible = false;
                ButonSalveaza.Visible = false;
                ButonSterge.Visible = false;
            }

        }
        protected void GridUtilizatori_SelectedIndexChanged(object sender, EventArgs e)
        {
            ButonModifica.Visible = true;
            ButonAdauga.Visible = true;
            ButonRenunta.Visible = false;
            ButonSalveaza.Visible = false;
            ButonSterge.Visible = true;
        }

        protected void Adauga_Click(object sender, EventArgs e)
        {
            PanelAdauga.Visible = true;
            PanelListaCereri.Visible = false;
            ButonModifica.Visible = false;
            ButonAdauga.Visible = false;
            ButonRenunta.Visible = true;
            ButonSalveaza.Visible = true;
            ButonSterge.Visible = false;
            Session["TipActiune"] = "0";
            UtilizatorInit();
        }
        public void UtilizatorInit()
        {
            tbUtilizator.Text = "";
            tbNume.Text = "";
            tbParola.Text = "";
            tbConfirmaParola.Text = "";
            ckbAdministrator.Checked = false;
        }
        public bool UtilizatorVerifica()
        {

            String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            String cmdStr = "select count(utilizator) from utilizatori where utilizator ='" + tbUtilizator.Text + "'";
            SqlCommand cmd = new SqlCommand(cmdStr, con);
            cmd.Parameters.Clear();
            uint exista = Convert.ToUInt32(cmd.ExecuteScalar());
            if (exista > 0)
            {

                lblUtilizatorExista.Visible = true;
                con.Close();
                return true;
            }
            else
            {
                lblUtilizatorExista.Visible = false;
                con.Close();
                return false;
            }


        }
        public void UtilizatorDisable(bool dezactiveaza)
        {
            if (dezactiveaza)
            {
                tbUtilizator.ReadOnly = true;
                tbUtilizator.CssClass = "dezactivata";
                tbUtilizator.Enabled = false;
            }
            else
            {
                tbUtilizator.ReadOnly = false;
                tbUtilizator.CssClass.Replace("dezactivata", "");
                tbUtilizator.Enabled = true;
            }
        }
        protected void Modifica_Click(object sender, EventArgs e)
        {
            PanelAdauga.Visible = true;
            PanelListaCereri.Visible = false;
            // populam campurile
            tbNume.Text = GridListaUtilizatori.SelectedRow.Cells[2].Text.ToString();
            tbUtilizator.Text = GridListaUtilizatori.SelectedRow.Cells[1].Text.ToString();
            CheckBox admin = GridListaUtilizatori.SelectedRow.Cells[5].Controls[0] as CheckBox;
            if (admin.Checked) ckbAdministrator.Checked = true;
            else ckbAdministrator.Checked = false;
            UtilizatorDisable(true);
            Session["TipActiune"] = "1";
            ButonModifica.Visible = false;
            ButonAdauga.Visible = false;
            ButonRenunta.Visible = true;
            ButonSalveaza.Visible = true;
            ButonSterge.Visible = false;
            lblSchimbaParola.Visible = true;

        }
        protected void ddlSocietateAdauga_DataBound(object sender, EventArgs e)
        {
            try
            { ddlSocietateAdauga.SelectedValue = GridListaUtilizatori.DataKeys[GridListaUtilizatori.SelectedIndex].Values["idSocietate"].ToString(); }
            catch { }
        }
        protected void Sterge_Click(object sender, EventArgs e)
        {
            // Sterge utilizatorul
            if (GridListaUtilizatori.SelectedIndex >= 0)
            {
                String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();
                String cmdStr = "delete from utilizatori where idUtilizator = '" + GridListaUtilizatori.SelectedDataKey[0].ToString() + "'";
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.ExecuteNonQuery();
                GridListaUtilizatori.DataBind();
                GridListaUtilizatori.SelectedIndex = -1;
                con.Close();
                ButonAdauga.Visible = true;
                ButonModifica.Visible = false;
                ButonSterge.Visible = false;
                GridListaUtilizatori.SelectedIndex = -1;
                GridListaUtilizatori.DataBind();
            }
            ButonModifica.Visible = false;
            ButonAdauga.Visible = true;
            ButonRenunta.Visible = false;
            ButonSalveaza.Visible = false;
            ButonSterge.Visible = false;
            GridListaUtilizatori.SelectedIndex = -1;
        }

        protected void ButonSalveaza_Click(object sender, EventArgs e)
        {
            String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            String cmdStr = "";
            //  criptez parola
            string Parola = Criptare.Encrypt(tbParola.Text);
            // string Parola = tbParola.Text;
            // verific admin
            //modificare
            if (Session["TipActiune"].ToString() == "1")
            {
                // verificam daca se schimba parola
                if (tbParola.Text.Length > 0 && tbParola.Text == tbConfirmaParola.Text)
                    Parola = ", Parola ='" + Criptare.Encrypt(tbParola.Text) + "' ";
                //Parola = ", Parola ='" + tbParola.Text + "' ";
                else
                    Parola = "";
                cmdStr = "update Utilizatori set Utilizator = '" + tbUtilizator.Text + "', Nume= '" + tbNume.Text + "', idSocietate='" + ddlSocietateAdauga.SelectedValue.ToString() + "', Admin='" + ckbAdministrator.Checked.ToString() + "' " + Parola + " WHERE idUtilizator='" + GridListaUtilizatori.SelectedDataKey.Value.ToString() + "'";
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("idSocietate", GridListaUtilizatori.SelectedDataKey[0]);
                cmd.Parameters.AddWithValue("societate", tbNume.Text);
                cmd.ExecuteNonQuery();
                lblSchimbaParola.Visible = false;
                UtilizatorDisable(false);
            }

            ////// adaugare
            else if (Session["TipActiune"].ToString() == "0")
            {
                if (UtilizatorVerifica() || tbUtilizator.Text.Length < 1 || tbNume.Text.Length < 1)
                { return; }
                if (tbParola.Text.Length > 0 && tbParola.Text == tbConfirmaParola.Text)
                    Parola = Criptare.Encrypt(tbParola.Text);
                //Parola = tbParola.Text;
                else
                {
                    validatorParole.IsValid = false;
                    return;
                }
                cmdStr = "insert into Utilizatori (Utilizator, Nume, Parola, idSocietate, Admin) values ( '" + tbUtilizator.Text + "','" + tbNume.Text + "','" + Parola + "','" + ddlSocietateAdauga.SelectedValue.ToString() + "','" + ckbAdministrator.Checked.ToString() + "')";
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.Parameters.Clear();
                cmd.ExecuteNonQuery();
                Parola = "";
            }
            con.Close();
            GridListaUtilizatori.DataBind();
            PanelAdauga.Visible = false;
            PanelListaCereri.Visible = true;
            ButonAdauga.Visible = true;
            ButonModifica.Visible = false;
            ButonSterge.Visible = false;
            ButonSalveaza.Visible = false;
            ButonRenunta.Visible = false;
            GridListaUtilizatori.SelectedIndex = -1;
            GridListaUtilizatori.DataBind();
        }

        protected void ButonRenunta_Click(object sender, EventArgs e)
        {
            PanelAdauga.Visible = false;
            PanelListaCereri.Visible = true;
            ButonModifica.Visible = false;
            ButonAdauga.Visible = true;
            ButonRenunta.Visible = false;
            ButonSalveaza.Visible = false;
            ButonSterge.Visible = false;
            GridListaUtilizatori.SelectedIndex = -1;
            UtilizatorDisable(false);
        }

        protected void ddlSocietati_Toate(object sender, EventArgs e)
        {
            GridListaUtilizatori.SelectedIndex = -1;

        }

        protected void GridListaUtilizatori_DataBound(object sender, EventArgs e)
        {
            if (ddlSocietati.SelectedItem.Value == "0")
            {
                SqlUtilizatori.SelectCommand = "select Utilizatori.IdUtilizator, Utilizatori.Utilizator, Utilizatori.Nume, Utilizatori.Parola, Utilizatori.idSocietate, Utilizatori.Admin, societati.societate from utilizatori INNER JOIN Societati ON Utilizatori.idSocietate = Societati.IdSocietate order by Utilizatori.Utilizator";

            }
            else
            {
                SqlUtilizatori.SelectCommand = "SELECT Utilizatori.IdUtilizator, Utilizatori.Utilizator, Utilizatori.Nume, Utilizatori.Parola, Utilizatori.idSocietate, Utilizatori.Admin, Societati.Societate FROM Utilizatori INNER JOIN Societati ON Utilizatori.idSocietate = Societati.IdSocietate WHERE (Utilizatori.idSocietate = " + ddlSocietati.SelectedValue.ToString() + ") order by Utilizatori.Utilizator";
            }
        }

        protected void tbParola_TextChanged(object sender, EventArgs e)
        {
        }

        protected void tbConfirmaParola_TextChanged(object sender, EventArgs e)
        {
        }

    }
}