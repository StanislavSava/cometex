﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using cometex.Clase;
using cometex.Utils;
using System.Data;

namespace cometex.Admin
{
    public partial class Conturi : System.Web.UI.Page
    {
        List<Button> ButoaneVizibile = new List<Button> { };
        List<Button> ButoaneAscunse = new List<Button> { };
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (ddlSocietati.Items.Count == 0) ddlSocietati.DataBind();
            if (ddlCont.Items.Count < 0) SAGA.SAGAPopuleazaDropDownListConturi(ddlCont, ddlSocietati.SelectedItem.Text.Substring(0, ddlSocietati.SelectedItem.Text.IndexOf(" - ")));
            btnReseteazaConturi.Enabled = lblConturiAsociate.Text.Length > 24 ? true : false;
            btnAdaugaContDinDdl.Enabled = ddlCont.Items.Count > 0 ? true : false;
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Master.AtribuieButoaneMeniu(MeniuUtils.AranjeazaElementeMeniu(22, new List<int> { 4, 11 }));
                GridListaConturi.DataBind();
                if (GridListaConturi.SelectedIndex == -1)
                {
                    ButonAdauga.Visible = true;
                    ButonModifica.Visible = false;
                    ButonSterge.Visible = false;
                    GridListaConturi.SelectedIndex = -1;
                }
                else
                {
                    ButonAdauga.Visible = true;
                    ButonModifica.Visible = true;
                    ButonSterge.Visible = true;
                }
            }
        }
        protected void GridListaSocietati_SelectedIndexChanged(object sender, EventArgs e)
        {
            SAGA.SAGAPopuleazaDropDownListConturi(ddlCont, ddlSocietati.SelectedItem.Text.Substring(0, ddlSocietati.SelectedItem.Text.IndexOf(" - ")));
            ButonAdauga.Visible = true;
            ButonModifica.Visible = true;
            ButonSterge.Visible = true;
        }
        protected void Adauga_Click(object sender, EventArgs e)
        {
            PanelAdauga.Visible = true;
            PanelListaCereri.Visible = false;
            SAGA.SAGAPopuleazaDropDownListConturi(ddlCont, ddlSocietati.SelectedItem.Text.Substring(0, ddlSocietati.SelectedItem.Text.IndexOf(" - ")));
            lblConturiAsociate.Text = "Conturi asociate:<br />";
            ddlCont.SelectedIndex = 0;
            rblModCalcul.SelectedValue = "Sold";
            Session["TipActiune"] = "0";
        }
        protected void Modifica_Click(object sender, EventArgs e)
        {
            PanelAdauga.Visible = true;
            PanelListaCereri.Visible = false;
            SAGA.SAGAPopuleazaDropDownListConturi(ddlCont, ddlSocietati.SelectedItem.Text.Substring(0, ddlSocietati.SelectedItem.Text.IndexOf(" - ")));
            ddlSocietati.SelectedValue = ((Label)GridListaConturi.SelectedRow.Cells[1].FindControl("lblIdSocietate")).Text;
            ddlGrafic.SelectedValue = ((Label)GridListaConturi.SelectedRow.Cells[1].FindControl("lblIdGrafic")).Text;

            lblConturiAsociate.Text = "Conturi asociate:<br />";
            lblConturiAsociate.Text += ConturiUtils.ListaConturiStringModificaInit(((Label)GridListaConturi.SelectedRow.Cells[1].FindControl("lblCont")).Text, "#", ddlCont);
            rblModCalcul.SelectedValue = ((Label)GridListaConturi.SelectedRow.Cells[4].FindControl("lblModCalcul")).Text; ;
            Session["TipActiune"] = "1";
        }
        protected void Sterge_Click(object sender, EventArgs e)
        {
            if (GridListaConturi.SelectedIndex >= 0)
            {
                String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();
                String cmdStr = "delete from conturi where id = @idCont";
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("idCont", GridListaConturi.SelectedDataKey[0]);
                cmd.ExecuteNonQuery();
                GridListaConturi.DataBind();
                GridListaConturi.SelectedIndex = -1;
                con.Close();
                ButonAdauga.Visible = true;
                ButonModifica.Visible = false;
                ButonSterge.Visible = false;
                GridListaConturi.SelectedIndex = -1;
                GridListaConturi.DataBind();
            }
        }
        protected void ButonSalveaza_Click(object sender, EventArgs e)
        {
            String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            String cmdStr = "";

            if (Session["TipActiune"].ToString() == "1")
            {
                cmdStr = @"UPDATE [dbo].[Conturi]
                           SET  
                                 [cont] = @cont
                                ,[grafic] = @grafic
                                ,[idSocietate] = @idSocietate
                                ,[modCalcul] = @modCalcul
                         WHERE id = @id";
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("id", ((Label)GridListaConturi.SelectedRow.Cells[1].FindControl("lblContId")).Text);
                cmd.Parameters.AddWithValue("grafic", ddlGrafic.SelectedValue);
                cmd.Parameters.AddWithValue("cont", ConturiUtils.ListaConturiStringAdauga(lblConturiAsociate.Text, "<br />", " -"));
                cmd.Parameters.AddWithValue("idSocietate", ddlSocietati.SelectedValue);
                cmd.Parameters.AddWithValue("modCalcul", rblModCalcul.SelectedValue);
                cmd.ExecuteNonQuery();
            }
            else if (Session["TipActiune"].ToString() == "0")
            {
                cmdStr = @"INSERT INTO [dbo].[Conturi] 
                    (    [cont]
                        ,[grafic]
                        ,[idSocietate]
                        ,[modCalcul])  
                        VALUES 
                    (    @cont
                        ,@grafic
                        ,@idSocietate
                        ,@modCalcul
                    )";
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("grafic", ddlGrafic.SelectedValue);
                cmd.Parameters.AddWithValue("cont", ConturiUtils.ListaConturiStringAdauga(lblConturiAsociate.Text, "<br />", " -"));
                cmd.Parameters.AddWithValue("idSocietate", ddlSocietati.SelectedValue);
                cmd.Parameters.AddWithValue("modCalcul", rblModCalcul.SelectedValue);
                cmd.ExecuteNonQuery();
            }
            con.Close();
            GridListaConturi.DataBind();
            PanelAdauga.Visible = false;
            PanelListaCereri.Visible = true;
            ButonAdauga.Visible = true;
            ButonModifica.Visible = false;
            ButonSterge.Visible = false;
            GridListaConturi.SelectedIndex = -1;
            GridListaConturi.DataBind();
        }
        protected void ButonRenunta_Click(object sender, EventArgs e)
        {
            PanelAdauga.Visible = false;
            PanelListaCereri.Visible = true;
        }

        protected void btnFacturare_Click(object sender, EventArgs e)
        {
            //List<List<string>> caleProgramSagaFolderSocietati = SAGA.SAGAListaSocietati();
            //Perioade perioada = SAGA.PerioadaSold(DateTime.Now.ToString("dd.MM.yyyy"));
            //string cont = "5121";
            //string caleProgramSaga = "D:\\SAGA C.3.0";

            //List<DataTable> date = SAGA.SagaValoriCont(perioada.DataInceput, perioada.DataFinal, cont, caleProgramSaga, caleProgramSagaFolderSocietati);

        }

        protected void DropDownListCont_DataBound(object sender, EventArgs e)
        {

        }

        protected void btnAdaugaContDinDdl_Click(object sender, EventArgs e)
        {
            lblConturiAsociate.Text += ddlCont.SelectedItem.Text + "<br />";
            int index = ddlCont.SelectedIndex;
            ddlCont.Items.Remove(ddlCont.SelectedItem);
            ddlCont.SelectedIndex = ddlCont.Items.Count <= index ? 0 : index;

        }

        protected void btnReseteazaConturi_Click(object sender, EventArgs e)
        {
            lblConturiAsociate.Text = "Conturi asociate:<br />";
            ddlCont.Items.Clear();
            SAGA.SAGAPopuleazaDropDownListConturi(ddlCont, ddlSocietati.SelectedItem.Text.Substring(0, ddlSocietati.SelectedItem.Text.IndexOf(" - ")));
        }

        protected void ddlSocietati_SelectedIndexChanged(object sender, EventArgs e)
        {
            SAGA.SAGAPopuleazaDropDownListConturi(ddlCont, ddlSocietati.SelectedItem.Text.Substring(0, ddlSocietati.SelectedItem.Text.IndexOf(" - ")));
        }
    }
}