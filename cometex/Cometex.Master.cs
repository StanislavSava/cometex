﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cometex
{
    public partial class text : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["idUtilizator"] != null)
            {
                try
                {
                    LabelUser.Text = " Salut " + Session["numeUtilizator"].ToString();
                }
                catch { }
            }
            else Response.Redirect("~/login.aspx");

        }

        protected void SchimbaLabelUtilizatorConectat(string utilizatorNou)
        {
            LabelUser.Text = utilizatorNou;
        }
        protected void LinkButtonDeconectare_Click(object sender, EventArgs e)
        {
            Session["idUtilizator"] = null;
            LabelUser.Text = "";
            Response.Redirect("~/login.aspx");
        }
    }
}