﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cometex.Clase;

namespace cometex.Utils
{
    class MeniuUtils
    {
        public static List<Meniu> AranjeazaElementeMeniu(int NumarElementeMeniu, List<int> ElementeActiv)
        {
            List<Meniu> elementeMeniu = new List<Meniu>();
            for (int i = 1; i <= NumarElementeMeniu; i++)
            {
                Meniu elementMeniu = new Meniu();
                elementMeniu.Atribute.Clear();
                if (ElementeActiv.Contains(i))
                {
                    elementMeniu.Atribute.Add("class");
                    elementMeniu.Atribute.Add("btn-primary");
                    elementMeniu.Atribute.Add("disabled");
                    elementMeniu.Atribute.Add("false");
                }
                else
                {
                    elementMeniu.Atribute.Add("class");
                    elementMeniu.Atribute.Add("btn-default");
                    elementMeniu.Atribute.Add("disabled");
                    elementMeniu.Atribute.Add("false");
                }
                elementeMeniu.Add(elementMeniu);
            }
            return elementeMeniu;
        }
    }
}
