﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cometex.Clase;

namespace cometex.Utils
{
    class GrilaUtils
    {
        public static bool VerificaDacaSeIncadreazaInGrilaPeLunaScadentei(Cheltuieli cheltuiala)
        {
            bool SeIncadreazaInGrila = false;
            Cheltuieli cheltuieliLunaInCursPanaAcum = DatabaseUtils.TOTALCheltuieliPeLunaPeCategoriiPeOSocietateCuPlafon(cheltuiala.Societatea, cheltuiala.CategorieCheltuiala, cheltuiala.Scadenta);
            if (cheltuieliLunaInCursPanaAcum.Societatea.Id == 0)
            {
                cheltuieliLunaInCursPanaAcum = new Cheltuieli();
                cheltuieliLunaInCursPanaAcum = DatabaseUtils.PlafonOSocietateOCategorie(cheltuiala.Societatea, cheltuiala.CategorieCheltuiala);
            }
            if ((cheltuiala.CheltuialaBrut + cheltuieliLunaInCursPanaAcum.CheltuialaBrut) <= cheltuieliLunaInCursPanaAcum.Plafon)
                SeIncadreazaInGrila = true;
            return SeIncadreazaInGrila;
        }
        public static bool VerificaDacaSeIncadreazaInGrilaPeLunaScadentei(Cheltuieli cheltuiala, decimal sumaModificata)
        {
            bool SeIncadreazaInGrila = false;
            Cheltuieli cheltuieliLunaInCursPanaAcum = DatabaseUtils.TOTALCheltuieliPeLunaPeCategoriiPeOSocietateCuPlafon(cheltuiala.Societatea, cheltuiala.CategorieCheltuiala, cheltuiala.Scadenta);
            if (cheltuieliLunaInCursPanaAcum.Societatea.Id == 0)
            {
                cheltuieliLunaInCursPanaAcum = new Cheltuieli();
                cheltuieliLunaInCursPanaAcum = DatabaseUtils.PlafonOSocietateOCategorie(cheltuiala.Societatea, cheltuiala.CategorieCheltuiala);
            }
            if ((cheltuiala.CheltuialaBrut + cheltuieliLunaInCursPanaAcum.CheltuialaBrut - sumaModificata) <= cheltuieliLunaInCursPanaAcum.Plafon)
                SeIncadreazaInGrila = true;
            return SeIncadreazaInGrila;
        }
        public static decimal ValoarePlafonDeCheltuiala(CategoriiCheltuieli categorie, Societati societate)
        {
            List<Cheltuieli> listaPlafoane = DatabaseUtils.ListaPlafoaneOSocietate(societate);
            decimal valoare = listaPlafoane[0].CheltuialaBrut;
            return valoare;

        }
    }
}
