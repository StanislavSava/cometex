﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cometex.Utils
{
    class JavascriptUtils
    {
        protected static void GraficMultiplu()
        { }
        protected static string CreeazaStringPentruGrafic()
        {
            string x = "";
            return x;
        }
        public static string CreeazaListaInt(List<int> lista)
        {
            string listaInt = "";
            foreach (int i in lista)
                listaInt += i.ToString() + ",";
            return listaInt.Remove(listaInt.Count() - 1);
        }
        public static string CreeazaListaString(List<string> lista)
        {
            string listaString = "";
            foreach (string text in lista)
                listaString += "\"" + text + "\",";
            return listaString.Remove(listaString.Count() - 1);
        }


        public static List<string> CuloriRGBADoarValori()
        {
            List<string> culori = new List<string>
            {
                "255,255,0", "0,255,255", "255,0,255", "192,192,192","128,128,128","128,0,0","128,128,0","0,128,0","128,0,128","0,128,128","0,0,128"
            };
            return culori;
        }
        public static List<string> CuloriRGBACuRGBAText(List<string> culori)
        {
            List<string> culoriCuText = new List<string> { };
            foreach (string culoare in culori)
                culoriCuText.Add("rgba(" + culoare + ")");
            return culoriCuText;
        }
        public static List<string> CuloriRGBACuRGBATextSiTransparenta(List<string> culori, string transparenta)
        {
            List<string> culoriCuText = new List<string> { };
            foreach (string culoare in culori)
                culoriCuText.Add("rgba(" + culoare + "," + transparenta + ")");
            return culoriCuText;
        }
        // PENTRU AREA CHART //
        // fundal - rgba cu transparenta "rgba(60,141,188,1)"
        // margine - rgba fara transparenta "rgba(60,141,188)"
        // punct fundal - rgba fara transparenta "rgba(60,141,188)"
        // punct margine - rgba fara transparenta "rgba(60,141,188)"
        // punct highlight fundal - rgba fara transparenta "rgba(60,141,188)"
        // punct highlight margine - rgba fara transparenta "rgba(60,141,188)"
        public static string GraficValoriString(List<string> valori, bool cuGhilimele)
        {
            // primeste o List<string>
            // rezulta==>   ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
            string text = "[";
            foreach (string valoare in valori)
            {
                if (cuGhilimele) text += "\"" + valoare + "\",";
                else text += "" + valoare + ",";
            }
            return text.Remove(text.Count() - 1) + "]";
        }
        public static string CreeazaDataSetValori(Clase.Labels labels)
        {
            // primeste o List<string>
            // rezulta==>   ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
            string text = "[";
            foreach (string label in labels.labels) text += "\"" + label + "\",";
            return text.Remove(text.Count() - 1) + "]";
        }
        public static List<string> CuloriText(List<Clase.CuloriRGB> culori)
        {
            // fara transparenta
            List<string> culoriCuText = new List<string> { };
            foreach (Clase.CuloriRGB culoare in culori)
            {
                culoriCuText.Add("rgba(" + culoare.R + "," + culoare.G + "," + culoare.B + "," + culoare.transparenta + ")");
            }
            return culoriCuText;
        }
        public static string CuloareText(Clase.CuloriRGB culoare)
        {
            string culoareCuText = "rgba(" + culoare.R + "," + culoare.G + "," + culoare.B + "," + culoare.transparenta + ")";
            return culoareCuText;
        }


    }
}