﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Data;
using cometex.Clase;
using System.Data.OleDb;
using System.Web.UI.WebControls;
using FirebirdSql.Data.FirebirdClient;

namespace cometex.Utils
{
    public static class DatabaseUtils
    {
        public static SqlConnection ConnectToDatabase()
        {
            string conString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
            SqlConnection connection = new SqlConnection(conString);
            connection.Open();
            return connection;
        }
        public static bool VerificaExistentaCheltuieliPeCategorie(CategoriiCheltuieli categorie)
        {
            SqlConnection conexiune = ConnectToDatabase();
            bool areCheltuieli = false; 
            using (conexiune)
            {
                String queryString = @"SELECT top 1  * FROM Investitii WHERE [idCategorie] = " + categorie.Id.ToString();
                SqlCommand cmd = new SqlCommand(queryString, conexiune);
                SqlDataReader dateGasite = cmd.ExecuteReader();
                while (dateGasite.Read())
                {
                    areCheltuieli = true;
                    break;
                }
                conexiune.Close();
            }
            return areCheltuieli;
        }
        public static List<Cheltuieli> ListaCheltuieliPeLunaInCurs(Societati societatea, DateTime dataScadentei)
        {
            Cheltuieli cheltuiala = new Cheltuieli();
            List<Cheltuieli> listaCheltuieli = new List<Cheltuieli>();
            SqlConnection conexiune = ConnectToDatabase();
            using (conexiune)
            {
                String queryString = @"SELECT Societati.Societate, Categorii.Categorie, Investitii.IdInvestitie, Investitii.idSocietate, Investitii.idCategorie, Investitii.Denumire, Investitii.Obs, Investitii.DataIntroducere, Investitii.IntrodusDe, Investitii.Scadenta, Investitii.Suma, Investitii.Status, Investitii.RezolutionatDe, Investitii.DataRezolutie, Investitii.SumaNeta
                        FROM Investitii INNER JOIN
                             Societati ON Investitii.idSocietate = Societati.IdSocietate INNER JOIN
                             Categorii ON Investitii.idCategorie = Categorii.IdCategorie
                        WHERE 
                            Investitii.idSocietate = @idSocietate AND DATEPART(mm, Investitii.Scadenta) = @luna AND DATEPART(yyyy, Investitii.Scadenta) =  @an";
                SqlCommand cmd = new SqlCommand(queryString, conexiune);
                cmd.Parameters.AddWithValue("idSocietate", societatea.Id.ToString());
                cmd.Parameters.AddWithValue("luna", dataScadentei.Month);
                cmd.Parameters.AddWithValue("an", dataScadentei.Year);
                SqlDataReader dateGasite = cmd.ExecuteReader();
                while (dateGasite.Read())
                {
                    cheltuiala = new Cheltuieli();
                    cheltuiala.IdCheltuieli = Convert.ToInt32(dateGasite["IdInvestitie"]);
                    cheltuiala.Societatea.Id = Convert.ToInt32(dateGasite["idSocietate"]);
                    cheltuiala.Societatea.Denumire = dateGasite["Societate"].ToString();
                    cheltuiala.CategorieCheltuiala.Id = Convert.ToInt32(dateGasite["idCategorie"]);
                    cheltuiala.CategorieCheltuiala.Denumire = dateGasite["Categorie"].ToString();
                    cheltuiala.CheltuialaBrut = Convert.ToDecimal(dateGasite["Suma"]);
                    cheltuiala.CheltuialaNet = Convert.ToDecimal(dateGasite["SumaNeta"]);
                    cheltuiala.Destinatia = dateGasite["Denumire"].ToString();
                    cheltuiala.Explicatii = dateGasite["Obs"].ToString();
                    cheltuiala.DelegatAIntrodusCheltuiala.Id = Convert.ToInt32(dateGasite["IntrodusDe"]);
                    cheltuiala.RezolutionatDe.Id = Convert.ToInt32(dateGasite["RezolutionatDe"]);
                    cheltuiala.RezolutionatLaData = Convert.ToDateTime(dateGasite["DataRezolutie"]);
                    cheltuiala.Scadenta = Convert.ToDateTime(dateGasite["Scadenta"]);
                    cheltuiala.Status.Id = Convert.ToInt32(dateGasite["Status"]);
                    listaCheltuieli.Add(cheltuiala);
                }
                dateGasite.Close();
                conexiune.Close();
            }
            return listaCheltuieli;
        }
        public static List<Cheltuieli> CheltuieliPeLunaPeZile(string societatea, DateTime dataScadentei, string categoria)
        {
            Cheltuieli cheltuiala = new Cheltuieli();
            List<Cheltuieli> listaCheltuieli = new List<Cheltuieli>();
            SqlConnection conexiune = ConnectToDatabase();
            using (conexiune)
            {
                String queryString = @"SELECT  Investitii.Scadenta, sum(Investitii.Suma) as SumaPeZi
                        FROM Investitii INNER JOIN
                             Societati ON Investitii.idSocietate = Societati.IdSocietate INNER JOIN
                             Categorii ON Investitii.idCategorie = Categorii.IdCategorie
                     
						
						WHERE 
                            convert(nvarchar, Investitii.idSocietate) like ('%' + @idSocietate + '%') AND convert(nvarchar,Investitii.idCategorie) like @idCategorie AND DATEPART(mm, Investitii.Scadenta) = '12' AND DATEPART(yyyy, Investitii.Scadenta) =  '2015' AND Investitii.Status = '1'

							   group by Investitii.Scadenta";
                SqlCommand cmd = new SqlCommand(queryString, conexiune);
                cmd.Parameters.AddWithValue("idSocietate", societatea);
                cmd.Parameters.AddWithValue("idCategorie", categoria);
                cmd.Parameters.AddWithValue("luna", dataScadentei.Month);
                cmd.Parameters.AddWithValue("an", dataScadentei.Year);
                SqlDataReader dateGasite = cmd.ExecuteReader();
                while (dateGasite.Read())
                {
                    cheltuiala = new Cheltuieli();
                    cheltuiala.Societatea.Id = Convert.ToInt32(societatea);
                    cheltuiala.CheltuialaBrut = Convert.ToDecimal(dateGasite["SumaPeZi"]);
                    cheltuiala.Scadenta = Convert.ToDateTime(dateGasite["Scadenta"]);
                    listaCheltuieli.Add(cheltuiala);
                }
                dateGasite.Close();
                conexiune.Close();
            }
            return listaCheltuieli;
        }
        public static List<Cheltuieli> ListaTOTALCheltuieliPeLunaPeCategoriiPeOSocietate(Societati societatea, DateTime dataScadentei)
        {
            Cheltuieli cheltuiala = new Cheltuieli();
            List<Cheltuieli> listaCheltuieli = new List<Cheltuieli>();
            SqlConnection conexiune = ConnectToDatabase();
            using (conexiune)
            {
                String queryString = @"SELECT sum(Investitii.Suma) as sumatotala, investitii.idSocietate, Categorii.IdCategorie

                        FROM Investitii INNER JOIN
                             Societati ON Investitii.idSocietate = Societati.IdSocietate INNER JOIN
                             Categorii ON Investitii.idCategorie = Categorii.IdCategorie
                        WHERE (Investitii.idSocietate = @idSocietate) AND DATEPART(mm, Investitii.Scadenta) = @luna AND DATEPART(yyyy, Investitii.Scadenta) = @an and Investitii.Status <> 2
						group by Categorii.IdCategorie, investitii.idSocietate
";
                SqlCommand cmd = new SqlCommand(queryString, conexiune);
                cmd.Parameters.AddWithValue("idSocietate", societatea.Id.ToString());
                cmd.Parameters.AddWithValue("luna", dataScadentei.Month);
                cmd.Parameters.AddWithValue("an", dataScadentei.Year);
                SqlDataReader dateGasite = cmd.ExecuteReader();
                while (dateGasite.Read())
                {
                    cheltuiala = new Cheltuieli();
                    cheltuiala.CheltuialaBrut = Convert.ToInt32(dateGasite["sumatotala"]);
                    cheltuiala.Societatea.Id = Convert.ToInt32(dateGasite["idSocietate"]);
                    cheltuiala.CategorieCheltuiala.Id = Convert.ToInt32(dateGasite["IdCategorie"]);
                    listaCheltuieli.Add(cheltuiala);
                }
                dateGasite.Close();
                conexiune.Close();
            }
            return listaCheltuieli;
        }
        public static Cheltuieli TOTALCheltuieliPeLunaPeCategoriiPeOSocietateCuPlafon(Societati societatea, CategoriiCheltuieli categoria, DateTime dataScadentei)
        {
            Cheltuieli cheltuiala = new Cheltuieli();
            SqlConnection conexiune = ConnectToDatabase();
            using (conexiune)
            {
                String queryString = @"SELECT sum(Investitii.Suma) as sumatotala, investitii.idSocietate, Categorii.IdCategorie, PlafoaneGrila.Suma, PlafoaneGrila.idPlafon

                        FROM Investitii INNER JOIN
                             Societati ON Investitii.idSocietate = Societati.IdSocietate INNER JOIN
                             Categorii ON Investitii.idCategorie = Categorii.IdCategorie INNER JOIN
							 PlafoaneGrila ON (PlafoaneGrila.idSocietate =  Investitii.idSocietate AND PlafoaneGrila.idCategorie = Investitii.idCategorie)
                        WHERE (Investitii.idSocietate = @idSocietate) AND DATEPART(mm, Investitii.Scadenta) = @luna AND DATEPART(yyyy, Investitii.Scadenta) = @an and Investitii.Status <> 2 and Investitii.idCategorie = @idCategorie
						group by Categorii.IdCategorie, investitii.idSocietate,  PlafoaneGrila.Suma, PlafoaneGrila.idPlafon
";
                SqlCommand cmd = new SqlCommand(queryString, conexiune);
                cmd.Parameters.AddWithValue("idSocietate", societatea.Id.ToString());
                cmd.Parameters.AddWithValue("idCategorie", categoria.Id.ToString());
                cmd.Parameters.AddWithValue("luna", dataScadentei.Month);
                cmd.Parameters.AddWithValue("an", dataScadentei.Year);
                SqlDataReader dateGasite = cmd.ExecuteReader();
                while (dateGasite.Read())
                {
                    cheltuiala = new Cheltuieli();
                    cheltuiala.CheltuialaBrut = Convert.ToInt32(dateGasite["sumatotala"]);
                    cheltuiala.Societatea.Id = Convert.ToInt32(dateGasite["idSocietate"]);
                    cheltuiala.CategorieCheltuiala.Id = Convert.ToInt32(dateGasite["IdCategorie"]);
                    cheltuiala.Plafon = Convert.ToInt32(dateGasite["Suma"]);
                    break;
                }
                dateGasite.Close();
                conexiune.Close();
            }
            return cheltuiala;
        }

        public static List<Cheltuieli> ListaPlafoaneOSocietate(Societati societatea)
        {
            Cheltuieli plafon = new Cheltuieli();
            List<Cheltuieli> listaPlafoane = new List<Cheltuieli>();
            SqlConnection conexiune = ConnectToDatabase();
            using (conexiune)
            {
                String queryString = @"SELECT PlafoaneGrila.idPlafon, PlafoaneGrila.idSocietate, PlafoaneGrila.idCategorie, PlafoaneGrila.Suma, PlafoaneGrila.Observatii, Categorii.Categorie, Societati.Societate FROM PlafoaneGrila             INNER JOIN
                         Categorii ON PlafoaneGrila.idCategorie = Categorii.IdCategorie   INNER JOIN
                         Societati ON PlafoaneGrila.idSocietate = Societati.IdSocietate   WHERE (PlafoaneGrila.idSocietate = @idSocietate)";
                SqlCommand cmd = new SqlCommand(queryString, conexiune);
                cmd.Parameters.AddWithValue("idSocietate", societatea.Id.ToString());
                SqlDataReader dateGasite = cmd.ExecuteReader();
                while (dateGasite.Read())
                {
                    plafon = new Cheltuieli();
                    plafon.IdCheltuieli = Convert.ToInt32(dateGasite["idPlafon"]);
                    plafon.Societatea.Id = Convert.ToInt32(dateGasite["idSocietate"]);
                    plafon.Societatea.Denumire = dateGasite["Societate"].ToString();
                    plafon.CategorieCheltuiala.Id = Convert.ToInt32(dateGasite["idCategorie"]);
                    plafon.CategorieCheltuiala.Denumire = dateGasite["Categorie"].ToString();
                    plafon.CheltuialaBrut = Convert.ToDecimal(dateGasite["Suma"]);
                    plafon.Destinatia = dateGasite["Observatii"].ToString();
                    listaPlafoane.Add(plafon);
                }
                dateGasite.Close();
                conexiune.Close();
            }
            return listaPlafoane;

        }
        public static Cheltuieli PlafonOSocietateOCategorie(Societati societatea, CategoriiCheltuieli categoria)
        {
            Cheltuieli plafon = new Cheltuieli();
            SqlConnection conexiune = ConnectToDatabase();
            using (conexiune)
            {
                String queryString = @"SELECT PlafoaneGrila.idPlafon, PlafoaneGrila.idSocietate, PlafoaneGrila.idCategorie, PlafoaneGrila.Suma, PlafoaneGrila.Observatii, Categorii.Categorie, Societati.Societate FROM PlafoaneGrila             INNER JOIN
                         Categorii ON PlafoaneGrila.idCategorie = Categorii.IdCategorie   INNER JOIN
                         Societati ON PlafoaneGrila.idSocietate = Societati.IdSocietate   WHERE (PlafoaneGrila.idSocietate = @idSocietate AND PlafoaneGrila.idCategorie = @idCategorie)";
                SqlCommand cmd = new SqlCommand(queryString, conexiune);
                cmd.Parameters.AddWithValue("idSocietate", societatea.Id.ToString());
                cmd.Parameters.AddWithValue("idCategorie", categoria.Id.ToString());
                SqlDataReader dateGasite = cmd.ExecuteReader();
                while (dateGasite.Read())
                {
                    plafon = new Cheltuieli();
                    plafon.IdCheltuieli = Convert.ToInt32(dateGasite["idPlafon"]);
                    plafon.Societatea.Id = Convert.ToInt32(dateGasite["idSocietate"]);
                    plafon.Societatea.Denumire = dateGasite["Societate"].ToString();
                    plafon.CategorieCheltuiala.Id = Convert.ToInt32(dateGasite["idCategorie"]);
                    plafon.CategorieCheltuiala.Denumire = dateGasite["Categorie"].ToString();
                    plafon.Plafon = Convert.ToDecimal(dateGasite["Suma"]);
                    plafon.Destinatia = dateGasite["Observatii"].ToString();
                }
                dateGasite.Close();
                conexiune.Close();
            }
            return plafon;

        }
        public static decimal GridFooterCheltuieliSelectateInFunctieDeStatus(Societati societatea, CategoriiCheltuieli categoria, string utilizator, Statusuri status)
        {
            decimal total = 0;
            SqlConnection conexiune = ConnectToDatabase();
            using (conexiune)
            {
                String queryString = @" (SELECT SUM(Suma) AS sumaPanaAcum000 FROM Investitii AS i WHERE 
                    (CONVERT (nvarchar, Status) LIKE @status) AND (CONVERT (nvarchar, IntrodusDe) LIKE CONVERT (nvarchar, @utilizator)) AND (CONVERT (nvarchar, idSocietate) LIKE CONVERT (nvarchar, @societate)) AND (CONVERT (nvarchar, idCategorie) LIKE CONVERT (nvarchar, @categorie))) AS Total";
                SqlCommand cmd = new SqlCommand(queryString, conexiune);
                cmd.Parameters.AddWithValue("utilizator", utilizator);
                cmd.Parameters.AddWithValue("societate", societatea.Id.ToString());
                cmd.Parameters.AddWithValue("categorie", categoria.Id.ToString());
                SqlDataReader dateGasite = cmd.ExecuteReader();

                while (dateGasite.Read())
                {
                    total = Convert.ToDecimal(dateGasite["Total"]);
                    break;
                }
                dateGasite.Close();
                conexiune.Close();
            }
            return total;
        }
        public static string GridFooterCheltuieliSelectateInFunctieDeStatus(string societate, string categorie, string utilizator, string status, DateTime scadenta)
        {
            string total = string.Empty;
            string tip = "%";
            SqlConnection conexiune = ConnectToDatabase();
            using (conexiune)
            {
                String queryString = @" SELECT Coalesce(SUM(Suma),0) AS Total FROM Investitii AS i 
                 inner join statusuri as s on i.status = s.idstatus
                  WHERE 
                    (CONVERT (nvarchar, i.Status) LIKE @status AND tip like @tip) AND (CONVERT (nvarchar, IntrodusDe) LIKE CONVERT (nvarchar, @utilizator)) AND (CONVERT (nvarchar, idSocietate) LIKE CONVERT (nvarchar, @societate)) AND (CONVERT (nvarchar, idCategorie) LIKE CONVERT (nvarchar, @categorie)) AND (CONVERT (datetime, Scadenta) > CONVERT (datetime, @scadenta, 103) )";
                SqlCommand cmd = new SqlCommand(queryString, conexiune);
                cmd.Parameters.AddWithValue("utilizator", utilizator);
                cmd.Parameters.AddWithValue("societate", societate);
                cmd.Parameters.AddWithValue("categorie", categorie);
                if (status == "grila") { status = "%"; tip = "grila%"; }
                else if (status == "exceptionala") { status = "%"; tip = "exceptionala%"; }
                cmd.Parameters.AddWithValue("status", status);
                cmd.Parameters.AddWithValue("tip", tip);
                cmd.Parameters.AddWithValue("scadenta", scadenta);
                SqlDataReader dateGasite = cmd.ExecuteReader();

                while (dateGasite.Read())
                {
                    total = dateGasite["Total"].ToString();
                    break;
                }
                dateGasite.Close();
                conexiune.Close();
            }
            return total;
        }
        public static string GridFooterCheltuieliSelectateInFunctieDeStatus(string societate, string categorie, string utilizator, string status, string statusTip, DateTime scadenta)
        {
            string total = string.Empty;
            SqlConnection conexiune = ConnectToDatabase();
            using (conexiune)
            {
                String queryString = @" SELECT Coalesce(SUM(Suma),0) AS Total FROM Investitii AS i 
                 inner join statusuri as s on i.status = s.idstatus
                  WHERE 
                    (CONVERT (nvarchar, s.idStatusSimplificat) LIKE @statusSimplificat AND tip like @statusTip) AND (CONVERT (nvarchar, IntrodusDe) LIKE CONVERT (nvarchar, @utilizator)) AND (CONVERT (nvarchar, idSocietate) LIKE CONVERT (nvarchar, @societate)) AND (CONVERT (nvarchar, idCategorie) LIKE CONVERT (nvarchar, @categorie)) AND (CONVERT (datetime, Scadenta) > CONVERT (datetime, @scadenta, 103) )";
                SqlCommand cmd = new SqlCommand(queryString, conexiune);
                cmd.Parameters.AddWithValue("utilizator", utilizator);
                cmd.Parameters.AddWithValue("societate", societate);
                cmd.Parameters.AddWithValue("categorie", categorie);
                cmd.Parameters.AddWithValue("statusSimplificat", status + "%");
                cmd.Parameters.AddWithValue("statusTip", statusTip + "%");
                cmd.Parameters.AddWithValue("scadenta", scadenta);
                SqlDataReader dateGasite = cmd.ExecuteReader();

                while (dateGasite.Read())
                {
                    total = dateGasite["Total"].ToString();
                    break;
                }
                dateGasite.Close();
                conexiune.Close();
            }
            return total;
        }

        public static void InsereazaPlafon(Cheltuieli plafon)
        {
            // introducem in PLAFOANE

            SqlConnection con = ConnectToDatabase();
            using (con)
            {
                String queryString = @"
INSERT INTO [dbo].[PlafoaneGrila]
           ([idSocietate]
           ,[idCategorie]
           ,[Suma]   )
     VALUES
           (
                convert(int, @idSocietate)
                ,convert(int, @idCategorie)
                ,convert(decimal(18,2),@Suma)
            )";
                SqlCommand cmd = new SqlCommand(queryString, con);
                cmd.Parameters.AddWithValue("idSocietate", plafon.Societatea.Id);
                cmd.Parameters.AddWithValue("idCategorie", plafon.CategorieCheltuiala.Id);
                cmd.Parameters.AddWithValue("Suma", plafon.CheltuialaBrut);
                Int32 rowsAffected = cmd.ExecuteNonQuery();
                con.Close();
            }

        }
        public static void ModificaPlafon(Cheltuieli plafon)
        {
            SqlConnection con = ConnectToDatabase();
            using (con)
            {
                String queryString = @"UPDATE[dbo].[PlafoaneGrila] SET [Suma] = convert(decimal(18,2),@Suma) 
                    WHERE idPlafon = convert(int, @idPlafon)";
                SqlCommand cmd = new SqlCommand(queryString, con);
                cmd.Parameters.AddWithValue("idPlafon", plafon.IdCheltuieli);
                cmd.Parameters.AddWithValue("Suma", plafon.CheltuialaBrut);
                Int32 rowsAffected = cmd.ExecuteNonQuery();
                con.Close();
            }

        }
        public static void StergePlafon(Cheltuieli plafon)
        {
            SqlConnection con = ConnectToDatabase();
            using (con)
            {
                String queryString = @"DELETE FROM [dbo].[PlafoaneGrila] WHERE idPlafon = convert(int, @idPlafon)";
                SqlCommand cmd = new SqlCommand(queryString, con);
                cmd.Parameters.AddWithValue("idPlafon", plafon.IdCheltuieli);
                Int32 rowsAffected = cmd.ExecuteNonQuery();
                con.Close();
            }

        }
        #region Cheltuieli
        ///<summary>
        ///Datele unei CHELTUIELI
        ///</summary>
        public static Cheltuieli CitesteCheltuiala(int id)
        {
            Cheltuieli cheltuiala = new Cheltuieli();
            string select = @"SELECT [IdInvestitie]
                              ,c.[idSocietate]
                              ,[idCategorie]
                              ,[Denumire]
                              ,[Obs]
                              ,[DataIntroducere]
                              ,[IntrodusDe]
                              ,[Scadenta]
                              ,[Suma]
                              ,[Status]
                              ,Coalesce([RezolutionatDe],0) as RezolutionatDe
                              ,Coalesce([DataRezolutie], '1900.01.01') as DataRezolutie
                              ,Coalesce([SumaNeta],0) as SumaNeta
                              ,Coalesce([SAGAStatus],0) as SAGAStatus
                              ,Coalesce([SAGAExplicatii],'') as SAGAExplicatii
                              ,Coalesce([SAGASuma],0) as SAGASuma
							  , sagacod
                                FROM[dbo].[Investitii] as c
                            inner join societati as s  on c.idsocietate = s.idsocietate
								where IdInvestitie = @id";
            SqlConnection conexiune = DatabaseUtils.ConnectToDatabase();
            using (conexiune)
            {
                SqlCommand cmd = new SqlCommand(select, conexiune);
                cmd.Parameters.AddWithValue("id", id);
                SqlDataReader dateGasite = cmd.ExecuteReader();
                while (dateGasite.Read())
                {
                    cheltuiala = new Cheltuieli();
                    cheltuiala.IdCheltuieli = Convert.ToInt32(dateGasite["IdInvestitie"]);
                    cheltuiala.Societatea.Id = Convert.ToInt32(dateGasite["idSocietate"]);
                    cheltuiala.CategorieCheltuiala.Id = Convert.ToInt32(dateGasite["idCategorie"]);
                    cheltuiala.Destinatia = dateGasite["Denumire"].ToString().Trim();
                    cheltuiala.Explicatii = dateGasite["Obs"].ToString().Trim();
                    cheltuiala.IntrodusLaData = Convert.ToDateTime(dateGasite["DataIntroducere"]);
                    cheltuiala.Scadenta = Convert.ToDateTime(dateGasite["Scadenta"]);
                    cheltuiala.CheltuialaBrut = Convert.ToDecimal(dateGasite["Suma"]);
                    cheltuiala.Status.Id = Convert.ToInt32(dateGasite["Status"]);
                    cheltuiala.RezolutionatDe.Id = Convert.ToInt32(dateGasite["RezolutionatDe"]);
                    cheltuiala.RezolutionatLaData = Convert.ToDateTime(dateGasite["DataRezolutie"]);
                    cheltuiala.CheltuialaNet = Convert.ToDecimal(dateGasite["SumaNeta"]);
                    cheltuiala.SAGAStatus = Convert.ToInt32(dateGasite["SAGAStatus"]);
                    cheltuiala.SAGAExplicatii = Convert.ToString(dateGasite["SAGAExplicatii"]);
                    cheltuiala.SAGASuma = Convert.ToDecimal(dateGasite["SAGASuma"]);
                    cheltuiala.Societatea.SAGAfolder = dateGasite["sagacod"].ToString().Trim();
                    break;
                }
                conexiune.Close();
                conexiune.Dispose();
            }
            return cheltuiala;
        }
        ///Datele unei CHELTUIELI
        ///</summary>
        public static void UpdateCheltuiala(Cheltuieli cheltuiala)
        {
            string updateString = @"UPDATE [dbo].[Investitii] SET " +
               (cheltuiala.Societatea.Id > 0 ? " [idSocietate] = " + cheltuiala.Societatea.Id.ToString() : "") +
               (cheltuiala.CategorieCheltuiala.Id > 0 ? ",[idCategorie] = " + cheltuiala.CategorieCheltuiala.Id.ToString() : "") +
               (cheltuiala.Status.Id >= 0 ? ",[Status] = " + cheltuiala.Status.Id.ToString() : "") +
               (cheltuiala.Destinatia.Length > 0 ? ", [Denumire] = '" + cheltuiala.Destinatia + "' " : "") +
               (cheltuiala.Explicatii.Length > 0 ? ", [Obs] = '" + cheltuiala.Explicatii + "' " : "") +
               (cheltuiala.IntrodusLaData > Convert.ToDateTime("01.01.1900") ? ", [DataIntroducere] = Convert(DateTime,'" + cheltuiala.IntrodusLaData + "',104) " : "") +
               (cheltuiala.DelegatAIntrodusCheltuiala.Id > 0 ? ", [IntrodusDe] = " + cheltuiala.DelegatAIntrodusCheltuiala.Id : "") +
               (cheltuiala.Scadenta > Convert.ToDateTime("01.01.1900") ? ", [Scadenta] = Convert(DateTime,'" + cheltuiala.Scadenta + "',104) " : "") +
               (cheltuiala.CheltuialaBrut > 0 ? ", [Suma] = convert(decimal(18,2),'" + cheltuiala.CheltuialaBrut.ToString().Replace(",", ".") + "') ": "") +
               (cheltuiala.RezolutionatDe.Id > 0 ? ", [RezolutionatDe] = " + cheltuiala.RezolutionatDe.Id : "") +
               (cheltuiala.RezolutionatLaData > Convert.ToDateTime("01.01.1900") ? ", [DataRezolutie] = Convert(DateTime,'" + cheltuiala.RezolutionatLaData + "',104) " : "") +
               (cheltuiala.SAGAStatus > 0 ? ", [SAGAStatus] = " + cheltuiala.SAGAStatus : "") +
               (cheltuiala.SAGAExplicatii.Length > 0 ? ", [SAGAExplicatii] = '" + cheltuiala.SAGAExplicatii + "' " : "") +
               (cheltuiala.SAGASuma > 0 ? ", [SAGASuma] = convert(decimal(18,2),'" + cheltuiala.SAGASuma.ToString().Replace(",",".") + "') " : "") +
             " WHERE IdInvestitie = " + cheltuiala.IdCheltuieli;

            SqlConnection conexiune = DatabaseUtils.ConnectToDatabase();
            using (conexiune)
            {
                SqlCommand cmd = new SqlCommand(updateString, conexiune);
                cmd.ExecuteNonQuery();
                conexiune.Close();
                conexiune.Dispose();
            }
        }
        public static Int32 InsertCheltuiala(Cheltuieli cheltuiala)
        {
            Int32 id = 0; 
             string updateString =
                @"INSERT INTO [dbo].[Investitii]  
          (idSocietate, idCategorie, status, Denumire, Obs, DataIntroducere, IntrodusDe, Scadenta, Suma, SumaNeta,  RezolutionatDe, DataRezolutie, SAGAStatus, SAGAExplicatii, SAGASuma) values
          (" +
               (cheltuiala.Societatea.Id > 0 ? cheltuiala.Societatea.Id.ToString() : "0") +
              " , " + (cheltuiala.CategorieCheltuiala.Id > 0 ? cheltuiala.CategorieCheltuiala.Id.ToString() : "0") +
               " , " + (cheltuiala.Status.Id >= 0 ? cheltuiala.Status.Id.ToString() : "-1") +
               " , '" + (cheltuiala.Destinatia.Length > 0 ? cheltuiala.Destinatia  : "") + "' " +
               " , '" + (cheltuiala.Explicatii.Length > 0 ? cheltuiala.Explicatii : "") + "' " +
               " , Convert(DateTime, '" + (cheltuiala.IntrodusLaData > Convert.ToDateTime("01.01.1900") ? cheltuiala.IntrodusLaData : Convert.ToDateTime("01.01.1900")) + "', 104) " +
               " , " + (cheltuiala.DelegatAIntrodusCheltuiala.Id > 0 ? cheltuiala.DelegatAIntrodusCheltuiala.Id : 0) +
               " , Convert(DateTime, '" + (cheltuiala.Scadenta > Convert.ToDateTime("01.01.1900") ? cheltuiala.Scadenta : Convert.ToDateTime("01.01.1900")) + "', 104) " +
               " , Convert(decimal(18,2),'" + (cheltuiala.CheltuialaBrut > 0 ? cheltuiala.CheltuialaBrut.ToString().Replace(",", ".") : "0") + "') " +
               ", 0" +
               " , " + (cheltuiala.RezolutionatDe.Id > 0 ? cheltuiala.RezolutionatDe.Id : 0) +
               " , Convert(DateTime, '" + (cheltuiala.RezolutionatLaData > Convert.ToDateTime("01.01.1900") ? cheltuiala.RezolutionatLaData  : Convert.ToDateTime("01.01.1900")) + "',104) " +
               " , " + (cheltuiala.SAGAStatus > 0 ? cheltuiala.SAGAStatus : 0) +
               " , '" + (cheltuiala.SAGAExplicatii.Length > 0 ? cheltuiala.SAGAExplicatii : "") + "' " +
               " , Convert(decimal(18,2),'" + (cheltuiala.SAGASuma > 0 ? cheltuiala.SAGASuma.ToString().Replace(",", ".") : "0") + "') " +
             "); SELECT SCOPE_IDENTITY();";

            SqlConnection conexiune = DatabaseUtils.ConnectToDatabase();
            using (conexiune)
            {
                SqlCommand cmd = new SqlCommand(updateString, conexiune);
                id = Convert.ToInt32(cmd.ExecuteScalar());
                conexiune.Close();
                conexiune.Dispose();
            }
            return id;
        }
        ///<summary>
        ///Toate cheltuielile NEVERIFICATE IN SAGA  pentru TOATE societatile
        ///</summary>
        public static List<Cheltuieli> ListaCheltuieliNeVerificateInSAGA()
        {
            Cheltuieli cheltuiala = new Cheltuieli();
            List<Cheltuieli> cheltuieli = new List<Cheltuieli>();
            string select = @"SELECT [IdInvestitie]
                              ,c.[idSocietate]
                              ,[idCategorie]
                              ,[Denumire]
                              ,[Obs]
                              ,[DataIntroducere]
                              ,[IntrodusDe]
                              ,[Scadenta]
                              ,[Suma]
                              ,[Status]
                              ,Coalesce([RezolutionatDe],0) as RezolutionatDe
                              ,Coalesce([DataRezolutie], '1900.01.01') as DataRezolutie
                              ,Coalesce([SumaNeta],0) as SumaNeta
                              ,Coalesce([SAGAStatus],0) as SAGAStatus
                              ,Coalesce([SAGAExplicatii],'') as SAGAExplicatii
                              ,Coalesce([SAGASuma],0) as SAGASuma
							  , sagacod
                                FROM[dbo].[Investitii] as c
								inner join societati as s on 
								c.[idSocietate] = s.[idSocietate]
                                where sagastatus is null or sagaexplicatii is null or sagasuma is null  or sagastatus = 0 or sagastatus = 2  or sagastatus = 1";
            SqlConnection conexiune = DatabaseUtils.ConnectToDatabase();
            using (conexiune)
            {
                SqlCommand cmd = new SqlCommand(select, conexiune);
                SqlDataReader dateGasite = cmd.ExecuteReader();
                while (dateGasite.Read())
                {
                    cheltuiala = new Cheltuieli();
                    cheltuiala.IdCheltuieli = Convert.ToInt32(dateGasite["IdInvestitie"]);
                    cheltuiala.Societatea.Id = Convert.ToInt32(dateGasite["idSocietate"]);
                    cheltuiala.CategorieCheltuiala.Id = Convert.ToInt32(dateGasite["idCategorie"]);
                    cheltuiala.Destinatia = dateGasite["Denumire"].ToString();
                    cheltuiala.Explicatii = dateGasite["Obs"].ToString();
                    cheltuiala.IntrodusLaData = Convert.ToDateTime(dateGasite["DataIntroducere"]);
                    cheltuiala.Scadenta = Convert.ToDateTime(dateGasite["Scadenta"]);
                    cheltuiala.CheltuialaBrut = Convert.ToDecimal(dateGasite["Suma"]);
                    cheltuiala.Status.Id = Convert.ToInt32(dateGasite["Status"]);
                    cheltuiala.RezolutionatDe.Id = Convert.ToInt32(dateGasite["RezolutionatDe"]);
                    cheltuiala.RezolutionatLaData = Convert.ToDateTime(dateGasite["DataRezolutie"]);
                    cheltuiala.CheltuialaNet = Convert.ToDecimal(dateGasite["SumaNeta"]);
                    cheltuiala.SAGAStatus = Convert.ToInt32(dateGasite["SAGAStatus"]);
                    cheltuiala.SAGAExplicatii = Convert.ToString(dateGasite["SAGAExplicatii"]);
                    cheltuiala.SAGASuma = Convert.ToDecimal(dateGasite["SAGASuma"]);
                    cheltuiala.Societatea.SAGAfolder = dateGasite["sagacod"].ToString();
                    cheltuieli.Add(cheltuiala);
                }
                conexiune.Close();
                conexiune.Dispose();
            }
            return cheltuieli;
        }
        ///<summary>
        ///Toate cheltuielile NEVERIFICATE IN SAGA pentru O SINGURA societate specificata
        ///</summary>
        public static List<Cheltuieli> ListaCheltuieliNeVerificateInSAGA(Societati societate)
        {
            Cheltuieli cheltuiala = new Cheltuieli();
            List<Cheltuieli> cheltuieli = new List<Cheltuieli>();
            string select = @"SELECT [IdInvestitie]
                              ,c.[idSocietate]
                              ,[idCategorie]
                              ,[Denumire]
                              ,[Obs]
                              ,[DataIntroducere]
                              ,[IntrodusDe]
                              ,[Scadenta]
                              ,[Suma]
                              ,[Status]
                              ,Coalesce([RezolutionatDe],0) as RezolutionatDe
                              ,Coalesce([DataRezolutie], '1900.01.01') as DataRezolutie
                              ,Coalesce([SumaNeta],0) as SumaNeta
                              ,Coalesce([SAGAStatus],0) as SAGAStatus
                              ,Coalesce([SAGAExplicatii],'') as SAGAExplicatii
                              ,Coalesce([SAGASuma],0) as SAGASuma
							  , sagacod
                                FROM[dbo].[Investitii] as c
								inner join societati as s on 
								c.[idSocietate] = s.[idSocietate]
                                where sagastatus is null or sagaexplicatii is null or sagasuma is null  or sagastatus = 0 or sagastatus = 2  or sagastatus = 1 and c.idSocietate = @societate";
            SqlConnection conexiune = DatabaseUtils.ConnectToDatabase();
            using (conexiune)
            {
                SqlCommand cmd = new SqlCommand(select, conexiune);
                cmd.Parameters.AddWithValue("societate", societate.Id);

                SqlDataReader dateGasite = cmd.ExecuteReader();
                while (dateGasite.Read())
                {
                    cheltuiala = new Cheltuieli();
                    cheltuiala.IdCheltuieli = Convert.ToInt32(dateGasite["IdInvestitie"]);
                    cheltuiala.Societatea.Id = Convert.ToInt32(dateGasite["idSocietate"]);
                    cheltuiala.CategorieCheltuiala.Id = Convert.ToInt32(dateGasite["idCategorie"]);
                    cheltuiala.Destinatia = dateGasite["Denumire"].ToString();
                    cheltuiala.Explicatii = dateGasite["Obs"].ToString();
                    cheltuiala.IntrodusLaData = Convert.ToDateTime(dateGasite["DataIntroducere"]);
                    cheltuiala.Scadenta = Convert.ToDateTime(dateGasite["Scadenta"]);
                    cheltuiala.CheltuialaBrut = Convert.ToDecimal(dateGasite["Suma"]);
                    cheltuiala.Status.Id = Convert.ToInt32(dateGasite["Status"]);
                    cheltuiala.RezolutionatDe.Id = Convert.ToInt32(dateGasite["RezolutionatDe"]);
                    cheltuiala.RezolutionatLaData = Convert.ToDateTime(dateGasite["DataRezolutie"]);
                    cheltuiala.CheltuialaNet = Convert.ToDecimal(dateGasite["SumaNeta"]);
                    cheltuiala.SAGAStatus = Convert.ToInt32(dateGasite["SAGAStatus"]);
                    cheltuiala.SAGAExplicatii = Convert.ToString(dateGasite["SAGAExplicatii"]);
                    cheltuiala.SAGASuma = Convert.ToDecimal(dateGasite["SAGASuma"]);
                    cheltuiala.Societatea.SAGAfolder = dateGasite["sagacod"].ToString();
                    cheltuieli.Add(cheltuiala);
                }
                conexiune.Close();
                conexiune.Dispose();
            }
            return cheltuieli;
        }
        #endregion
        #region Societati
        public static void PopuleazaSocietatiCuCodSAGA() { }
        public static List<Societati> ListaSocietati()
        {
            List<Societati> societati = new List<Societati>();
            Societati societate = new Societati();
            string select = @"SELECT [IdSocietate] ,[Societate] ,[CuloareGrafice] , [SAGACod], [SuprafataTotala], [idFacturare]  FROM[dbo].[Societati]";
            SqlConnection conexiune = ConnectToDatabase();
            using (conexiune)
            {
                SqlCommand cmd = new SqlCommand(select, conexiune);
                SqlDataReader dateGasite = cmd.ExecuteReader();
                while (dateGasite.Read())
                {
                    societate.Id = Convert.ToInt32(dateGasite["IdSocietate"]);
                    societate.Denumire = dateGasite["Societate"].ToString();
                    societate.SAGAfolder = dateGasite["SAGACod"].ToString();
                    societate.SuprafataTotala = Convert.ToDecimal(dateGasite["SuprafataTotala"]);
                    societate.IdFacturare = Convert.ToInt32(dateGasite["idFacturare"]);
                    societati.Add(societate);
                    societate = new Societati();
                }
                dateGasite.Close();
                conexiune.Close();
            }
            return societati;
        }
        public static Societati DetaliiSocietate(int id)
        {
            Societati societate = new Societati();
            string select = @"SELECT [IdSocietate] ,[Societate] ,[CuloareGrafice] ,[SAGACod], [SuprafataTotala], [idFacturare]  FROM[dbo].[Societati] WHERE IdSocietate = @id ";
            SqlConnection conexiune = ConnectToDatabase();
            using (conexiune)
            {
                SqlCommand cmd = new SqlCommand(select, conexiune);
                cmd.Parameters.AddWithValue("id", id);
                SqlDataReader dateGasite = cmd.ExecuteReader();
                while (dateGasite.Read())
                {
                    societate.Id = Convert.ToInt32(dateGasite["IdSocietate"]);
                    societate.Denumire = dateGasite["Societate"].ToString();
                    societate.SAGAfolder = dateGasite["SAGACod"].ToString();
                    societate.SuprafataTotala = Convert.ToDecimal(dateGasite["SuprafataTotala"]);
                    societate.IdFacturare = Convert.ToInt32(dateGasite["idFacturare"]);
                }
                dateGasite.Close();
                conexiune.Close();
            }
            return societate;
        }
        public static Societati DetaliiSocietateDupaIdFacturare(int id)
        {
            Societati societate = new Societati();
            string select = @"SELECT [IdSocietate] ,[Societate] ,[CuloareGrafice] ,[SAGACod], [SuprafataTotala], [idFacturare]  FROM[dbo].[Societati] WHERE idFacturare = @id ";
            SqlConnection conexiune = ConnectToDatabase();
            using (conexiune)
            {
                SqlCommand cmd = new SqlCommand(select, conexiune);
                cmd.Parameters.AddWithValue("id", id);
                SqlDataReader dateGasite = cmd.ExecuteReader();
                while (dateGasite.Read())
                {
                    societate.Id = Convert.ToInt32(dateGasite["IdSocietate"]);
                    societate.Denumire = dateGasite["Societate"].ToString();
                    societate.SAGAfolder = dateGasite["SAGACod"].ToString();
                    societate.SuprafataTotala = Convert.ToDecimal(dateGasite["SuprafataTotala"]);
                    societate.IdFacturare = Convert.ToInt32(dateGasite["idFacturare"]);
                }
                dateGasite.Close();
                conexiune.Close();
            }
            return societate;
        }
        #endregion
        #region SAGA
        public static GraficCont ConturiInFunctieDeGraficSiSocietate(Societati societate, string grafic)
        {
            GraficCont graficCont = new GraficCont();
            SqlConnection conexiune = ConnectToDatabase();
            using (conexiune)
            {
                String queryString = @" SELECT  [id]
                                      ,[cont]
                                      ,[grafic]
                                      ,[idSocietate]
                                      ,[modCalcul]
                                  FROM [dbo].[Conturi] where grafic = @grafic and idSocietate = @societate ";
                SqlCommand cmd = new SqlCommand(queryString, conexiune);
                cmd.Parameters.AddWithValue("grafic", grafic);
                cmd.Parameters.AddWithValue("societate", societate.Id);
                SqlDataReader dateGasite = cmd.ExecuteReader();
                while (dateGasite.Read())
                {
                    graficCont.conturi = ConturiUtils.ListaConturiCreeaza(dateGasite["cont"].ToString(), "#");
                    graficCont.modCalcul = dateGasite["modCalcul"].ToString();
                    graficCont.societate = societate;
                    graficCont.idGrafic = Convert.ToInt32(grafic);
                    break;
                }
                dateGasite.Close();
                conexiune.Close();
            }
            return graficCont;
        }
        public static void DBF(Label x)
        {
            OleDbConnection yourConnectionHandler = new OleDbConnection(ConfigurationManager.ConnectionStrings["CometexSaga"].ConnectionString);

            // Open the connection, and if open successfully, you can try to query it
            yourConnectionHandler.Open();

            if (yourConnectionHandler.State == ConnectionState.Open)
            {
                string mySQL = "select * from intrari where DENUMIRE LIKE '%%'";  // dbf table name

                OleDbCommand MyQuery = new OleDbCommand(mySQL, yourConnectionHandler);

                using (OleDbConnection connection = yourConnectionHandler)
                {
                    OleDbCommand command = new OleDbCommand(mySQL, connection);
                    OleDbDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        x.Text += reader.GetString(3) + reader.GetString(4) + reader.GetDateTime(6) + "<br />";
                    }
                    reader.Close();
                }
                yourConnectionHandler.Close();
            }

        }
        public static void ScrieLog(string mesaj, string pagina, string data)
        {
            SqlConnection conexiune = ConnectToDatabase();
            using (conexiune)
            {
                String queryString = @" INSERT INTO [dbo].[log]
           ([logtext]
           ,[data]
           ,[pagina])
     VALUES
           (@mesaj
           ,convert(datetime,@data,104)
           ,@pagina)";
                SqlCommand cmd = new SqlCommand(queryString, conexiune);
                cmd.Parameters.AddWithValue("mesaj", mesaj);
                cmd.Parameters.AddWithValue("data", Convert.ToDateTime(data));
                cmd.Parameters.AddWithValue("pagina", pagina);

                Int32 rowsAffected = cmd.ExecuteNonQuery();
                conexiune.Close();
            }

        }
        #endregion
        #region Rapoarte
        #endregion
    }
}