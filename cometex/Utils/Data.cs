﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace cometex.Utils
{
    public class Data
    {
        public static TextBox DataCurenta(TextBox tb)
        {
            tb.Text = DateTime.Now.ToString("dd.MM.yyyy");
            return tb;
        } 
        public static TextBox PrimaZiAAnului(TextBox tb)
        {
            tb.Text = "01.01." + DateTime.Now.Year.ToString();
            return tb;
        }
    }
}