﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace cometex.Utils
{
    class DataSourceChiriasi
    {


        #region raport chirii utilitati - top restantieri
        public static List<Chirii> RaportTopRestantieri()
        {
            List<Chirii> chirii = new List<Chirii>();
            RaportTopRestantieriChiriiAlimentara();
            RaportTopRestantieriChiriiAstral();
            RaportTopRestantieriChiriiCometex();
            RaportTopRestantieriChiriiComtex();
            RaportTopRestantieriChiriiGaleriileVictoria();
            RaportTopRestantieriChiriiMinerva();
            RaportTopRestantieriChiriiMinervaGaleriileComerciale();
            RaportTopRestantieriChiriiRomana();
            RaportTopRestantieriChiriiUnicom();
            RaportTopRestantieriUtilitatiAlimentara();
            RaportTopRestantieriUtilitatiAstral();
            RaportTopRestantieriUtilitatiCometex();
            RaportTopRestantieriUtilitatiComtex();
            RaportTopRestantieriUtilitatiGaleriileVictoria();
            RaportTopRestantieriUtilitatiMinerva();
            RaportTopRestantieriUtilitatiMinervaGaleriileComerciale();
            RaportTopRestantieriUtilitatiRomana();
            RaportTopRestantieriUtilitatiUnicom();

            return chirii;
        }
        public static List<Chirii> RaportTopRestantieri(int societateId)
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(societateId);
            if (chirii.Count == 0) return chirii;
            DateTime data = DateTime.Now;
            List<Chirii> chiriiTampon = new List<Chirii>();
            //chirii
            chiriiTampon = SAGA.CitesteRestanteCuCoeficient(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "K", HttpContext.Current.Session["coeficientTopRestantieri"].ToString());
            // utilitati
            chiriiTampon = SAGA.CitesteRestanteCuCoeficient(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "U", HttpContext.Current.Session["coeficientTopRestantieri"].ToString());
            return chiriiTampon;
        }
        public static List<Chirii> RaportTopRestantieriChiriiAstral()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(42);
            if (chirii.Count == 0) return chirii;
            DateTime data = DateTime.Now;
            List<Chirii> chiriiTampon = new List<Chirii>();
            //chirii
            chiriiTampon = SAGA.CitesteRestanteCuCoeficient(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "K", HttpContext.Current.Session["coeficientTopRestantieri"].ToString());
            return chiriiTampon;
        }
        public static List<Chirii> RaportTopRestantieriUtilitatiAstral()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(42);
            if (chirii.Count == 0) return chirii;
            DateTime data = DateTime.Now;
            List<Chirii> chiriiTampon = new List<Chirii>();
            // utilitati
            chiriiTampon = SAGA.CitesteRestanteCuCoeficient(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "U", HttpContext.Current.Session["coeficientTopRestantieri"].ToString());
            return chiriiTampon;
        }

        public static List<Chirii> RaportTopRestantieriChiriiGaleriileVictoria()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(43);
            if (chirii.Count == 0) return chirii;
            DateTime data = DateTime.Now;
            List<Chirii> chiriiTampon = new List<Chirii>();
            //chirii
            chiriiTampon = SAGA.CitesteRestanteCuCoeficient(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "K", HttpContext.Current.Session["coeficientTopRestantieri"].ToString());
            return chiriiTampon;
        }
        public static List<Chirii> RaportTopRestantieriUtilitatiGaleriileVictoria()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(43);
            if (chirii.Count == 0) return chirii;
            DateTime data = DateTime.Now;
            List<Chirii> chiriiTampon = new List<Chirii>();
            // utilitati
            chiriiTampon = SAGA.CitesteRestanteCuCoeficient(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "U", HttpContext.Current.Session["coeficientTopRestantieri"].ToString());
            return chiriiTampon;
        }

        public static List<Chirii> RaportTopRestantieriChiriiMinerva()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(44);
            if (chirii.Count == 0) return chirii;
            DateTime data = DateTime.Now;
            List<Chirii> chiriiTampon = new List<Chirii>();
            //chirii
            chiriiTampon = SAGA.CitesteRestanteCuCoeficient(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "K", HttpContext.Current.Session["coeficientTopRestantieri"].ToString());
            return chiriiTampon;
        }
        public static List<Chirii> RaportTopRestantieriUtilitatiMinerva()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(44);
            if (chirii.Count == 0) return chirii;
            DateTime data = DateTime.Now;
            List<Chirii> chiriiTampon = new List<Chirii>();
            // utilitati
            chiriiTampon = SAGA.CitesteRestanteCuCoeficient(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "U", HttpContext.Current.Session["coeficientTopRestantieri"].ToString());
            return chiriiTampon;
        }

        public static List<Chirii> RaportTopRestantieriChiriiMinervaGaleriileComerciale()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(46);
            if (chirii.Count == 0) return chirii;
            DateTime data = DateTime.Now;
            List<Chirii> chiriiTampon = new List<Chirii>();
            //chirii
            chiriiTampon = SAGA.CitesteRestanteCuCoeficient(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "K", HttpContext.Current.Session["coeficientTopRestantieri"].ToString());
            return chiriiTampon;
        }
        public static List<Chirii> RaportTopRestantieriUtilitatiMinervaGaleriileComerciale()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(46);
            if (chirii.Count == 0) return chirii;
            DateTime data = DateTime.Now;
            List<Chirii> chiriiTampon = new List<Chirii>();
            // utilitati
            chiriiTampon = SAGA.CitesteRestanteCuCoeficient(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "U", HttpContext.Current.Session["coeficientTopRestantieri"].ToString());
            return chiriiTampon;
        }
        public static List<Chirii> RaportTopRestantieriChiriiCometex()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(48);
            if (chirii.Count == 0) return chirii;
            DateTime data = DateTime.Now;
            List<Chirii> chiriiTampon = new List<Chirii>();
            //chirii
            chiriiTampon = SAGA.CitesteRestanteCuCoeficient(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "K", HttpContext.Current.Session["coeficientTopRestantieri"].ToString());
            return chiriiTampon;
        }
        public static List<Chirii> RaportTopRestantieriUtilitatiCometex()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(48);
            if (chirii.Count == 0) return chirii;
            DateTime data = DateTime.Now;
            List<Chirii> chiriiTampon = new List<Chirii>();
            // utilitati
            chiriiTampon = SAGA.CitesteRestanteCuCoeficient(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "U", HttpContext.Current.Session["coeficientTopRestantieri"].ToString());
            return chiriiTampon;
        }
        public static List<Chirii> RaportTopRestantieriChiriiAlimentara()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(49);
            if (chirii.Count == 0) return chirii;
            DateTime data = DateTime.Now;
            List<Chirii> chiriiTampon = new List<Chirii>();
            //chirii
            chiriiTampon = SAGA.CitesteRestanteCuCoeficient(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "K", HttpContext.Current.Session["coeficientTopRestantieri"].ToString());
            return chiriiTampon;
        }
        public static List<Chirii> RaportTopRestantieriUtilitatiAlimentara()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(49);
            if (chirii.Count == 0) return chirii;
            DateTime data = DateTime.Now;
            List<Chirii> chiriiTampon = new List<Chirii>();
            // utilitati
            chiriiTampon = SAGA.CitesteRestanteCuCoeficient(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "U", HttpContext.Current.Session["coeficientTopRestantieri"].ToString());
            return chiriiTampon;
        }
        public static List<Chirii> RaportTopRestantieriChiriiUnicom()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(50);
            if (chirii.Count == 0) return chirii;
            DateTime data = DateTime.Now;
            List<Chirii> chiriiTampon = new List<Chirii>();
            //chirii
            chiriiTampon = SAGA.CitesteRestanteCuCoeficient(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "K", HttpContext.Current.Session["coeficientTopRestantieri"].ToString());
            return chiriiTampon;
        }
        public static List<Chirii> RaportTopRestantieriUtilitatiUnicom()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(50);
            if (chirii.Count == 0) return chirii;
            DateTime data = DateTime.Now;
            List<Chirii> chiriiTampon = new List<Chirii>();
            // utilitati
            chiriiTampon = SAGA.CitesteRestanteCuCoeficient(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "U", HttpContext.Current.Session["coeficientTopRestantieri"].ToString());
            return chiriiTampon;
        }
        public static List<Chirii> RaportTopRestantieriChiriiComtex()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(52);
            if (chirii.Count == 0) return chirii;
            DateTime data = DateTime.Now;
            List<Chirii> chiriiTampon = new List<Chirii>();
            //chirii
            chiriiTampon = SAGA.CitesteRestanteCuCoeficient(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "K", HttpContext.Current.Session["coeficientTopRestantieri"].ToString());
            return chiriiTampon;
        }
        public static List<Chirii> RaportTopRestantieriUtilitatiComtex()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(52);
            if (chirii.Count == 0) return chirii;
            DateTime data = DateTime.Now;
            List<Chirii> chiriiTampon = new List<Chirii>();
            // utilitati
            chiriiTampon = SAGA.CitesteRestanteCuCoeficient(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "U", HttpContext.Current.Session["coeficientTopRestantieri"].ToString());
            return chiriiTampon;
        }
        public static List<Chirii> RaportTopRestantieriChiriiRomana()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(53);
            if (chirii.Count == 0) return chirii;
            DateTime data = DateTime.Now;
            List<Chirii> chiriiTampon = new List<Chirii>();
            //chirii
            chiriiTampon = SAGA.CitesteRestanteCuCoeficient(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "K", HttpContext.Current.Session["coeficientTopRestantieri"].ToString());
            return chiriiTampon;
        }
        public static List<Chirii> RaportTopRestantieriUtilitatiRomana()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(53);
            if (chirii.Count == 0) return chirii;
            DateTime data = DateTime.Now;
            List<Chirii> chiriiTampon = new List<Chirii>();
            // utilitati
            chiriiTampon = SAGA.CitesteRestanteCuCoeficient(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "U", HttpContext.Current.Session["coeficientTopRestantieri"].ToString());
            return chiriiTampon;
        }


        #endregion

        #region raport chirii / utilitati pe doua luni



        public static List<Chirii> RaportChiriiPe2Luni()
        {
            List<Chirii> chirii = RaportChiriiPe2Luni(Convert.ToInt32(HttpContext.Current.Session["societateId"]));
            return chirii;
        }
        public static List<Chirii> RaportChiriiPe2Luni(int societateId)
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            //    chirii = SAGA.CitesteContracteDinFacturare(societateId);
            //  chirii = SAGA.ListaContracteIncadrateInPerioadaRaportului()


            DateTime data = DateTime.Now;

            data = Convert.ToDateTime(HttpContext.Current.Session["lunaRapoarteNominale"].ToString(), new CultureInfo("ro-RO")).AddMonths(1).AddDays(-1);
            chirii = SAGA.ListaContracteActive(societateId, data.AddDays(-data.Day + 1));
            if (chirii.Count == 0) return chirii;

            // umplem data raportului pentru primul contract, sa fim siguri
            chirii[0].DataInceputSoldLunaAnterioaraARaportului = data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day);
            chirii[0].DataFinalSoldLunaAnterioaraARaportului = data.AddDays(-data.Day);
            chirii[0].LunaAnterioaraARaportului = data.AddDays(-data.Day);
            chirii[0].LunaCurentaARaportului = data.AddDays(-data.Day).AddMonths(1);

            List<Chirii> chiriiTampon = new List<Chirii>();

            // daca e lista cu clientii plecati luam lista din SAGA
            if (HttpContext.Current.Session["esteListaContracteActive"].ToString() == "0")
            {
                chirii = SAGA.CitesteSolduriDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data.AddDays(-data.Day + 1), "ClientiPlecati", "%");
                HttpContext.Current.Session["esteListaContracteActive"] = "1";
                HttpContext.Current.Session["esteListaContracteActiveDupaPopulareClienti"] = "1";
            }

            //chirii
            // sold la inceputul lunii anterioare
            chiriiTampon = SAGA.CitesteSolduriDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), "SoldLaInceputulLuniiAnterioareARaportului", "K");
            // sold la finalul lunii anterioare
            chiriiTampon = SAGA.CitesteSolduriDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data.AddDays(-data.Day), "SoldLaFinalulLuniiAnterioareARaportului", "K");
            // sume pe luna anterioara
            chiriiTampon = SAGA.CitesteSolduriDinSAGAPanaLaData(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "SumePeLunaAnterioaraARaportului", "K");
            // sume pe luna curenta
            chiriiTampon = SAGA.CitesteSolduriDinSAGAPanaLaData(chirii, data.AddDays(-data.Day), data, "SumePeLunaCurentaARaportului", "K");

            // utilitati
            // sold la inceputul lunii anterioare
            chiriiTampon = SAGA.CitesteSolduriDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), "SoldUtilitatiLaInceputulLuniiAnterioareARaportului", "U");
            // sold la finalul lunii anterioare
            chiriiTampon = SAGA.CitesteSolduriDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data.AddDays(-data.Day), "SoldUtilitatiLaFinalulLuniiAnterioareARaportului", "U");
            // sume pe luna anterioara
            chiriiTampon = SAGA.CitesteSolduriDinSAGAPanaLaData(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "SumeUtilitatiPeLunaAnterioaraARaportului", "U");
            // sume pe luna curenta
            chiriiTampon = SAGA.CitesteSolduriDinSAGAPanaLaData(chirii, data.AddDays(-data.Day), data, "SumeUtilitatiPeLunaCurentaARaportului", "U");

            // alte debite
            if (HttpContext.Current.Session["esteListaContracteActiveDupaPopulareClienti"].ToString() == "0")
            {
                return chirii;
            }
            else
            {
                // sold la inceputul lunii anterioare
                chiriiTampon = SAGA.CitesteSolduriDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), "SoldAlteDebiteLaInceputulLuniiAnterioareARaportului", "X");
                // sold la finalul lunii anterioare
                chiriiTampon = SAGA.CitesteSolduriDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data.AddDays(-data.Day), "SoldAlteDebiteLaFinalulLuniiAnterioareARaportului", "X");
                // sume pe luna anterioara
                chiriiTampon = SAGA.CitesteSolduriDinSAGAPanaLaData(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "SumeAlteDebitePeLunaAnterioaraARaportului", "X");
                // sume pe luna curenta
                chiriiTampon = SAGA.CitesteSolduriDinSAGAPanaLaData(chirii, data.AddDays(-data.Day), data, "SumeAlteDebitePeLunaCurentaARaportului", "X");
            }

            return chirii;
        }

        #endregion

        #region rapoarte sintetice

        public static List<Chirii> CursBNR(List<Chirii> chirii)
        {
            Clase.Curs curs = new Clase.Curs();
            curs = Clase.Curs.Cauta(chirii[0].LunaAnterioaraARaportului);
            foreach (Chirii chirie in chirii)
            {
                chirie.CursBNRLunaAnterioaraARaportului = curs.Valoare;
            }
            return chirii;
        }
        public static List<Chirii> RaportSintetice()
        {
            // raport sintetice - luna anterioara
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            List<Chirii> chiriiTampon = new List<Chirii>();
            //chirii = SAGA.CitesteSinteticeDinFacturare();


            DateTime data = DateTime.Now;

            //data = DateTime.Now.AddMonths(1).AddDays(-1);

            chirii = SAGA.ListaContracteIncadrateInPerioadaRaportului(data.AddMonths(-1).AddDays(-data.Day + 1));
            if (chirii.Count == 0) return chirii;
            chirii = CursBNR(chirii);
            //chirii
            // sold la inceputul lunii anterioare
            chiriiTampon = SAGA.CitesteSolduriPeSocietateDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), "SoldLaInceputulLuniiAnterioareARaportului", "K");
            // sold la finalul lunii anterioare
            chiriiTampon = SAGA.CitesteSolduriPeSocietateDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data.AddDays(-data.Day), "SoldLaFinalulLuniiAnterioareARaportului", "K");
            // sume pe luna anterioara
            chiriiTampon = SAGA.CitesteSolduriPeSocietateDinSAGAPanaLaData(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "SumePeLunaAnterioaraARaportului", "K");

            // utilitati
            // sold la inceputul lunii anterioare
            chiriiTampon = SAGA.CitesteSolduriPeSocietateDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), "SoldUtilitatiLaInceputulLuniiAnterioareARaportului", "U");
            // sold la finalul lunii anterioare
            chiriiTampon = SAGA.CitesteSolduriPeSocietateDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data.AddDays(-data.Day), "SoldUtilitatiLaFinalulLuniiAnterioareARaportului", "U");
            // sume pe luna anterioara
            chiriiTampon = SAGA.CitesteSolduriPeSocietateDinSAGAPanaLaData(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "SumeUtilitatiPeLunaAnterioaraARaportului", "U");
            return chirii;
        }
        public static List<Chirii> RaportSinteticeLunaCurenta()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            List<Chirii> chiriiTampon = new List<Chirii>();
            // chirii = SAGA.CitesteSinteticeDinFacturare();

            DateTime data = DateTime.Now;

            chirii = SAGA.ListaContracteIncadrateInPerioadaRaportului(data.AddDays(-data.Day + 1));
            if (chirii.Count == 0) return chirii;
            chirii[0].LunaAnterioaraARaportului = data;

            //chirii
            // sold la inceputul lunii curente
            chiriiTampon = SAGA.CitesteSolduriPeSocietateDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data.AddDays(-data.Day), "SoldLaInceputulLuniiAnterioareARaportului", "K");
            // sold la finalul lunii curente
            chiriiTampon = SAGA.CitesteSolduriPeSocietateDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data, "SoldLaFinalulLuniiAnterioareARaportului", "K");
            // sume pe luna curente
            chiriiTampon = SAGA.CitesteSolduriPeSocietateDinSAGAPanaLaData(chirii, data.AddDays(-data.Day + 1), data, "SumePeLunaAnterioaraARaportului", "K");

            // utilitati
            // sold la inceputul lunii curente
            chiriiTampon = SAGA.CitesteSolduriPeSocietateDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data.AddDays(-data.Day), "SoldUtilitatiLaInceputulLuniiAnterioareARaportului", "U");
            // sold la finalul lunii curente
            chiriiTampon = SAGA.CitesteSolduriPeSocietateDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data, "SoldUtilitatiLaFinalulLuniiAnterioareARaportului", "U");
            // sume pe luna curente
            chiriiTampon = SAGA.CitesteSolduriPeSocietateDinSAGAPanaLaData(chirii, data.AddDays(-data.Day + 1), data, "SumeUtilitatiPeLunaAnterioaraARaportului", "U");
            return chirii;
        }
        public static List<Chirii> RaportSinteticeLunaAleasa()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            List<Chirii> chiriiTampon = new List<Chirii>();
            //chirii = SAGA.CitesteSinteticeDinFacturare();

            DateTime data = DateTime.Now;
            // luam luna din Sesiune
            data = Convert.ToDateTime(HttpContext.Current.Session["lunaRapoarteSintetice"].ToString(), new CultureInfo("ro-RO")).AddMonths(1).AddDays(-1);
            chirii = SAGA.ListaContracteIncadrateInPerioadaRaportului(data.AddDays(-data.Day + 1));

            if (chirii.Count == 0) return chirii;
           

            chirii[0].LunaAnterioaraARaportului = data;
            chirii[0].DataRaportarii = data.AddDays(-data.Day + 1);
            chirii = CursBNR(chirii);

            //chirii
            // sold la inceputul lunii curente
            chiriiTampon = SAGA.CitesteSolduriPeSocietateDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data.AddDays(-data.Day), "SoldLaInceputulLuniiAnterioareARaportului", "K");
            // sold la finalul lunii curente
            chiriiTampon = SAGA.CitesteSolduriPeSocietateDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data, "SoldLaFinalulLuniiAnterioareARaportului", "K");
            // sume pe luna curente
            chiriiTampon = SAGA.CitesteSolduriPeSocietateDinSAGAPanaLaData(chirii, data.AddDays(-data.Day + 1), data, "SumePeLunaAnterioaraARaportului", "K");

            // utilitati
            // sold la inceputul lunii curente
            chiriiTampon = SAGA.CitesteSolduriPeSocietateDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data.AddDays(-data.Day), "SoldUtilitatiLaInceputulLuniiAnterioareARaportului", "U");
            // sold la finalul lunii curente
            chiriiTampon = SAGA.CitesteSolduriPeSocietateDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data, "SoldUtilitatiLaFinalulLuniiAnterioareARaportului", "U");
            // sume pe luna curente
            chiriiTampon = SAGA.CitesteSolduriPeSocietateDinSAGAPanaLaData(chirii, data.AddDays(-data.Day + 1), data, "SumeUtilitatiPeLunaAnterioaraARaportului", "U");

            // alte debite
            if (HttpContext.Current.Session["esteListaContracteActive"].ToString() == "1")
            {
                return chirii;
            }
            else
            {
                // sold la inceputul lunii curente
                chiriiTampon = SAGA.CitesteSolduriPeSocietateDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data.AddDays(-data.Day + 1), "SoldAlteDebiteLaInceputulLuniiAnterioareARaportului", "X");
                // sold la finalul lunii curente
                chiriiTampon = SAGA.CitesteSolduriPeSocietateDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data, "SoldAlteDebiteLaFinalulLuniiAnterioareARaportului", "X");
                // sume pe luna curente
                chiriiTampon = SAGA.CitesteSolduriPeSocietateDinSAGAPanaLaData(chirii, data.AddDays(-data.Day + 1), data, "SumeAlteDebitePeLunaAnterioaraARaportului", "X");
            }
            return chirii;
        }

        #endregion

        #region debitori

        public static List<Debitor> RaportDebitoriAstral()
        {
            List<Debitor> debitori = RaportDebitoriPeSocietate(42);
            return debitori;
        }

        public static List<Debitor> RaportDebitoriAlimentara()
        {
            List<Debitor> debitori = RaportDebitoriPeSocietate(49);
            return debitori;
        }

        public static List<Debitor> RaportDebitoriComtex()
        {
            List<Debitor> debitori = RaportDebitoriPeSocietate(52);
            return debitori;
        }

        public static List<Debitor> RaportDebitoriCometex()
        {
            List<Debitor> debitori = RaportDebitoriPeSocietate(48);
            return debitori;
        }

        public static List<Debitor> RaportDebitoriGaleriileVictoria()
        {
            List<Debitor> debitori = RaportDebitoriPeSocietate(43);
            return debitori;
        }

        public static List<Debitor> RaportDebitoriMinerva()
        {
            List<Debitor> debitori = RaportDebitoriPeSocietate(44);
            return debitori;
        }

        public static List<Debitor> RaportDebitoriMinervaGaleriileComerciale()
        {
            List<Debitor> debitori = RaportDebitoriPeSocietate(46);
            return debitori;
        }

        public static List<Debitor> RaportDebitoriRomana()
        {
            List<Debitor> debitori = RaportDebitoriPeSocietate(53);
            return debitori;
        }

        public static List<Debitor> RaportDebitoriUnicom()
        {
            List<Debitor> debitori = RaportDebitoriPeSocietate(50);
            return debitori;
        }


        public static List<Debitor> RaportDebitoriPeSocietate(int idSocietateDinFacturare)
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(idSocietateDinFacturare);
            List<Debitor> debitoriTampon = new List<Debitor>();
            if (chirii.Count == 0) return debitoriTampon;
            DateTime data = DateTime.Now;

            debitoriTampon = SAGA.CitesteDebitoriDinSAGA(chirii);

            return debitoriTampon;
        }


        //public static List<Debitor> ListaDebitori()
        //{
        //    CultureInfo[] culture = { new CultureInfo("ro-RO") };
        //    List<Chirii> chirii = new List<Chirii>();
        //    //    chirii = SAGA.CitesteContracteDinFacturare(societateId);
        //    //  chirii = SAGA.ListaContracteIncadrateInPerioadaRaportului()


        //    DateTime data = DateTime.Now;

        //    data = Convert.ToDateTime(HttpContext.Current.Session["lunaRapoarteNominale"].ToString(), new CultureInfo("ro-RO")).AddMonths(1).AddDays(-1);
        //    chirii = SAGA.ListaContracteActive(societateId, data.AddDays(-data.Day + 1));
        //    if (chirii.Count == 0) return chirii;

        //    // umplem data raportului pentru primul contract, sa fim siguri
        //    chirii[0].DataInceputSoldLunaAnterioaraARaportului = data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day);
        //    chirii[0].DataFinalSoldLunaAnterioaraARaportului = data.AddDays(-data.Day);
        //    chirii[0].LunaAnterioaraARaportului = data.AddDays(-data.Day);
        //    chirii[0].LunaCurentaARaportului = data.AddDays(-data.Day).AddMonths(1);

        //    List<Chirii> chiriiTampon = new List<Chirii>();

        //    // daca e lista cu clientii plecati luam lista din SAGA
        //    if (HttpContext.Current.Session["esteListaContracteActive"].ToString() == "0")
        //    {
        //        chirii = SAGA.CitesteSolduriDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data.AddDays(-data.Day + 1), "ClientiPlecati", "%");
        //        HttpContext.Current.Session["esteListaContracteActive"] = "1";
        //        HttpContext.Current.Session["esteListaContracteActiveDupaPopulareClienti"] = "1";
        //    }

        //    //chirii
        //    // sold la inceputul lunii anterioare
        //    chiriiTampon = SAGA.CitesteSolduriDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), "SoldLaInceputulLuniiAnterioareARaportului", "K");
        //    // sold la finalul lunii anterioare
        //    chiriiTampon = SAGA.CitesteSolduriDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data.AddDays(-data.Day), "SoldLaFinalulLuniiAnterioareARaportului", "K");
        //    // sume pe luna anterioara
        //    chiriiTampon = SAGA.CitesteSolduriDinSAGAPanaLaData(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "SumePeLunaAnterioaraARaportului", "K");
        //    // sume pe luna curenta
        //    chiriiTampon = SAGA.CitesteSolduriDinSAGAPanaLaData(chirii, data.AddDays(-data.Day), data, "SumePeLunaCurentaARaportului", "K");

        //    // utilitati
        //    // sold la inceputul lunii anterioare
        //    chiriiTampon = SAGA.CitesteSolduriDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), "SoldUtilitatiLaInceputulLuniiAnterioareARaportului", "U");
        //    // sold la finalul lunii anterioare
        //    chiriiTampon = SAGA.CitesteSolduriDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data.AddDays(-data.Day), "SoldUtilitatiLaFinalulLuniiAnterioareARaportului", "U");
        //    // sume pe luna anterioara
        //    chiriiTampon = SAGA.CitesteSolduriDinSAGAPanaLaData(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "SumeUtilitatiPeLunaAnterioaraARaportului", "U");
        //    // sume pe luna curenta
        //    chiriiTampon = SAGA.CitesteSolduriDinSAGAPanaLaData(chirii, data.AddDays(-data.Day), data, "SumeUtilitatiPeLunaCurentaARaportului", "U");

        //    // alte debite
        //    if (HttpContext.Current.Session["esteListaContracteActiveDupaPopulareClienti"].ToString() == "0")
        //    {
        //        return chirii;
        //    }
        //    else
        //    {
        //        // sold la inceputul lunii anterioare
        //        chiriiTampon = SAGA.CitesteSolduriDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), "SoldAlteDebiteLaInceputulLuniiAnterioareARaportului", "X");
        //        // sold la finalul lunii anterioare
        //        chiriiTampon = SAGA.CitesteSolduriDinSAGAPanaLaData(chirii, Convert.ToDateTime("01.01.1900", new CultureInfo("ro-RO")), data.AddDays(-data.Day), "SoldAlteDebiteLaFinalulLuniiAnterioareARaportului", "X");
        //        // sume pe luna anterioara
        //        chiriiTampon = SAGA.CitesteSolduriDinSAGAPanaLaData(chirii, data.AddDays(-data.Day).AddDays(-data.AddDays(-data.Day).Day), data.AddDays(-data.Day), "SumeAlteDebitePeLunaAnterioaraARaportului", "X");
        //        // sume pe luna curenta
        //        chiriiTampon = SAGA.CitesteSolduriDinSAGAPanaLaData(chirii, data.AddDays(-data.Day), data, "SumeAlteDebitePeLunaCurentaARaportului", "X");
        //    }

        //    return chirii;
        //}

        public class Debitor
        {
            /* diverse */
            private DateTime dataRaportarii;
            public DateTime DataRaportarii { get { return dataRaportarii; } set { dataRaportarii = value; } }

            /* societate */
            private string societateDenumire;
            public string SocietateDenumire { get { return societateDenumire; } set { societateDenumire = value; } }
            private Int32 societateId;
            public Int32 SocietateId { get { return societateId; } set { societateId = value; } }
            private string societateFolderSaga;
            public string SocietateFolderSaga { get { return societateFolderSaga; } set { societateFolderSaga = value; } }

           

            /* debitori */
            private string debitorIdSAGA_Cod;
            public string DebitorIdSAGA_Cod { get { return debitorIdSAGA_Cod; } set { debitorIdSAGA_Cod = value; } }
            private string debitorDenumireSAGA;
            public string DebitorDenumireSAGA { get { return debitorDenumireSAGA; } set { debitorDenumireSAGA = value; } }
            private string debitorAnalitic;
            public string DebitorAnalitic { get { return debitorAnalitic; } set { debitorAnalitic = value; } }
            private string debitorJudet;
            public string DebitorJudet { get { return debitorJudet; } set { debitorJudet = value; } }
            private string debitorAdresa;
            public string DebitorAdresa { get { return debitorAdresa; } set { debitorAdresa = value; } }
            private string debitorReg_Com;
            public string DebitorReg_Com { get { return debitorReg_Com; } set { debitorReg_Com = value; } }
            private string debitorCod_Fiscal;
            public string DebitorCod_Fiscal { get { return debitorCod_Fiscal; } set { debitorCod_Fiscal = value; } }

            /* sume  */

            private DateTime dataCurentaARaportului;
            public DateTime DataCurentaARaportului { get { return dataCurentaARaportului; } set { dataCurentaARaportului = value; } }

            private decimal soldDinFacturi;
            public decimal SoldDinFacturi { get { return soldDinFacturi; } set { soldDinFacturi = value; } }

            private decimal soldDinRegistru;
            public decimal SoldDinRegistru { get { return soldDinRegistru; } set { soldDinRegistru = value; } }

            private decimal soldLaFinalulLuniiAnterioareARaportului;
            public decimal SoldLaFinalulLuniiAnterioareARaportului { get { return soldLaFinalulLuniiAnterioareARaportului; } set { soldLaFinalulLuniiAnterioareARaportului = value; } }

            private DateTime dataUltimeiFacturi;
            public DateTime DataUltimeiFacturi { get { return dataUltimeiFacturi; } set { dataUltimeiFacturi = value; } }

            public Debitor()
            {

            }
        }
        #endregion

        #region clasa obiect chirii
        public class Chirii
        {
            /* diverse */
            private DateTime dataRaportarii;
            public DateTime DataRaportarii { get { return dataRaportarii; } set { dataRaportarii = value; } }

            /* societate */
            private string societateDenumire;
            public string SocietateDenumire { get { return societateDenumire; } set { societateDenumire = value; } }
            private Int32 societateId;
            public Int32 SocietateId { get { return societateId; } set { societateId = value; } }
            private string societateFolderSaga;
            public string SocietateFolderSaga { get { return societateFolderSaga; } set { societateFolderSaga = value; } }

            private decimal societateSuprafataInchiriabilaTotala;
            public decimal SocietateSuprafataInchiriabilaTotala { get { return societateSuprafataInchiriabilaTotala; } set { societateSuprafataInchiriabilaTotala = value; } }
            private decimal societateSuprafataInchiriataTotala;
            public decimal SocietateSuprafataInchiriataTotala { get { return societateSuprafataInchiriataTotala; } set { societateSuprafataInchiriataTotala = value; } }
            private decimal societateSuprafataNeInchiriata;
            public decimal SocietateSuprafataNeInchiriata { get { return societateSuprafataNeInchiriata; } set { societateSuprafataNeInchiriata = value; } }
            private decimal societateProcentInchiriere;
            public decimal SocietateProcentInchiriere { get { return societateProcentInchiriere; } set { societateProcentInchiriere = value; } }

            private decimal societateChiriaTotalaPeLuna;
            public decimal SocietateChiriaTotalaPeLuna { get { return societateChiriaTotalaPeLuna; } set { societateChiriaTotalaPeLuna = value; } }
            private decimal societateChiriaUnitaraMedie;
            public decimal SocietateChiriaUnitaraMedie { get { return societateChiriaUnitaraMedie; } set { societateChiriaUnitaraMedie = value; } }




            private decimal societateNumarContracteInchiriere;
            public decimal SocietateNumarContracteInchiriere { get { return societateNumarContracteInchiriere; } set { societateNumarContracteInchiriere = value; } }
            private decimal societateNumarRestantieriLaChirie;
            public decimal SocietateNumarRestantieriLaChirie { get { return societateNumarRestantieriLaChirie; } set { societateNumarRestantieriLaChirie = value; } }
            private decimal societateNumarRestantieriLaUtilitati;
            public decimal SocietateNumarRestantieriLaUtilitati { get { return societateNumarRestantieriLaUtilitati; } set { societateNumarRestantieriLaUtilitati = value; } }

            /* chiriasi */
            private Int32 chiriasIdFacturare;
            public Int32 ChiriasIdFacturare { get { return chiriasIdFacturare; } set { chiriasIdFacturare = value; } }
            private string chiriasIdSAGA_Cod;
            public string ChiriasIdSAGA_Cod { get { return chiriasIdSAGA_Cod; } set { chiriasIdSAGA_Cod = value; } }
            private string chiriasDenumireSAGA;
            public string ChiriasDenumireSAGA { get { return chiriasDenumireSAGA; } set { chiriasDenumireSAGA = value; } }
            private string chiriasAnalitic;
            public string ChiriasAnalitic { get { return chiriasAnalitic; } set { chiriasAnalitic = value; } }

            /* contract */
            private Int32 contractIdFacturare;
            public Int32 ContractIdFacturare { get { return contractIdFacturare; } set { contractIdFacturare = value; } }
            private string contractDataStart;
            public string ContractDataStart { get { return contractDataStart; } set { contractDataStart = value; } }
            private string contractDataFinal;
            public string ContractDataFinal { get { return contractDataFinal; } set { contractDataFinal = value; } }
            private string contractSuprafataInchiriata;
            public string ContractSuprafataInchiriata { get { return contractSuprafataInchiriata; } set { contractSuprafataInchiriata = value; } }
            private string contractChiriaUnitara;
            public string ContractChiriaUnitara { get { return contractChiriaUnitara; } set { contractChiriaUnitara = value; } }
            private string contractChiriaTotala;
            public string ContractChiriaTotala { get { return contractChiriaTotala; } set { contractChiriaTotala = value; } }
            private string contractCursSchimbIdFacturare;
            public string ContractCursSchimbIdFacturare { get { return contractCursSchimbIdFacturare; } set { contractCursSchimbIdFacturare = value; } }
            private string contractCursSchimbDenumire;
            public string ContractCursSchimbDenumire { get { return contractCursSchimbDenumire; } set { contractCursSchimbDenumire = value; } }

            /* locatii */
            private string locatieIdFacturare;
            public string LocatieIdFacturare { get { return locatieIdFacturare; } set { locatieIdFacturare = value; } }
            private string locatieAdresa;
            public string LocatieAdresa { get { return locatieAdresa; } set { locatieAdresa = value; } }

            /* sume lunare */

            private decimal sumaLuna1ChirieFacturata;
            public decimal SumaLuna1ChirieFacturata { get { return sumaLuna1ChirieFacturata; } set { sumaLuna1ChirieFacturata = value; } }
            private decimal sumaLuna2ChirieFacturata;
            public decimal SumaLuna2ChirieFacturata { get { return sumaLuna2ChirieFacturata; } set { sumaLuna2ChirieFacturata = value; } }
            private decimal sumaLuna3ChirieFacturata;
            public decimal SumaLuna3ChirieFacturata { get { return sumaLuna3ChirieFacturata; } set { sumaLuna3ChirieFacturata = value; } }
            private decimal sumaLuna4ChirieFacturata;
            public decimal SumaLuna4ChirieFacturata { get { return sumaLuna4ChirieFacturata; } set { sumaLuna4ChirieFacturata = value; } }
            private decimal sumaLuna5ChirieFacturata;
            public decimal SumaLuna5ChirieFacturata { get { return sumaLuna5ChirieFacturata; } set { sumaLuna5ChirieFacturata = value; } }
            private decimal sumaLuna6ChirieFacturata;
            public decimal SumaLuna6ChirieFacturata { get { return sumaLuna6ChirieFacturata; } set { sumaLuna6ChirieFacturata = value; } }
            private decimal sumaLuna7ChirieFacturata;
            public decimal SumaLuna7ChirieFacturata { get { return sumaLuna7ChirieFacturata; } set { sumaLuna7ChirieFacturata = value; } }
            private decimal sumaLuna8ChirieFacturata;
            public decimal SumaLuna8ChirieFacturata { get { return sumaLuna8ChirieFacturata; } set { sumaLuna8ChirieFacturata = value; } }
            private decimal sumaLuna9ChirieFacturata;
            public decimal SumaLuna9ChirieFacturata { get { return sumaLuna9ChirieFacturata; } set { sumaLuna9ChirieFacturata = value; } }
            private decimal sumaLuna10ChirieFacturata;
            public decimal SumaLuna10ChirieFacturata { get { return sumaLuna10ChirieFacturata; } set { sumaLuna10ChirieFacturata = value; } }
            private decimal sumaLuna11ChirieFacturata;
            public decimal SumaLuna11ChirieFacturata { get { return sumaLuna11ChirieFacturata; } set { sumaLuna11ChirieFacturata = value; } }
            private decimal sumaLuna12ChirieFacturata;
            public decimal SumaLuna12ChirieFacturata { get { return sumaLuna12ChirieFacturata; } set { sumaLuna12ChirieFacturata = value; } }

            private decimal sumaLuna1ChirieIncasata;
            public decimal SumaLuna1ChirieIncasata { get { return sumaLuna1ChirieIncasata; } set { sumaLuna1ChirieIncasata = value; } }
            private decimal sumaLuna2ChirieIncasata;
            public decimal SumaLuna2ChirieIncasata { get { return sumaLuna2ChirieIncasata; } set { sumaLuna2ChirieIncasata = value; } }
            private decimal sumaLuna3ChirieIncasata;
            public decimal SumaLuna3ChirieIncasata { get { return sumaLuna3ChirieIncasata; } set { sumaLuna3ChirieIncasata = value; } }
            private decimal sumaLuna4ChirieIncasata;
            public decimal SumaLuna4ChirieIncasata { get { return sumaLuna4ChirieIncasata; } set { sumaLuna4ChirieIncasata = value; } }
            private decimal sumaLuna5ChirieIncasata;
            public decimal SumaLuna5ChirieIncasata { get { return sumaLuna5ChirieIncasata; } set { sumaLuna5ChirieIncasata = value; } }
            private decimal sumaLuna6ChirieIncasata;
            public decimal SumaLuna6ChirieIncasata { get { return sumaLuna6ChirieIncasata; } set { sumaLuna6ChirieIncasata = value; } }
            private decimal sumaLuna7ChirieIncasata;
            public decimal SumaLuna7ChirieIncasata { get { return sumaLuna7ChirieIncasata; } set { sumaLuna7ChirieIncasata = value; } }
            private decimal sumaLuna8ChirieIncasata;
            public decimal SumaLuna8ChirieIncasata { get { return sumaLuna8ChirieIncasata; } set { sumaLuna8ChirieIncasata = value; } }
            private decimal sumaLuna9ChirieIncasata;
            public decimal SumaLuna9ChirieIncasata { get { return sumaLuna9ChirieIncasata; } set { sumaLuna9ChirieIncasata = value; } }
            private decimal sumaLuna10ChirieIncasata;
            public decimal SumaLuna10ChirieIncasata { get { return sumaLuna10ChirieIncasata; } set { sumaLuna10ChirieIncasata = value; } }
            private decimal sumaLuna11ChirieIncasata;
            public decimal SumaLuna11ChirieIncasata { get { return sumaLuna11ChirieIncasata; } set { sumaLuna11ChirieIncasata = value; } }
            private decimal sumaLuna12ChirieIncasata;
            public decimal SumaLuna12ChirieIncasata { get { return sumaLuna12ChirieIncasata; } set { sumaLuna12ChirieIncasata = value; } }

            private decimal sumaLuna1UtilitatiFacturata;
            public decimal SumaLuna1UtilitatiFacturata { get { return sumaLuna1UtilitatiFacturata; } set { sumaLuna1UtilitatiFacturata = value; } }
            private decimal sumaLuna2UtilitatiFacturata;
            public decimal SumaLuna2UtilitatiFacturata { get { return sumaLuna2UtilitatiFacturata; } set { sumaLuna2UtilitatiFacturata = value; } }
            private decimal sumaLuna3UtilitatiFacturata;
            public decimal SumaLuna3UtilitatiFacturata { get { return sumaLuna3UtilitatiFacturata; } set { sumaLuna3UtilitatiFacturata = value; } }
            private decimal sumaLuna4UtilitatiFacturata;
            public decimal SumaLuna4UtilitatiFacturata { get { return sumaLuna4UtilitatiFacturata; } set { sumaLuna4UtilitatiFacturata = value; } }
            private decimal sumaLuna5UtilitatiFacturata;
            public decimal SumaLuna5UtilitatiFacturata { get { return sumaLuna5UtilitatiFacturata; } set { sumaLuna5UtilitatiFacturata = value; } }
            private decimal sumaLuna6UtilitatiFacturata;
            public decimal SumaLuna6UtilitatiFacturata { get { return sumaLuna6UtilitatiFacturata; } set { sumaLuna6UtilitatiFacturata = value; } }
            private decimal sumaLuna7UtilitatiFacturata;
            public decimal SumaLuna7UtilitatiFacturata { get { return sumaLuna7UtilitatiFacturata; } set { sumaLuna7UtilitatiFacturata = value; } }
            private decimal sumaLuna8UtilitatiFacturata;
            public decimal SumaLuna8UtilitatiFacturata { get { return sumaLuna8UtilitatiFacturata; } set { sumaLuna8UtilitatiFacturata = value; } }
            private decimal sumaLuna9UtilitatiFacturata;
            public decimal SumaLuna9UtilitatiFacturata { get { return sumaLuna9UtilitatiFacturata; } set { sumaLuna9UtilitatiFacturata = value; } }
            private decimal sumaLuna10UtilitatiFacturata;
            public decimal SumaLuna10UtilitatiFacturata { get { return sumaLuna10UtilitatiFacturata; } set { sumaLuna10UtilitatiFacturata = value; } }
            private decimal sumaLuna11UtilitatiFacturata;
            public decimal SumaLuna11UtilitatiFacturata { get { return sumaLuna11UtilitatiFacturata; } set { sumaLuna11UtilitatiFacturata = value; } }
            private decimal sumaLuna12UtilitatiFacturata;
            public decimal SumaLuna12UtilitatiFacturata { get { return sumaLuna12UtilitatiFacturata; } set { sumaLuna12UtilitatiFacturata = value; } }

            private decimal sumaLuna1UtilitatiIncasata;
            public decimal SumaLuna1UtilitatiIncasata { get { return sumaLuna1UtilitatiIncasata; } set { sumaLuna1UtilitatiIncasata = value; } }
            private decimal sumaLuna2UtilitatiIncasata;
            public decimal SumaLuna2UtilitatiIncasata { get { return sumaLuna2UtilitatiIncasata; } set { sumaLuna2UtilitatiIncasata = value; } }
            private decimal sumaLuna3UtilitatiIncasata;
            public decimal SumaLuna3UtilitatiIncasata { get { return sumaLuna3UtilitatiIncasata; } set { sumaLuna3UtilitatiIncasata = value; } }
            private decimal sumaLuna4UtilitatiIncasata;
            public decimal SumaLuna4UtilitatiIncasata { get { return sumaLuna4UtilitatiIncasata; } set { sumaLuna4UtilitatiIncasata = value; } }
            private decimal sumaLuna5UtilitatiIncasata;
            public decimal SumaLuna5UtilitatiIncasata { get { return sumaLuna5UtilitatiIncasata; } set { sumaLuna5UtilitatiIncasata = value; } }
            private decimal sumaLuna6UtilitatiIncasata;
            public decimal SumaLuna6UtilitatiIncasata { get { return sumaLuna6UtilitatiIncasata; } set { sumaLuna6UtilitatiIncasata = value; } }
            private decimal sumaLuna7UtilitatiIncasata;
            public decimal SumaLuna7UtilitatiIncasata { get { return sumaLuna7UtilitatiIncasata; } set { sumaLuna7UtilitatiIncasata = value; } }
            private decimal sumaLuna8UtilitatiIncasata;
            public decimal SumaLuna8UtilitatiIncasata { get { return sumaLuna8UtilitatiIncasata; } set { sumaLuna8UtilitatiIncasata = value; } }
            private decimal sumaLuna9UtilitatiIncasata;
            public decimal SumaLuna9UtilitatiIncasata { get { return sumaLuna9UtilitatiIncasata; } set { sumaLuna9UtilitatiIncasata = value; } }
            private decimal sumaLuna10UtilitatiIncasata;
            public decimal SumaLuna10UtilitatiIncasata { get { return sumaLuna10UtilitatiIncasata; } set { sumaLuna10UtilitatiIncasata = value; } }
            private decimal sumaLuna11UtilitatiIncasata;
            public decimal SumaLuna11UtilitatiIncasata { get { return sumaLuna11UtilitatiIncasata; } set { sumaLuna11UtilitatiIncasata = value; } }
            private decimal sumaLuna12UtilitatiIncasata;
            public decimal SumaLuna12UtilitatiIncasata { get { return sumaLuna12UtilitatiIncasata; } set { sumaLuna12UtilitatiIncasata = value; } }


            private DateTime dataCurentaARaportului;
            public DateTime DataCurentaARaportului { get { return dataCurentaARaportului; } set { dataCurentaARaportului = value; } }

            private DateTime lunaCurentaARaportului;
            public DateTime LunaCurentaARaportului { get { return lunaCurentaARaportului; } set { lunaCurentaARaportului = value; } }

            private DateTime lunaAnterioaraARaportului;
            public DateTime LunaAnterioaraARaportului { get { return lunaAnterioaraARaportului; } set { lunaAnterioaraARaportului = value; } }

            private decimal cursBNRLunaAnterioaraARaportului;
            public decimal CursBNRLunaAnterioaraARaportului { get { return cursBNRLunaAnterioaraARaportului; } set { cursBNRLunaAnterioaraARaportului = value; } }


            private DateTime dataInceputSoldLunaAnterioaraARaportului;
            public DateTime DataInceputSoldLunaAnterioaraARaportului { get { return dataInceputSoldLunaAnterioaraARaportului; } set { dataInceputSoldLunaAnterioaraARaportului = value; } }

            private DateTime dataFinalSoldLunaAnterioaraARaportului;
            public DateTime DataFinalSoldLunaAnterioaraARaportului { get { return dataFinalSoldLunaAnterioaraARaportului; } set { dataFinalSoldLunaAnterioaraARaportului = value; } }

            private decimal soldLaInceputulLuniiAnterioareARaportului;
            public decimal SoldLaInceputulLuniiAnterioareARaportului { get { return soldLaInceputulLuniiAnterioareARaportului; } set { soldLaInceputulLuniiAnterioareARaportului = value; } }

            private decimal soldLaFinalulLuniiAnterioareARaportului;
            public decimal SoldLaFinalulLuniiAnterioareARaportului { get { return soldLaFinalulLuniiAnterioareARaportului; } set { soldLaFinalulLuniiAnterioareARaportului = value; } }

            private decimal sumaFacturataPeLunaAnterioaraARaportului;
            public decimal SumaFacturataPeLunaAnterioaraARaportului { get { return sumaFacturataPeLunaAnterioaraARaportului; } set { sumaFacturataPeLunaAnterioaraARaportului = value; } }

            private decimal sumaFacturataPeLunaCurentaARaportului;
            public decimal SumaFacturataPeLunaCurentaARaportului { get { return sumaFacturataPeLunaCurentaARaportului; } set { sumaFacturataPeLunaCurentaARaportului = value; } }

            private decimal sumaIncasataPeLunaAnterioaraARaportului;
            public decimal SumaIncasataPeLunaAnterioaraARaportului { get { return sumaIncasataPeLunaAnterioaraARaportului; } set { sumaIncasataPeLunaAnterioaraARaportului = value; } }

            private decimal procentIncasariPeLunaAnterioaraARaportului;
            public decimal ProcentIncasariPeLunaAnterioaraARaportului { get { return procentIncasariPeLunaAnterioaraARaportului; } set { procentIncasariPeLunaAnterioaraARaportului = value; } }

            private DateTime dataUltimaIncasareLunaAnterioaraARaportului;
            public DateTime DataUltimaIncasareLunaAnterioaraARaportului { get { return dataUltimaIncasareLunaAnterioaraARaportului; } set { dataUltimaIncasareLunaAnterioaraARaportului = value; } }

            private decimal sumaIncasataPeLunaCurentaARaportului;
            public decimal SumaIncasataPeLunaCurentaARaportului { get { return sumaIncasataPeLunaCurentaARaportului; } set { sumaIncasataPeLunaCurentaARaportului = value; } }

            private DateTime dataUltimaIncasareLunaCurentaARaportului;
            public DateTime DataUltimaIncasareLunaCurentaARaportului { get { return dataUltimaIncasareLunaCurentaARaportului; } set { dataUltimaIncasareLunaCurentaARaportului = value; } }

            private decimal nrChiriiRestante;
            public decimal NrChiriiRestante { get { return nrChiriiRestante; } set { nrChiriiRestante = value; } }


            private decimal soldUtilitatiLaInceputulLuniiAnterioareARaportului;
            public decimal SoldUtilitatiLaInceputulLuniiAnterioareARaportului { get { return soldUtilitatiLaInceputulLuniiAnterioareARaportului; } set { soldUtilitatiLaInceputulLuniiAnterioareARaportului = value; } }

            private decimal soldUtilitatiLaFinalulLuniiAnterioareARaportului;
            public decimal SoldUtilitatiLaFinalulLuniiAnterioareARaportului { get { return soldUtilitatiLaFinalulLuniiAnterioareARaportului; } set { soldUtilitatiLaFinalulLuniiAnterioareARaportului = value; } }

            private decimal sumaUtilitatiFacturataPeLunaAnterioaraARaportului;
            public decimal SumaUtilitatiFacturataPeLunaAnterioaraARaportului { get { return sumaUtilitatiFacturataPeLunaAnterioaraARaportului; } set { sumaUtilitatiFacturataPeLunaAnterioaraARaportului = value; } }

            private decimal sumaUtilitatiFacturataPeLunaCurentaARaportului;
            public decimal SumaUtilitatiFacturataPeLunaCurentaARaportului { get { return sumaUtilitatiFacturataPeLunaCurentaARaportului; } set { sumaUtilitatiFacturataPeLunaCurentaARaportului = value; } }

            private decimal sumaUtilitatiIncasataPeLunaAnterioaraARaportului;
            public decimal SumaUtilitatiIncasataPeLunaAnterioaraARaportului { get { return sumaUtilitatiIncasataPeLunaAnterioaraARaportului; } set { sumaUtilitatiIncasataPeLunaAnterioaraARaportului = value; } }

            private DateTime dataUltimaIncasareUtilitatiLunaAnterioaraARaportului;
            public DateTime DataUltimaIncasareUtilitatiLunaAnterioaraARaportului { get { return dataUltimaIncasareUtilitatiLunaAnterioaraARaportului; } set { dataUltimaIncasareUtilitatiLunaAnterioaraARaportului = value; } }

            private decimal sumaUtilitatiIncasataPeLunaCurentaARaportului;
            public decimal SumaUtilitatiIncasataPeLunaCurentaARaportului { get { return sumaUtilitatiIncasataPeLunaCurentaARaportului; } set { sumaUtilitatiIncasataPeLunaCurentaARaportului = value; } }

            private DateTime dataUltimaIncasareUtilitatiLunaCurentaARaportului;
            public DateTime DataUltimaIncasareUtilitatiLunaCurentaARaportului { get { return dataUltimaIncasareUtilitatiLunaCurentaARaportului; } set { dataUltimaIncasareUtilitatiLunaCurentaARaportului = value; } }

            private decimal procentIncasariUtilitatiPeLunaAnterioaraARaportului;
            public decimal ProcentIncasariUtilitatiPeLunaAnterioaraARaportului { get { return procentIncasariUtilitatiPeLunaAnterioaraARaportului; } set { procentIncasariUtilitatiPeLunaAnterioaraARaportului = value; } }



            private decimal soldAlteDebiteLaInceputulLuniiAnterioareARaportului;
            public decimal SoldAlteDebiteLaInceputulLuniiAnterioareARaportului { get { return soldAlteDebiteLaInceputulLuniiAnterioareARaportului; } set { soldAlteDebiteLaInceputulLuniiAnterioareARaportului = value; } }

            private decimal soldAlteDebiteLaFinalulLuniiAnterioareARaportului;
            public decimal SoldAlteDebiteLaFinalulLuniiAnterioareARaportului { get { return soldAlteDebiteLaFinalulLuniiAnterioareARaportului; } set { soldAlteDebiteLaFinalulLuniiAnterioareARaportului = value; } }

            private decimal sumaAlteDebiteFacturataPeLunaAnterioaraARaportului;
            public decimal SumaAlteDebiteFacturataPeLunaAnterioaraARaportului { get { return sumaAlteDebiteFacturataPeLunaAnterioaraARaportului; } set { sumaAlteDebiteFacturataPeLunaAnterioaraARaportului = value; } }

            private decimal sumaAlteDebiteFacturataPeLunaCurentaARaportului;
            public decimal SumaAlteDebiteFacturataPeLunaCurentaARaportului { get { return sumaAlteDebiteFacturataPeLunaCurentaARaportului; } set { sumaAlteDebiteFacturataPeLunaCurentaARaportului = value; } }

            private decimal sumaAlteDebiteIncasataPeLunaAnterioaraARaportului;
            public decimal SumaAlteDebiteIncasataPeLunaAnterioaraARaportului { get { return sumaAlteDebiteIncasataPeLunaAnterioaraARaportului; } set { sumaAlteDebiteIncasataPeLunaAnterioaraARaportului = value; } }

            private DateTime dataUltimaIncasareAlteDebiteLunaAnterioaraARaportului;
            public DateTime DataUltimaIncasareAlteDebiteLunaAnterioaraARaportului { get { return dataUltimaIncasareAlteDebiteLunaAnterioaraARaportului; } set { dataUltimaIncasareAlteDebiteLunaAnterioaraARaportului = value; } }

            private decimal sumaAlteDebiteIncasataPeLunaCurentaARaportului;
            public decimal SumaAlteDebiteIncasataPeLunaCurentaARaportului { get { return sumaAlteDebiteIncasataPeLunaCurentaARaportului; } set { sumaAlteDebiteIncasataPeLunaCurentaARaportului = value; } }

            private DateTime dataUltimaIncasareAlteDebiteLunaCurentaARaportului;
            public DateTime DataUltimaIncasareAlteDebiteLunaCurentaARaportului { get { return dataUltimaIncasareAlteDebiteLunaCurentaARaportului; } set { dataUltimaIncasareAlteDebiteLunaCurentaARaportului = value; } }

            private decimal procentIncasariAlteDebitePeLunaAnterioaraARaportului;
            public decimal ProcentIncasariAlteDebitePeLunaAnterioaraARaportului { get { return procentIncasariAlteDebitePeLunaAnterioaraARaportului; } set { procentIncasariAlteDebitePeLunaAnterioaraARaportului = value; } }



            private decimal coeficientChiriiRestante;
            public decimal CoeficientChiriiRestante { get { return coeficientChiriiRestante; } set { coeficientChiriiRestante = value; } }
            private decimal coeficientUtilitatiRestante;
            public decimal CoeficientUtilitatiRestante { get { return coeficientUtilitatiRestante; } set { coeficientUtilitatiRestante = value; } }



            public Chirii()
            {

            }
        }
        #endregion
















        #region nefolosite
        #region raport chirii / utilitati pe toate lunile
        public static List<Chirii> RaportChirii()
        {
            List<Chirii> chirii = RaportChirii(Convert.ToInt32(HttpContext.Current.Session["societateId"]));
            return chirii;
        }

        public static List<Chirii> RaportChirii(int societateId)
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            List<Chirii> chiriiTampon = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(societateId);
            if (chirii.Count == 0) return chirii;
            chiriiTampon = SAGA.CitesteContracteFacturiDinSAGA(chirii);
            return chirii;
        }

        #endregion
        #region raport chirii / utilitati pe fiecare societate - pt raport toate in unul - neutilizat
        public static List<Chirii> RaportChiriiAstral()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(42);
            if (chirii.Count == 0) return chirii;
            return SAGA.CitesteContracteFacturiDinSAGA(chirii);
        }
        public static List<Chirii> RaportChiriiGaleriileVictoria()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(43);
            if (chirii.Count == 0) return chirii;
            return SAGA.CitesteContracteFacturiDinSAGA(chirii);
        }
        public static List<Chirii> RaportChiriiMinerva()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(44);
            if (chirii.Count == 0) return chirii;
            return SAGA.CitesteContracteFacturiDinSAGA(chirii);
        }
        public static List<Chirii> RaportChiriiMinervaGaleriileComerciale()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(46);
            if (chirii.Count == 0) return chirii;
            return SAGA.CitesteContracteFacturiDinSAGA(chirii);
        }
        public static List<Chirii> RaportChiriiCometex()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(48);
            if (chirii.Count == 0) return chirii;
            return SAGA.CitesteContracteFacturiDinSAGA(chirii);
        }
        public static List<Chirii> RaportChiriiAlimentara()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(49);
            if (chirii.Count == 0) return chirii;
            return SAGA.CitesteContracteFacturiDinSAGA(chirii);
        }
        public static List<Chirii> RaportChiriiUnicom()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(50);
            if (chirii.Count == 0) return chirii;
            return SAGA.CitesteContracteFacturiDinSAGA(chirii);
        }
        public static List<Chirii> RaportChiriiComtex()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(52);
            if (chirii.Count == 0) return chirii;
            return SAGA.CitesteContracteFacturiDinSAGA(chirii);
        }
        public static List<Chirii> RaportChiriiRomana()
        {
            CultureInfo[] culture = { new CultureInfo("ro-RO") };
            List<Chirii> chirii = new List<Chirii>();
            chirii = SAGA.CitesteContracteDinFacturare(53);
            if (chirii.Count == 0) return chirii;
            return SAGA.CitesteContracteFacturiDinSAGA(chirii);
        }
        #endregion
        #endregion
    }
}
