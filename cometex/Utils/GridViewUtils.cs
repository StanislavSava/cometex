﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using cometex.Clase;


namespace cometex
{
    public static class GridViewUtils
    {
        public static bool VerificaDacaRandEsteNeselectat(GridView GridView)
        {
            if (VerificaDacaExistaRanduri(GridView))
            {
                if (GridView.SelectedIndex == -1)
                {
                    return true;
                }

                return false;
            }
            return true;
        }
        public static bool VerificaDacaExistaRanduri(GridView Gridview)
        {
            if (Gridview.Rows.Count < 1)
                return false;
            else return true;
        }
        public static void InlocuiesteDateInGrid(GridView gridview, int coloana, string ceSaInlocuiasca, string cuCeSaInlocuiasca)
        {
            string scannedNumber = ceSaInlocuiasca;
            var matchedRows = gridview.Rows.Cast<GridViewRow>().Where(r => scannedNumber.Equals(r.Cells[coloana].Text));
            foreach (var row in matchedRows)
            {
                row.Cells[coloana].Text = cuCeSaInlocuiasca;
            }
        }
        ///<summary>
        ///Inlocuieste date in grid pentru mai multe coloane, cautand o singura valoare in PRIMA coloana definita, si inlocuind apoi diverse valori in alte coloane.
        ///</summary>
        public static void InlocuiesteDateInGrid(GridView grid, List<int> coloane, string ceSaInlocuiasca, List<string> cuCeSaInlocuiasca)
        {
            string valoareaCautata = ceSaInlocuiasca;
            int coloanaDeCautat = coloane[0];
            var randuriGasite = grid.Rows.Cast<GridViewRow>().Where(r => valoareaCautata.Equals(((Label)r.Cells[coloanaDeCautat].FindControl("")).Text));
            foreach (var rand in randuriGasite)
            {
                int indexColoana = 0;
                foreach (int coloana in coloane)
                {
                    rand.Cells[coloana].Text = cuCeSaInlocuiasca[indexColoana];
                    indexColoana++;
                }
            }
        }
        ///<summary>
        ///Inlocuieste date in grid pentru mai multe coloane, cautand o singura valoare in PRIMA coloana definita, si inlocuind apoi diverse valori in alte coloane.
        ///</summary>
        public static void InlocuiesteDateInGrid(GridView grid, string ceSaInlocuiasca, string cuCeSaInlocuiasca)
        {
            string valoareaCautata = ceSaInlocuiasca;
            //    GridViewRowCollection randuriGasite = grid.Rows.Cast<GridViewRow>().Where(r => valoareaCautata.Equals(r.Cells..Text));
            foreach (GridViewRow rand in grid.Rows)
            {
                foreach (TableCell celula in rand.Cells)
                {
                    if (((Label)celula.FindControl("LabelRezolutieData")).Text.Contains("1900"))
                    {
                        ((Label)celula.FindControl("LabelRezolutieData")).Text = cuCeSaInlocuiasca;
                    }
                }
            }
        }
        public static void UnesteCeluleFooter(GridView grid, Int32 start)
        {
            int numarCeluleInitial = 0;
            int numarCelule = 0;
            if (grid.Rows.Count > 0) numarCelule = numarCeluleInitial = grid.FooterRow.Cells.Count;
            for (int i = numarCelule; i-- > 0;)
                if (i != start - 1) grid.FooterRow.Cells.RemoveAt(i);
            if (grid.Rows.Count > 0) grid.FooterRow.Cells[0].ColumnSpan = numarCeluleInitial;
        }
        public static Cheltuieli DateChetuialaDinRandGrid(GridViewRow rand)
        {
            Cheltuieli cheltuiala = new Cheltuieli();
            cheltuiala.CategorieCheltuiala.Id = Convert.ToInt32(((Label)rand.FindControl("lblIdCategorie")).Text);
            cheltuiala.CategorieCheltuiala.Denumire = ((Label)rand.FindControl("lblDestinatia")).Text;
            cheltuiala.CheltuialaBrut = Convert.ToDecimal(((Label)rand.FindControl("lblSuma")).Text);
            cheltuiala.DelegatAIntrodusCheltuiala.Id = Convert.ToInt32(((Label)rand.FindControl("lblIntrodusDe")).Text);
            cheltuiala.Destinatia = ((Label)rand.FindControl("lblDestinatia")).Text;
            cheltuiala.Explicatii = ((Label)rand.FindControl("lblExplicatii")).Text;
            cheltuiala.IdCheltuieli = Convert.ToInt32(((Label)rand.FindControl("lblIdCheltuiala")).Text);
            cheltuiala.IntrodusLaData = Convert.ToDateTime(((Label)rand.FindControl("lblDataIntroducere")).Text);
            try
            {
                cheltuiala.Plafon = Convert.ToDecimal(((Label)rand.FindControl("lblSumaPlafon")).Text);
            }
            catch { cheltuiala.Plafon = 0; }
            try
            {
                cheltuiala.RezolutionatDe.Id = Convert.ToInt32(((Label)rand.FindControl("lblIdRezolutionatDe")).Text);

                cheltuiala.RezolutionatDe.NumePrenume = ((Label)rand.FindControl("lblNumeRezolutionatDe")).Text;
                cheltuiala.RezolutionatLaData = Convert.ToDateTime(((Label)rand.FindControl("lblDataRezolutie")).Text);
            }
            catch {
                cheltuiala.RezolutionatDe.Id = 0;
                cheltuiala.RezolutionatDe.NumePrenume = "-";
            }
            cheltuiala.SAGAExplicatii = ((Label)rand.FindControl("gvLblSAGAExplicatii")).Text;
            cheltuiala.SAGAStatus = Convert.ToInt32(((Label)rand.FindControl("lblSAGAStatus")).Text);
            ////////cheltuiala.SAGASuma = Convert.ToDecimal(((Label)rand.FindControl("????")).Text);
            cheltuiala.Scadenta = Convert.ToDateTime(((Label)rand.FindControl("lblScadenta")).Text);
            //cheltuiala.SeIncadreazaInGrila
            cheltuiala.Societatea.Id = Convert.ToInt32(((Label)rand.FindControl("lblIdSocietate")).Text);
            cheltuiala.Societatea.Denumire = ((Label)rand.FindControl("lblDenumireSocietate")).Text;
            cheltuiala.Status.Id = Convert.ToInt32(((Label)rand.FindControl("lblIdStatus")).Text);
            cheltuiala.Status.Denumire = ((Label)rand.FindControl("lblDenumireStatus")).Text;
            return cheltuiala;
        }
    }
}