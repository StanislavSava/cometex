﻿using FirebirdSql.Data.FirebirdClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using cometex.Clase;
using System.Web;

namespace cometex.Utils
{
    class SAGA
    {

        /// <summary>
        /// Lista societatilor din grup
        /// </summary>
        /// <returns></returns>
        public static List<List<string>> SAGAListaSocietati()
        {
            List<List<string>> pathuri = new List<List<string>>();
            string commandText = @"SELECT  SagaPath, CompanyName, CompaniesId 
                                      FROM  Companies";

            using (SqlConnection connection = new SqlConnection(SqlConnectionServices.ConnectionString))
            {
                SqlCommand command = new SqlCommand(commandText, connection);
                command.Connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    List<string> date = new List<string>() { reader[0].ToString(), reader[1].ToString(), reader[2].ToString() };
                    pathuri.Add(date);
                }

                reader.Close();
                connection.Close();
            }
            return pathuri;
        }

        /// <summary>
        /// Pentru sintetice, sume totale pe societatile grupului
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static List<DataSourceChiriasi.Chirii> ListaContracteIncadrateInPerioadaRaportului(DateTime data)
        {
            DataSourceChiriasi.Chirii contractChirie = new DataSourceChiriasi.Chirii();
            List<DataSourceChiriasi.Chirii> listaContracteChirie = new List<DataSourceChiriasi.Chirii>();
            // consideram in functie de 01 a lunii
            // string data = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd.MM.yyyy");

            string select = @"SELECT 
                                
	                             COALESCE(sum(SurfaceShop),0) AS SuprafataInchiriata
	                            ,COALESCE(sum(coalesce(contracts.TotalSurfacePrice,0)),0) AS ChiriaTotalaPeLuna
								,Companies.CompaniesId AS SocietateId
	                            ,Companies.CompanyName AS SocietateDenumire
                                ,[Companies].[SagaPath] AS SocietateFolderSaga  
                                ,COUNT(Contracts.ContractsId) AS numarContracte
                                 FROM  [Facturare].[dbo].Companies 
                              FULL JOIN Contracts  ON [contracts].[companyid] = [companies].[companiesid]
							  WHERE
                                        [Companies].[CompaniesId] <> 37 
							        AND (
                                            (
                                                    CONVERT(datetime,contracts.StartDate,104) <= CONVERT(datetime,'" + data + @"',104)
											    AND CONVERT(datetime,contracts.FinishDate,104) >= CONVERT(datetime,'" + data + @"',104)
                                            )
                                            OR Contracts.ContractsId IS NULL
                                        )
                              GROUP BY   [Companies].[CompaniesId]
                                        ,[Companies].[CompanyName]
                                        ,[Companies].[SagaPath]
                              ORDER BY [Companies].[CompanyName]";
            using (SqlConnection connection = new SqlConnection(SqlConnectionServices.ConnectionString))
            {
                SqlCommand command = new SqlCommand(select, connection);
                command.Connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                int contor = 0;
                while (reader.Read())
                {
                    contractChirie = new DataSourceChiriasi.Chirii();
                    contractChirie.SocietateDenumire = reader["SocietateDenumire"].ToString().Trim();
                    contractChirie.SocietateId = Convert.ToInt32(reader["SocietateId"]);
                    contractChirie.SocietateFolderSaga = reader["SocietateFolderSaga"].ToString().Trim();
                    contractChirie.SocietateSuprafataInchiriabilaTotala = DatabaseUtils.DetaliiSocietateDupaIdFacturare(contractChirie.SocietateId).SuprafataTotala;
                    contractChirie.SocietateSuprafataInchiriataTotala = Convert.ToDecimal(reader["SuprafataInchiriata"]);
                    contractChirie.SocietateSuprafataNeInchiriata = contractChirie.SocietateSuprafataInchiriabilaTotala - contractChirie.SocietateSuprafataInchiriataTotala;
                    if (contractChirie.SocietateSuprafataInchiriabilaTotala > 0)
                        contractChirie.SocietateProcentInchiriere = (contractChirie.SocietateSuprafataInchiriataTotala * 100) / contractChirie.SocietateSuprafataInchiriabilaTotala;
                    else contractChirie.SocietateProcentInchiriere = 0;
                    contractChirie.SocietateChiriaTotalaPeLuna = Convert.ToDecimal(reader["ChiriaTotalaPeLuna"]);
                    if (contractChirie.SocietateSuprafataInchiriataTotala > 0)
                        contractChirie.SocietateChiriaUnitaraMedie = contractChirie.SocietateChiriaTotalaPeLuna / contractChirie.SocietateSuprafataInchiriataTotala;
                    else contractChirie.SocietateChiriaUnitaraMedie = 0;

                    if (contor == 0)
                    {
                        contractChirie.LunaAnterioaraARaportului = DateTime.Now.AddDays(-DateTime.Now.Day);
                    }
                    contractChirie.SocietateNumarContracteInchiriere = Convert.ToInt32(reader["numarContracte"]); ;
                    listaContracteChirie.Add(contractChirie);
                }
                reader.Close();
                connection.Close();
            }
            foreach (DataSourceChiriasi.Chirii chirie in listaContracteChirie)
            {

            }
            return listaContracteChirie;
        }

        /// <summary>
        /// Pentru nominale, lista cu clientii care au contracte active
        /// </summary>
        /// <param name="societate"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static List<DataSourceChiriasi.Chirii> ListaContracteActive(int societate, DateTime data)
        {
            // situatii nominale chirii si utilitati
            DataSourceChiriasi.Chirii contractChirie = new DataSourceChiriasi.Chirii();
            List<DataSourceChiriasi.Chirii> listaContracteChirie = new List<DataSourceChiriasi.Chirii>();
            // consideram in functie de 01 a lunii
            data = data.AddDays(-data.Day + 1);
            string select = @"SELECT 
                    [Companies].CompanyName as SocietateDenumire
                    ,[Companies].[CompaniesId] as SocietateId
                    ,[Companies].[SagaPath] as SocietateFolderSaga
                    ,[Clients].[ClientName] as ChiriasDenumireSAGA
                    ,[Contracts].[ClientId] as ChiriasIdFacturare
                    ,[Clients].[SagaCode] as ChiriasIdSAGA_Cod
                    ,[Locations].[LocationName] + ' ' +  [Contracts].[RentAreaPosition] as LocatieAdresa
                    ,[Contracts].[SurfaceShop] as ContractSuprafataInchiriata
                    ,[ContractsId] as ContractIdFacturare
                    ,[Contracts].[Number] as ContractNumar
                    ,[Contracts].[SurfaceShopPrice] as ContractChiriaUnitara
                    ,[Contracts].[TotalSurfacePrice] as ContractChiriaTotala
                    ,[ExchangeRate].[ExchangeName] as ContractCursSchimbDenumire
 ,CONVERT(datetime,contracts.StartDate,104) as start
,CONVERT(datetime,contracts.FinishDate,104) as finish
                      --FROM [Facturare].[dbo].[Contracts] 

                      FROM  [Facturare].[dbo].Companies 
                      FULL JOIN Contracts  ON [contracts].[companyid] = [companies].[companiesid]

                      --FULL JOIN Companies on companies.CompaniesId = Contracts.CompanyId
                      FULL JOIN Clients on Clients.ClientsId = Contracts.ClientId
                      FULL JOIN Locations on Locations.LocationId = Contracts.LocationsId
                      FULL JOIN ExchangeRate on ExchangeRate.ExchangeRateId = Contracts.ExchangeRateId
                      where 
                      [Companies].[CompaniesId] = " + societate +
                      @" AND (
                                   (
                                            CONVERT(datetime,contracts.StartDate,104) <= CONVERT(datetime,'" + data.ToString("dd.MM.yyyy") + @"',104)
									    AND CONVERT(datetime,contracts.FinishDate,104) >= CONVERT(datetime,'" + data.ToString("dd.MM.yyyy") + @"',104)
                                   )
                                   OR Contracts.ContractsId IS NULL
                                )"
                      +
                      @" ORDER BY Companies.CompanyName, ContractsId";
            using (SqlConnection connection = new SqlConnection(SqlConnectionServices.ConnectionString))
            {
                SqlCommand command = new SqlCommand(select, connection);
                command.Connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    contractChirie = new DataSourceChiriasi.Chirii();
                    contractChirie.SocietateDenumire = reader["SocietateDenumire"].ToString().Trim();
                    contractChirie.SocietateId = Convert.ToInt32(reader["SocietateId"]);
                    contractChirie.SocietateFolderSaga = reader["SocietateFolderSaga"].ToString().Trim();
                    contractChirie.ChiriasDenumireSAGA = reader["ChiriasDenumireSAGA"].ToString().Trim();
                    contractChirie.ChiriasIdFacturare = Convert.ToInt32(reader["ChiriasIdFacturare"]);
                    contractChirie.ChiriasIdSAGA_Cod = reader["ChiriasIdSAGA_Cod"].ToString().Trim();
                    contractChirie.LocatieAdresa = reader["LocatieAdresa"].ToString().Trim();
                    contractChirie.ContractSuprafataInchiriata = reader["ContractSuprafataInchiriata"].ToString().Trim();
                    contractChirie.ContractIdFacturare = Convert.ToInt32(reader["ContractIdFacturare"]);
                    contractChirie.ContractChiriaUnitara = reader["ContractChiriaUnitara"].ToString().Trim();
                    contractChirie.ContractChiriaTotala = reader["ContractChiriaTotala"].ToString().Trim();
                    contractChirie.ContractCursSchimbDenumire = reader["ContractCursSchimbDenumire"].ToString().Trim();
                    contractChirie.ChiriasAnalitic = reader["ContractNumar"].ToString().Trim();

                    contractChirie.ContractDataStart = reader["start"].ToString().Trim();
                    contractChirie.ContractDataFinal = reader["finish"].ToString().Trim();

                    listaContracteChirie.Add(contractChirie);
                }
                reader.Close();
                connection.Close();
            }
            foreach (DataSourceChiriasi.Chirii chirie in listaContracteChirie)
            {
                chirie.SocietateNumarContracteInchiriere = listaContracteChirie.Count;
            }
            return listaContracteChirie;
        }

        public static List<DataSourceChiriasi.Chirii> CitesteContracteFacturiDinSAGA(List<DataSourceChiriasi.Chirii> listaChirii)
        {
            FbConnectionStringBuilder ret = new FbConnectionStringBuilder();
            ret.Database = ConfigurationManager.AppSettings["caleSAGA"] + "\\" + listaChirii[0].SocietateFolderSaga + "\\cont_baza.fdb";
            ret.DataSource = "86.120.111.69";
            ret.Port = 3050;
            ret.UserID = "SYSDBA";
            ret.Password = "masterkey";
            ret.Charset = "NONE";
            ret.ConnectionTimeout = 60000;
            ret.Pooling = false;
            FbConnection sagaConnection = new FbConnection(ret.ToString());
            List<List<string>> lista = new List<List<string>>();
            List<string> listaElement = new List<string>();
            string select = "";
            List<DataSourceChiriasi.Chirii> listaChiriiFinala = new List<DataSourceChiriasi.Chirii>();
            DataSourceChiriasi.Chirii chirieFinala = new DataSourceChiriasi.Chirii();

            select = @"select nr_iesire, cod as ChiriasIdSAGA_Cod, denumire, data, total as SumaLunaXFacturata, total - neachitat as  SumaLunaXIncasata from IESIRI where 
                    data >= '01.01." + DateTime.Now.Year + "' and data < '01.01." + DateTime.Now.AddYears(1).Year +
                    @"'  and(nr_iesire like '%U%' or nr_iesire like '%K%')
                    order by nr_iesire, data";
            string Log = "";
            using (sagaConnection)
            {
                try
                {
                    sagaConnection.Open();
                    FbCommand fbCommand = new FbCommand(select, sagaConnection);
                    FbDataReader reader = fbCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        // extragem luna din data
                        DateTime luna = Convert.ToDateTime(reader["data"]);

                        // cautam contractul in lista de chirii
                        foreach (DataSourceChiriasi.Chirii chirie in listaChirii)
                        {
                            chirieFinala = new DataSourceChiriasi.Chirii();
                            if (chirie.ChiriasIdSAGA_Cod.Trim() == reader["ChiriasIdSAGA_Cod"].ToString().Trim())
                            {

                                if (reader["nr_iesire"].ToString().IndexOf("K") >= 0)
                                {
                                    // este factura de chirie
                                    switch (luna.Month)
                                    {
                                        case 1:
                                            chirie.SumaLuna1ChirieFacturata += Convert.ToDecimal(reader["SumaLunaXFacturata"]);
                                            chirie.SumaLuna1ChirieIncasata += Convert.ToDecimal(reader["SumaLunaXIncasata"]);
                                            break;
                                        case 2:
                                            chirie.SumaLuna2ChirieFacturata += Convert.ToDecimal(reader["SumaLunaXFacturata"]);
                                            chirie.SumaLuna2ChirieIncasata += Convert.ToDecimal(reader["SumaLunaXIncasata"]);
                                            break;
                                        case 3:
                                            chirie.SumaLuna3ChirieFacturata += Convert.ToDecimal(reader["SumaLunaXFacturata"]);
                                            chirie.SumaLuna3ChirieIncasata += Convert.ToDecimal(reader["SumaLunaXIncasata"]);
                                            break;
                                        case 4:
                                            chirie.SumaLuna4ChirieFacturata += Convert.ToDecimal(reader["SumaLunaXFacturata"]);
                                            chirie.SumaLuna4ChirieIncasata += Convert.ToDecimal(reader["SumaLunaXIncasata"]);
                                            break;
                                        case 5:
                                            chirie.SumaLuna5ChirieFacturata += Convert.ToDecimal(reader["SumaLunaXFacturata"]);
                                            chirie.SumaLuna5ChirieIncasata += Convert.ToDecimal(reader["SumaLunaXIncasata"]);
                                            break;
                                        case 6:
                                            chirie.SumaLuna6ChirieFacturata += Convert.ToDecimal(reader["SumaLunaXFacturata"]);
                                            chirie.SumaLuna6ChirieIncasata += Convert.ToDecimal(reader["SumaLunaXIncasata"]);
                                            break;
                                        case 7:
                                            chirie.SumaLuna7ChirieFacturata += Convert.ToDecimal(reader["SumaLunaXFacturata"]);
                                            chirie.SumaLuna7ChirieIncasata += Convert.ToDecimal(reader["SumaLunaXIncasata"]);
                                            break;
                                        case 8:
                                            chirie.SumaLuna8ChirieFacturata += Convert.ToDecimal(reader["SumaLunaXFacturata"]);
                                            chirie.SumaLuna8ChirieIncasata += Convert.ToDecimal(reader["SumaLunaXIncasata"]);
                                            break;
                                        case 9:
                                            chirie.SumaLuna9ChirieFacturata += Convert.ToDecimal(reader["SumaLunaXFacturata"]);
                                            chirie.SumaLuna9ChirieIncasata += Convert.ToDecimal(reader["SumaLunaXIncasata"]);
                                            break;
                                        case 10:
                                            chirie.SumaLuna10ChirieFacturata += Convert.ToDecimal(reader["SumaLunaXFacturata"]);
                                            chirie.SumaLuna10ChirieIncasata += Convert.ToDecimal(reader["SumaLunaXIncasata"]);
                                            break;
                                        case 11:
                                            chirie.SumaLuna11ChirieFacturata += Convert.ToDecimal(reader["SumaLunaXFacturata"]);
                                            chirie.SumaLuna11ChirieIncasata += Convert.ToDecimal(reader["SumaLunaXIncasata"]);
                                            break;
                                        case 12:
                                            chirie.SumaLuna12ChirieFacturata += Convert.ToDecimal(reader["SumaLunaXFacturata"]);
                                            chirie.SumaLuna12ChirieIncasata += Convert.ToDecimal(reader["SumaLunaXIncasata"]);
                                            break;
                                    }
                                }
                                else
                                {
                                    // este factura de utilitati
                                    switch (luna.Month)
                                    {
                                        case 1:
                                            chirie.SumaLuna1UtilitatiFacturata += Convert.ToDecimal(reader["SumaLunaXFacturata"]);
                                            chirie.SumaLuna1UtilitatiIncasata += Convert.ToDecimal(reader["SumaLunaXIncasata"]);
                                            break;
                                        case 2:
                                            chirie.SumaLuna2UtilitatiFacturata += Convert.ToDecimal(reader["SumaLunaXFacturata"]);
                                            chirie.SumaLuna2UtilitatiIncasata += Convert.ToDecimal(reader["SumaLunaXIncasata"]);
                                            break;
                                        case 3:
                                            chirie.SumaLuna3UtilitatiFacturata += Convert.ToDecimal(reader["SumaLunaXFacturata"]);
                                            chirie.SumaLuna3UtilitatiIncasata += Convert.ToDecimal(reader["SumaLunaXIncasata"]);
                                            break;
                                        case 4:
                                            chirie.SumaLuna4UtilitatiFacturata += Convert.ToDecimal(reader["SumaLunaXFacturata"]);
                                            chirie.SumaLuna4UtilitatiIncasata += Convert.ToDecimal(reader["SumaLunaXIncasata"]);
                                            break;
                                        case 5:
                                            chirie.SumaLuna5UtilitatiFacturata += Convert.ToDecimal(reader["SumaLunaXFacturata"]);
                                            chirie.SumaLuna5UtilitatiIncasata += Convert.ToDecimal(reader["SumaLunaXIncasata"]);
                                            break;
                                        case 6:
                                            chirie.SumaLuna6UtilitatiFacturata += Convert.ToDecimal(reader["SumaLunaXFacturata"]);
                                            chirie.SumaLuna6UtilitatiIncasata += Convert.ToDecimal(reader["SumaLunaXIncasata"]);
                                            break;
                                        case 7:
                                            chirie.SumaLuna7UtilitatiFacturata += Convert.ToDecimal(reader["SumaLunaXFacturata"]);
                                            chirie.SumaLuna7UtilitatiIncasata += Convert.ToDecimal(reader["SumaLunaXIncasata"]);
                                            break;
                                        case 8:
                                            chirie.SumaLuna8UtilitatiFacturata += Convert.ToDecimal(reader["SumaLunaXFacturata"]);
                                            chirie.SumaLuna8UtilitatiIncasata += Convert.ToDecimal(reader["SumaLunaXIncasata"]);
                                            break;
                                        case 9:
                                            chirie.SumaLuna9UtilitatiFacturata += Convert.ToDecimal(reader["SumaLunaXFacturata"]);
                                            chirie.SumaLuna9UtilitatiIncasata += Convert.ToDecimal(reader["SumaLunaXIncasata"]);
                                            break;
                                        case 10:
                                            chirie.SumaLuna10UtilitatiFacturata += Convert.ToDecimal(reader["SumaLunaXFacturata"]);
                                            chirie.SumaLuna10UtilitatiIncasata += Convert.ToDecimal(reader["SumaLunaXIncasata"]);
                                            break;
                                        case 11:
                                            chirie.SumaLuna11UtilitatiFacturata += Convert.ToDecimal(reader["SumaLunaXFacturata"]);
                                            chirie.SumaLuna11UtilitatiIncasata += Convert.ToDecimal(reader["SumaLunaXIncasata"]);
                                            break;
                                        case 12:
                                            chirie.SumaLuna12UtilitatiFacturata += Convert.ToDecimal(reader["SumaLunaXFacturata"]);
                                            chirie.SumaLuna12UtilitatiIncasata += Convert.ToDecimal(reader["SumaLunaXIncasata"]);
                                            break;
                                    }
                                }
                                listaChiriiFinala.Add(chirie);
                                break;
                            }

                        }
                    }
                    Log = "Conectare cu succes";
                    DatabaseUtils.ScrieLog(Log, " - CitesteContracteFacturiDinSAGA" + listaChirii[0].SocietateFolderSaga, DateTime.Now.ToString());
                }
                catch (Exception x)
                {
                    Log = "Nu s-a putut conecta: " + x.ToString();
                    DatabaseUtils.ScrieLog(Log, " - CitesteContracteFacturiDinSAGA" + listaChirii[0].SocietateFolderSaga, DateTime.Now.ToString());
                }
            }
            return listaChiriiFinala;
        }

        /// <summary>
        /// Date debitori din SAGA
        /// </summary>
        public static List<DataSourceChiriasi.Debitor> CitesteDebitoriDinSAGA(List<DataSourceChiriasi.Chirii> listaChirii)
        {
            FbConnectionStringBuilder ret = new FbConnectionStringBuilder();
            ret.Database = ConfigurationManager.AppSettings["caleSAGA"] + "\\" + listaChirii[0].SocietateFolderSaga + "\\cont_baza.fdb";
            ret.DataSource = "86.120.111.69";
            ret.Port = 3050;
            ret.UserID = "SYSDBA";
            ret.Password = "masterkey";
            ret.Charset = "NONE";
            ret.ConnectionTimeout = 60000;
            ret.Pooling = false;
            FbConnection sagaConnection = new FbConnection(ret.ToString());
            List<List<string>> lista = new List<List<string>>();
            List<string> listaElement = new List<string>();
            string select = "";
            List<DataSourceChiriasi.Debitor> listaDebitori = new List<DataSourceChiriasi.Debitor>();

            string filtruDupaCoduriSAGA = " AND ies.cod NOT IN (";

            foreach (DataSourceChiriasi.Chirii contract in listaChirii)
            {
                filtruDupaCoduriSAGA += contract.ChiriasIdSAGA_Cod + ",";
            }
            filtruDupaCoduriSAGA = filtruDupaCoduriSAGA.Remove(filtruDupaCoduriSAGA.Length - 1) + ") ";


            select = @"SELECT 
                             c.DENUMIRE
                            ,c.JUDET                             
                            ,c.ADRESA 
                            ,c.REG_COM                            
                            ,c.COD_FISCAL 
                            ,SUM(ies.neachitat) AS soldDinFacturi
                            ,( coalesce((SELECT SUM(reg.SUMA) as a from REGISTRU reg where trim(coalesce(reg.CONT_D,'')) = trim(coalesce(ies.CONT_CLI,''))),0) 
                            - coalesce((SELECT SUM(reg.SUMA) as a from REGISTRU reg where trim(coalesce(reg.CONT_C,'')) = trim(coalesce(ies.CONT_CLI,''))),0) 
                             ) as soldDinRegistru
                            ,COALESCE(
                                (SELECT FIRST 1 i.data 
                                    FROM iesiri i 
                                    WHERE i.cod = ies.cod 
                                        ORDER BY i.data DESC),'-') AS dataFactura
                         --   ,COALESCE(
                         --       (SELECT FIRST 1 i.nr_iesire 
                          --          FROM iesiri i 
                          --          WHERE 
                          --          i.cod = ies.cod 
                          --              ORDER BY i.data DESC),'-') AS nrFactura

                          --  ,COALESCE(
                          --      (SELECT FIRST 1 i.cont_cli 
                          --          FROM iesiri i 
                           --         WHERE i.cod = ies.cod 
                           --             ORDER BY i.data DESC),'-') AS codClient 

                            --,ies.cod AS ChiriasIdSAGA_Cod
                           -- ,SUM(ies.total) AS sumaFacturata
                        FROM iesiri ies 
                        JOIN CLIENTI c ON (c.COD = ies.COD)
                        WHERE 1=1 " + filtruDupaCoduriSAGA + @"
                        GROUP BY 
                            ies.cod
                            ,ies.CONT_CLI 
                            ,c.DENUMIRE
                            ,c.ADRESA
                            ,c.COD_FISCAL
                            ,c.JUDET 
                            ,c.REG_COM 
                        ORDER BY soldDinRegistru desc, datafactura, ies.cod";
            string Log = "";
            using (sagaConnection)
            {
                try
                {
                    sagaConnection.Open();
                    FbCommand fbCommand = new FbCommand(select, sagaConnection);
                    FbDataReader reader = fbCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        if ((Decimal)reader["soldDinRegistru"] > 0)
                        {
                            DataSourceChiriasi.Debitor debitor = new DataSourceChiriasi.Debitor();
                            debitor.DebitorDenumireSAGA = reader["DENUMIRE"].ToString().Trim();
                            debitor.DebitorJudet = reader["JUDET"].ToString().Trim();
                            debitor.DebitorAdresa = reader["ADRESA"].ToString().Trim();
                            debitor.DebitorReg_Com = reader["REG_COM"].ToString().Trim();
                            debitor.DebitorCod_Fiscal = reader["COD_FISCAL"].ToString().Trim();
                            debitor.SoldDinFacturi = (Decimal)reader["soldDinFacturi"];
                            debitor.SoldDinRegistru = (Decimal)reader["soldDinRegistru"];
                            debitor.DataUltimeiFacturi = Convert.ToDateTime(reader["dataFactura"]);
                            
                            debitor.SocietateFolderSaga = listaChirii[0].SocietateFolderSaga;
                            debitor.SocietateDenumire = listaChirii[0].SocietateDenumire;
                            listaDebitori.Add(debitor);
                        }
                    }
                    Log = "Conectare cu succes";
                    DatabaseUtils.ScrieLog(Log, " - CitesteContracteFacturiDinSAGA" + listaChirii[0].SocietateFolderSaga, DateTime.Now.ToString());
                }
                catch (Exception x)
                {
                    Log = "Nu s-a putut conecta: " + x.ToString();
                    DatabaseUtils.ScrieLog(Log, " - CitesteContracteFacturiDinSAGA" + listaChirii[0].SocietateFolderSaga, DateTime.Now.ToString());
                }
            }
            return listaDebitori;
        }

        /// <summary>
        /// Date contracte din SAGA
        /// </summary>
        /// <param name="listaChirii"></param>
        /// <param name="dataInceput"></param>
        /// <param name="dataFinal"></param>
        /// <param name="camp"></param>
        /// <param name="tip"></param>
        /// <returns></returns>
        public static List<DataSourceChiriasi.Chirii> CitesteSolduriDinSAGAPanaLaData(List<DataSourceChiriasi.Chirii> listaChirii, DateTime dataInceput, DateTime dataFinal, string camp, string tip)
        {
            FbConnectionStringBuilder ret = new FbConnectionStringBuilder();
            ret.Database = ConfigurationManager.AppSettings["caleSAGA"] + "\\" + listaChirii[0].SocietateFolderSaga + "\\cont_baza.fdb";
            ret.DataSource = "86.120.111.69";
            ret.Port = 3050;
            ret.UserID = "SYSDBA";
            ret.Password = "masterkey";
            ret.Charset = "NONE";
            ret.ConnectionTimeout = 60000;
            ret.Pooling = false;
            FbConnection sagaConnection = new FbConnection(ret.ToString());
            List<List<string>> lista = new List<List<string>>();
            List<string> listaElement = new List<string>();
            string select = "";
            List<DataSourceChiriasi.Chirii> listaChiriiFinala = new List<DataSourceChiriasi.Chirii>();
            DataSourceChiriasi.Chirii chirieFinala = new DataSourceChiriasi.Chirii();

            string filtruDupaCoduriSAGA = FiltruDupaCoduriSAGA(listaChirii);
            string filtruTip = FiltruTip(tip);
            string filtruHaving = string.Empty;
            if (tip == "%")
            {
                filtruHaving = " HAVING SUM(neachitat) > 0 ";
            }



            //select = @"select cod as ChiriasIdSAGA_Cod, denumire,  sum(neachitat) as soldChirii, sum(total) as sumaFacturata
            //            from IESIRI where data >= '" + dataInceput.ToString("dd.MM.yyyy") + @"'  and data <= '" + dataFinal.ToString("dd.MM.yyyy") + @"'  and  nr_iesire like '%" + tip + @"%'                  
            //            group by cod, denumire
            //            order by cod";
            select = @"SELECT 
                            cod AS ChiriasIdSAGA_Cod
                            ,denumire
                            ,SUM(neachitat) AS soldChirii
                            ,SUM(total) AS sumaFacturata
                            ,COALESCE(
                                (SELECT FIRST 1 i.nr_iesire 
                                    FROM iesiri i 
                                    WHERE i.data >= '" + dataInceput.ToString("dd.MM.yyyy")
                                    + @"' AND i.data <= '" + dataFinal.ToString("dd.MM.yyyy")
                                    + @"' "
                                    + filtruTip
                                    + @" AND i.cod = ies.cod 
                                        ORDER BY i.data DESC),'-') AS nrFactura
                            ,COALESCE(
                                (SELECT FIRST 1 i.cont_cli 
                                    FROM iesiri i 
                                    WHERE i.data >= '" + dataInceput.ToString("dd.MM.yyyy")
                                    + @"' AND i.data <= '" + dataFinal.ToString("dd.MM.yyyy")
                                    + @"' "
                                    + filtruTip
                                    + @" AND i.cod = ies.cod 
                                        ORDER BY i.data DESC),'-') AS codClient 
                            ,COALESCE(
                                (SELECT FIRST 1 data FROM REGISTRU r
                                    WHERE data > '" + dataInceput.ToString("dd.MM.yyyy") + @"' 
                                    AND (explicatie SIMILAR TO 'Incas.%' || (select first 1 i.nr_iesire from iesiri i where i.data >= '" + dataInceput.ToString("dd.MM.yyyy") + @"' and i.data <= '" + dataFinal.ToString("dd.MM.yyyy")
                                    + @"' "
                                    + filtruTip
                                    + @" AND i.cod = ies.cod order by i.data desc ) 
                                        || '%' 
                                        OR explicatie SIMILAR TO 'Incas.%' || 
                                            (select first 1 i.nr_iesire from iesiri i where i.data >= '" + dataInceput.ToString("dd.MM.yyyy")
                                     + @"' AND i.data <= '" + dataFinal.ToString("dd.MM.yyyy")
                                     + @"' "
                                     + filtruTip
                                     + @" AND i.cod = ies.cod ORDER BY i.data desc ) || '%') and r.CONT_C = (select first 1 i.cont_cli from iesiri i where i.data >= '" + dataInceput.ToString("dd.MM.yyyy") + @"' and i.data <= '" + dataFinal.ToString("dd.MM.yyyy")
                                     + @"' "
                                     + filtruTip
                                     + @" AND i.cod = ies.cod ORDER BY i.data desc ) ORDER BY data DESC), '01.01.0001')
                                    AS dataUltimeiPlati
 
                        FROM iesiri ies 
                        WHERE data >= '" + dataInceput.ToString("dd.MM.yyyy")
                        + @"' AND data <= '" + dataFinal.ToString("dd.MM.yyyy")
                        + @"' "
                        + filtruTip
                        + filtruDupaCoduriSAGA
                        + @" GROUP BY cod, denumire "
                        + filtruHaving
                        + @"ORDER BY cod";
            string Log = "";
            using (sagaConnection)
            {
                try
                {
                    sagaConnection.Open();
                    FbCommand fbCommand = new FbCommand(select, sagaConnection);
                    FbDataReader reader = fbCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        // daca e lista cu clienti plecati 
                        if (camp == "ClientiPlecati")
                        {
                            DataSourceChiriasi.Chirii chirie = new DataSourceChiriasi.Chirii();
                            chirie.ChiriasIdSAGA_Cod = reader["ChiriasIdSAGA_Cod"].ToString().Trim();
                            chirie.ChiriasDenumireSAGA = reader["denumire"].ToString().Trim();
                            chirie.SocietateFolderSaga = listaChirii[0].SocietateFolderSaga;
                            chirie.SocietateDenumire = listaChirii[0].SocietateDenumire;
                            listaChiriiFinala.Add(chirie);
                        }
                        else {
                            // cautam contractul in lista de chirii
                            foreach (DataSourceChiriasi.Chirii chirie in listaChirii)
                            {
                                chirieFinala = new DataSourceChiriasi.Chirii();
                                if (chirie.ChiriasIdSAGA_Cod.Trim() == reader["ChiriasIdSAGA_Cod"].ToString().Trim())
                                {
                                    switch (camp)
                                    {
                                        case "SoldLaInceputulLuniiAnterioareARaportului":
                                            chirie.SoldLaInceputulLuniiAnterioareARaportului = Convert.ToDecimal(reader["soldChirii"]);
                                            chirie.DataInceputSoldLunaAnterioaraARaportului = dataFinal;
                                            break;
                                        case "SoldLaFinalulLuniiAnterioareARaportului":
                                            chirie.SoldLaFinalulLuniiAnterioareARaportului = Convert.ToDecimal(reader["soldChirii"]);
                                            chirie.DataFinalSoldLunaAnterioaraARaportului = dataFinal;
                                            chirie.LunaAnterioaraARaportului = dataFinal;
                                            chirie.LunaCurentaARaportului = dataFinal.AddMonths(1);
                                            break;
                                        case "SumePeLunaAnterioaraARaportului":
                                            chirie.SumaFacturataPeLunaAnterioaraARaportului = Convert.ToDecimal(reader["sumaFacturata"]);
                                            chirie.SumaIncasataPeLunaAnterioaraARaportului = Convert.ToDecimal(reader["sumaFacturata"]) - Convert.ToDecimal(reader["soldChirii"]);
                                            chirie.DataUltimaIncasareLunaAnterioaraARaportului = Convert.ToDateTime(reader["dataUltimeiPlati"], new CultureInfo("ro-RO"));
                                            break;
                                        case "SumePeLunaCurentaARaportului":
                                            chirie.SumaFacturataPeLunaCurentaARaportului = Convert.ToDecimal(reader["sumaFacturata"]);
                                            chirie.SumaIncasataPeLunaCurentaARaportului = Convert.ToDecimal(reader["sumaFacturata"]) - Convert.ToDecimal(reader["soldChirii"]);
                                            chirie.DataUltimaIncasareLunaCurentaARaportului = Convert.ToDateTime(reader["dataUltimeiPlati"], new CultureInfo("ro-RO"));
                                            break;
                                        case "SoldUtilitatiLaInceputulLuniiAnterioareARaportului":
                                            chirie.SoldUtilitatiLaInceputulLuniiAnterioareARaportului = Convert.ToDecimal(reader["soldChirii"]);
                                            break;
                                        case "SoldUtilitatiLaFinalulLuniiAnterioareARaportului":
                                            chirie.SoldUtilitatiLaFinalulLuniiAnterioareARaportului = Convert.ToDecimal(reader["soldChirii"]);
                                            break;
                                        case "SumeUtilitatiPeLunaAnterioaraARaportului":
                                            chirie.SumaUtilitatiFacturataPeLunaAnterioaraARaportului = Convert.ToDecimal(reader["sumaFacturata"]);
                                            chirie.SumaUtilitatiIncasataPeLunaAnterioaraARaportului = Convert.ToDecimal(reader["sumaFacturata"]) - Convert.ToDecimal(reader["soldChirii"]);
                                            chirie.DataUltimaIncasareUtilitatiLunaAnterioaraARaportului = Convert.ToDateTime(reader["dataUltimeiPlati"], new CultureInfo("ro-RO"));
                                            break;
                                        case "SumeUtilitatiPeLunaCurentaARaportului":
                                            chirie.SumaUtilitatiFacturataPeLunaCurentaARaportului = Convert.ToDecimal(reader["sumaFacturata"]);
                                            chirie.SumaUtilitatiIncasataPeLunaCurentaARaportului = Convert.ToDecimal(reader["sumaFacturata"]) - Convert.ToDecimal(reader["soldChirii"]);
                                            chirie.DataUltimaIncasareUtilitatiLunaCurentaARaportului = Convert.ToDateTime(reader["dataUltimeiPlati"], new CultureInfo("ro-RO"));
                                            break;

                                        case "SoldAlteDebiteLaInceputulLuniiAnterioareARaportului":
                                            chirie.SoldAlteDebiteLaInceputulLuniiAnterioareARaportului = Convert.ToDecimal(reader["soldChirii"]);
                                            break;
                                        case "SoldAlteDebiteLaFinalulLuniiAnterioareARaportului":
                                            chirie.SoldAlteDebiteLaFinalulLuniiAnterioareARaportului = Convert.ToDecimal(reader["soldChirii"]);
                                            break;
                                        case "SumeAlteDebitePeLunaAnterioaraARaportului":
                                            chirie.SumaAlteDebiteFacturataPeLunaAnterioaraARaportului = Convert.ToDecimal(reader["sumaFacturata"]);
                                            chirie.SumaAlteDebiteIncasataPeLunaAnterioaraARaportului = Convert.ToDecimal(reader["sumaFacturata"]) - Convert.ToDecimal(reader["soldChirii"]);
                                            chirie.DataUltimaIncasareAlteDebiteLunaAnterioaraARaportului = Convert.ToDateTime(reader["dataUltimeiPlati"], new CultureInfo("ro-RO"));
                                            break;
                                        case "SumeAlteDebitePeLunaCurentaARaportului":
                                            chirie.SumaAlteDebiteFacturataPeLunaCurentaARaportului = Convert.ToDecimal(reader["sumaFacturata"]);
                                            chirie.SumaAlteDebiteIncasataPeLunaCurentaARaportului = Convert.ToDecimal(reader["sumaFacturata"]) - Convert.ToDecimal(reader["soldChirii"]);
                                            chirie.DataUltimaIncasareAlteDebiteLunaCurentaARaportului = Convert.ToDateTime(reader["dataUltimeiPlati"], new CultureInfo("ro-RO"));
                                            break;
                                    }

                                    listaChiriiFinala.Add(chirie);
                                    break;
                                }
                            }
                        }
                    }
                    Log = "Conectare cu succes";
                    DatabaseUtils.ScrieLog(Log, " - CitesteContracteFacturiDinSAGA" + listaChirii[0].SocietateFolderSaga, DateTime.Now.ToString());
                }
                catch (Exception x)
                {
                    Log = "Nu s-a putut conecta: " + x.ToString();
                    DatabaseUtils.ScrieLog(Log, " - CitesteContracteFacturiDinSAGA" + listaChirii[0].SocietateFolderSaga, DateTime.Now.ToString());
                }
            }
            return listaChiriiFinala;
        }

        /// <summary>
        ///  Creeaza un filtru WHERE din lista de contracte active din Facturare
        /// </summary>
        /// <param name="listaContracte"></param>
        /// <returns></returns>
        public static string FiltruDupaCoduriSAGA(List<DataSourceChiriasi.Chirii> listaContracte)
        {

            string filtruDupaCoduriSAGA = string.Empty;
            if (listaContracte.Count > 0)
            {
                if (HttpContext.Current.Session["esteListaContracteActive"].ToString() == "1")
                {
                    filtruDupaCoduriSAGA = " AND cod IN (";
                }
                else
                {
                    filtruDupaCoduriSAGA = " AND cod NOT IN (";
                }
                foreach (DataSourceChiriasi.Chirii contract in listaContracte)
                {
                    filtruDupaCoduriSAGA += contract.ChiriasIdSAGA_Cod + ",";
                }
                filtruDupaCoduriSAGA = filtruDupaCoduriSAGA.Remove(filtruDupaCoduriSAGA.Length - 1) + ") ";
            }
            return filtruDupaCoduriSAGA;
        }
        /// <summary>
        /// Creeza un filtru WHERE dupa K - chirii, U - utilitati sau orice alt string pentru alte debite
        /// </summary>
        /// <param name="tip"></param>
        /// <returns></returns>
        public static string FiltruTip(string tip)
        {
            string filtruTip = string.Empty;
            if (tip == "K" || tip == "U" || tip == "%")
                filtruTip = " AND  nr_iesire like '%" + tip + @"%' ";
            else filtruTip = " AND nr_iesire NOT LIKE '%K%' AND nr_iesire NOT LIKE '%U%' ";
            return filtruTip;
        }

        /// <summary>
        /// Pentru restantieri
        /// </summary>
        /// <param name="listaChirii"></param>
        /// <param name="dataInceput"></param>
        /// <param name="dataFinal"></param>
        /// <param name="tip"></param>
        /// <param name="coeficientMinim"></param>
        /// <returns></returns>
        public static List<DataSourceChiriasi.Chirii> CitesteRestanteCuCoeficient(List<DataSourceChiriasi.Chirii> listaChirii, DateTime dataInceput, DateTime dataFinal, string tip, string coeficientMinim)
        {
            FbConnectionStringBuilder ret = new FbConnectionStringBuilder();
            ret.Database = ConfigurationManager.AppSettings["caleSAGA"] + "\\" + listaChirii[0].SocietateFolderSaga + "\\cont_baza.fdb";
            ret.DataSource = "86.120.111.69";
            ret.Port = 3050;
            ret.UserID = "SYSDBA";
            ret.Password = "masterkey";
            ret.Charset = "NONE";
            ret.ConnectionTimeout = 60000;
            ret.Pooling = false;
            FbConnection sagaConnection = new FbConnection(ret.ToString());
            List<List<string>> lista = new List<List<string>>();
            List<string> listaElement = new List<string>();
            string select = "";
            List<DataSourceChiriasi.Chirii> listaChiriiFinala = new List<DataSourceChiriasi.Chirii>();
            DataSourceChiriasi.Chirii chirieFinala = new DataSourceChiriasi.Chirii();

            select = @"
                        select 
                             cod as ChiriasIdSAGA_Cod
                            ,denumire
                            ,sum(neachitat) as soldChirii
                            ,(select first 1 i1.total from iesiri as i1 where 
                                    i1.data >= '" + dataInceput.ToString("dd.MM.yyyy") + @"'  and i1.data <= '" + dataFinal.ToString("dd.MM.yyyy") + @"'  and  i1.nr_iesire like '%" + tip + @"%' and i1.cod = iesiri.cod )
                                    as ultimaChirie
                            ,sum(total) as sumaFacturata
                            ,case   when 
                                            (select first 1 i1.total FROM iesiri AS i1 
                                            where i1.data >= '" + dataInceput.ToString("dd.MM.yyyy") + @"'  and i1.data <= '" + dataFinal.ToString("dd.MM.yyyy") + @"'  and  i1.nr_iesire like '%" + tip + @"%' and i1.cod = iesiri.cod ) 
                                            > 0 
                                    then    
                                            sum(neachitat) / 
                                            (select first 1 i1.total from iesiri as i1 
                                            where i1.data >= '" + dataInceput.ToString("dd.MM.yyyy") + @"'  and i1.data <= '" + dataFinal.ToString("dd.MM.yyyy") + @"'  and  i1.nr_iesire like '%" + tip + @"%' and i1.cod = iesiri.cod ) 
                                    else    0 
                                    end 
                                    as coeficient
                        from IESIRI where 
                                            data <= '" + dataFinal.ToString("dd.MM.yyyy") + @"'  
                                        and nr_iesire like '%" + tip + @"%'  
                    
                        group by cod, denumire 
                            having 
                                (case   when 
                                            (select first 1 i1.total FROM iesiri AS i1 
                                            where i1.data >= '" + dataInceput.ToString("dd.MM.yyyy") + @"'  and i1.data <= '" + dataFinal.ToString("dd.MM.yyyy") + @"'  and  i1.nr_iesire like '%" + tip + @"%' and i1.cod = iesiri.cod ) 
                                            > 0 
                                        then    
                                            sum(neachitat) / 
                                            (select first 1 i1.total from iesiri as i1 
                                            where i1.data >= '" + dataInceput.ToString("dd.MM.yyyy") + @"'  and i1.data <= '" + dataFinal.ToString("dd.MM.yyyy") + @"'  and  i1.nr_iesire like '%" + tip + @"%' and i1.cod = iesiri.cod ) 
                                        else    0 
                                        end) 
                                    > " + coeficientMinim.Replace(",", ".") + @" 
                        order by  coeficient desc, denumire desc
";
            string Log = "";
            using (sagaConnection)
            {
                try
                {
                    sagaConnection.Open();
                    FbCommand fbCommand = new FbCommand(select, sagaConnection);
                    FbDataReader reader = fbCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        // cautam contractul in lista de chirii
                        foreach (DataSourceChiriasi.Chirii chirie in listaChirii)
                        {
                            chirieFinala = new DataSourceChiriasi.Chirii();
                            if (chirie.ChiriasIdSAGA_Cod.Trim() == reader["ChiriasIdSAGA_Cod"].ToString().Trim())
                            {
                                chirie.LunaAnterioaraARaportului = dataFinal;
                                switch (tip)
                                {
                                    case "U":
                                        chirie.SoldUtilitatiLaFinalulLuniiAnterioareARaportului = Convert.ToDecimal(reader["soldChirii"]);
                                        chirie.CoeficientUtilitatiRestante = Convert.ToDecimal(reader["coeficient"]);
                                        break;
                                    case "K":
                                        chirie.SoldLaFinalulLuniiAnterioareARaportului = Convert.ToDecimal(reader["soldChirii"]);
                                        chirie.CoeficientChiriiRestante = Convert.ToDecimal(reader["coeficient"]);
                                        break;
                                }
                                listaChiriiFinala.Add(chirie);
                                break;
                            }
                        }
                    }
                    Log = "Conectare cu succes";
                    DatabaseUtils.ScrieLog(Log, " - CitesteContracteFacturiDinSAGA" + listaChirii[0].SocietateFolderSaga, DateTime.Now.ToString());
                }
                catch (Exception x)
                {
                    Log = "Nu s-a putut conecta: " + x.ToString();
                    DatabaseUtils.ScrieLog(Log, " - CitesteContracteFacturiDinSAGA" + listaChirii[0].SocietateFolderSaga, DateTime.Now.ToString());
                }
            }
            return listaChiriiFinala;
        }

        /// <summary>
        /// Pentru sintetice
        /// </summary>
        /// <param name="listaChirii"></param>
        /// <param name="dataInceput"></param>
        /// <param name="dataFinal"></param>
        /// <param name="camp"></param>
        /// <param name="tip"></param>
        /// <returns></returns>
        public static List<DataSourceChiriasi.Chirii> CitesteSolduriPeSocietateDinSAGAPanaLaData(List<DataSourceChiriasi.Chirii> listaChirii, DateTime dataInceput, DateTime dataFinal, string camp, string tip)
        {
            List<List<string>> lista = new List<List<string>>();
            List<string> listaElement = new List<string>();
            string select = "";
            List<DataSourceChiriasi.Chirii> listaChiriiFinala = new List<DataSourceChiriasi.Chirii>();
            DataSourceChiriasi.Chirii chirieFinala = new DataSourceChiriasi.Chirii();
            DateTime dataPentruListaDeContracte = Convert.ToDateTime(HttpContext.Current.Session["lunaRapoarteSintetice"].ToString(), new CultureInfo("ro-RO"));
            foreach (DataSourceChiriasi.Chirii societate in listaChirii)
            {
                FbConnectionStringBuilder ret = new FbConnectionStringBuilder();
                ret.Database = ConfigurationManager.AppSettings["caleSAGA"] + "\\" + societate.SocietateFolderSaga + "\\cont_baza.fdb";
                ret.DataSource = "86.120.111.69";
                ret.Port = 3050;
                ret.UserID = "SYSDBA";
                ret.Password = "masterkey";
                ret.Charset = "NONE";
                ret.ConnectionTimeout = 60000;
                ret.Pooling = false;
                FbConnection sagaConnection = new FbConnection(ret.ToString());

                string filtruDupaCoduriSAGA = FiltruDupaCoduriSAGA(ListaContracteActive(societate.SocietateId, dataPentruListaDeContracte));

                select = @" SELECT 'x' as x, coalesce(sum(neachitat),0) as soldChirii, coalesce(sum(total),0) as sumaFacturata, coalesce(sum(total - neachitat),0) as sumaIncasata
                        from IESIRI where data >= '" + dataInceput.ToString("dd.MM.yyyy") + @"'  and data <= '" + dataFinal.ToString("dd.MM.yyyy") + "'"
                        + FiltruTip(tip)
                        + filtruDupaCoduriSAGA;
                string Log = "";
                using (sagaConnection)
                {
                    try
                    {
                        sagaConnection.Open();
                        FbCommand fbCommand = new FbCommand(select, sagaConnection);
                        FbDataReader reader = fbCommand.ExecuteReader();
                        while (reader.Read())
                        {
                            switch (camp)
                            {
                                case "SoldLaInceputulLuniiAnterioareARaportului":
                                    societate.SoldLaInceputulLuniiAnterioareARaportului = Convert.ToDecimal(reader["soldChirii"]);
                                    break;
                                case "SoldLaFinalulLuniiAnterioareARaportului":
                                    societate.SoldLaFinalulLuniiAnterioareARaportului = Convert.ToDecimal(reader["soldChirii"]);
                                    break;
                                case "SumePeLunaAnterioaraARaportului":
                                    societate.SumaFacturataPeLunaAnterioaraARaportului = Convert.ToDecimal(reader["sumaFacturata"]);
                                    societate.SumaIncasataPeLunaAnterioaraARaportului = Convert.ToDecimal(reader["sumaIncasata"]);
                                    if (societate.SumaFacturataPeLunaAnterioaraARaportului > 0) societate.ProcentIncasariPeLunaAnterioaraARaportului = societate.SumaIncasataPeLunaAnterioaraARaportului * 100 / societate.SumaFacturataPeLunaAnterioaraARaportului;
                                    else societate.ProcentIncasariPeLunaAnterioaraARaportului = 0;

                                    break;
                                case "SoldUtilitatiLaInceputulLuniiAnterioareARaportului":
                                    societate.SoldUtilitatiLaInceputulLuniiAnterioareARaportului = Convert.ToDecimal(reader["soldChirii"]);
                                    break;
                                case "SoldUtilitatiLaFinalulLuniiAnterioareARaportului":
                                    societate.SoldUtilitatiLaFinalulLuniiAnterioareARaportului = Convert.ToDecimal(reader["soldChirii"]);
                                    break;
                                case "SumeUtilitatiPeLunaAnterioaraARaportului":
                                    societate.SumaUtilitatiFacturataPeLunaAnterioaraARaportului = Convert.ToDecimal(reader["sumaFacturata"]);
                                    societate.SumaUtilitatiIncasataPeLunaAnterioaraARaportului = Convert.ToDecimal(reader["sumaIncasata"]);
                                    if (societate.SumaUtilitatiFacturataPeLunaAnterioaraARaportului > 0) societate.ProcentIncasariUtilitatiPeLunaAnterioaraARaportului = societate.SumaUtilitatiIncasataPeLunaAnterioaraARaportului * 100 / societate.SumaUtilitatiFacturataPeLunaAnterioaraARaportului;
                                    else societate.ProcentIncasariUtilitatiPeLunaAnterioaraARaportului = 0;
                                    break;

                                case "SoldAlteDebiteLaInceputulLuniiAnterioareARaportului":
                                    societate.SoldAlteDebiteLaInceputulLuniiAnterioareARaportului = Convert.ToDecimal(reader["soldChirii"]);
                                    break;
                                case "SoldAlteDebiteLaFinalulLuniiAnterioareARaportului":
                                    societate.SoldAlteDebiteLaFinalulLuniiAnterioareARaportului = Convert.ToDecimal(reader["soldChirii"]);
                                    break;
                                case "SumeAlteDebitePeLunaAnterioaraARaportului":
                                    societate.SumaAlteDebiteFacturataPeLunaAnterioaraARaportului = Convert.ToDecimal(reader["sumaFacturata"]);
                                    societate.SumaAlteDebiteIncasataPeLunaAnterioaraARaportului = Convert.ToDecimal(reader["sumaIncasata"]);
                                    if (societate.SumaAlteDebiteFacturataPeLunaAnterioaraARaportului > 0) societate.ProcentIncasariAlteDebitePeLunaAnterioaraARaportului = societate.SumaAlteDebiteIncasataPeLunaAnterioaraARaportului * 100 / societate.SumaAlteDebiteFacturataPeLunaAnterioaraARaportului;
                                    else societate.ProcentIncasariAlteDebitePeLunaAnterioaraARaportului = 0;
                                    break;
                            }

                            listaChiriiFinala.Add(societate);
                            break;
                        }
                        Log = "Conectare cu succes";
                        DatabaseUtils.ScrieLog(Log, " - CitesteContracteFacturiDinSAGA" + listaChirii[0].SocietateFolderSaga, DateTime.Now.ToString());
                    }
                    catch (Exception x)
                    {
                        Log = "Nu s-a putut conecta: " + x.ToString();
                        DatabaseUtils.ScrieLog(Log, " - CitesteContracteFacturiDinSAGA" + listaChirii[0].SocietateFolderSaga, DateTime.Now.ToString());
                    }
                }
            }
            return listaChiriiFinala;
        }


        public static DataTable GetFacturiFromSaga(string month, int year, string path, string sagaPath)
        {
            // month = GetMonth(MasterModel.WorkDate.Month.ToString());

            string select = @"SELECT  trim(IESIRI.COD) as COD, SUM(TOTAL) as TOTAL, coalesce(sum(SUMA),0) as incasat
                                FROM  IESIRI 
                           LEFT JOIN  NOTE_FACTURI 
                                  ON  NOTE_FACTURI.ID_FACTURA = IESIRI.ID_IESIRE 
                               WHERE  EXTRACT (YEAR FROM IESIRI.DATA ) = '" + year + "' AND EXTRACT (month FROM IESIRI.DATA ) ='" + month + @"'
                            GROUP BY  IESIRI.COD";
            string connectionString = @"User=SYSDBA;Password=masterkey;Database=D:\\\\Saga C.3.0\\0001\\cont_baza.fdb;DataSource=86.120.111.69;Port=3050;Dialect=3;Charset=NONE;Role=;Connection lifetime=15;Pooling=true;MinPoolSize=0;MaxPoolSize=50;Packet Size=8192;ServerType=0";
            //  string connectionString = @"User=SYSDBA;Password=masterkey;Database=" + path + "\\" + sagaPath + "\\cont_baza.fdb;DataSource=86.120.111.69;Port=3050;Dialect=3;Charset=NONE;Role=;Connection lifetime=15;Pooling=true;MinPoolSize=0;MaxPoolSize=50;Packet Size=8192;ServerType=0";


            FbConnectionStringBuilder ret = new FbConnectionStringBuilder();
            ret.DataSource = "86.120.111.69";
            ret.Port = 3050;
            ret.Database = path + "\\" + sagaPath + "\\cont_baza.fdb";
            ret.UserID = "SYSDBA";
            ret.Password = "masterkey";
            ret.Charset = "NONE";
            FbConnection sagaConnection = new FbConnection(ret.ToString());

            try
            {
                sagaConnection.Open();
            }
            catch
            {
                sagaConnection.Close();
                return null;
            }

            FbDataAdapter adapter = new FbDataAdapter(select, sagaConnection);
            DataTable table = new DataTable();
            adapter.Fill(table);
            sagaConnection.Close();
            return table;
        }
        public static string GetMonth(string month)
        {
            if ((month.StartsWith("1") && Convert.ToInt32(month) >= 10) || month.StartsWith("0"))
            {
                return month;
            }
            else
            {
                month = "0" + month;
                return month;
            }
        }
        public static List<List<string>> SagaValoriCont(string dataInceput, string dataFinal, string grafic, string caleProgramSaga, Societati societate, out string Log)
        {
            List<string> SQLDebitorCreditor = SAGACreeazaStringWhereSQL(societate, grafic);
            List<DataTable> tabele = new List<DataTable>();
            CultureInfo cultura = new CultureInfo("ro-RO", false);
            cultura.DateTimeFormat.DateSeparator = ".";
            string select = "";
            switch (SQLDebitorCreditor[2])
            {
                case "Debitor":
                    select = @"SELECT coalesce(sum(suma),0) as SumaDeb, extract(day from data) || '/' || extract(month from data ) 
                            FROM REGISTRU a 
                            where data >= cast('" + Convert.ToDateTime(dataInceput, cultura).ToString("d", cultura) + @"' as date) and data <= cast('" + Convert.ToDateTime(dataFinal, cultura).ToString("d", cultura) + @"' as date) 
                  " + SQLDebitorCreditor[0] + @"   GROUP BY data";
                    break;
                case "Creditor":
                    select = @"SELECT coalesce(sum(suma),0) as SumaCred, extract(day from data) || '/' || extract(month from data ) 
                            FROM REGISTRU a 
                            where data >= cast('" + Convert.ToDateTime(dataInceput, cultura).ToString("d", cultura) + @"' as date) and data <= cast('" + Convert.ToDateTime(dataFinal, cultura).ToString("d", cultura) + @"' as date) 
                  " + SQLDebitorCreditor[1] + @"   GROUP BY data";
                    break;
                case "Rulaj":
                    select = @"SELECT (coalesce(sumadeb, 0) - coalesce(sumacred, 0)) as suma, 
                                CASE
                                WHEN credit.data is null THEN extract(day from debit.data) || '/' || extract(month from debit.data )
                                WHEN credit.data is not null THEN extract(day from credit.data) || '/' || extract(month from credit.data )
                                WHEN debit.data is null THEN extract(day from credit.data) || '/' || extract(month from credit.data )
                                WHEN debit.data is not null THEN extract(day from debit.data) || '/' || extract(month from debit.data )
                                END as data,
                                CASE    
                                WHEN debit.data is null then credit.data
                                else  debit.data END as dataComuna
                                from
                                    (SELECT coalesce(sum(suma), 0) as SumaDeb, data FROM REGISTRU a 
                                where
                                    data >= cast('" + Convert.ToDateTime(dataInceput, cultura).ToString("d", cultura) + @"' as date) and data <= cast('" + Convert.ToDateTime(dataFinal, cultura).ToString("d", cultura) + @"' as date) " + SQLDebitorCreditor[0] + @" group by data) as debit 
                            full JOIN
                            (SELECT coalesce(sum(suma), 0) as SumaCred, data FROM REGISTRU a where 
                                data >= cast('" + Convert.ToDateTime(dataInceput, cultura).ToString("d", cultura) + @"' as date) and
                                data <= cast('" + Convert.ToDateTime(dataFinal, cultura).ToString("d", cultura) + @"' as date) " + SQLDebitorCreditor[1] + @"
                                group by data) as credit on debit.data = credit.data 
                            order by dataComuna";
                    break;
                case "Sold":
                    //select = @"SELECT 
                    //            case 
                    //            WHEN DEBIT.SOLD is null THEN
                    //            coalesce(  
                    //                (select sum(suma) from registru bb 
                    //                    where ( bb.data>='01.01.' || extract(year from credit.data)) and bb.data <= credit.data " + SQLDebitorCreditor[0] + @"),0) 
                    //                - CREDIT.SOLD
                    //            WHEN CREDIT.SOLD is null THEN 
                    //                DEBIT.SOLD - 
                    //                 coalesce( 
                    //                    (select sum(suma) from registru aa where  ( aa.data>='01.01.' || extract(year from debit.data)) and aa.data <= debit.data " + SQLDebitorCreditor[1] + @"),0)
                    //            ELSE (DEBIT.SOLD - CREDIT.SOLD) END AS suma
                    //        ,   CASE
                    //            WHEN credit.data is null THEN extract(day from debit.data) || '/' || extract(month from debit.data )
                    //            WHEN credit.data is not null THEN extract(day from credit.data) || '/' || extract(month from credit.data )
                    //            WHEN debit.data is null THEN extract(day from credit.data) || '/' || extract(month from credit.data )
                    //            WHEN debit.data is not null THEN extract(day from debit.data) || '/' || extract(month from debit.data )
                    //            END as data
                    //        ,   credit.sold as soldCredit
                    //        ,   debit.sold as soldDebit
                    //        ,   CASE    
                    //              WHEN debit.data is null then credit.data
                    //                else  debit.data END as dataComuna
                    //    FROM 
                    //        (SELECT 
                    //           (select sum(suma) from registru aa 
                    //                where (aa.data>='01.01.' || extract(year from a.data)) and aa.data <= a.data " + SQLDebitorCreditor[1] + @") as sold
                    //             , sum(suma) as sumaZi
                    //             , a.DATA as data 
                    //            FROM REGISTRU a 
                    //                where a.data >= cast('" + Convert.ToDateTime(dataInceput, cultura).ToString("d", cultura) + @"' as date) and a.data <= cast('" + Convert.ToDateTime(dataFinal, cultura).ToString("d", cultura) + @"' as date) " + SQLDebitorCreditor[1] + @"
                    //                group by a.data) 
                    //        as credit
                    //  FULL JOIN 
                    //        (SELECT (select sum(suma) from registru bb 
                    //                where ( bb.data>='01.01.' || extract(year from b.data)) and bb.data <= b.data  " + SQLDebitorCreditor[0] + @") as sold
                    //            , sum(suma) as sumaZi
                    //            , b.DATA as data
                    //            FROM REGISTRU b 
                    //                where b.data >= cast('" + Convert.ToDateTime(dataInceput, cultura).ToString("d", cultura) + @"' as date) and b.data <= cast('" + Convert.ToDateTime(dataFinal, cultura).ToString("d", cultura) + @"' as date)  " + SQLDebitorCreditor[0] + @"
                    //                group by b.data) as debit
                    //on debit.data = credit.data
                    //order by datacomuna";
                    select = @"

                        SELECT 
                                case 
                                WHEN DEBIT.SOLD is null THEN
                                coalesce(  
                                    (select coalesce(sum(suma),0) from registru bb 
                                        where bb.data <= credit.data " + SQLDebitorCreditor[0] + @"),0) 
                                    - CREDIT.SOLD
                                WHEN CREDIT.SOLD is null THEN 
                                    DEBIT.SOLD - 
                                     coalesce( 
                                        (select coalesce(sum(suma),0) from registru aa where  aa.data <= debit.data  " + SQLDebitorCreditor[1] + @"),0)
                                ELSE (DEBIT.SOLD - CREDIT.SOLD) END AS suma
                            ,   CASE
                                WHEN credit.data is null THEN extract(day from debit.data) || '/' || extract(month from debit.data )
                                WHEN credit.data is not null THEN extract(day from credit.data) || '/' || extract(month from credit.data )
                                WHEN debit.data is null THEN extract(day from credit.data) || '/' || extract(month from credit.data )
                                WHEN debit.data is not null THEN extract(day from debit.data) || '/' || extract(month from debit.data )
                                END as data
                            ,   credit.sold as soldCredit
                            ,   debit.sold as soldDebit
                            ,   CASE    
                                  WHEN debit.data is null then credit.data
                                    else  debit.data END as dataComuna
                        FROM 
                            (SELECT 
                               (select coalesce(sum(suma),0) from registru aa 
                                    where aa.data <= a.data  " + SQLDebitorCreditor[1] + @") as sold
                                 , coalesce(sum(suma),0) as sumaZi
                                 , a.DATA as data 
                                FROM REGISTRU a 
                                    where a.data >= cast('" + Convert.ToDateTime(dataInceput, cultura).ToString("d", cultura) + @"' as date) and a.data <= cast('" + Convert.ToDateTime(dataFinal, cultura).ToString("d", cultura) + @"' as date)  " + SQLDebitorCreditor[1] + @"
                                    group by a.data) 
                            as credit
                      FULL JOIN 
                            (SELECT (select coalesce(sum(suma),0) from registru bb 
                                    where bb.data <= b.data   " + SQLDebitorCreditor[0] + @") as sold
                                , coalesce(sum(suma),0) as sumaZi
                                , b.DATA as data
                                FROM REGISTRU b 
                                    where b.data >= cast('" + Convert.ToDateTime(dataInceput, cultura).ToString("d", cultura) + @"' as date) and b.data <= cast('" + Convert.ToDateTime(dataFinal, cultura).ToString("d", cultura) + @"' as date)  " + SQLDebitorCreditor[0] + @"
                                    group by b.data) as debit
                    on debit.data = credit.data
                    order by datacomuna";
                    break;
            }

            FbConnectionStringBuilder ret = new FbConnectionStringBuilder();
            ret.Database = caleProgramSaga + "\\" + societate.SAGAfolder + "\\cont_baza.fdb";
            ret.DataSource = "86.120.111.69";
            ret.Port = 3050;
            ret.UserID = "SYSDBA";
            ret.Password = "masterkey";
            ret.Charset = "NONE";
            ret.ConnectionTimeout = 60000;
            ret.Pooling = false;
            FbConnection sagaConnection = new FbConnection(ret.ToString());
            List<List<string>> lista = new List<List<string>>();
            List<string> listaElement = new List<string>();
            using (sagaConnection)
            {
                try
                {

                    sagaConnection.Open();
                    FbCommand fbCommand = new FbCommand(select, sagaConnection);
                    FbDataReader reader = fbCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        listaElement = new List<string>();
                        for (int i = 0; i < reader.FieldCount; i++)
                            listaElement.Add(reader[i].ToString());
                        lista.Add(listaElement);
                    }
                    Log = "Conectare cu succes";
                    DatabaseUtils.ScrieLog(Log, "grafic - SagaValoriCont" + societate.SAGAfolder, DateTime.Now.ToString());
                }
                catch (Exception x)
                {
                    Log = "Nu s-a putut conecta: " + x.ToString();
                    DatabaseUtils.ScrieLog(Log, "grafic - SagaValoriCont" + societate.SAGAfolder, DateTime.Now.ToString());
                }
            }

            return lista;
        }
        public static void SAGAConexiune()
        {

        }







        #region cheltuieli in saga
        public static List<string> SAGACreeazaStringWhereSQL(Societati societate, string grafic)
        {
            List<string> SQLDebitCreditor = new List<string>();
            // luam datele din Conturi cu parametri grafic si idsocietate
            string SQL = " AND (";
            string SQLCreditor = "";
            string SQLDebitor = "";
            GraficCont graficCont = new GraficCont();
            int contor = 0;
            graficCont = DatabaseUtils.ConturiInFunctieDeGraficSiSocietate(societate, grafic);
            foreach (string cont in graficCont.conturi)
            {
                SQL += " DEINLOCUIT = '" + cont + "' OR ";
                contor++;
            }
            if (contor > 0)
            {
                SQL = SQL.Remove(SQL.Length - 3) + " ) ";
                SQLCreditor = SQL.Replace("DEINLOCUIT", "cont_c");
                SQLDebitor = SQL.Replace("DEINLOCUIT", "cont_d");
                SQLDebitCreditor.Add(SQLDebitor);
                SQLDebitCreditor.Add(SQLCreditor);
            }
            else SQL = "";
            // adaugam si modul de calcul ca SQLDebitCreditor[2]
            SQLDebitCreditor.Add(graficCont.modCalcul.ToString());
            return SQLDebitCreditor;
        }
        public static DataSourceCashFlow.CashflowConturi SAGACreeazaStringWhereSQL(DataSourceCashFlow.CashflowConturi conturiCuDiez)
        {
            if (conturiCuDiez.ContCurent.Length > 3) conturiCuDiez.ContCurent = SQLCont(conturiCuDiez.ContCurent);
            else conturiCuDiez.ContCurent = " AND 1=2 ";
            if (conturiCuDiez.Depozit.Length > 3) conturiCuDiez.Depozit = SQLCont(conturiCuDiez.Depozit);
            else conturiCuDiez.Depozit = " AND 1=2 ";
            if (conturiCuDiez.DepozitTaxe.Length > 3) conturiCuDiez.DepozitTaxe = SQLCont(conturiCuDiez.DepozitTaxe);
            else conturiCuDiez.DepozitTaxe = " AND 1=2 ";
            if (conturiCuDiez.Casierie.Length > 3) conturiCuDiez.Casierie = SQLCont(conturiCuDiez.Casierie);
            else conturiCuDiez.Casierie = " AND 1=2 ";
            return conturiCuDiez;
        }
        public static string SQLCont(string contCuDiez)
        {
            string SQL = " AND (";
            string[] splitter = { "#" };
            string[] conturi = { };
            conturi = contCuDiez.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
            int contor = 0;
            foreach (string cont in conturi)
            {
                SQL += " DEINLOCUIT = '" + cont + "' OR ";
                contor++;
            }
            if (contor > 0) SQL = SQL.Remove(SQL.Length - 3) + " ) ";
            else SQL = "";
            return SQL;
        }
        public static DropDownList SAGAPopuleazaDropDownListConturi(DropDownList ddl, string SAGACodSocietate)
        {
            ListItem cont = new ListItem();
            if (ddl.Items.Count > 0) ddl.Items.Clear();

            string caleProgramSaga = ConfigurationManager.AppSettings["caleSAGA"].ToString();
            string select = "SELECT a.CONT, a.DENUMIRE FROM CONTURI a ORDER BY a.CONT ";
            FbConnectionStringBuilder ret = new FbConnectionStringBuilder(); ret.Database = caleProgramSaga + "\\" + SAGACodSocietate + "\\cont_baza.fdb";
            ret.DataSource = "86.120.111.69";
            ret.Port = 3050;
            ret.UserID = "SYSDBA";
            ret.Password = "masterkey";
            ret.Charset = "NONE";
            ret.ConnectionTimeout = 60000;
            ret.ServerType = FbServerType.Default;
            ret.Pooling = false;
            FbConnection sagaConnection = new FbConnection(ret.ToString());
            using (sagaConnection)
            {
                try
                {
                    sagaConnection.Open();
                    FbCommand interogare = new FbCommand(select, sagaConnection);
                    FbDataReader reader = interogare.ExecuteReader();
                    while (reader.Read())
                    {
                        ListItem element = new ListItem();
                        element.Value = reader.GetString(0).Trim();
                        element.Text = reader.GetString(0).Trim() + " - " + reader.GetString(1).Trim();
                        ddl.Items.Add(element);
                    }
                    reader.Close();
                    string Log = "Conectare cu succes";
                    DatabaseUtils.ScrieLog(Log, "SAGAPopuleazaDropDownListConturi: " + SAGACodSocietate, DateTime.Now.ToString());
                }
                catch (Exception x)
                {
                    string Log = "Nu s-a putut conecta: " + x.ToString();
                    DatabaseUtils.ScrieLog(Log, "SAGAPopuleazaDropDownListConturi: " + SAGACodSocietate, DateTime.Now.ToString());
                    return null;
                }
            }
            return ddl;
        }
        public class SAGALinieRegistru
        {
            private string id_nota;
            public string ID_NOTA { get { return id_nota; } set { id_nota = value; } }
            private string cont_d;
            public string CONT_D { get { return cont_d; } set { cont_d = value; } }
            private string cont_c;
            public string CONT_C { get { return cont_c; } set { cont_c = value; } }
            private string suma;
            public string SUMA { get { return suma; } set { suma = value; } }
            private string explicatie;
            public string EXPLICATIE { get { return explicatie; } set { explicatie = value; } }
            private string data;
            public string DATA { get { return data; } set { data = value; } }
            private string ndp;
            public string NDP { get { return ndp; } set { ndp = value; } }
            public SAGALinieRegistru()
            {

            }
        }
        public static List<SAGALinieRegistru> SAGACitesteLiniiRegistru(Cheltuieli cheltuiala)
        {
            FbConnectionStringBuilder ret = new FbConnectionStringBuilder();
            ret.Database = ConfigurationManager.AppSettings["caleSAGA"] + "\\" + cheltuiala.Societatea.SAGAfolder + "\\cont_baza.fdb";
            ret.DataSource = "86.120.111.69";
            ret.Port = 3050;
            ret.UserID = "SYSDBA";
            ret.Password = "masterkey";
            ret.Charset = "NONE";
            ret.ConnectionTimeout = 60000;
            ret.Pooling = false;
            FbConnection sagaConnection = new FbConnection(ret.ToString());
            List<List<string>> lista = new List<List<string>>();
            List<string> listaElement = new List<string>();
            string select = "SELECT a.ID_NOTA, a.CONT_D, a.CONT_C, a.SUMA, a.EXPLICATIE, a.DATA, a.NDP FROM REGISTRU a where ndp like '%*" + cheltuiala.IdCheltuieli + "%' order by id_nota desc ";
            SAGALinieRegistru linie = new SAGALinieRegistru();
            List<SAGALinieRegistru> listaLinii = new List<SAGALinieRegistru>();
            string Log = "";
            using (sagaConnection)
            {
                try
                {
                    sagaConnection.Open();
                    FbCommand fbCommand = new FbCommand(select, sagaConnection);
                    FbDataReader reader = fbCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        linie.ID_NOTA = reader["ID_NOTA"].ToString();
                        linie.CONT_D = reader["CONT_D"].ToString();
                        linie.CONT_C = reader["CONT_C"].ToString();
                        linie.SUMA = reader["SUMA"].ToString();
                        linie.EXPLICATIE = reader["EXPLICATIE"].ToString();
                        linie.DATA = reader["DATA"].ToString();
                        linie.NDP = reader["NDP"].ToString();
                        listaLinii.Add(linie);
                        linie = new SAGALinieRegistru();
                    }
                    Log = "Conectare cu succes";
                    DatabaseUtils.ScrieLog(Log, "linie cheltuiala - SAGALiniiRegistru" + cheltuiala.Societatea.SAGAfolder, DateTime.Now.ToString());
                }
                catch (Exception x)
                {
                    Log = "Nu s-a putut conecta: " + x.ToString();
                    DatabaseUtils.ScrieLog(Log, "linie cheltuiala - SAGALiniiRegistru" + cheltuiala.Societatea.SAGAfolder, DateTime.Now.ToString());
                }
            }
            return listaLinii;
        }
        public static List<SAGALinieRegistru> SAGACitesteLiniiRegistru(Societati societatea, DateTime data, string cont)
        {
            FbConnectionStringBuilder ret = new FbConnectionStringBuilder();
            ret.Database = ConfigurationManager.AppSettings["caleSAGA"] + "\\" + societatea.SAGAfolder + "\\cont_baza.fdb";
            ret.DataSource = "86.120.111.69";
            ret.Port = 3050;
            ret.UserID = "SYSDBA";
            ret.Password = "masterkey";
            ret.Charset = "NONE";
            ret.ConnectionTimeout = 60000;
            ret.Pooling = false;
            FbConnection sagaConnection = new FbConnection(ret.ToString());
            List<List<string>> lista = new List<List<string>>();
            List<string> listaElement = new List<string>();
            string select = "";
            if (cont.Length > 4)
                select = "SELECT a.ID_NOTA, a.CONT_D, a.CONT_C, a.SUMA, a.EXPLICATIE, a.DATA, a.NDP FROM REGISTRU a where data='" + data.ToString("dd.MM.yyyy") + "' and ndp like '%*%' " + cont + " order by id_nota desc ";
            else
                select = "";
            SAGALinieRegistru linie = new SAGALinieRegistru();
            List<SAGALinieRegistru> listaLinii = new List<SAGALinieRegistru>();
            string Log = "";
            using (sagaConnection)
            {
                try
                {
                    sagaConnection.Open();
                    FbCommand fbCommand = new FbCommand(select, sagaConnection);
                    FbDataReader reader = fbCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        linie.ID_NOTA = reader["ID_NOTA"].ToString().Trim();
                        linie.CONT_D = reader["CONT_D"].ToString().Trim();
                        linie.CONT_C = reader["CONT_C"].ToString().Trim();
                        linie.SUMA = reader["SUMA"].ToString().Trim();
                        linie.EXPLICATIE = reader["EXPLICATIE"].ToString().Trim();
                        linie.DATA = reader["DATA"].ToString().Trim();
                        linie.NDP = reader["NDP"].ToString().Trim();
                        listaLinii.Add(linie);
                        linie = new SAGALinieRegistru();
                    }
                    Log = "Conectare cu succes";
                    DatabaseUtils.ScrieLog(Log, "linie cheltuiala - SAGALiniiRegistru" + societatea.SAGAfolder, DateTime.Now.ToString());
                }
                catch (Exception x)
                {
                    Log = "Nu s-a putut conecta: " + x.ToString();
                    DatabaseUtils.ScrieLog(Log, "linie cheltuiala - SAGALiniiRegistru" + societatea.SAGAfolder, DateTime.Now.ToString());
                }
            }
            return listaLinii;
        }

        public class SAGAUpdateInvestitie
        {
            public Int32 SAGAStatus = 0;
            public string SAGAExplicatii = string.Empty;
            public decimal SAGASuma = 0;
            public Int32 IdInvestitie = 0;
            public SAGAUpdateInvestitie()
            {

            }
        }
        ///<summary>
        ///Verifica daca a fost introdusa cheltuiala in SAGA si updateaza statusul, explicatia si suma
        ///Pentru O SINGURA cheltuiala
        ///</summary>
        public static void UpdateCheltuialaCuStatusSAGA(Cheltuieli cheltuiala)
        {
            if (cheltuiala == null) return;
            SAGAUpdateInvestitie sagaUpdateInvestitie = SAGAComparaSuma(cheltuiala);
            string cmdStr = "";
            SqlConnection conexiune = DatabaseUtils.ConnectToDatabase();
            using (conexiune)
            {
                cmdStr = @"UPDATE [dbo].[Investitii] SET
                     [SAGAStatus] = Convert(int, @SAGAStatus)
                    ,[SAGAExplicatii] = @SAGAExplicatii
                    ,[SAGASuma] = Convert(decimal(18,2), @SAGASuma)
                            WHERE IdInvestitie = Convert(int, @IdInvestitie)";
                SqlCommand cmd = new SqlCommand(cmdStr, conexiune);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("SAGAStatus", sagaUpdateInvestitie.SAGAStatus);
                cmd.Parameters.AddWithValue("SAGAExplicatii", sagaUpdateInvestitie.SAGAExplicatii);
                cmd.Parameters.AddWithValue("SAGASuma", sagaUpdateInvestitie.SAGASuma);
                cmd.Parameters.AddWithValue("IdInvestitie", sagaUpdateInvestitie.IdInvestitie);
                cmd.ExecuteNonQuery();
                conexiune.Close();
                conexiune.Dispose();
            }
        }
        ///<summary>
        ///Verifica daca a fost introdusa cheltuiala in SAGA si updateaza statusul, explicatia si suma
        ///Pentru O LISTA DE cheltuieli
        ///</summary>
        public static void UpdateCheltuialaCuStatusSAGA(List<Cheltuieli> cheltuieli)
        {
            if (cheltuieli == null) return;
            string update = "";
            foreach (Cheltuieli cheltuiala in cheltuieli)
            {
                SAGAUpdateInvestitie sagaUpdateInvestitie = SAGAComparaSuma(cheltuiala);
                update += @"UPDATE [dbo].[Investitii] SET
                     [SAGAStatus] = Convert(int, " + sagaUpdateInvestitie.SAGAStatus.ToString() + @")
                    ,[SAGAExplicatii] = '" + sagaUpdateInvestitie.SAGAExplicatii + @"'
                    ,[SAGASuma] = Convert(decimal(18,2), " + sagaUpdateInvestitie.SAGASuma.ToString() + @")
                            WHERE IdInvestitie = Convert(int, " + sagaUpdateInvestitie.IdInvestitie.ToString() + @"); ";
            }
            if (update.Length < 1) return;
            SqlConnection conexiune = DatabaseUtils.ConnectToDatabase();
            using (conexiune)
            {
                SqlCommand cmd = new SqlCommand(update, conexiune);
                cmd.ExecuteNonQuery();
                conexiune.Close();
                conexiune.Dispose();
            }
        }
        ///<summary>
        ///Verifica daca a fost introdusa cheltuiala in SAGA si updateaza statusul, explicatia si suma
        ///Pentru O LISTA DE cheltuieli pentru O SOCIETATE
        ///</summary>
        public static void UpdateCheltuialaCuStatusSAGA(List<Cheltuieli> cheltuieli, Societati societate)
        {
            if (cheltuieli == null) return;
            string update = "";
            foreach (Cheltuieli cheltuiala in cheltuieli)
            {
                SAGAUpdateInvestitie sagaUpdateInvestitie = SAGAComparaSuma(cheltuiala);
                update += @"UPDATE [dbo].[Investitii] SET
                     [SAGAStatus] = Convert(int, " + sagaUpdateInvestitie.SAGAStatus.ToString() + @")
                    ,[SAGAExplicatii] = '" + sagaUpdateInvestitie.SAGAExplicatii + @"'
                    ,[SAGASuma] = Convert(decimal(18,2), " + sagaUpdateInvestitie.SAGASuma.ToString() + @")
                            WHERE IdInvestitie = Convert(int, " + sagaUpdateInvestitie.IdInvestitie.ToString() + @"); ";
            }
            if (update.Length < 1) return;
            SqlConnection conexiune = DatabaseUtils.ConnectToDatabase();
            using (conexiune)
            {
                SqlCommand cmd = new SqlCommand(update, conexiune);
                cmd.ExecuteNonQuery();
                conexiune.Close();
                conexiune.Dispose();
            }
        }

        public static SAGAUpdateInvestitie SAGAComparaSuma(Cheltuieli cheltuiala)
        {
            SAGAUpdateInvestitie sagaUpdateInvestitie = new SAGAUpdateInvestitie();
            sagaUpdateInvestitie.IdInvestitie = cheltuiala.IdCheltuieli;

            List<SAGALinieRegistru> listaLinii = SAGACitesteLiniiRegistru(cheltuiala);

            switch (listaLinii.Count)
            {
                case 0:
                    // daca nu e nicio inregistrare status 0 - nu e introdusa
                    sagaUpdateInvestitie.SAGAStatus = 0;
                    sagaUpdateInvestitie.SAGAExplicatii = "<div class=\"label label-warning\">SAGA:  - </div>";
                    sagaUpdateInvestitie.SAGASuma = 0;
                    break;
                case 1:
                    // daca este o inreg si suma corespunde - status 1 - e OK
                    sagaUpdateInvestitie.SAGAExplicatii = "SAGA: ";
                    sagaUpdateInvestitie.SAGAStatus = cheltuiala.CheltuialaBrut == Convert.ToDecimal(listaLinii[0].SUMA) ? 1 : 2;
                    sagaUpdateInvestitie.SAGAExplicatii += listaLinii[0].EXPLICATIE + ": " + listaLinii[0].SUMA + "lei <br />";
                    //sagaUpdateInvestitie.SAGAExplicatii = listaLinii[0].ID_NOTA + "#" + listaLinii[0].EXPLICATIE + "#" + listaLinii[0].NDP;
                    sagaUpdateInvestitie.SAGASuma = Convert.ToDecimal(listaLinii[0].SUMA);
                    break;
                default:
                    // daca sunt mai multe inregistrari si suma totala corespunde - status 1, daca nu, 2
                    sagaUpdateInvestitie.SAGAExplicatii = "SAGA: ";
                    foreach (SAGALinieRegistru linie in listaLinii)
                    {
                        sagaUpdateInvestitie.SAGAExplicatii += linie.EXPLICATIE + ": " + linie.SUMA + "lei <br />";
                        //sagaUpdateInvestitie.SAGAExplicatii += linie.ID_NOTA + "#" + linie.EXPLICATIE + "#" + linie.NDP + " | ";
                        sagaUpdateInvestitie.SAGASuma += Convert.ToDecimal(linie.SUMA);
                    }
                    sagaUpdateInvestitie.SAGAStatus = cheltuiala.CheltuialaBrut == sagaUpdateInvestitie.SAGASuma ? 1 : 2;
                    break;
            }
            return sagaUpdateInvestitie;
        }
        #endregion
















        #region nefolosite si neimportante
        public static CultureInfo cultura = new CultureInfo("ro-RO", false);
        public class SqlConnectionServices
        {
            public static string ConnectionString = ConfigurationManager.ConnectionStrings["FacturareConnectionString"].ConnectionString;

            public static SqlConnection Connection = new SqlConnection(ConnectionString);

            public static void OpenConnection()
            {
                Connection.Open();
            }

            public static void CloseConnection()
            {
                Connection.Close();
            }
        }

        /// <summary>
        /// nu il mai folosim - e pentru chirii.rdlc cu toate societatile
        /// </summary>
        /// <param name="societate"></param>
        /// <returns></returns>
        public static List<DataSourceChiriasi.Chirii> CitesteContracteDinFacturare(int societate)
        {
            // situatii nominale chirii si utilitati
            DataSourceChiriasi.Chirii contractChirie = new DataSourceChiriasi.Chirii();
            List<DataSourceChiriasi.Chirii> listaContracteChirie = new List<DataSourceChiriasi.Chirii>();
            // consideram in functie de 01 a lunii
            string data = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd.MM.yyyy");
            string select = @"SELECT 
                    [Companies].CompanyName as SocietateDenumire
                    ,[Companies].[CompaniesId] as SocietateId
                    ,[Companies].[SagaPath] as SocietateFolderSaga
                    ,[Clients].[ClientName] as ChiriasDenumireSAGA
                    ,[Contracts].[ClientId] as ChiriasIdFacturare
                    ,[Clients].[SagaCode] as ChiriasIdSAGA_Cod
                    ,[Locations].[LocationName] as LocatieAdresa
                    ,[Contracts].[SurfaceShop] as ContractSuprafataInchiriata
                    ,[ContractsId] as ContractIdFacturare
                    ,[Contracts].[SurfaceShopPrice] as ContractChiriaUnitara
                    ,[Contracts].[TotalSurfacePrice] as ContractChiriaTotala
                    ,[ExchangeRate].[ExchangeName] as ContractCursSchimbDenumire
 
                      FROM [Facturare].[dbo].[Contracts] 
                      INNER JOIN Companies on companies.CompaniesId = Contracts.CompanyId
                      INNER JOIN Clients on Clients.ClientsId = Contracts.ClientId
                      INNER JOIN Locations on Locations.LocationId = Contracts.LocationsId
                      INNER JOIN ExchangeRate on ExchangeRate.ExchangeRateId = Contracts.ExchangeRateId
                      where 
                      [Companies].[CompaniesId] = " + societate +
                      @" AND
                            ( 
                                    CONVERT(datetime,contracts.StartDate,104) <= CONVERT(datetime,'" + data + @"',104)
							    AND CONVERT(datetime,contracts.FinishDate,104) >= CONVERT(datetime,'" + data + @"',104)
                            )"
                      +
                      @" AND companiesid <>37
                      order by Companies.CompanyName, ContractsId";
            using (SqlConnection connection = new SqlConnection(SqlConnectionServices.ConnectionString))
            {
                SqlCommand command = new SqlCommand(select, connection);
                command.Connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    contractChirie = new DataSourceChiriasi.Chirii();
                    contractChirie.SocietateDenumire = reader["SocietateDenumire"].ToString().Trim();
                    contractChirie.SocietateId = Convert.ToInt32(reader["SocietateId"]);
                    contractChirie.SocietateFolderSaga = reader["SocietateFolderSaga"].ToString().Trim();
                    contractChirie.ChiriasDenumireSAGA = reader["ChiriasDenumireSAGA"].ToString().Trim();
                    contractChirie.ChiriasIdFacturare = Convert.ToInt32(reader["ChiriasIdFacturare"]);
                    contractChirie.ChiriasIdSAGA_Cod = reader["ChiriasIdSAGA_Cod"].ToString().Trim();
                    contractChirie.LocatieAdresa = reader["LocatieAdresa"].ToString().Trim();
                    contractChirie.ContractSuprafataInchiriata = reader["ContractSuprafataInchiriata"].ToString().Trim();
                    contractChirie.ContractIdFacturare = Convert.ToInt32(reader["ContractIdFacturare"]);
                    contractChirie.ContractChiriaUnitara = reader["ContractChiriaUnitara"].ToString().Trim();
                    contractChirie.ContractChiriaTotala = reader["ContractChiriaTotala"].ToString().Trim();
                    contractChirie.ContractCursSchimbDenumire = reader["ContractCursSchimbDenumire"].ToString().Trim();
                    listaContracteChirie.Add(contractChirie);
                }
                reader.Close();
                connection.Close();
            }
            return listaContracteChirie;
        }
        //public static List<DataSourceChiriasi.Chirii> CitesteContracteExpirateDinFacturare(int societate)
        //{
        //    DataSourceChiriasi.Chirii contractChirie = new DataSourceChiriasi.Chirii();
        //    List<DataSourceChiriasi.Chirii> listaContracteChirie = new List<DataSourceChiriasi.Chirii>();

        //    string select = @"SELECT 
        //            [Companies].CompanyName as SocietateDenumire
        //            ,[Companies].[CompaniesId] as SocietateId
        //            ,[Companies].[SagaPath] as SocietateFolderSaga
        //            ,[Clients].[ClientName] as ChiriasDenumireSAGA
        //            ,[Contracts].[ClientId] as ChiriasIdFacturare
        //            ,[Clients].[SagaCode] as ChiriasIdSAGA_Cod
        //            ,[Locations].[LocationName] as LocatieAdresa
        //            ,[Contracts].[SurfaceShop] as ContractSuprafataInchiriata
        //            ,[ContractsId] as ContractIdFacturare
        //            ,[Contracts].[SurfaceShopPrice] as ContractChiriaUnitara
        //            ,[Contracts].[TotalSurfacePrice] as ContractChiriaTotala
        //            ,[ExchangeRate].[ExchangeName] as ContractCursSchimbDenumire

        //              FROM [Facturare].[dbo].[Contracts] 
        //              INNER JOIN Companies on companies.CompaniesId = Contracts.CompanyId
        //              INNER JOIN Clients on Clients.ClientsId = Contracts.ClientId
        //              INNER JOIN Locations on Locations.LocationId = Contracts.LocationsId
        //              INNER JOIN ExchangeRate on ExchangeRate.ExchangeRateId = Contracts.ExchangeRateId
        //              where 
        //              [Companies].[CompaniesId] = " + societate +
        //              @" AND
        //                    ( 
        //                            CONVERT(datetime,contracts.StartDate,104) <= CONVERT(datetime,'" + DateTime.Now.AddMonths(1).AddDays(-1).ToString("dd.MM.yyyy") + @"',104)
        //   AND CONVERT(datetime,contracts.FinishDate,104) >= CONVERT(datetime,'" + DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd.MM.yyyy") + @"',104)"
        //              +
        //              @" AND companiesid <>37
        //              order by Companies.CompanyName, ContractsId";
        //    using (SqlConnection connection = new SqlConnection(SqlConnectionServices.ConnectionString))
        //    {
        //        SqlCommand command = new SqlCommand(select, connection);
        //        command.Connection.Open();
        //        SqlDataReader reader = command.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            contractChirie = new DataSourceChiriasi.Chirii();
        //            contractChirie.SocietateDenumire = reader["SocietateDenumire"].ToString().Trim();
        //            contractChirie.SocietateId = Convert.ToInt32(reader["SocietateId"]);
        //            contractChirie.SocietateFolderSaga = reader["SocietateFolderSaga"].ToString().Trim();
        //            contractChirie.ChiriasDenumireSAGA = reader["ChiriasDenumireSAGA"].ToString().Trim();
        //            contractChirie.ChiriasIdFacturare = Convert.ToInt32(reader["ChiriasIdFacturare"]);
        //            contractChirie.ChiriasIdSAGA_Cod = reader["ChiriasIdSAGA_Cod"].ToString().Trim();
        //            contractChirie.LocatieAdresa = reader["LocatieAdresa"].ToString().Trim();
        //            contractChirie.ContractSuprafataInchiriata = reader["ContractSuprafataInchiriata"].ToString().Trim();
        //            contractChirie.ContractIdFacturare = Convert.ToInt32(reader["ContractIdFacturare"]);
        //            contractChirie.ContractChiriaUnitara = reader["ContractChiriaUnitara"].ToString().Trim();
        //            contractChirie.ContractChiriaTotala = reader["ContractChiriaTotala"].ToString().Trim();
        //            contractChirie.ContractCursSchimbDenumire = reader["ContractCursSchimbDenumire"].ToString().Trim();
        //            listaContracteChirie.Add(contractChirie);
        //        }
        //        reader.Close();
        //        connection.Close();
        //    }
        //    return listaContracteChirie;
        //}

        //public static List<DataSourceChiriasi.Chirii> CitesteSinteticeDinFacturare()
        //{
        //    DataSourceChiriasi.Chirii contractChirie = new DataSourceChiriasi.Chirii();
        //    List<DataSourceChiriasi.Chirii> listaContracteChirie = new List<DataSourceChiriasi.Chirii>();

        //    string select = @"SELECT 
        //                     coalesce (sum(SurfaceShop),0) as SuprafataInchiriata
        //                     ,coalesce (sum(coalesce(contracts.TotalSurfacePrice,0)),0) as ChiriaTotalaPeLuna
        //,Companies.CompaniesId as SocietateId
        //                     ,Companies.CompanyName as SocietateDenumire
        //                        ,[Companies].[SagaPath] as SocietateFolderSaga
        //                         FROM  [Facturare].[dbo].Companies 
        //                      full  join Contracts  on [contracts].[companyid] = [companies].[companiesid]
        // where 
        // [Companies].[CompaniesId] <> 37 
        // and (convert(datetime,contracts.FinishDate,104) < GETDATE()
        // or Contracts.ContractsId is null)
        //                      group by   [Companies].CompaniesId
        //                                ,[Companies].[CompanyName]
        //                                ,[Companies].[SagaPath]
        //                      order by [Companies].CompanyName";
        //    using (SqlConnection connection = new SqlConnection(SqlConnectionServices.ConnectionString))
        //    {
        //        SqlCommand command = new SqlCommand(select, connection);
        //        command.Connection.Open();
        //        SqlDataReader reader = command.ExecuteReader();
        //        int contor = 0;
        //        while (reader.Read())
        //        {
        //            contractChirie = new DataSourceChiriasi.Chirii();
        //            contractChirie.SocietateDenumire = reader["SocietateDenumire"].ToString().Trim();
        //            contractChirie.SocietateId = Convert.ToInt32(reader["SocietateId"]);
        //            contractChirie.SocietateFolderSaga = reader["SocietateFolderSaga"].ToString().Trim();
        //            contractChirie.SocietateSuprafataInchiriabilaTotala = DatabaseUtils.DetaliiSocietateDupaIdFacturare(contractChirie.SocietateId).SuprafataTotala;
        //            contractChirie.SocietateSuprafataInchiriataTotala = Convert.ToDecimal(reader["SuprafataInchiriata"]);
        //            contractChirie.SocietateSuprafataNeInchiriata = contractChirie.SocietateSuprafataInchiriabilaTotala - contractChirie.SocietateSuprafataInchiriataTotala;
        //            if (contractChirie.SocietateSuprafataInchiriabilaTotala > 0)
        //                contractChirie.SocietateProcentInchiriere = (contractChirie.SocietateSuprafataInchiriataTotala * 100) / contractChirie.SocietateSuprafataInchiriabilaTotala;
        //            else contractChirie.SocietateProcentInchiriere = 0;
        //            contractChirie.SocietateChiriaTotalaPeLuna = Convert.ToDecimal(reader["ChiriaTotalaPeLuna"]);
        //            if (contractChirie.SocietateSuprafataInchiriataTotala > 0)
        //                contractChirie.SocietateChiriaUnitaraMedie = contractChirie.SocietateChiriaTotalaPeLuna / contractChirie.SocietateSuprafataInchiriataTotala;
        //            else contractChirie.SocietateChiriaUnitaraMedie = 0;

        //            if (contor == 0)
        //            {
        //                contractChirie.LunaAnterioaraARaportului = DateTime.Now.AddDays(-DateTime.Now.Day);
        //            }

        //            listaContracteChirie.Add(contractChirie);
        //        }
        //        reader.Close();
        //        connection.Close();
        //    }
        //    return listaContracteChirie;
        //}

        //public static List<DataSourceChiriasi.Chirii> ListaContracteNeincadrateInPerioadaRaportului(DateTime dataInceput, DateTime dataFinal)
        //{
        //    DataSourceChiriasi.Chirii contractChirie = new DataSourceChiriasi.Chirii();
        //    List<DataSourceChiriasi.Chirii> listaContracteChirie = new List<DataSourceChiriasi.Chirii>();

        //    string select = @"SELECT 
        //                      COALESCE(sum(SurfaceShop),0) AS SuprafataInchiriata
        //                     ,COALESCE(sum(coalesce(contracts.TotalSurfacePrice,0)),0) AS ChiriaTotalaPeLuna
        //,Companies.CompaniesId AS SocietateId
        //                     ,Companies.CompanyName AS SocietateDenumire
        //                        ,[Companies].[SagaPath] AS SocietateFolderSaga
        //                         FROM  [Facturare].[dbo].Companies 
        //                      FULL JOIN Contracts  ON [contracts].[companyid] = [companies].[companiesid]
        // WHERE
        //                                [Companies].[CompaniesId] <> 37 
        //       AND (
        //                                    (
        //                                        CONVERT(datetime,contracts.StartDate,104) > CONVERT(datetime," + dataFinal + @",104)
        //                   OR CONVERT(datetime,contracts.FinishDate,104) < CONVERT(datetime," + dataInceput + @",104)
        //                                    )
        //                                    OR Contracts.ContractsId IS NULL
        //                                )
        //                      GROUP BY   [Companies].[CompaniesId]
        //                                ,[Companies].[CompanyName]
        //                                ,[Companies].[SagaPath]
        //                      ORDER BY [Companies].[CompanyName]";
        //    using (SqlConnection connection = new SqlConnection(SqlConnectionServices.ConnectionString))
        //    {
        //        SqlCommand command = new SqlCommand(select, connection);
        //        command.Connection.Open();
        //        SqlDataReader reader = command.ExecuteReader();
        //        int contor = 0;
        //        while (reader.Read())
        //        {
        //            contractChirie = new DataSourceChiriasi.Chirii();
        //            contractChirie.SocietateDenumire = reader["SocietateDenumire"].ToString().Trim();
        //            contractChirie.SocietateId = Convert.ToInt32(reader["SocietateId"]);
        //            contractChirie.SocietateFolderSaga = reader["SocietateFolderSaga"].ToString().Trim();
        //            contractChirie.SocietateSuprafataInchiriabilaTotala = DatabaseUtils.DetaliiSocietateDupaIdFacturare(contractChirie.SocietateId).SuprafataTotala;
        //            contractChirie.SocietateSuprafataInchiriataTotala = Convert.ToDecimal(reader["SuprafataInchiriata"]);
        //            contractChirie.SocietateSuprafataNeInchiriata = contractChirie.SocietateSuprafataInchiriabilaTotala - contractChirie.SocietateSuprafataInchiriataTotala;
        //            if (contractChirie.SocietateSuprafataInchiriabilaTotala > 0)
        //                contractChirie.SocietateProcentInchiriere = (contractChirie.SocietateSuprafataInchiriataTotala * 100) / contractChirie.SocietateSuprafataInchiriabilaTotala;
        //            else contractChirie.SocietateProcentInchiriere = 0;
        //            contractChirie.SocietateChiriaTotalaPeLuna = Convert.ToDecimal(reader["ChiriaTotalaPeLuna"]);
        //            if (contractChirie.SocietateSuprafataInchiriataTotala > 0)
        //                contractChirie.SocietateChiriaUnitaraMedie = contractChirie.SocietateChiriaTotalaPeLuna / contractChirie.SocietateSuprafataInchiriataTotala;
        //            else contractChirie.SocietateChiriaUnitaraMedie = 0;

        //            if (contor == 0)
        //            {
        //                contractChirie.LunaAnterioaraARaportului = DateTime.Now.AddDays(-DateTime.Now.Day);
        //            }

        //            listaContracteChirie.Add(contractChirie);
        //        }
        //        reader.Close();
        //        connection.Close();
        //    }
        //    return listaContracteChirie;
        //}
        #endregion

    }
}
