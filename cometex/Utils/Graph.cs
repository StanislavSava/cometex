﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cometex.Utils
{
    public class areaChartData
    {
        public areaChartData()
        {
            datasets = new List<datasets>();
        }

        public string labels { get; set; }
        public List<datasets> datasets { get; set; }
    }

    public class datasets
    {
        public datasets()
        {

        }
        public string label { get; set; }
        public bool fill { get; set; }
        public float lineTension { get; set; }
        public string backgroundColor { get; set; }
        public string borderColor { get; set; }
        public string pointColor { get; set; }
        public string data { get; set; }
        public string pointBorderColor { get; set; }
        public string pointBackgroundColor { get; set; }
        //public int pointBorderWidth { get; set; }
        //public int pointRadius { get; set; }
        //public string pointHoverBackgroundColor { get; set; }
        //public string pointHoverBorderColor { get; set; }
        //public int pointHoverBorderWidth { get; set; }
        //public int pointHoverRadius { get; set; }

    }
}