﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using cometex.Clase;

namespace cometex.Utils
{
    public class EmailOperare
    {
        static public void TrimiteEmail(MesajeCls mesaj)
        {
            try
            {
                System.Net.Mail.MailAddress sender = new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings["EmailAdresaExpeditor"].ToString(), ConfigurationManager.AppSettings["EmailText"].ToString());

                try
                {
                    if (mesaj.AdresaExpeditor.Count > 0)
                        sender = new System.Net.Mail.MailAddress(mesaj.AdresaExpeditor[0], mesaj.AdresaExpeditor[1]);
                }
                catch
                {
                    //daca nu avem valori  folosim implicit valorile din config
                }

                System.Net.Mail.MailAddress receiver = new System.Net.Mail.MailAddress(mesaj.AdresaDestinatar[0], mesaj.AdresaDestinatar[1]);
                System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage(sender, receiver);


                email.Subject = mesaj.Subiect;
                email.Body = mesaj.Continut;
                email.IsBodyHtml = true;
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EmailCredentialsUsername"].ToString(), ConfigurationManager.AppSettings["EmailCredentialsPassword"].ToString());

                System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient(ConfigurationManager.AppSettings["EmailClientHost"].ToString(), Convert.ToInt32(ConfigurationManager.AppSettings["EmailClientPort"].ToString()));
                smtpClient.EnableSsl = true;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = credentials;
                smtpClient.Send(email);
            }
            catch
            {
                throw;
            }
        }
    }
}