﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace cometex.Utils
{
    public class ConturiUtils
    {
        public static List<string[]> ListaConturiCreeaza(string text, string delimitator1, string delimitator2)
        {
            List<string[]> listaConturi = new List<string[]>();
            string[] splitter = { delimitator1 };
            string[] conturi = text.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
            foreach (string cont in conturi)
            {
                try
                {
                    string[] element = { cont.Substring(0, cont.IndexOf(delimitator2)).Trim(), cont.Substring(cont.IndexOf(delimitator2) + delimitator2.Length).Trim() };
                    listaConturi.Add(element);
                }
                catch (Exception x)
                {
                    // throw x;
                }
            }
            return listaConturi;
        }
        public static List<string> ListaConturiCreeaza(string text, string delimitator1)
        {
            string[] splitter = { delimitator1 };
            List<string> listaConturi = text.Split(splitter, StringSplitOptions.RemoveEmptyEntries).ToList();
            return listaConturi;
        }
        public static string ListaConturiStringAdauga(string text, string delimitator1, string delimitator2)
        {
            string listaConturi = "";
            string[] splitter = { delimitator1 };
            string[] conturi = text.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
            foreach (string cont in conturi)
            {
                try
                {
                    listaConturi += "#" + cont.Substring(0, cont.IndexOf(delimitator2)).Trim();
                }
                catch (Exception x)
                {
                    // throw x;
                }
            }
            return listaConturi;
        }

        public static string ListaConturiStringModificaInit(string conturiString, string delimitator, DropDownList ddl)
        {
            string listaConturi = "";
            string[] splitter = { delimitator };
            string[] conturi = conturiString.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
            foreach (string cont in conturi)
            {
                try
                {
                    ddl.SelectedValue = cont;
                    listaConturi += ddl.SelectedItem.Text + "<br />";
                    ddl.Items.Remove(ddl.SelectedItem);
                }
                catch (Exception x)
                {
                    // throw x;
                }
            }
            return listaConturi;
        }
    }
}