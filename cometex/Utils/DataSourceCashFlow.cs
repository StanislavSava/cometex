﻿using FirebirdSql.Data.FirebirdClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using cometex.Clase;
using cometex.Utils;
using System.Configuration;
using System.Globalization;
using System.Data.SqlClient;

namespace cometex.Utils
{
    public class DataSourceCashFlow
    {

        public static List<CampRaport> CampuriRaport()
        {
            return CampuriRaportq(HttpContext.Current.Session["dataSoldPrecedent"].ToString(), HttpContext.Current.Session["dataSoldFinal"].ToString());
        }

        public static List<CampRaport> CampuriRaportq(string dataPrecedenta, string dataFinala)
        {
            List<CampRaport> camp = new List<CampRaport>();
            CashflowConturi conturi = new CashflowConturi();
            CultureInfo cultura = new CultureInfo("ro-RO");
            cultura.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
            cultura.DateTimeFormat.LongDatePattern = "dd.MM.yyyy";

            // ia lista de societati 
            List<Societati> societati = DatabaseUtils.ListaSocietati();

            foreach (Societati societate in societati)
            {
                conturi = CashFlowConturi(societate.Id.ToString());
                CampRaport rand = ValoriLinieCashFlowDinBazaDeDate(societate, Convert.ToDateTime(dataPrecedenta, cultura), Convert.ToDateTime(dataFinala, cultura), conturi);
                rand.SocietateDenumire = societate.Denumire;
                camp.Add(rand);
                rand = new CampRaport();
            }
            return camp;
        }

        public static CampRaport ValoriLinieCashFlowDinBazaDeDate(Societati societate, DateTime dataPrecedenta, DateTime dataFinala, CashflowConturi conturi)
        {
            CultureInfo cultura = new CultureInfo("ro-RO");
            cultura.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
            cultura.DateTimeFormat.LongDatePattern = "dd.MM.yyyy";
            CashflowConturi conturiSQL = SAGA.SAGACreeazaStringWhereSQL(conturi);
            string select = @" select
                
                        /* solduri */
               
                       Coalesce((select 
                                        coalesce(sum(suma),0) from registru r
                   where r.data <= '" + dataPrecedenta.ToString("dd.MM.yyyy") + @"'" + conturiSQL.ContCurent.Replace("DEINLOCUIT", "cont_d") + @")
                   -   (select coalesce(sum(suma),0) from registru r
                   where r.data <= '" + dataPrecedenta.ToString("dd.MM.yyyy") + @"'" + conturiSQL.ContCurent.Replace("DEINLOCUIT", "cont_c") + @"),0)
                                    as ContCurent_SoldPrecedent
                    , Coalesce((select coalesce(sum(suma),0) from registru r
                   where r.data <= '" + dataFinala.ToString("dd.MM.yyyy") + @"'" + conturiSQL.ContCurent.Replace("DEINLOCUIT", "cont_d") + @")
                   - (select coalesce(sum(suma),0) from registru r
                   where r.data <= '" + dataFinala.ToString("dd.MM.yyyy") + @"'" + conturiSQL.ContCurent.Replace("DEINLOCUIT", "cont_c") + @"),0)
                                    as ContCurent_SoldFinal
                    , Coalesce((select coalesce(sum(suma),0) from registru r
                   where r.data <= '" + dataPrecedenta.ToString("dd.MM.yyyy") + @"'" + conturiSQL.Depozit.Replace("DEINLOCUIT", "cont_d") + @")
                   - (select coalesce(sum(suma),0) from registru r
                   where r.data <= '" + dataPrecedenta.ToString("dd.MM.yyyy") + @"'" + conturiSQL.Depozit.Replace("DEINLOCUIT", "cont_c") + @"),0)
                                    as Depozit_SoldPrecedent
                    , Coalesce((select coalesce(sum(suma),0) from registru r
                   where r.data <= '" + dataFinala.ToString("dd.MM.yyyy") + @"'" + conturiSQL.Depozit.Replace("DEINLOCUIT", "cont_d") + @")
                   - (select coalesce(sum(suma),0) from registru r
                   where r.data <= '" + dataFinala.ToString("dd.MM.yyyy") + @"'" + conturiSQL.Depozit.Replace("DEINLOCUIT", "cont_c") + @"),0)
                                    as Depozit_SoldFinal
                    , Coalesce((select coalesce(sum(suma),0) from registru r
                   where r.data <= '" + dataPrecedenta.ToString("dd.MM.yyyy") + @"'" + conturiSQL.DepozitTaxe.Replace("DEINLOCUIT", "cont_d") + @")
                   - (select coalesce(sum(suma),0) from registru r
                   where r.data <= '" + dataPrecedenta.ToString("dd.MM.yyyy") + @"'" + conturiSQL.DepozitTaxe.Replace("DEINLOCUIT", "cont_c") + @"),0)
                                    as DepozitTaxe_SoldPrecedent
                    , Coalesce((select coalesce(sum(suma),0) from registru r
                   where r.data <= '" + dataFinala.ToString("dd.MM.yyyy") + @"'" + conturiSQL.DepozitTaxe.Replace("DEINLOCUIT", "cont_d") + @")
                   - (select coalesce(sum(suma),0) from registru r
                   where r.data <= '" + dataFinala.ToString("dd.MM.yyyy") + @"'" + conturiSQL.DepozitTaxe.Replace("DEINLOCUIT", "cont_c") + @"),0)
                                    as DepozitTaxe_SoldFinal
                    , Coalesce((select coalesce(sum(suma),0) from registru r
                   where r.data <= '" + dataPrecedenta.ToString("dd.MM.yyyy") + @"'" + conturiSQL.Casierie.Replace("DEINLOCUIT", "cont_d") + @")
                   - (select coalesce(sum(suma),0) from registru r
                   where r.data <= '" + dataPrecedenta.ToString("dd.MM.yyyy") + @"'" + conturiSQL.Casierie.Replace("DEINLOCUIT", "cont_c") + @"),0)
                                    as Casierie_SoldPrecedent
                    , Coalesce((select coalesce(sum(suma),0)  from registru r
                   where r.data <= '" + dataFinala.ToString("dd.MM.yyyy") + @"'" + conturiSQL.Casierie.Replace("DEINLOCUIT", "cont_d") + @")
                   - (select coalesce(sum(suma),0)  from registru r
                   where r.data <= '" + dataFinala.ToString("dd.MM.yyyy") + @"'" + conturiSQL.Casierie.Replace("DEINLOCUIT", "cont_c") + @"),0)
                                    as Casierie_SoldFinal
                    
                /* incasari in ziua respectiva */
                /* K  chirii                   */
                    , Coalesce((SELECT coalesce(sum(suma),0) FROM REGISTRU r
                where data = '" + dataFinala.ToString("dd.MM.yyyy") + @"' 
               /* and (explicatie like 'Incas%K %' or explicatie like 'Incas%k %')*/
                AND (explicatie SIMILAR TO 'Incas.%K[[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]]%' 
                    OR explicatie SIMILAR TO 'Incas.%k[[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]]%') 
                                " + conturiSQL.ContCurent.Replace("DEINLOCUIT", "cont_d") + @"),0)
                                    as ContCurent_IncasariChirii
                                    
                    , Coalesce((SELECT coalesce(sum(suma),0) FROM REGISTRU r
                where data = '" + dataFinala.ToString("dd.MM.yyyy") + @"' 
                /*and (explicatie like 'Incas%K %' or explicatie like 'Incas%k %')*/
                    AND (explicatie SIMILAR TO 'Incas.%K[[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]]%' 
                    OR explicatie SIMILAR TO 'Incas.%k[[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]]%')  
                                " + conturiSQL.Depozit.Replace("DEINLOCUIT", "cont_d") + @"),0)
                                    as Depozit_IncasariChirii                    
                                    
                    , Coalesce((SELECT coalesce(sum(suma),0) FROM REGISTRU r
                where data = '" + dataFinala.ToString("dd.MM.yyyy") + @"' 
                /*and (explicatie like 'Incas%K %' or explicatie like 'Incas%k %')*/
                    AND (explicatie SIMILAR TO 'Incas.%K[[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]]%' 
                    OR explicatie SIMILAR TO 'Incas.%k[[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]]%')  
                                " + conturiSQL.DepozitTaxe.Replace("DEINLOCUIT", "cont_d") + @"),0)
                                    as DepozitTaxe_IncasariChirii
                                    
                    , Coalesce((SELECT coalesce(sum(suma),0) FROM REGISTRU r
                where data = '" + dataFinala.ToString("dd.MM.yyyy") + @"' 
                /*and (explicatie like 'Incas%K %' or explicatie like 'Incas%k %')*/
                    AND (explicatie SIMILAR TO 'Incas.%K[[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]]%' 
                    OR explicatie SIMILAR TO 'Incas.%k[[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]]%')  
                                " + conturiSQL.Casierie.Replace("DEINLOCUIT", "cont_d") + @"),0)
                                    as Casierie_IncasariChirii
                                    
                /* U  utilitati              */
                                                                        
                    , Coalesce((SELECT coalesce(sum(suma),0) FROM REGISTRU r
                where data = '" + dataFinala.ToString("dd.MM.yyyy") + @"' 
                /*and (explicatie like 'Incas%U %' or explicatie like 'Incas%u %') and (explicatie not like 'Incas%K %' or explicatie not like 'Incas%k %')*/
                    AND (explicatie SIMILAR TO 'Incas.%U[[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]]%' 
                    OR explicatie SIMILAR TO 'Incas.%u[[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]]%')  
                    AND (explicatie NOT SIMILAR TO 'Incas.%K[[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]]%' 
                    OR explicatie NOT SIMILAR TO 'Incas.%k[[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]]%')  
                                " + conturiSQL.ContCurent.Replace("DEINLOCUIT", "cont_d") + @"),0)
                                    as ContCurent_IncasariUtilitati
                                    
                    , Coalesce((SELECT coalesce(sum(suma),0) FROM REGISTRU r
                where data = '" + dataFinala.ToString("dd.MM.yyyy") + @"' 
                /*and (explicatie like 'Incas%U %' or explicatie like 'Incas%u %') and (explicatie not like 'Incas%K %' or explicatie not like 'Incas%k %')*/
                    AND (explicatie SIMILAR TO 'Incas.%U[[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]]%' 
                    OR explicatie SIMILAR TO 'Incas.%u[[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]]%')  
                    AND (explicatie NOT SIMILAR TO 'Incas.%K[[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]]%' 
                    OR explicatie NOT SIMILAR TO 'Incas.%k[[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]]%')  
                                " + conturiSQL.Depozit.Replace("DEINLOCUIT", "cont_d") + @"),0)
                                    as Depozit_IncasariUtilitati                    
                                    
                    , Coalesce((SELECT coalesce(sum(suma),0) FROM REGISTRU r
                where data = '" + dataFinala.ToString("dd.MM.yyyy") + @"' 
                /*and (explicatie like 'Incas%U %' or explicatie like 'Incas%u %') and (explicatie not like 'Incas%K %' or explicatie not like 'Incas%k %')*/
                    AND (explicatie SIMILAR TO 'Incas.%U[[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]]%' 
                    OR explicatie SIMILAR TO 'Incas.%u[[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]]%')  
                    AND (explicatie NOT SIMILAR TO 'Incas.%K[[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]]%' 
                    OR explicatie NOT SIMILAR TO 'Incas.%k[[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]]%')  
                                " + conturiSQL.DepozitTaxe.Replace("DEINLOCUIT", "cont_d") + @"),0)
                                    as DepozitTaxe_IncasariUtilitati
                                    
                    , Coalesce((SELECT coalesce(sum(suma),0) FROM REGISTRU r
                where data = '" + dataFinala.ToString("dd.MM.yyyy") + @"' 
                    /*and (explicatie like 'Incas%U %' or explicatie like 'Incas%u %') and (explicatie not like 'Incas%K %' or explicatie not like 'Incas%k %')*/
                    AND (explicatie SIMILAR TO 'Incas.%U[[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]]%' 
                    OR explicatie SIMILAR TO 'Incas.%u[[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]]%')  
                    AND (explicatie NOT SIMILAR TO 'Incas.%K[[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]]%' 
                    OR explicatie NOT SIMILAR TO 'Incas.%k[[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]][[:SPACE:][:DIGIT:]]%')  
                                " + conturiSQL.Casierie.Replace("DEINLOCUIT", "cont_d") + @"),0)
                                    as Casierie_IncasariUtilitati
                /* plati in ziua respectiva */
          
                /* decontari interne */
                    
                    , coalesce((SELECT coalesce(sum(suma),0)
                FROM REGISTRU a where data = '" + dataFinala.ToString("dd.MM.yyyy") + @"' and cont_c = '581' " + conturiSQL.ContCurent.Replace("DEINLOCUIT", "cont_d") + @"),0) as              ContCurent_DecontariInterne
                    , coalesce((SELECT coalesce(sum(suma),0)
                FROM REGISTRU a where data = '" + dataFinala.ToString("dd.MM.yyyy") + @"' and cont_c = '581' " + conturiSQL.Depozit.Replace("DEINLOCUIT", "cont_d") + @"),0) as                 Depozit_DecontariInterne
                    , coalesce((SELECT coalesce(sum(suma),0)
                FROM REGISTRU a where data = '" + dataFinala.ToString("dd.MM.yyyy") + @"' and cont_c = '581' " + conturiSQL.DepozitTaxe.Replace("DEINLOCUIT", "cont_d") + @"),0) as             DepozitTaxe_DecontariInterne
                    , coalesce((SELECT coalesce(sum(suma),0)
                FROM REGISTRU a where data = '" + dataFinala.ToString("dd.MM.yyyy") + @"' and cont_c = '581' " + conturiSQL.Casierie.Replace("DEINLOCUIT", "cont_d") + @"),0) as                Casierie_DecontariInterne
                
                    
                ,   sum(suma)
                   from registru where data = '22.01.2016' /* nu conteaza data, doar sa poata face selectul */
                


             ";
            FbConnectionStringBuilder ret = new FbConnectionStringBuilder();
            ret.Database = ConfigurationManager.AppSettings["caleSAGA"].ToString() + "\\" + societate.SAGAfolder + "\\cont_baza.fdb";
            ret.DataSource = "86.120.111.69";
            ret.Port = 3050;
            ret.UserID = "SYSDBA";
            ret.Password = "masterkey";
            ret.Charset = "NONE";
            ret.ConnectionTimeout = 60000;
            ret.Pooling = false;
            FbConnection sagaConnection = new FbConnection(ret.ToString());
            List<string> lista = new List<string>();
            CampRaport campRaport = new CampRaport();
            string Log = "";
            using (sagaConnection)
            {
                try
                {

                    sagaConnection.Open();
                    FbCommand fbCommand = new FbCommand(select, sagaConnection);
                    FbDataReader reader = fbCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        campRaport.ContCurent_SoldPrecedent = Convert.ToDecimal(reader["ContCurent_SoldPrecedent"]);
                        campRaport.ContCurent_SoldFinal = Convert.ToDecimal(reader["ContCurent_SoldFinal"]);

                        campRaport.Depozit_SoldPrecedent = Convert.ToDecimal(reader["Depozit_SoldPrecedent"]);
                        campRaport.Depozit_SoldFinal = Convert.ToDecimal(reader["Depozit_SoldFinal"]);

                        campRaport.DepozitTaxe_SoldPrecedent = Convert.ToDecimal(reader["DepozitTaxe_SoldPrecedent"]);
                        campRaport.DepozitTaxe_SoldFinal = Convert.ToDecimal(reader["DepozitTaxe_SoldFinal"]);

                        campRaport.Casierie_SoldPrecedent = Convert.ToDecimal(reader["Casierie_SoldPrecedent"]);
                        campRaport.Casierie_SoldFinal = Convert.ToDecimal(reader["Casierie_SoldFinal"]);


                        campRaport.ContCurent_IncasariChirii = Convert.ToDecimal(reader["ContCurent_IncasariChirii"]);

                        campRaport.Depozit_IncasariChirii = Convert.ToDecimal(reader["Depozit_IncasariChirii"]);

                        campRaport.DepozitTaxe_IncasariChirii = Convert.ToDecimal(reader["DepozitTaxe_IncasariChirii"]);

                        campRaport.Casierie_IncasariChirii = Convert.ToDecimal(reader["Casierie_IncasariChirii"]);

                        campRaport.ContCurent_IncasariUtilitati = Convert.ToDecimal(reader["ContCurent_IncasariUtilitati"]);

                        campRaport.Depozit_IncasariUtilitati = Convert.ToDecimal(reader["Depozit_IncasariUtilitati"]);

                        campRaport.DepozitTaxe_IncasariUtilitati = Convert.ToDecimal(reader["DepozitTaxe_IncasariUtilitati"]);

                        campRaport.Casierie_IncasariUtilitati = Convert.ToDecimal(reader["Casierie_IncasariUtilitati"]);


                        campRaport.ContCurent_DecontariInterne = Convert.ToDecimal(reader["ContCurent_DecontariInterne"]);

                        campRaport.Depozit_DecontariInterne = Convert.ToDecimal(reader["Depozit_DecontariInterne"]);

                        campRaport.DepozitTaxe_DecontariInterne = Convert.ToDecimal(reader["DepozitTaxe_DecontariInterne"]);

                        campRaport.Casierie_DecontariInterne = Convert.ToDecimal(reader["Casierie_DecontariInterne"]);



                        campRaport.DataCashFlow = Convert.ToDateTime(HttpContext.Current.Session["dataSoldFinal"], cultura);
                        lista = new List<string>();
                        for (int i = 0; i < reader.FieldCount; i++)
                            lista.Add(reader[i].ToString());
                    }
                    Log = "Conectare cu succes";
                    DatabaseUtils.ScrieLog(Log, "cashflow - ValoriLinieCashFlowDinBazaDeDate" + societate.SAGAfolder, DateTime.Now.ToString());
                }
                catch (Exception x)
                {
                    Log = "Nu s-a putut conecta: " + x.ToString();
                    DatabaseUtils.ScrieLog(Log, "cashflow - ValoriLinieCashFlowDinBazaDeDate" + societate.SAGAfolder, DateTime.Now.ToString());
                }
            }
            // plati 
            //campRaport = ConcateneazaLiniiRegistru(societate, dataFinala, conturiSQL, campRaport);
            campRaport = SumeCheltuieli(societate, dataFinala, conturiSQL, campRaport);
            return campRaport;
        }
        public static CampRaport ConcateneazaLiniiRegistru(Societati societate, DateTime data, CashflowConturi conturi, CampRaport campPlati)
        {
            CashflowConturi conturiSQL = conturi;// SAGA.SAGACreeazaStringWhereSQL(conturi);
            // verificam pentru fiecare in cheltuieli ce status au
            // in grila sau aprobate 
            List<SAGA.SAGALinieRegistru> linii = new List<SAGA.SAGALinieRegistru>();
            // cont curent
            int contorGrila = 0;
            int contorAprobate = 0;
            foreach (SAGA.SAGALinieRegistru linie in SAGA.SAGACitesteLiniiRegistru(societate, data, conturiSQL.ContCurent.Replace("DEINLOCUIT", "cont_c")))
            {
                Cheltuieli cheltuiala = new Cheltuieli();
                try { cheltuiala = DatabaseUtils.CitesteCheltuiala(Convert.ToInt32(linie.NDP.Substring(linie.NDP.IndexOf("*") + 1))); } catch { break; }
                // daca e incadrata in grila
                if (cheltuiala.Status.Id == 1 && cheltuiala.RezolutionatDe.Id == 1006)
                {
                    campPlati.ContCurent_PlatiGrilaDestinatie += linie.EXPLICATIE + " | ";
                    campPlati.ContCurent_PlatiGrilaSuma += Convert.ToDecimal(linie.SUMA);
                    contorGrila++;
                }
                else if (cheltuiala.Status.Id == 1)
                {
                    campPlati.ContCurent_PlatiAprobateDestinatie += linie.EXPLICATIE + " | ";
                    campPlati.ContCurent_PlatiAprobateSuma += Convert.ToDecimal(linie.SUMA);
                    contorAprobate++;
                }
            }
            if (contorGrila == 0)
            {
                campPlati.ContCurent_PlatiGrilaDestinatie = "nicio plata";
                campPlati.ContCurent_PlatiGrilaSuma = 0;
            }
            else contorGrila = 0;
            if (contorAprobate == 0)
            {
                campPlati.ContCurent_PlatiAprobateDestinatie = "nicio plata";
                campPlati.ContCurent_PlatiAprobateSuma = 0;
            }
            else contorAprobate = 0;

            linii = new List<SAGA.SAGALinieRegistru>();
            // depozit termen lung
            foreach (SAGA.SAGALinieRegistru linie in SAGA.SAGACitesteLiniiRegistru(societate, data, conturiSQL.Depozit.Replace("DEINLOCUIT", "cont_c")))
            {
                Cheltuieli cheltuiala = new Cheltuieli();
                try { cheltuiala = DatabaseUtils.CitesteCheltuiala(Convert.ToInt32(linie.NDP.Substring(linie.NDP.IndexOf("*") + 1))); } catch { break; }
                // daca e incadrata in grila
                if (cheltuiala.Status.Id == 1 && cheltuiala.RezolutionatDe.Id == 1006)
                {
                    campPlati.Depozit_PlatiGrilaDestinatie += linie.EXPLICATIE + " | ";
                    campPlati.Depozit_PlatiGrilaSuma += Convert.ToDecimal(linie.SUMA);
                    contorGrila++;
                }
                else if (cheltuiala.Status.Id == 1)
                {
                    campPlati.Depozit_PlatiAprobateDestinatie += linie.EXPLICATIE + " | ";
                    campPlati.Depozit_PlatiAprobateSuma += Convert.ToDecimal(linie.SUMA);
                    contorAprobate++;
                }
            }
            if (contorGrila == 0)
            {
                campPlati.Depozit_PlatiGrilaDestinatie = "nicio plata";
                campPlati.Depozit_PlatiGrilaSuma = 0;
            }
            else contorGrila = 0;
            if (contorAprobate == 0)
            {
                campPlati.Depozit_PlatiAprobateDestinatie = "nicio plata";
                campPlati.Depozit_PlatiAprobateSuma = 0;
            }
            else contorAprobate = 0;


            linii = new List<SAGA.SAGALinieRegistru>();
            // depozit taxe
            foreach (SAGA.SAGALinieRegistru linie in SAGA.SAGACitesteLiniiRegistru(societate, data, conturiSQL.DepozitTaxe.Replace("DEINLOCUIT", "cont_c")))
            {
                Cheltuieli cheltuiala = new Cheltuieli();
                try { cheltuiala = DatabaseUtils.CitesteCheltuiala(Convert.ToInt32(linie.NDP.Substring(linie.NDP.IndexOf("*") + 1))); } catch { break; }
                // daca e incadrata in grila
                if (cheltuiala.Status.Id == 1 && cheltuiala.RezolutionatDe.Id == 1006)
                {
                    campPlati.DepozitTaxe_PlatiGrilaDestinatie += linie.EXPLICATIE + " | ";
                    campPlati.DepozitTaxe_PlatiGrilaSuma += Convert.ToDecimal(linie.SUMA);
                    contorGrila++;
                }
                else if (cheltuiala.Status.Id == 1)
                {
                    campPlati.DepozitTaxe_PlatiAprobateDestinatie += linie.EXPLICATIE + " | ";
                    campPlati.DepozitTaxe_PlatiAprobateSuma += Convert.ToDecimal(linie.SUMA);
                    contorAprobate++;
                }
            }
            if (contorGrila == 0)
            {
                campPlati.DepozitTaxe_PlatiGrilaDestinatie = "nicio plata";
                campPlati.DepozitTaxe_PlatiGrilaSuma = 0;
            }
            else contorGrila = 0;
            if (contorAprobate == 0)
            {
                campPlati.DepozitTaxe_PlatiAprobateDestinatie = "nicio plata";
                campPlati.DepozitTaxe_PlatiAprobateSuma = 0;
            }
            else contorAprobate = 0;


            // casierie
            foreach (SAGA.SAGALinieRegistru linie in SAGA.SAGACitesteLiniiRegistru(societate, data, conturiSQL.Casierie.Replace("DEINLOCUIT", "cont_c")))
            {
                Cheltuieli cheltuiala = new Cheltuieli();
                try { cheltuiala = DatabaseUtils.CitesteCheltuiala(Convert.ToInt32(linie.NDP.Substring(linie.NDP.IndexOf("*") + 1))); } catch { break; }
                // daca e incadrata in grila
                if (cheltuiala.Status.Id == 1 && cheltuiala.RezolutionatDe.Id == 1006)
                {
                    campPlati.Casierie_PlatiGrilaDestinatie += linie.EXPLICATIE + " | ";
                    campPlati.Casierie_PlatiGrilaSuma += Convert.ToDecimal(linie.SUMA);
                    contorGrila++;
                }
                else if (cheltuiala.Status.Id == 1)
                {
                    campPlati.Casierie_PlatiAprobateDestinatie += linie.EXPLICATIE + " | ";
                    campPlati.Casierie_PlatiAprobateSuma += Convert.ToDecimal(linie.SUMA);
                    contorAprobate++;
                }
            }
            if (contorGrila == 0)
            {
                campPlati.Casierie_PlatiGrilaDestinatie = "nicio plata";
                campPlati.Casierie_PlatiGrilaSuma = 0;
            }
            if (contorAprobate == 0)
            {
                campPlati.Casierie_PlatiAprobateDestinatie = "nicio plata";
                campPlati.Casierie_PlatiAprobateSuma = 0;
            }
            return campPlati;
        }
        public static CampRaport SumeCheltuieli(Societati societate, DateTime data, CashflowConturi conturi, CampRaport campCheltuieli)
        {
            CashflowConturi conturiSQL = conturi;// 
            // verificam pentru fiecare in cheltuieli ce status au
            List<SAGA.SAGALinieRegistru> linii = new List<SAGA.SAGALinieRegistru>();
            // cont curent
            int contorGrila = 0;
            int contorAprobate = 0;
            foreach (SAGA.SAGALinieRegistru linie in SAGA.SAGACitesteLiniiRegistru(societate, data, conturiSQL.ContCurent.Replace("DEINLOCUIT", "cont_c")))
            {
                Cheltuieli cheltuiala = new Cheltuieli();
                try { cheltuiala = DatabaseUtils.CitesteCheltuiala(Convert.ToInt32(linie.NDP.Substring(linie.NDP.IndexOf("*") + 1))); } catch { break; }

                switch (cheltuiala.Status.Id)
                {
                    case 1:
                        // daca e incadrata in grila status = 1
                        campCheltuieli.ContCurent_GrilaIncadrate += Convert.ToDecimal(linie.SUMA);
                        break;
                    case 3:
                        // daca e aprobata grila status = 3
                        campCheltuieli.ContCurent_GrilaAprobate += Convert.ToDecimal(linie.SUMA);
                        break;
                    case 5:
                        // daca e exceptionala aprobata status = 5
                        campCheltuieli.ContCurent_ExceptionaleAprobate += Convert.ToDecimal(linie.SUMA);
                        break;
                    case 8:
                        // daca e plata aprobata status = 8
                        campCheltuieli.ContCurent_PlatiAprobate += Convert.ToDecimal(linie.SUMA);
                        break;
                }
            }

            linii = new List<SAGA.SAGALinieRegistru>();
            // depozit termen lung
            foreach (SAGA.SAGALinieRegistru linie in SAGA.SAGACitesteLiniiRegistru(societate, data, conturiSQL.Depozit.Replace("DEINLOCUIT", "cont_c")))
            {
                Cheltuieli cheltuiala = new Cheltuieli();
                try { cheltuiala = DatabaseUtils.CitesteCheltuiala(Convert.ToInt32(linie.NDP.Substring(linie.NDP.IndexOf("*") + 1))); } catch { break; }
                switch (cheltuiala.Status.Id)
                {
                    case 1:
                        // daca e incadrata in grila status = 1
                        campCheltuieli.Depozit_GrilaIncadrate += Convert.ToDecimal(linie.SUMA);
                        break;
                    case 3:
                        // daca e aprobata grila status = 3
                        campCheltuieli.Depozit_GrilaAprobate += Convert.ToDecimal(linie.SUMA);
                        break;
                    case 5:
                        // daca e exceptionala aprobata status = 5
                        campCheltuieli.Depozit_ExceptionaleAprobate += Convert.ToDecimal(linie.SUMA);
                        break;
                    case 8:
                        // daca e plata aprobata status = 8
                        campCheltuieli.Depozit_PlatiAprobate += Convert.ToDecimal(linie.SUMA);
                        break;
                }
            }

            linii = new List<SAGA.SAGALinieRegistru>();
            // depozit taxe
            foreach (SAGA.SAGALinieRegistru linie in SAGA.SAGACitesteLiniiRegistru(societate, data, conturiSQL.DepozitTaxe.Replace("DEINLOCUIT", "cont_c")))
            {
                Cheltuieli cheltuiala = new Cheltuieli();
                try { cheltuiala = DatabaseUtils.CitesteCheltuiala(Convert.ToInt32(linie.NDP.Substring(linie.NDP.IndexOf("*") + 1))); } catch { break; }

                switch (cheltuiala.Status.Id)
                {
                    case 1:
                        // daca e incadrata in grila status = 1
                        campCheltuieli.DepozitTaxe_GrilaIncadrate += Convert.ToDecimal(linie.SUMA);
                        break;
                    case 3:
                        // daca e aprobata grila status = 3
                        campCheltuieli.DepozitTaxe_GrilaAprobate += Convert.ToDecimal(linie.SUMA);
                        break;
                    case 5:
                        // daca e exceptionala aprobata status = 5
                        campCheltuieli.DepozitTaxe_ExceptionaleAprobate += Convert.ToDecimal(linie.SUMA);
                        break;
                    case 8:
                        // daca e plata aprobata status = 8
                        campCheltuieli.DepozitTaxe_PlatiAprobate += Convert.ToDecimal(linie.SUMA);
                        break;
                }
            }

            // casierie
            foreach (SAGA.SAGALinieRegistru linie in SAGA.SAGACitesteLiniiRegistru(societate, data, conturiSQL.Casierie.Replace("DEINLOCUIT", "cont_c")))
            {
                Cheltuieli cheltuiala = new Cheltuieli();
                try { cheltuiala = DatabaseUtils.CitesteCheltuiala(Convert.ToInt32(linie.NDP.Substring(linie.NDP.IndexOf("*") + 1))); } catch { break; }

                switch (cheltuiala.Status.Id)
                {
                    case 1:
                        // daca e incadrata in grila status = 1
                        campCheltuieli.Casierie_GrilaIncadrate += Convert.ToDecimal(linie.SUMA);
                        break;
                    case 3:
                        // daca e aprobata grila status = 3
                        campCheltuieli.Casierie_GrilaAprobate += Convert.ToDecimal(linie.SUMA);
                        break;
                    case 5:
                        // daca e exceptionala aprobata status = 5
                        campCheltuieli.Casierie_ExceptionaleAprobate += Convert.ToDecimal(linie.SUMA);
                        break;
                    case 8:
                        // daca e plata aprobata status = 8
                        campCheltuieli.Casierie_PlatiAprobate += Convert.ToDecimal(linie.SUMA);
                        break;
                }
            }
            return campCheltuieli;
        }

        public static DataSourceCashFlow.CashflowConturi CashFlowConturi(string societate)
        {
            DataSourceCashFlow.CashflowConturi cashFlowConturi = new DataSourceCashFlow.CashflowConturi();
            String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);
            using (con)
            {
                con.Open();
                String cmdStr = "";
                cmdStr = @"SELECT [id],[camp],[conturiAsociate],[raport],[campText],[societate] FROM [dbo].[ConturiAsociateRapoarte] where raport = 'CashFlow' and societate = @societate";
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("societate", societate);
                SqlDataReader dateGasite = cmd.ExecuteReader();
                while (dateGasite.Read())
                {
                    switch (dateGasite["camp"].ToString())
                    {
                        case "ContCurent":
                            cashFlowConturi.ContCurent = dateGasite["conturiAsociate"].ToString();
                            cashFlowConturi.ContCurentText = dateGasite["campText"].ToString();
                            cashFlowConturi.ContCurentId = dateGasite["id"].ToString();
                            break;
                        case "DepozitPeTermenLung":
                            cashFlowConturi.Depozit = dateGasite["conturiAsociate"].ToString();
                            cashFlowConturi.DepozitText = dateGasite["campText"].ToString();
                            cashFlowConturi.DepozitId = dateGasite["id"].ToString();

                            break;
                        case "DepozitPlatiTaxeSiImpozite":
                            cashFlowConturi.DepozitTaxe = dateGasite["conturiAsociate"].ToString();
                            cashFlowConturi.DepozitTaxeText = dateGasite["campText"].ToString();
                            cashFlowConturi.DepozitTaxeId = dateGasite["id"].ToString();
                            break;
                        case "Casierie":
                            cashFlowConturi.Casierie = dateGasite["conturiAsociate"].ToString();
                            cashFlowConturi.CasierieText = dateGasite["campText"].ToString();
                            cashFlowConturi.CasierieId = dateGasite["id"].ToString();
                            break;
                    }
                }
                con.Close();
            }
            return cashFlowConturi;
        }
        //public static DataSourceCashFlow.CashflowConturi CashFlowConturi()
        //{
        //    DataSourceCashFlow.CashflowConturi conturi = new DataSourceCashFlow.CashflowConturi();
        //    string select = @"SELECT  [id] ,[camp],[conturiAsociate],[raport],[campText] FROM[dbo].[ConturiAsociateRapoarte] WHERE raport = 'CashFlow'";
        //    SqlConnection conexiune = DatabaseUtils.ConnectToDatabase();
        //    using (conexiune)
        //    {
        //        SqlCommand cmd = new SqlCommand(select, conexiune);
        //        SqlDataReader dateGasite = cmd.ExecuteReader();
        //        while (dateGasite.Read())
        //        {
        //            switch (dateGasite["camp"].ToString())
        //            {
        //                case "ContCurent":
        //                    conturi.ContCurent = dateGasite["conturiAsociate"].ToString();
        //                    conturi.ContCurentText = dateGasite["campText"].ToString();
        //                    conturi.ContCurentId = dateGasite["id"].ToString();
        //                    break;
        //                case "Depozit":
        //                    conturi.Depozit = dateGasite["conturiAsociate"].ToString();
        //                    conturi.DepozitText = dateGasite["campText"].ToString();
        //                    conturi.DepozitId = dateGasite["id"].ToString();
        //                    break;
        //                case "DepozitTaxe":
        //                    conturi.DepozitTaxe = dateGasite["conturiAsociate"].ToString();
        //                    conturi.DepozitTaxeText = dateGasite["campText"].ToString();
        //                    conturi.DepozitTaxeId = dateGasite["id"].ToString();
        //                    break;
        //                case "Casierie":
        //                    conturi.Casierie = dateGasite["conturiAsociate"].ToString();
        //                    conturi.CasierieText = dateGasite["campText"].ToString();
        //                    conturi.CasierieId = dateGasite["id"].ToString();
        //                    break;
        //                default:
        //                    break;
        //            }
        //        }
        //        dateGasite.Close();
        //        conexiune.Close();
        //        conexiune.Dispose();
        //    }
        //    return conturi;
        //}
        public class CashflowConturi
        {
            private string contCurent;
            public string ContCurent { get { return contCurent; } set { contCurent = value; } }

            private string depozit;
            public string Depozit { get { return depozit; } set { depozit = value; } }

            private string depozitTaxe;
            public string DepozitTaxe { get { return depozitTaxe; } set { depozitTaxe = value; } }

            private string casierie;
            public string Casierie { get { return casierie; } set { casierie = value; } }


            private string contCurentText;
            public string ContCurentText { get { return contCurentText; } set { contCurentText = value; } }

            private string depozitText;
            public string DepozitText { get { return depozitText; } set { depozitText = value; } }

            private string depozitTaxeText;
            public string DepozitTaxeText { get { return depozitTaxeText; } set { depozitTaxeText = value; } }

            private string casierieText;
            public string CasierieText { get { return casierieText; } set { casierieText = value; } }


            private string contCurentId;
            public string ContCurentId { get { return contCurentId; } set { contCurentId = value; } }

            private string depozitId;
            public string DepozitId { get { return depozitId; } set { depozitId = value; } }

            private string depozitTaxeId;
            public string DepozitTaxeId { get { return depozitTaxeId; } set { depozitTaxeId = value; } }

            private string casierieId;
            public string CasierieId { get { return casierieId; } set { casierieId = value; } }
            public CashflowConturi()
            {

            }

        }
        public class CampRaport
        {
            private string societateDenumire;
            public string SocietateDenumire { get { return societateDenumire; } set { societateDenumire = value; } }
            private DateTime dataCashFlow;
            public DateTime DataCashFlow { get { return dataCashFlow; } set { dataCashFlow = value; } }
            private Int32 societateId;
            public Int32 SocietateId { get { return societateId; } set { societateId = value; } }
            private string societateFolderSaga;
            public string SocietateFolderSaga { get { return societateFolderSaga; } set { societateFolderSaga = value; } }

            public decimal ContCurent_SoldPrecedent { get { return contCurent_SoldPrecedent; } set { contCurent_SoldPrecedent = value; } }
            public decimal ContCurent_SoldFinal { get { return contCurent_SoldFinal; } set { contCurent_SoldFinal = value; } }
            public decimal ContCurent_IncasariChirii { get { return contCurent_IncasariChirii; } set { contCurent_IncasariChirii = value; } }
            public decimal ContCurent_IncasariUtilitati { get { return contCurent_IncasariUtilitati; } set { contCurent_IncasariUtilitati = value; } }
            public decimal ContCurent_PlatiGrilaSuma { get { return contCurent_PlatiGrilaSuma; } set { contCurent_PlatiGrilaSuma = value; } }
            public string ContCurent_PlatiGrilaDestinatie { get { return contCurent_PlatiGrilaDestinatie; } set { contCurent_PlatiGrilaDestinatie = value; } }
            public decimal ContCurent_PlatiAprobateSuma { get { return contCurent_PlatiAprobateSuma; } set { contCurent_PlatiAprobateSuma = value; } }
            public DateTime ContCurent_PlatiAprobateData { get { return contCurent_PlatiAprobateData; } set { contCurent_PlatiAprobateData = value; } }
            public string ContCurent_PlatiAprobateDestinatie { get { return contCurent_PlatiAprobateDestinatie; } set { contCurent_PlatiAprobateDestinatie = value; } }
            public decimal ContCurent_DecontariInterne { get { return contCurent_DecontariInterne; } set { contCurent_DecontariInterne = value; } }
            private decimal contCurent_SoldPrecedent;
            private decimal contCurent_SoldFinal;
            private decimal contCurent_IncasariChirii;
            private decimal contCurent_IncasariUtilitati;
            private decimal contCurent_PlatiGrilaSuma;
            private string contCurent_PlatiGrilaDestinatie;
            private decimal contCurent_PlatiAprobateSuma;
            private DateTime contCurent_PlatiAprobateData;
            private string contCurent_PlatiAprobateDestinatie;
            private decimal contCurent_DecontariInterne;

            private decimal contCurent_GrilaIncadrate;
            public decimal ContCurent_GrilaIncadrate { get { return contCurent_GrilaIncadrate; } set { contCurent_GrilaIncadrate = value; } }

            private decimal contCurent_GrilaAprobate;
            public decimal ContCurent_GrilaAprobate { get { return contCurent_GrilaAprobate; } set { contCurent_GrilaAprobate = value; } }

            private decimal contCurent_ExceptionaleAprobate;
            public decimal ContCurent_ExceptionaleAprobate { get { return contCurent_ExceptionaleAprobate; } set { contCurent_ExceptionaleAprobate = value; } }

            private decimal contCurent_PlatiAprobate;
            public decimal ContCurent_PlatiAprobate { get { return contCurent_PlatiAprobate; } set { contCurent_PlatiAprobate = value; } }


            private decimal depozit_SoldPrecedent;
            private decimal depozit_SoldFinal;
            private decimal depozit_IncasariChirii;
            private decimal depozit_IncasariUtilitati;
            private decimal depozit_PlatiGrilaSuma;
            private string depozit_PlatiGrilaDestinatie;
            private decimal depozit_PlatiAprobateSuma;
            private DateTime depozit_PlatiAprobateData;
            private string depozit_PlatiAprobateDestinatie;
            private decimal depozit_DecontariInterne;
            public decimal Depozit_SoldPrecedent { get { return depozit_SoldPrecedent; } set { depozit_SoldPrecedent = value; } }
            public decimal Depozit_SoldFinal { get { return depozit_SoldFinal; } set { depozit_SoldFinal = value; } }
            public decimal Depozit_IncasariChirii { get { return depozit_IncasariChirii; } set { depozit_IncasariChirii = value; } }
            public decimal Depozit_IncasariUtilitati { get { return depozit_IncasariUtilitati; } set { depozit_IncasariUtilitati = value; } }
            public decimal Depozit_PlatiGrilaSuma { get { return depozit_PlatiGrilaSuma; } set { depozit_PlatiGrilaSuma = value; } }
            public string Depozit_PlatiGrilaDestinatie { get { return depozit_PlatiGrilaDestinatie; } set { depozit_PlatiGrilaDestinatie = value; } }
            public decimal Depozit_PlatiAprobateSuma { get { return depozit_PlatiAprobateSuma; } set { depozit_PlatiAprobateSuma = value; } }
            public DateTime Depozit_PlatiAprobateData { get { return depozit_PlatiAprobateData; } set { depozit_PlatiAprobateData = value; } }
            public string Depozit_PlatiAprobateDestinatie { get { return depozit_PlatiAprobateDestinatie; } set { depozit_PlatiAprobateDestinatie = value; } }
            public decimal Depozit_DecontariInterne { get { return depozit_DecontariInterne; } set { depozit_DecontariInterne = value; } }

            private decimal depozit_GrilaIncadrate;
            public decimal Depozit_GrilaIncadrate { get { return depozit_GrilaIncadrate; } set { depozit_GrilaIncadrate = value; } }

            private decimal depozit_GrilaAprobate;
            public decimal Depozit_GrilaAprobate { get { return depozit_GrilaAprobate; } set { depozit_GrilaAprobate = value; } }

            private decimal depozit_ExceptionaleAprobate;
            public decimal Depozit_ExceptionaleAprobate { get { return depozit_ExceptionaleAprobate; } set { depozit_ExceptionaleAprobate = value; } }

            private decimal depozit_PlatiAprobate;
            public decimal Depozit_PlatiAprobate { get { return depozit_PlatiAprobate; } set { depozit_PlatiAprobate = value; } }




            private decimal depozitTaxe_SoldPrecedent;
            private decimal depozitTaxe_SoldFinal;
            private decimal depozitTaxe_IncasariChirii;
            private decimal depozitTaxe_IncasariUtilitati;
            private decimal depozitTaxe_PlatiGrilaSuma;
            private string depozitTaxe_PlatiGrilaDestinatie;
            private decimal depozitTaxe_PlatiAprobateSuma;
            private DateTime depozitTaxe_PlatiAprobateData;
            private string depozitTaxe_PlatiAprobateDestinatie;
            private decimal depozitTaxe_DecontariInterne;
            public decimal DepozitTaxe_SoldPrecedent { get { return depozitTaxe_SoldPrecedent; } set { depozitTaxe_SoldPrecedent = value; } }
            public decimal DepozitTaxe_SoldFinal { get { return depozitTaxe_SoldFinal; } set { depozitTaxe_SoldFinal = value; } }
            public decimal DepozitTaxe_IncasariChirii { get { return depozitTaxe_IncasariChirii; } set { depozitTaxe_IncasariChirii = value; } }
            public decimal DepozitTaxe_IncasariUtilitati { get { return depozitTaxe_IncasariUtilitati; } set { depozitTaxe_IncasariUtilitati = value; } }
            public decimal DepozitTaxe_PlatiGrilaSuma { get { return depozitTaxe_PlatiGrilaSuma; } set { depozitTaxe_PlatiGrilaSuma = value; } }
            public string DepozitTaxe_PlatiGrilaDestinatie { get { return depozitTaxe_PlatiGrilaDestinatie; } set { depozitTaxe_PlatiGrilaDestinatie = value; } }
            public decimal DepozitTaxe_PlatiAprobateSuma { get { return depozitTaxe_PlatiAprobateSuma; } set { depozitTaxe_PlatiAprobateSuma = value; } }
            public DateTime DepozitTaxe_PlatiAprobateData { get { return depozitTaxe_PlatiAprobateData; } set { depozitTaxe_PlatiAprobateData = value; } }
            public string DepozitTaxe_PlatiAprobateDestinatie { get { return depozitTaxe_PlatiAprobateDestinatie; } set { depozitTaxe_PlatiAprobateDestinatie = value; } }
            public decimal DepozitTaxe_DecontariInterne { get { return depozitTaxe_DecontariInterne; } set { depozitTaxe_DecontariInterne = value; } }

            private decimal depozitTaxe_GrilaIncadrate;
            public decimal DepozitTaxe_GrilaIncadrate { get { return depozitTaxe_GrilaIncadrate; } set { depozitTaxe_GrilaIncadrate = value; } }

            private decimal depozitTaxe_GrilaAprobate;
            public decimal DepozitTaxe_GrilaAprobate { get { return depozitTaxe_GrilaAprobate; } set { depozitTaxe_GrilaAprobate = value; } }

            private decimal depozitTaxe_ExceptionaleAprobate;
            public decimal DepozitTaxe_ExceptionaleAprobate { get { return depozitTaxe_ExceptionaleAprobate; } set { depozitTaxe_ExceptionaleAprobate = value; } }

            private decimal depozitTaxe_PlatiAprobate;
            public decimal DepozitTaxe_PlatiAprobate { get { return depozitTaxe_PlatiAprobate; } set { depozitTaxe_PlatiAprobate = value; } }




            private decimal casierie_SoldPrecedent;
            private decimal casierie_SoldFinal;
            private decimal casierie_IncasariChirii;
            private decimal casierie_IncasariUtilitati;
            private decimal casierie_PlatiGrilaSuma;
            private string casierie_PlatiGrilaDestinatie;
            private decimal casierie_PlatiAprobateSuma;
            private DateTime casierie_PlatiAprobateData;
            private string casierie_PlatiAprobateDestinatie;
            private decimal casierie_DecontariInterne;
            public decimal Casierie_SoldPrecedent { get { return casierie_SoldPrecedent; } set { casierie_SoldPrecedent = value; } }
            public decimal Casierie_SoldFinal { get { return casierie_SoldFinal; } set { casierie_SoldFinal = value; } }
            public decimal Casierie_IncasariChirii { get { return casierie_IncasariChirii; } set { casierie_IncasariChirii = value; } }
            public decimal Casierie_IncasariUtilitati { get { return casierie_IncasariUtilitati; } set { casierie_IncasariUtilitati = value; } }
            public decimal Casierie_PlatiGrilaSuma { get { return casierie_PlatiGrilaSuma; } set { casierie_PlatiGrilaSuma = value; } }
            public string Casierie_PlatiGrilaDestinatie { get { return casierie_PlatiGrilaDestinatie; } set { casierie_PlatiGrilaDestinatie = value; } }
            public decimal Casierie_PlatiAprobateSuma { get { return casierie_PlatiAprobateSuma; } set { casierie_PlatiAprobateSuma = value; } }
            public DateTime Casierie_PlatiAprobateData { get { return casierie_PlatiAprobateData; } set { casierie_PlatiAprobateData = value; } }
            public string Casierie_PlatiAprobateDestinatie { get { return casierie_PlatiAprobateDestinatie; } set { casierie_PlatiAprobateDestinatie = value; } }
            public decimal Casierie_DecontariInterne { get { return casierie_DecontariInterne; } set { casierie_DecontariInterne = value; } }


            private decimal casierie_GrilaIncadrate;
            public decimal Casierie_GrilaIncadrate { get { return casierie_GrilaIncadrate; } set { casierie_GrilaIncadrate = value; } }

            private decimal casierie_GrilaAprobate;
            public decimal Casierie_GrilaAprobate { get { return casierie_GrilaAprobate; } set { casierie_GrilaAprobate = value; } }

            private decimal casierie_ExceptionaleAprobate;
            public decimal Casierie_ExceptionaleAprobate { get { return casierie_ExceptionaleAprobate; } set { casierie_ExceptionaleAprobate = value; } }

            private decimal casierie_PlatiAprobate;
            public decimal Casierie_PlatiAprobate { get { return casierie_PlatiAprobate; } set { casierie_PlatiAprobate = value; } }




            public CampRaport()
            {

            }

        }
    }
}