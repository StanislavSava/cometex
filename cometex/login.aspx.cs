﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cometex
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
                LabelEroare.Visible = false;
        }
        protected void ButtonConectare_Click(object sender, EventArgs e)
        {
            String connectionString = ConfigurationManager.ConnectionStrings["CometexInvestitii_dbConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            string Parola = Criptare.Encrypt(TextBoxParola.Text);
            String cmdStr = "select * from utilizatori where utilizator = '"+ TextBoxUtilizator.Text + "' and parola = '"+ Parola +"'";
            SqlCommand cmd = new SqlCommand(cmdStr, con);
            try
            {
                SqlDataReader tab = cmd.ExecuteReader();
                while (tab.Read())
                {
                    Session["numeUtilizator"] = tab["Nume"];
                    Session["idUtilizator"] = tab["idUtilizator"];
                    Session["idSocietate"] = tab["idSocietate"];
                    Session["EsteAdmin"] = tab["Admin"];
                    Session["Utilizator"] = tab["Utilizator"];
                    Session["email"] = tab["email"];
                }
            }
            catch { throw; }
            finally {
                con.Close();
            }

            try
            {
                if ((bool)Session["EsteAdmin"])
                {
                    Response.Redirect("~/Admin/Grila.aspx");
                }
                else
                {
                    Response.Redirect("~/User/Grila.aspx");
                }
            }
            catch
            {
                LabelEroare.Text = "Utilizator sau parola gresite! Mai incercati!";
                LabelEroare.Visible = true;
            }
        }
    }
}