SELECT [unitateId]
      ,[gospodarieId]
      /*,capitole.[an]*/
	  ,2015 as an
      ,'' as adresa
	  ,'' as zona
	  ,[capitolId]
	  ,[col1]
	  ,0 as tip
	  ,denumire1 as destinatie
	  ,0 as anTerminare
  FROM [TNTRegistruAgricol3].[tntcomputers].[capitole] 
  INNER JOIN sabloanecapitole on capitole.codRand = sabloaneCapitole.codRand and capitole.codCapitol = sabloaneCapitole.capitol and capitole.an = sabloaneCapitole.an
  WHERE capitole.an = 2014 
	and codcapitol = '11' 
	and capitole.codrand  in (13,14,15,16,17,18,19) 
	and col1 > 0 
	and sabloaneCapitole.capitol = '11'
	and unitateid = 181