SELECT        
	SUM(tab4.suma4) AS sumaSpreAprobare, 
	SUM(tab6.suma6) AS sumaRespinsa, 
	SUM(tab5.suma5) AS sumaAprobata,
	CASE	WHEN cat4 IS NOT NULL THEN cat4 
			WHEN cat6 IS NOT NULL THEN cat6 
			ELSE cat5 END AS categorie,
	CASE	WHEN soc4 IS NOT NULL THEN soc4 
			WHEN soc6 IS NOT NULL THEN soc6 
			ELSE soc5 END AS societate

FROM 
  (SELECT Categorii.Categorie AS cat4, Investitii.Suma AS suma4, Societati.Societate AS soc4
     FROM Categorii INNER JOIN Investitii ON Categorii.IdCategorie = Investitii.idCategorie INNER JOIN Societati ON Investitii.idSocietate = Societati.IdSocietate
     WHERE (CAST(Investitii.idSocietate AS varchar) LIKE @idSocietate) AND (Investitii.DataIntroducere BETWEEN @dateTimeStart AND @dateTimeEnd) AND (Investitii.Status = 4)) 
     AS tab4 
	 
FULL OUTER JOIN
  (SELECT Categorii.Categorie AS cat6, Investitii.Suma AS suma6, Societati.Societate AS soc6
     FROM Categorii INNER JOIN Investitii ON Categorii.IdCategorie = Investitii.idCategorie INNER JOIN Societati ON Investitii.idSocietate = Societati.IdSocietate
	 WHERE (CAST(Investitii.idSocietate AS varchar) LIKE @idSocietate) AND (Investitii.DataIntroducere BETWEEN @dateTimeStart AND @dateTimeEnd) AND (Investitii.Status = 6)) 
	 AS tab6 
	 ON  tab4.soc4 = tab6.soc6 
	 AND tab4.cat4 = tab6.cat6 

FULL OUTER JOIN
  (SELECT Categorii.Categorie AS cat5, Investitii.Suma AS suma5, Societati.Societate AS soc5
     FROM Categorii INNER JOIN Investitii ON Categorii.IdCategorie = Investitii.idCategorie INNER JOIN Societati ON Investitii.idSocietate = Societati.IdSocietate
	 WHERE (CAST(Investitii.idSocietate AS varchar) LIKE @idSocietate) AND (Investitii.DataIntroducere BETWEEN @dateTimeStart AND @dateTimeEnd) AND (Investitii.Status = 5)) 
	 AS tab5 
	 ON  tab4.soc4 = tab6.soc6 
	 AND tab4.soc4 = tab5.soc5 
	 AND tab6.soc6 = tab5.soc5
	 AND tab4.cat4 = tab6.cat6 
	 AND tab4.cat4 = tab5.cat5 
	 AND tab6.cat6 = tab5.cat5

GROUP BY 
	CASE	WHEN cat4 IS NOT NULL THEN cat4 
			WHEN cat6 IS NOT NULL THEN cat6 
			ELSE cat5 END, 
	CASE	WHEN soc4 IS NOT NULL THEN soc4 
			WHEN soc6 IS NOT NULL THEN soc6 
			ELSE soc5 END

ORDER BY societate, categorie