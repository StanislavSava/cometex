select  SUM(tab1.SUMA1) AS S1,SUM(tab2.SUMA2) AS S2,SUM(tab3.SUMA3) AS S3,
                         SUM(tab4.SUMA4) AS S4,  
		case when cat1 is not null then cat1  when cat2 is not null then cat2 when cat3 is not null then cat3 else cat4 end as categorie, 
	    case when SOC1 is not null then soc1  when soc2 is not null then soc2 when soc3 is not null then soc3 else soc4 end as societate
 from
(SELECT        Categorii_1.Categorie AS cat4, Investitii_1.Suma AS SUMA4, Societati_1.Societate AS soc4
	 FROM Categorii AS Categorii_1 INNER JOIN      Investitii AS Investitii_1 ON Categorii_1.IdCategorie = Investitii_1.idCategorie INNER JOIN  Societati AS Societati_1 ON Investitii_1.idSocietate = Societati_1.IdSocietate    
	 WHERE        (CAST(Investitii_1.idSocietate AS varchar) LIKE '%') 
	AND (Investitii_1.DataIntroducere BETWEEN '01.01.2015' AND '01.01.2017') AND (Investitii_1.Status = 3) 
	) as tab4 
full outer join
( SELECT        Categorii_1.Categorie AS cat1, Investitii_1.Suma AS SUMA1, Societati_1.Societate AS soc1
     FROM            Categorii AS Categorii_1 INNER JOIN  Investitii AS Investitii_1 ON Categorii_1.IdCategorie = Investitii_1.idCategorie INNER JOIN  Societati AS Societati_1 ON Investitii_1.idSocietate = Societati_1.IdSocietate
     WHERE        (CAST(Investitii_1.idSocietate AS varchar) LIKE '%') AND (Investitii_1.DataIntroducere BETWEEN '01.01.2015' AND '01.01.2017') AND (Investitii_1.Status = 0)
      ) as tab1 on soc1 = soc4 and cat1 = cat4


full outer join
( SELECT        Categorii_1.Categorie AS cat2, Investitii_1.Suma AS SUMA2, Societati_1.Societate AS soc2
     FROM            Categorii AS Categorii_1 INNER JOIN  Investitii AS Investitii_1 ON Categorii_1.IdCategorie = Investitii_1.idCategorie INNER JOIN  Societati AS Societati_1 ON Investitii_1.idSocietate = Societati_1.IdSocietate
     WHERE        (CAST(Investitii_1.idSocietate AS varchar) LIKE '%') AND (Investitii_1.DataIntroducere BETWEEN '01.01.2015' AND '01.01.2017') AND (Investitii_1.Status = 1)
      ) as tab2 on 
	  soc1 = soc4 
	  and soc1 = soc2
	  and soc2 = soc4
	  and cat1 = cat4
	  and cat1 = cat2
	  and cat2 = cat4


full outer join
( SELECT        Categorii_1.Categorie AS cat3, Investitii_1.Suma AS SUMA3, Societati_1.Societate AS soc3
     FROM            Categorii AS Categorii_1 INNER JOIN  Investitii AS Investitii_1 ON Categorii_1.IdCategorie = Investitii_1.idCategorie INNER JOIN  Societati AS Societati_1 ON Investitii_1.idSocietate = Societati_1.IdSocietate
     WHERE        (CAST(Investitii_1.idSocietate AS varchar) LIKE '%') AND (Investitii_1.DataIntroducere BETWEEN '01.01.2015' AND '01.01.2017') AND (Investitii_1.Status = 2)
      ) as tab3 on 
	  	  soc1 = soc4 
	  and soc1 = soc2
	  and soc1 = soc3
	  and soc2 = soc3
	  and soc2 = soc4
	  and soc3 = soc4
	  and cat1 = cat2
	  and cat1 = cat3
	  and cat1 = cat4
	  and cat2 = cat3
	and cat2 = cat4
	and cat3 = cat4




	  GROUP BY  case when cat1 is not null then cat1  when cat2 is not null then cat2 when cat3 is not null then cat3 else cat4 end , 
	  
	  case when SOC1 is not null then soc1  when soc2 is not null then soc2 when soc3 is not null then soc3 else soc4 end 


	  order by 
	  societate, categorie
