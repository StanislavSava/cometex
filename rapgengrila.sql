SELECT        
	SUM(tab0.suma0) AS sumaSpreAprobare, 
	SUM(tab1.suma1) AS sumaIncadrataInGrila, 
	SUM(tab2.suma2) AS sumaRespinsa, 
	SUM(tab3.suma3) AS sumaAprobata,
	CASE	WHEN cat0 IS NOT NULL THEN cat0 
			WHEN cat1 IS NOT NULL THEN cat1 
			WHEN cat2 IS NOT NULL THEN cat2 
			ELSE cat3 END AS categorie,
	CASE	WHEN soc0 IS NOT NULL THEN soc0 
			WHEN soc1 IS NOT NULL THEN soc1 
			WHEN soc2 IS NOT NULL THEN soc2 
			ELSE soc3 END AS societate

FROM 
  (SELECT Categorii.Categorie AS cat0, Investitii.Suma AS suma0, Societati.Societate AS soc0
     FROM Categorii INNER JOIN Investitii ON Categorii.IdCategorie = Investitii.idCategorie INNER JOIN Societati ON Investitii.idSocietate = Societati.IdSocietate
     WHERE (CAST(Investitii.idSocietate AS varchar) LIKE @idSocietate) AND (Investitii.DataIntroducere BETWEEN @dateTimeStart AND @dateTimeEnd) AND (Investitii.Status = 0)) 
     AS tab0 
	 
FULL OUTER JOIN
  (SELECT Categorii.Categorie AS cat1, Investitii.Suma AS suma1, Societati.Societate AS soc1
     FROM Categorii INNER JOIN Investitii ON Categorii.IdCategorie = Investitii.idCategorie INNER JOIN Societati ON Investitii.idSocietate = Societati.IdSocietate
	 WHERE (CAST(Investitii.idSocietate AS varchar) LIKE @idSocietate) AND (Investitii.DataIntroducere BETWEEN @dateTimeStart AND @dateTimeEnd) AND (Investitii.Status = 1)) 
	 AS tab1 
	 ON tab0.soc0 = tab1.soc1 
	 AND tab0.cat0 = tab1.cat1 

FULL OUTER JOIN
  (SELECT Categorii.Categorie AS cat2, Investitii.Suma AS suma2, Societati.Societate AS soc2
     FROM Categorii INNER JOIN Investitii ON Categorii.IdCategorie = Investitii.idCategorie INNER JOIN Societati ON Investitii.idSocietate = Societati.IdSocietate
	 WHERE (CAST(Investitii.idSocietate AS varchar) LIKE @idSocietate) AND (Investitii.DataIntroducere BETWEEN @dateTimeStart AND @dateTimeEnd) AND (Investitii.Status = 2)) 
	 AS tab2 
	 ON tab0.soc0 = tab1.soc1 
	 AND tab0.soc0 = tab2.soc2 
	 AND tab1.soc1 = tab2.soc2
	 AND tab0.cat0 = tab1.cat1
	 AND tab0.cat0 = tab2.cat2 
	 AND tab1.cat1 = tab2.cat2

FULL OUTER JOIN
  (SELECT Categorii.Categorie AS cat3, Investitii.Suma AS suma3, Societati.Societate AS soc3
     FROM Categorii INNER JOIN Investitii ON Categorii.IdCategorie = Investitii.idCategorie INNER JOIN Societati ON Investitii.idSocietate = Societati.IdSocietate
	 WHERE (CAST(Investitii.idSocietate AS varchar) LIKE @idSocietate) AND (Investitii.DataIntroducere BETWEEN @dateTimeStart AND @dateTimeEnd) AND (Investitii.Status = 3)) 
	 AS tab3 
	 ON tab0.soc0 = tab1.soc1 
	 AND tab0.soc0 = tab2.soc2 
	 AND tab0.soc0 = tab3.soc3 
	 AND tab1.soc1 = tab2.soc2
	 AND tab1.soc1 = tab3.soc3
	 AND tab2.soc2 = tab3.soc3
	 AND tab0.cat0 = tab1.cat1
	 AND tab0.cat0 = tab2.cat2 
	 AND tab0.cat0 = tab3.cat3 
	 AND tab1.cat1 = tab2.cat2
	 AND tab1.cat1 = tab3.cat3
	 AND tab2.cat2 = tab3.cat3

GROUP BY 
	CASE	WHEN cat0 IS NOT NULL THEN cat0 
			WHEN cat1 IS NOT NULL THEN cat1 
			WHEN cat2 IS NOT NULL THEN cat2 
			ELSE cat3 END, 
	CASE	WHEN soc0 IS NOT NULL THEN soc0 
			WHEN soc1 IS NOT NULL THEN soc1 
			WHEN soc2 IS NOT NULL THEN soc2 
			ELSE soc3 END

ORDER BY societate, categorie